/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "anaconfig.h"

#include <fstream>
#include <getopt.h>
#include <string>
#include <sys/stat.h>

#include <TArrow.h>
#include <TCanvas.h>
#include <TDirectory.h>
#include <TError.h>
#include <TF1.h>
#include <TFile.h>
#include <TGaxis.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH2.h>
#include <TImage.h>
#include <TKey.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TMath.h>
#include <TObjString.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TVector.h>
#include <TVirtualFitter.h>

#include <RootTools.h>

#include <DifferentialContext.h>
#include <DifferentialFactory.h>

#include "LambdaFitter.h"

#include <FitterFactory.h>
#include <SmartFactory.h>
class gSTyle;

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

// Processing data bar width
const int bar_dotqty = 10000;
const int bar_width = 20;

const Option_t h1opts[] = "h,E1";

void fit_callback(DifferentialFactory* fac, DistributionFactory* sigfac, int fit_res, TH1* h,
                  uint x_pos, uint y_pos, uint z_pos)
{
    bool integral_only = false;

    // declare functions for fit and signal
    TF1* tfLambdaSum = nullptr;
    TF1* tfLambdaSig = nullptr;

    if (fit_res > 0)
    {
        tfLambdaSum = (TF1*)h->GetListOfFunctions()->At(0);
        tfLambdaSig = (TF1*)h->GetListOfFunctions()->At(1);
    }
    else if (fit_res < 0)
        integral_only = true;
    else
        return;

    FitResultData frd;

    if (!integral_only)
    {

        // get signal and mean value from fit
        // parameters are determined here, sadly no place for flexibility
        double signal = tfLambdaSig->GetParameter(0);
        double mean = tfLambdaSig->GetParameter(1);
        // relative fractions
        double r = tfLambdaSig->GetParameter(4);
        double q = 1.0 - r;

        // sigmas
        double s1 = TMath::Abs(tfLambdaSig->GetParameter(2));
        double s2 = TMath::Abs(tfLambdaSig->GetParameter(5));

        // averaged width
        double width = (r * s1 * s1 + q * s2 * s2) / (r * s1 + q * s2);

        // errors
        double signal_err = tfLambdaSig->GetParError(0);

        // find bin width at the mean value
        int fSBmean = h->FindBin(mean);
        double bin_width = h->GetBinWidth(fSBmean);

        // fil return structure and say 'bye bye'
        frd.fit_ok = true;
        frd.mean = mean;
        frd.sigma = width;
        frd.signal = signal / bin_width;
        frd.signal_err = signal_err / bin_width;
        frd.chi2 = tfLambdaSum->GetChisquare();
        frd.ndf = tfLambdaSum->GetNDF();

        std::cout << "    Signal: " << frd.signal << " +/- " << frd.signal_err << std::endl;

        if (sigfac)
        {
            // 		fac->hSliceXYDiff[x_pos]->SetBinContent(1+y_pos, frd.signal); FIXME
            // 		fac->hSliceXYDiff[x_pos]->SetBinError(1+y_pos, frd.signal_err); FIXME
            sigfac->hSignalCounter->SetBinContent(1 + x_pos, 1 + y_pos, 1 + z_pos, frd.signal);
            sigfac->hSignalCounter->SetBinError(1 + x_pos, 1 + y_pos, 1 + z_pos, frd.signal_err);
        }
        else
        {
            fac->hSignalCounter->SetBinContent(1 + x_pos, 1 + y_pos, 1 + z_pos, frd.signal);
            fac->hSignalCounter->SetBinError(1 + x_pos, 1 + y_pos, 1 + z_pos, frd.signal_err);
        }

        if (frd.mean != 0)
        {
            // 			fac->hSliceXYFitQA[x_pos]->SetBinContent(1+y_pos, frd.mean); FIXME x3
            // 			fac->hSliceXYFitQA[x_pos]->SetBinError(1+y_pos, frd.sigma);
            // 			fac->hSliceXYChi2NDF[x_pos]->SetBinContent(1+y_pos, frd.chi2/frd.ndf);
        }
        else
        {
            // 			TVirtualPad * pad = fac->cDiscreteXYDiff[x_pos]->cd(y_pos); FIXME x2
            // 			pad->SetFillColor(42);
            TVirtualPad* pad = fac->diffs->getPad(x_pos, y_pos, z_pos);
            pad->SetFillColor(42);
        }
    }
    else
    {
        frd.signal = fac->diffs->get(x_pos, y_pos, z_pos)->Integral();

        if (frd.signal < 0) // FIXME old value 500
        {
            frd.signal = 0;
        }

        frd.signal_err =
            RootTools::calcTotalError(fac->diffs->get(x_pos, y_pos, z_pos), 1,
                                      fac->diffs->get(x_pos, y_pos, z_pos)->GetNbinsX());
        // 		fac->hSliceXYDiff[x_pos]->SetBinContent(1+y_pos, frd.signal); FIXME x2
        // 		fac->hSliceXYDiff[x_pos]->SetBinError(1+y_pos, frd.signal_err);
        sigfac->hSignalCounter->SetBinContent(1 + x_pos, 1 + y_pos, 1 + z_pos, frd.signal);
        sigfac->hSignalCounter->SetBinError(1 + x_pos, 1 + y_pos, 1 + z_pos, frd.signal_err);
    }
}

LambdaFitter::LambdaFitter()
    : ToolsAbstract(), nParNum(2 + 2 + 3 + 3 + 6), // S/B, range, GausFit, PolFit
      fLambdaMCutMin(1102.0), fLambdaMCutMax(1130.0), fSidebandL(0), fSidebandR(0), fSidebandBgL(0),
      fSidebandBgR(0), flag_noaux(0), flag_sideband(0), flag_nodifffit(0)
{
    NicePalette();

    TGaxis::SetMaxDigits(3);

    TVirtualFitter::SetPrecision(1e-6);

    stdfit = HistFitParams::parseEntryFromFile(
        " *	s2gaus pol5(6)+aexpo 0 1085 1150 508861 : 0 1e+07 1115.13 : 1113 1119 3.91283 : 3.5 "
        "4.5 1478.67 : 0 200000 0.498504 : 0 1 1.64319 : 1 2.5 -2.43442e+08 673244 -629.392 0.1925 "
        "1.84235e-05 -1.16072e-08 -258136 -3228.11 -2989.57");
}

LambdaFitter::~LambdaFitter() {}

void LambdaFitter::AnaInitialize()
{
    // FIXME
    // 	if (flag_verbose)
    // 		flags_fit_a = "B";
    // 	else
    // 		flags_fit_a = "B,Q";
    //
    // 	flags_fit_b = "";

    // 	TDirectory * pwd = gDirectory;

    SetOutputFile("FittedLambda.root");

    TString input_file;
    if (optfile.Length())
        input_file = optfile;
    else
        input_file = "SecVertexReco.root";

    secvertfile = ImportFromFile(export_dir + "/" + input_file, (DifferentialContext**)nullptr, fac,
                                 "_DiscCtx");

    for (uint i = 0; i < ananum; ++i)
    {
        // 		fac[i]->rename(ananames[i].c_str());
        DistributionContext sigctx = fac[i]->ctx;
        TString tmpname = sigctx.name;
        tmpname.ReplaceAll("_Disc", "_Signal");
        sigctx.name = tmpname;
        sigctx.hist_name.Clear();
        sigfac[i] = new DistributionFactory(sigctx);
        sigfac[i]->init();
    }

    params = new TH1F("params", "params", nParNum, 0, nParNum);

    hSignalInvMass = (TH1D*)secvertfile->Get("h_SignalInvMass");
    cSignalInvMass = (TCanvas*)secvertfile->Get("c_SignalInvMass");

    if (cSignalInvMass) cSignalInvMass->Draw();

    fit_results = new SmartFactory("fit_results");
    hFitResults = fit_results->RegTH1<TH1F>("h_FitResults", "Fit results", 10, 0, 10, true);
}

void LambdaFitter::AnaExecute()
{
    // 	if (!(flag_exp or flag_fss))
    // 		return;
    if (flag_exp or flag_fss)
    {
        TSystem sys;
        par_fit_export = std::string(export_dir.Data()) + sys.BaseName(par_fit_params.c_str());

        const char* par_ref = par_fit_params.c_str();
        const char* par_aux = par_fit_export.c_str();

        ff.initFactoryFromFile(par_ref, par_aux);
    }

    FitResultData res;

    if (hSignalInvMass)
    {
        hSignalInvMass->Draw(/*"hist"*/ /*h1opts*/);
        if (flag_exp or flag_fss)
        {
            HistFitParams* hfp = ff.findParams(hSignalInvMass);
            if (hfp) res = FitInvMass(hSignalInvMass, *hfp);
        }
        else
        {
            res.signal = hSignalInvMass->Integral();
            res.signal_err =
                RootTools::calcTotalError(hSignalInvMass, 1, hSignalInvMass->GetNbinsX() + 1);
        }

        std::cout << "\nSignal: " << res.signal << " +/- " << res.signal << std::endl;

        TH1D* hclone = nullptr;
        TF1* fclone = (TF1*)hSignalInvMass->GetListOfFunctions()->At(2);
        if (fclone)
        {
            int binl = hSignalInvMass->FindBin(fclone->GetXmin());
            int binr = hSignalInvMass->FindBin(fclone->GetXmax());

            hclone =
                new TH1D("h_LambdaPureSignal", "Lambda Signal;[MeV/c^{2}];Counts", binr - binl,
                         hSignalInvMass->GetBinLowEdge(binl), hSignalInvMass->GetBinLowEdge(binr));

            for (int i = 0; i < binr - binl; ++i)
            {
                hclone->SetBinContent(1 + i, hSignalInvMass->GetBinContent(binl + i));
                hclone->SetBinError(1 + i, hSignalInvMass->GetBinError(binl + i));
            }

            hclone->SetLineColor(kBlack);
            hclone->SetLineWidth(1);
            hclone->SetFillColor(kGray);
            hclone->SetFillStyle(3000);

            hclone->Add(fclone, -1);
            hclone->Draw("same,][,hist");

            if (fSidebandL) fSidebandL->Draw("same,][,hist");
            if (fSidebandR) fSidebandR->Draw("same,][,hist");
            if (fSidebandBgL) fSidebandBgL->Draw("same,][,hist");
            if (fSidebandBgR) fSidebandBgR->Draw("same,][,hist");

            TLine l(hSignalInvMass->GetBinLowEdge(1), 0.,
                    hSignalInvMass->GetBinLowEdge(hSignalInvMass->GetNbinsX() + 1), 0.);
            l.SetLineStyle(2);
            l.Draw("same");

            fclone->Draw("same");
        }

        TF1* fsum = (TF1*)hSignalInvMass->GetListOfFunctions()->At(0);
        if (fsum) { fsum->Draw("same"); }

        TF1* fsig = (TF1*)hSignalInvMass->GetListOfFunctions()->At(1);
        if (fsig)
        {
            fsig->Draw("same");
            // 			TF1 * f_l = RootTools::makeBarOffsetFunction(fsig, -1);
            // 			TF1 * f_u = RootTools::makeBarOffsetFunction(fsig, +1);
            // 			f_l->SetLineStyle(1);
            // 			f_u->SetLineStyle(1);
            // 			f_l->SetLineColor(4);
            // 			f_u->SetLineColor(3);
            // 			f_l->Draw("same");
            // 			f_u->Draw("same");
        }
        TF1* fbkg = (TF1*)hSignalInvMass->GetListOfFunctions()->At(2);

        Double_t hmax = hSignalInvMass->GetBinContent(hSignalInvMass->GetMaximumBin());
        hSignalInvMass->GetYaxis()->SetRangeUser(-hmax * 0.05, hmax * 1.1);

        if (hclone)
        {
            double orig_tfs = gStyle->GetTitleFontSize();
            gStyle->SetTitleFontSize(0.08);
            TCanvas* cSignalInvMassNice =
                new TCanvas("c_SignalInvMassNice", "Invariant Mass", 800, 480);
            cSignalInvMassNice->cd();
            RootTools::NicePad(gPad, 0.1, 0.05, 0.2, 0.12);

            // 			TH1D * h_invmass = (TH1D*)hSignalInvMass->Clone();
            RootTools::NiceHistogram(hSignalInvMass, 1005, 1002, 0.08, 0.005, 0.08, 0.95, 0.07,
                                     0.008, 0.08, 0.75);
            // 			h_invmass->SetMarkerStyle(21);
            hSignalInvMass->SetTitle("");
            hSignalInvMass->Draw("E");

            hclone->SetMarkerStyle(21);
            hclone->SetMarkerSize(0.8);
            hclone->DrawClone("same,E");
            hclone->SetFillStyle(3000);
            if (fsig)
            {
                fsig->Draw("same");
                fsig->SetLineColor(2);
                fsig->SetLineWidth(2);
            }

            if (fbkg)
            {
                fbkg->Draw("same");
                fbkg->SetLineStyle(2);
                fbkg->SetLineColor(1);
                fbkg->SetLineWidth(2);
            }

            if (fsum)
            {
                fsum->SetLineWidth(2);
                fsum->Draw("same");
            }

            int _bmax = hclone->GetMaximumBin();
            double _max = hclone->GetBinContent(_bmax);

            TLatex* tex = new TLatex();
            tex->SetNDC();
            tex->SetTextFont(132);
            tex->SetTextSize(0.05);
            tex->DrawLatex(0.52, 0.75,
                           TString::Format("M_{#Lambda}^{EXP} = %.3f #pm %.3f MeV/c^{2}", res.mean,
                                           res.mean_err)
                               .Data());
            tex->DrawLatex(0.52, 0.68,
                           TString::Format("S = (%.1f #pm %.1f) #times 10^{3}    S/B = %.2f",
                                           res.signal / 1000., res.signal_err / 1000., res.sb)
                               .Data());
            tex->DrawLatex(0.52, 0.55, "M_{#Lambda}^{PDG} = 1115.683 #pm 0.006 MeV/c^{2}");

            int bmax = hSignalInvMass->GetMaximumBin();
            double max = hSignalInvMass->GetBinContent(bmax);
            TLine* line = new TLine(res.mean - 3 * res.sigma, 0, res.mean - 3 * res.sigma, max);
            line->SetLineStyle(7);
            line->Draw();
            line = new TLine(res.mean + 3 * res.sigma, 0, res.mean + 3 * res.sigma, max);
            line->SetLineStyle(7);
            line->Draw();

            TLine* l =
                new TLine(hSignalInvMass->GetBinLowEdge(1), 0.,
                          hSignalInvMass->GetBinLowEdge(hSignalInvMass->GetNbinsX() + 1), 0.);
            l->SetLineStyle(2);
            l->Draw("same");

            TArrow* ar1 = new TArrow(1115.683, max * 0.8, 1115.683, _max * 1.1, 0.02, "|>");
            ar1->SetLineWidth(2);
            ar1->Draw();

            SaveAndClose(cSignalInvMassNice);
            gStyle->SetTitleFontSize(orig_tfs);
        }
    }

    for (uint i = 0; i < ananum; ++i)
    {
        if (!flag_nodifffit) FitFactory(fac[i], sigfac[i]);
    }
}

void LambdaFitter::AnaFinalize()
{
    OpenExportFile();
    GetExportFile()->cd();

    SaveAndClose(cSignalInvMass);
    hSignalInvMass->Write();

    for (uint i = 0; i < ananum; ++i)
    {
        fac[i]->niceHists(def_pf_diff, def_gf_diff);
        //     fac[i]->niceDiffs(0.1, 0.05, 0.13, 0.1, 505, 1002, 0.08, 0.08, 0.9, 0.07, 0.07,
        //     0.55); fac[i]->niceSlices(0.1, 0.01, 0.13, 0.1, 505, 1002, 0.06, 0.07, 0.9, 0.07,
        //     0.07, 0.55);
        fac[i]->prepareDiffCanvas();
        fac[i]->finalize();

        // 		if (i == 0)
        // 		{
        // 			fac[i]->cSignalXY->cd(0);
        // // 			gPad->SetGridx(1);
        // // 			gPad->SetGridy(1);
        // // 			RootTools::DrawAngleLine(15, 0.59, 0.89, 275);
        // // 			RootTools::DrawAngleLine(45, 0.27, 0.64, 290);
        // // 			RootTools::DrawAngleLine(85, 0.07, 0.28, 339);
        // // 			RootTools::DrawMomentumLine(500, 0.75, 0.27, 325);
        // // 			RootTools::DrawMomentumLine(1000, 0.75, 0.24, 336);
        // 			RootTools::DrawMomentumLine(1000, 0.73, 0.38, 302);
        //
        // 			fac[i]->cSignalWithCutsXY->cd(0);
        // // 			gPad->SetGridx(1);
        // // 			gPad->SetGridy(1);
        // // 			RootTools::DrawAngleLine(15, 0.59, 0.89, 275);
        // // 			RootTools::DrawAngleLine(45, 0.27, 0.64, 290);
        // // 			RootTools::DrawAngleLine(85, 0.07, 0.28, 339);
        // // 			RootTools::DrawMomentumLine(500, 0.75, 0.27, 325);
        // 			RootTools::DrawMomentumLine(1000, 0.75, 0.48, 336);
        // 			RootTools::DrawMomentumLine(1100, 0.75, 0.52, 336);
        // 			RootTools::DrawMomentumLine(1200, 0.75, 0.56, 336);
        // 			RootTools::DrawMomentumLine(1300, 0.75, 0.60, 336);
        // 			RootTools::DrawMomentumLine(1400, 0.75, 0.64, 336);
        //
        // 			fac[i]->cDiscreteXYFull->cd(0);
        // 			RootTools::DrawMomentumLine(1000, 0.75, 0.48, 336);
        //
        // 			fac[i]->cDiscreteXYSigFull->cd(0);
        // 			RootTools::DrawMomentumLine(1000, 0.75, 0.48, 336);
        // 		}
        fac[i]->write(GetExportFile());
        if (sigfac[i])
        {
            sigfac[i]->niceHists(def_pf_diff, def_gf_diff);
            sigfac[i]->finalize();
            sigfac[i]->write(GetExportFile());
        }
    }

    for (uint i = 0; i < ananum; ++i)
    {
        fac[i]->ctx.Write();
        if (sigfac[i]) { sigfac[i]->ctx.Write(); }
    }

    fit_results->write(GetExportFile());

    setConfigVector()->Write();
    CloseExportFile();

    if (secvertfile) secvertfile->Close();

    if (!flag_noaux) { ff.exportFactoryToFile(); }
}

void LambdaFitter::FitFactory(DifferentialFactory* fac, DistributionFactory* sigfac)
{
    fac->setFitCallback(&fit_callback);
    fac->fitDiffHists(sigfac, ff, *stdfit, !(flag_fss or flag_exp));
}

int LambdaFitter::config_params(struct option*& opts)
{
    static struct option lopts[] = {
        /* These options set a flag. */
        {"fit-params", required_argument, 0, 1001},     {"fit-matrix", required_argument, 0, 1002},
        {"noaux", no_argument, &flag_noaux, 1},         {"sideband", required_argument, 0, 1010},
        {"nodifffit", no_argument, &flag_nodifffit, 1},
    };
    opts = lopts;

    return sizeof(lopts) / sizeof(option);
}

void LambdaFitter::config_execute(int code, char* optarg)
{
    switch (code)
    {
        case 1001:
            par_fit_params = optarg;
            break;
        case 1010:
            flag_sideband = atoi(optarg);
            break;
        case 0:
            optfile = optarg;
            break;
        default:
            break;
    }
}

// Quadratic background function
Double_t background(Double_t* x, Double_t* par)
{
    return par[0] + par[1] * x[0] /* + par[2]*x[0]*x[0]*/;
}

// Lorenzian Peak function
Double_t lorentzianPeak(Double_t* x, Double_t* par)
{
    return (0.5 * par[0] * par[1] / TMath::Pi()) /
           TMath::Max(1.e-10, (x[0] - par[2]) * (x[0] - par[2]) + .25 * par[1] * par[1]);
}

// Sum of background and peak function
Double_t fitFunction(Double_t* x, Double_t* par)
{
    return background(x, par) + lorentzianPeak(x, &par[2]);
}

FitResultData LambdaFitter::FitInvMass(TH1* hist, HistFitParams& hfp)
{
    FitResultData result;

    TF1* tfLambdaSig = hfp.funSig;
    TF1* tfLambdaBkg = hfp.funBkg;
    TF1* tfLambdaSum = hfp.funSum;

    if (hfp.rebin != 0)
    {
        printf(" ++ Rebinning by %d\n", hfp.rebin);
        hist->Rebin(hfp.rebin); // FIXME
    }

    tfLambdaSig->SetLineColor(kBlack);
    tfLambdaSig->SetLineWidth(1);

    tfLambdaBkg->SetLineColor(kGreen);
    tfLambdaBkg->SetLineWidth(1);
    tfLambdaBkg->SetLineStyle(7);

    tfLambdaSum->SetLineColor(kRed);
    tfLambdaSum->SetLineWidth(1);

    bool res = ff.fit(&hfp, hist, "", "");

    if (res)
    {
        hfp.update();
        tfLambdaSum = (TF1*)hist->GetListOfFunctions()->At(0);
        tfLambdaSig = (TF1*)hist->GetListOfFunctions()->At(1);
        tfLambdaBkg = (TF1*)hist->GetListOfFunctions()->At(2);
    }
    else
        return result;

    printf(" Main Fit end\n");
    printf("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n");

    double mean = tfLambdaSig->GetParameter(1);
    int fSBmean = hist->FindBin(mean);
    double bin_width = hist->GetBinWidth(fSBmean);

    double signal = tfLambdaSig->GetParameter(0) / bin_width;
    double r = tfLambdaSig->GetParameter(4);
    double q = 1.0 - r;

    double s1 = TMath::Abs(tfLambdaSig->GetParameter(2));
    double s2 = TMath::Abs(tfLambdaSig->GetParameter(5));

    // 	double width = (r*s1*s1 + q*s2*s2)/(r*s1 + q*s2);
    double width = (r * s1 + q * s2);

    double signal_err = tfLambdaSig->GetParError(0) / bin_width;
    result.mean = tfLambdaBkg->GetParameter(1);
    result.mean_err = tfLambdaBkg->GetParError(1);

    // calculate S/B
    double fSBmin = mean - 3.0 * width;
    double fSBmax = mean + 3.0 * width;

    TH1* hsigclone = ((TH1*)hist->Clone("hsig"));
    hsigclone->Sumw2();

    double int_s = tfLambdaSig->Integral(fSBmin, fSBmax);
    double int_b = tfLambdaBkg->Integral(fSBmin, fSBmax);
    double int_t = tfLambdaSum->Integral(fSBmin, fSBmax);
    double s_delta = int_t - int_b;
    double s_error = sqrt(int_t + int_b);
    double chi2 = tfLambdaSum->GetChisquare();
    double ndf = tfLambdaSum->GetNDF();

    printf(" s=%g+-%g, b=%g+-%g, t=%g+-%g, d=%g+-%g, err=%g == %g\n", int_s, sqrt(int_s), int_b,
           sqrt(int_b), int_t, sqrt(int_t), s_delta, sqrt(s_delta), s_error,
           s_error / s_delta * 100);

    hsigclone->Add(tfLambdaBkg, -1);

    // FIXME here should be now correct error calcualtion
    double fLambdaS = int_s / bin_width;
    double fLambdaB = int_b / bin_width;

    hsigclone->Delete();

    TLatex* latex = new TLatex();
    latex->SetNDC();
    latex->SetTextSize(0.05);

    latex->DrawLatex(0.50, 0.80, "Statistics:");
    latex->DrawLatex(
        0.52, 0.74,
        TString::Format("S=%g#pm%.0f (%.1f%%)", signal, signal_err, signal_err / signal * 100.0));
    latex->DrawLatex(0.52, 0.68, TString::Format("B=%g", fLambdaB));
    latex->DrawLatex(0.52, 0.62, TString::Format("S/B=%g", fLambdaS / fLambdaB));
    latex->DrawLatex(0.52, 0.56,
                     TString::Format("#chi^{2}/ndf=%.1f/%.0f=%.2f", chi2, ndf, chi2 / ndf));

    hFitResults->SetBinContent(1, fLambdaS);
    hFitResults->SetBinContent(2, fLambdaB);
    hFitResults->SetBinContent(3, fLambdaS / fLambdaB);

    // show S/B lines on canvas
    TLine* tlSBline = new TLine();
    tlSBline->SetLineStyle(7);
    double line_height = tfLambdaSum->Eval(mean);
    tlSBline->DrawLine(fSBmin, 0., fSBmin, line_height);
    tlSBline->DrawLine(fSBmax, 0., fSBmax, line_height);

    tfLambdaSig->Write();
    tfLambdaBkg->Write();
    tfLambdaSum->Write();

    if (flag_sideband > 0)
    {
        int fSBminBin = hist->FindBin(fSBmin);
        int fSBmaxBin = hist->FindBin(fSBmax);

        int fSBminBin_ = fSBminBin;
        int fSBmaxBin_ = fSBmaxBin + 1;
        double fSBmin_ = fSBmin;
        double fSBmax_ = fSBmax;

        TH1* hbgclone = ((TH1*)hist->Clone("hbg"));
        hbgclone->Add(tfLambdaSig, -1);

        double sb_mean = (fSBmin_ + fSBmax_) / 2.0;
        int sb_mean_bin = hist->FindBin(sb_mean);

        double sb_intlb = hbgclone->Integral(fSBminBin_, sb_mean_bin);
        double sb_intrb = hbgclone->Integral(sb_mean_bin, fSBmaxBin_);

        int sb_x_left_bin = RootTools::FindEqualIntegralRange(hist, sb_intlb, fSBminBin_, -1.0);
        int sb_x_right_bin = RootTools::FindEqualIntegralRange(hist, sb_intrb, fSBmaxBin_, 1.0);

        double sb_x_left = hist->GetBinCenter(sb_x_left_bin);
        double sb_x_right = hist->GetBinCenter(sb_x_right_bin);

        double sb_x_left_int = hbgclone->Integral(sb_x_left_bin, fSBminBin_);
        double sb_x_right_int = hbgclone->Integral(fSBmaxBin_, sb_x_right_bin);

        fSidebandL = (TF1*)tfLambdaSum->Clone("h_SidebandL"); //, sb_x_left_bin, fSBminBin_);
        fSidebandR = (TF1*)tfLambdaSum->Clone("h_SidebandR"); //, fSBmaxBin_, sb_x_right_bin);

        fSidebandL->SetRange(fSBmin - 3.0 * width, fSBmin);
        fSidebandL->SetLineColor(kBlack);
        fSidebandL->SetLineWidth(1);
        fSidebandL->SetFillColor(kPink);

        fSidebandR->SetRange(fSBmax, fSBmax + 3.0 * width);
        fSidebandR->SetLineColor(kBlack);
        fSidebandR->SetLineWidth(1);
        fSidebandR->SetFillColor(kPink);

        fSidebandL->SetFillStyle(3003);
        fSidebandR->SetFillStyle(3003);

        fSidebandBgL = (TF1*)tfLambdaBkg->Clone("h_SidebandLB");
        fSidebandBgR = (TF1*)tfLambdaBkg->Clone("h_SidebandRB");

        hbgclone->Delete();

        fSidebandBgL->SetRange(fSBmin, mean);
        fSidebandBgL->SetLineColor(kGreen);
        fSidebandBgL->SetLineWidth(1);
        fSidebandBgL->SetFillColor(kGreen);

        fSidebandBgR->SetRange(mean, fSBmax);
        fSidebandBgR->SetLineColor(kBlue);
        fSidebandBgR->SetLineWidth(1);
        fSidebandBgR->SetFillColor(kBlue);

        fSidebandBgL->SetFillStyle(3004);
        fSidebandBgR->SetFillStyle(3005);

        // 		fSidebandLB->Draw("same,][,hist");
        // 		fSidebandRB->Draw("same,][,hist");

        double int_bg_ol = fSidebandL->Integral(fSBmin - 3.0 * width, fSBmin) / bin_width;
        double int_bg_or = fSidebandR->Integral(fSBmax, fSBmax + 3.0 * width) / bin_width;

        double int_bg_il = fSidebandBgL->Integral(fSBmin, mean) / bin_width;
        double int_bg_ir = fSidebandBgR->Integral(mean, fSBmax) / bin_width;

        latex->DrawLatex(0.57, 0.39, "SideBand:");
        latex->DrawLatex(
            0.60, 0.35,
            TString::Format("DIV: %.1f %.1f %.1f %.1f\n", sb_x_left, fSBmin_, fSBmax_, sb_x_right));
        latex->DrawLatex(0.60, 0.32,
                         TString::Format(" L : %.1f %.1f : r= %f\n", sb_x_left_int, sb_intlb,
                                         sb_intlb / sb_x_left_int));
        latex->DrawLatex(0.60, 0.29,
                         TString::Format(" R : %.1f %.1f : r= %f\n", sb_intrb, sb_x_right_int,
                                         sb_intrb / sb_x_right_int));
        // 		latex->DrawLatex(0.60, 0.26, TString::Format(" I : %.1f %.1f : r= %f\n", sb_intl,
        // sb_intr, sb_intl/sb_intr));
        latex->DrawLatex(0.60, 0.26,
                         TString::Format(" I : %.1f %.1f : r= %f\n", int_bg_il, int_bg_ir,
                                         int_bg_il / int_bg_ir));
        latex->DrawLatex(0.60, 0.23,
                         TString::Format(" O : %.1f %.1f : r= %f\n", int_bg_ol, int_bg_or,
                                         int_bg_ol / int_bg_or));
    }

    result.signal = signal;
    result.signal_err = signal_err;
    result.sigma = width;
    result.sb = fLambdaS / fLambdaB;
    return result;
}
