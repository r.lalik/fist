#include <cppunit/extensions/HelperMacros.h>

#include <TextDbReader.h>

class CustomTextDbReader : public TextDbReader
{
public:
	CustomTextDbReader(const char * dbfilename) : TextDbReader(dbfilename) { }
	CustomTextDbReader(const std::string & dbfilename) : TextDbReader(dbfilename) {}

	void parse();
};

void CustomTextDbReader::parse()
{
	DatabaseRecord dbe;
	dbe.location = "aaa_@_bbb";
	dbe.cs = 137.1;
// 	dbe.color = 7;
	dbe.label = "test_channel";
	dbe.scale = 11.2358;
	dbe.cs_errors_str = "0.43|+3.55-2.83|2+3.55-2.83|2+3.55-2.83+1|2.67|-2.86||13";

	dbmap[dbe.label] = dbe;
}

class BasicCase : public CPPUNIT_NS::TestFixture
{
	CPPUNIT_TEST_SUITE( BasicCase );
	CPPUNIT_TEST( MyTest );
	CPPUNIT_TEST_SUITE_END();

public:
	void setUp();

protected:
	void MyTest();

	CustomTextDbReader * cdb;
};

CPPUNIT_TEST_SUITE_REGISTRATION( BasicCase );

void BasicCase::setUp()
{
	cdb = new CustomTextDbReader("test");
	cdb->parse();
}

void BasicCase::MyTest()
{
	float fnum = 2.00001f;
	CPPUNIT_ASSERT_DOUBLES_EQUAL( fnum, 2.0f, 0.0005 );

	auto ce = cdb->getErrors("test_channel");

// 	"0.43|+3.55-2.83|2+3.55-2.83|2+3.55-2.83+1|2.67|-2.86";
	CPPUNIT_ASSERT_DOUBLES_EQUAL( ce[0].low, 0.43f, 0.0001 );
	CPPUNIT_ASSERT_DOUBLES_EQUAL( ce[0].high, 0.43f, 0.0001 );

	CPPUNIT_ASSERT_DOUBLES_EQUAL( ce[1].low, 2.83f, 0.0001 );
	CPPUNIT_ASSERT_DOUBLES_EQUAL( ce[1].high, 3.55f, 0.0001 );

	CPPUNIT_ASSERT_DOUBLES_EQUAL( ce[2].low, 2.0f, 0.0001 );
	CPPUNIT_ASSERT_DOUBLES_EQUAL( ce[2].high, 2.0f, 0.0001 );

	CPPUNIT_ASSERT_DOUBLES_EQUAL( ce[3].low, 2.0f, 0.0001 );
	CPPUNIT_ASSERT_DOUBLES_EQUAL( ce[3].high, 2.0f, 0.0001 );

	CPPUNIT_ASSERT_DOUBLES_EQUAL( ce[4].low, 2.67f, 0.0001 );
	CPPUNIT_ASSERT_DOUBLES_EQUAL( ce[4].high, 2.67f, 0.0001 );

	CPPUNIT_ASSERT_DOUBLES_EQUAL( ce[5].low, 2.86f, 0.0001 );
	CPPUNIT_ASSERT_DOUBLES_EQUAL( ce[5].high, 0.0f, 0.0001 );

	CPPUNIT_ASSERT_DOUBLES_EQUAL( ce[6].low, 0.0f, 0.0001 );
	CPPUNIT_ASSERT_DOUBLES_EQUAL( ce[6].high, 0.0f, 0.0001 );

	CPPUNIT_ASSERT_DOUBLES_EQUAL( ce[7].low, 13.0f, 0.0001 );
	CPPUNIT_ASSERT_DOUBLES_EQUAL( ce[7].high, 13.0f, 0.0001 );
}
