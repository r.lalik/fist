/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LAMBDAFITTER_H
#define LAMBDAFITTER_H

#ifndef __CINT__

#include "map"

class TH1F;
class TCanvas;
class TChain;
class TF1;
class TFile;
class TH1;
class TH1F;
class TH1I;
class TH2;
class TH2F;
class TStyle;

class DifferentialFactory;
class SmartFactory;

#endif /* __CINT__ */

#include "AnaTools.h"
#include "ToolsAbstract.h"
#include "globals.h"

#include <FitterFactory.h>

// typedef std::map<TString, HistFitParams> pmap;
// typedef std::map<TString, HistFitParams>::const_interator pmapit;

struct FitResultData
{
    FitResultData()
        : fit_ok(false), mean(0), mean_err(0), sigma(0), chi2(0), ndf(0), signal(0), signal_err(0),
          sb(0)
    {
    }
    bool fit_ok;
    float mean;
    float mean_err;
    float sigma;
    float chi2;
    int ndf;
    float signal;
    float signal_err;
    float sb;
};

void fit_callback(DifferentialFactory* fac, DistributionFactory* sigfac, int fit_res, TH1* h,
                  uint x_pos, uint y_pos, uint z_pos);

class LambdaFitter : public ToolsAbstract
{
private:
    // chain
    TChain* chain;

public:
    LambdaFitter();
    // 	LambdaFitter(Int_t nxnum = 4, Int_t channum = 128, const TString & exportdir = "Export");
    virtual ~LambdaFitter();

    void AnaInitialize();
    void AnaExecute();
    void AnaFinalize();

    // 	enum HLimits { HTime, HChan, HADC, HLast };

    // 	void SetLimits(HLimits type, Int_t bins, Int_t lower, Int_t upper);

    int config_params(struct option*& opts);
    void config_execute(int code, char* optarg);

    FitResultData FitInvMass(TH1* hist, HistFitParams& hfp);

    void FitFactory(DifferentialFactory* fac, DistributionFactory* sigfac);

private:
    void PrepareArgsList();
    // 	void FetchFitInfo(TF1 * fun, double & mean, double & width, double & sig, double & bkg, TPad
    // * pad = nullptr);

    // params vector
    const Int_t nParNum;
    TH1F* params;

public:
    // 	Float_t lay_delta;
    // 	Float_t lapt_delta;
    //
    // 	Float_t lacthcm_delta;
    // 	Float_t lapcm_delta;

    Float_t fLambdaMCutMin, fLambdaMCutMax;

private:
    TFile* secvertfile;
    TString optfile;
    // Lambda +++++++++++++++++++++++++++++++++++++++++++++
    TH1D* hSignalInvMass; // Invariant mass of p-pi
    TCanvas* cSignalInvMass;

    TF1* fSidebandL;
    TF1* fSidebandR;
    TF1* fSidebandBgL;
    TF1* fSidebandBgR;
    TCanvas* cSideband;

    DifferentialFactory** fac;
    DistributionFactory** sigfac;

    // prog args
    std::string par_fit_params;
    std::string par_fit_matrix;
    std::string par_fit_export;
    // 	Bool_t par_doAccCorr;

    int flag_noaux;
    int flag_sideband;
    int flag_nodifffit;

    HistFitParams* stdfit;
    FitterFactory ff;

    TH1F* hFitResults;
    SmartFactory* fit_results;
};

#endif // LAMBDAFITTER_H
