/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __CINT__

#include "getopt.h"
#include <fstream>
#include <string>

#include "TCanvas.h"
#include "TChain.h"
#include "TClonesArray.h"
#include "TDirectory.h"
#include "TError.h"
#include "TF1.h"
#include "TFile.h"
#include "TGaxis.h"
#include "TH2.h"
#include "TImage.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TLorentzVector.h"
#include "TMath.h"
#include "TStyle.h"
#include "TVector.h"

#endif /* __CINT__ */

#include "AnaTools.h"
#include "RootTools.h"
#include "UrQMDReader.h"

// #include "hphysicsconstants.h"
// #include "hpidphysicsconstants.h"

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

static const Float_t E_kin_beam = 3500.0;
static const Float_t E_kin_target = 0.0;
static const Float_t E_total_beam = E_kin_beam + 938;     // HPidPhysicsConstants::mass(14);
static const Float_t E_total_target = E_kin_target + 938; // HPidPhysicsConstants::mass(14);
static const Float_t pz_beam =
    sqrt(E_total_beam * E_total_beam -
         /*HPidPhysicsConstants::mass(14)*HPidPhysicsConstants::mass(14)*/ 938 * 938);

static const TLorentzVector Vec_pp35_beam = TLorentzVector(0.0, 0.0, pz_beam, E_total_beam);
static const TLorentzVector Vec_pp35_target = TLorentzVector(0.0, 0.0, 0.0, E_total_target);
static const TLorentzVector Vec_pp35_sum = Vec_pp35_beam + Vec_pp35_target;
static const Float_t cmrap = Vec_pp35_sum.Rapidity();

TLorentzVector Vec_beam_target = Vec_pp35_beam + Vec_pp35_target;

Float_t PiConst = TMath::Pi();

// Processing data bar width
const Int_t bar_dotqty = 10000;
const Int_t bar_width = 20;

const Option_t h1opts[] = "h,E1";

UrQMDReader::UrQMDReader() : SecVertexReco() { flag_urqmd = true; }

UrQMDReader::~UrQMDReader() {}

// void UrQMDReader::InitTree()
// {
// 	if (chain) return;
//
// 	chain = new TChain("pp35_Lambda");
//
// 	chain->SetBranchAddress("fP",				&b_fP);
// 	chain->SetBranchAddress("fTheta",			&b_fTheta);
// 	chain->SetBranchAddress("fPhi",				&b_fPhi);
//
// 	chain->SetBranchAddress("fM",				&b_fM);
// 	chain->SetBranchAddress("fY",				&b_fY);
// 	chain->SetBranchAddress("fY_cms",			&b_fY_cms);
// 	chain->SetBranchAddress("fPt",				&b_fPt);
// 	chain->SetBranchAddress("fCosTheta_cms",	&b_fCosTheta_cms);
//
// 	chain->SetBranchAddress("fPrimary",			&b_fPrimary);
//
// 	if (!par_fNoVertexCuts)
// 	{
// 	chain->SetBranchAddress("fMinTrackDist",	&b_fMinTrackDist);
// 	chain->SetBranchAddress("fVertDistX",		&b_fVertDistX);
// 	chain->SetBranchAddress("fVertDistA",		&b_fVertDistA);
// 	chain->SetBranchAddress("fVertDistB",		&b_fVertDistB);
// 	}
// }

void UrQMDReader::AnaInitialize()
{
    SecVertexReco::AnaInitialize();

    //	CMS_Beta	= Vec_beam_target.Beta();
    //	CMS_Beta_x	= CMS_Beta*sin(Vec_beam_target.Theta())*cos(Vec_beam_target.Phi());	 // x
    // component of BetaVector 	CMS_Beta_y	=
    // CMS_Beta*sin(Vec_beam_target.Theta())*sin(Vec_beam_target.Phi());	 // y component of
    // BetaVector 	CMS_Beta_z	= CMS_Beta*cos(Vec_beam_target.Theta());

    data_scaledown /= 41.83; // * 140000000;
    PR(data_scaledown);
}

void UrQMDReader::ReadEvent(ULong64_t ev)
{
    SecVertexReco::ReadEvent(ev);

    //	Float_t p_x	= b_fP*sin(b_fTheta)*cos(b_fPhi);	 // x component of BetaVector
    //	Float_t p_y	= b_fP*sin(b_fTheta)*sin(b_fPhi);	 // y component of BetaVector
    //	Float_t p_z	= b_fP*cos(b_fTheta);

    //	TLorentzVector track_cms(p_x, p_y, p_z, sqrt(b_fM*b_fM + b_fP*b_fP));
    // PR(b_fM);PR(b_fP);
    //	track_cms.Boost(-CMS_Beta_x, -CMS_Beta_y, -CMS_Beta_z);
    // 	b_fY_cms				= track_cms.Rapidity();
    // 	b_fP_cms				= track_cms.P();
    // 	b_fCosTheta_cms			= cos(track_cms.Theta());
}