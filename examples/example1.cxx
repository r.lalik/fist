#include "ToolsAbstract.h"

#include <fstream>

class TestReader : public ToolsAbstract
{
    void execute() override {}
};

int main(int argc, char** argv)
{
    TestReader ta;
    ta.configure(argc, argv);

    return 0;
}
