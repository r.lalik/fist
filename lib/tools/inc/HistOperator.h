/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HISTOPERATOR_H
#define HISTOPERATOR_H

#ifndef __CINT__

#include "TChain.h"
#include "TString.h"
// #include "TVector.h"

class Dim2AnalysisFactory;
class MultiDimAnalysisContext;

class TCanvas;
class TChain;
class TF1;
class TFile;
class TGraphAsymmErrors;
class TH1;
class TH1F;
class TH1I;
class TH2;
class TH2F;
class TObjArray;
class TStyle;

#endif /* __CINT__ */

#include <string>
#include <vector>

#include "SimpleToolsAbstract.h"
#include "globals.h"

enum HistOperationCommand
{
    HO_NOP,
    HO_ADD,
    HO_SUB,
    HO_MULT,
    HO_DIV,
    HO_NEG,
    HO_NORM,
    HO_NORMX,
    HO_NDIV,
    HO_PROJ,
    HO_SMOOTH,
    HO_BINNORM,
    HO_ERRORS,
    HO_ANGDIST,
    HO_COPYRELERRS,
    HO_BINOMERRORS,
    HO_EFFICIENCY,
    HO_APPLYSYST,
    HO_TOTALERRORS,
    HO_TOTALERRORS2,
    HO_FIXANGDIST,
    HO_EOA
};

struct HistOperation
{
    HistOperationCommand cmd;
    TString arg;
};

class HistOperator : public SimpleToolsAbstract
{
private:
    // chain
    // 	TChain * chain;

public:
    HistOperator();
    virtual ~HistOperator();

    void AnaInitialize();
    void AnaExecute();
    void AnaFinalize();

    void PrintAnalysisInfo();

    int config_params(struct option*& opts);
    void config_execute(int code, char* optarg);

private:
    void PrepareArgsList();

    void PreparePlots(TCanvas* c, TH2* hexp, TH2* hgibuu, TH2* hurqmd);

    template <typename T1, typename T2> bool ProcessContextsGroup(const TString& ctx_suffix);

    void NoNumberAllowed();
    void NoFactoryAllowed();

public:
private:
    TFile* inputfile;

    std::vector<HistOperation> hist_operations;

    std::vector<TString> infile;
    TString outputfile;

    std::string hop_names[HO_EOA];
    bool no_export_operation;

    int has_csscale;
    int flag_rise, flag_lower;
};

#endif // HISTOPERATOR_H
