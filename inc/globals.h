#ifndef GLOBALS_H
#define GLOBALS_H

#include <TString.h>

// unit conversion
// 0.000001 for nb
// 0.001 for ub
// 1 for mb
const Float_t expscaleunit = 1;
const TString unitprefix = "ub";

// exp data scaling
const Float_t expnorm = 3./13925./12164./1.6 * expscaleunit;

// scale for Pluto
// Pluto scale is given in mb
const Float_t plutoscale = expscaleunit;
const Float_t plutofiles = 20.;
const Float_t plutoevents = 20000000.;
const Float_t plutonorm = 1./plutoevents*plutoscale;

// scale for BiGUU data
// 1. for ub
const Float_t gibuuscale = 1.;

const Float_t urqmdscale = 1.;

// static const size_t ananum = 5;
// static const std::string ananames[ananum] = { "Lambda_InvMass", "S1385p_InvMass", "Lambda_PtYcm", "Lambda_PcmCosThcm", "Lambda_XcmPcm", };

// static const size_t ananum = 4;
// static const std::string ananames[ananum] = { "PtYcm", "PcmCosThcm", "XcmPcm", "SPdist" };

//static const size_t ananum = 4;
//static const std::string ananames[ananum] = { "PtYcm", "PtY", "PcmCosThcm", "XcmPcm" };
#endif /* GLOBALS_H */

