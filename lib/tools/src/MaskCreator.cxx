/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __CINT__

#include "getopt.h"
#include <fstream>
#include <string>

#include "TCanvas.h"
#include "TChain.h"
#include "TDirectory.h"
#include "TEllipse.h"
#include "TError.h"
#include "TF1.h"
#include "TFile.h"
#include "TGaxis.h"
#include "TH2.h"
#include "TImage.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TMath.h"
#include "TStyle.h"
#include "TVector3.h"

#endif /* __CINT__ */

#include "AnaTools.h"
#include "MaskCreator.h"
#include "RootTools.h"

#include "DatabaseManager.h"
#include "DifferentialFactory.h"

#undef NDEBUG
#include <assert.h>

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";
#define test_array(arr, size) assert((sizeof(arr) / sizeof(arr[0])) == (size));

using namespace RootTools;

// Processing data bar width
const Int_t bar_dotqty = 10000;
const Int_t bar_width = 20;

const Option_t h1opts[] = "h,E1,same";

MaskCreator::MaskCreator() : SecVertexReco() {}

MaskCreator::~MaskCreator() {}

void MaskCreator::AnaExecute()
{
    for (uint i = 0; i < lacthcm_bins / 2 + 1; ++i)
    {
        for (uint j = 0; j < 3; ++j)
        {
            facLambdaPcmCosThcmDiff->hSignalCounter->SetBinContent(i + 1, j + 1, 1.0);
            //       FIXME make it for Sig
        }
    }

    int pty_mask[13][8] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                           0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                           0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                           0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
                           0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0};

    for (uint i = 0; i < laycm_bins; ++i)
    {
        for (uint j = 0; j < lapt_bins; ++j)
        {
            facLambdaPtYcmDiff->hSignalCounter->SetBinContent(i + 1, j + 1,
                                                              pty_mask[13 - j - 1][i]);
            //       FIXME make it for Sig
        }
    }
}
