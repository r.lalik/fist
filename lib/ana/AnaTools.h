/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ANATOOLS_H
#define ANATOOLS_H

#ifndef __CINT__

class TCanvas;
class TChain;
class TF1;
class TFile;
class TH1;
class TH2;
class TH2F;
class TStyle;

#endif /* __CINT__ */

#include <TCanvas.h>
#include <TFile.h>
#include <TH1.h>

#include <string>

namespace pp35Tools
{

struct DataEntry
{
    std::string path;
    std::string file;
    std::string id;
    std::string label;
    float cs;
    double scale;
    int color;
    bool fit;
    float fitpar;
};

struct DataEntrySet
{
    DataEntry entry;
    TFile* file;
};

void FitInvMass(TH1* hist, TCanvas* can, TH1* params = NULL);

Double_t LegendrePolynomials5_FitFunc(Double_t* x_val, Double_t* par);
}; // namespace pp35Tools

#endif // ANATOOLS_H
