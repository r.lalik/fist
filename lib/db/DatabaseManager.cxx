#include <exception>
#include <string>

#include <TObjArray.h>
#include <TObjString.h>
#include <TString.h>

#include <SqlDbReader.h>
#include <TextDbReader.h>

#include "DatabaseManager.h"
#include <RootTools.h>

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

AnaDatabase* DatabaseManager::db = nullptr;

AnaDatabase* DatabaseManager::getDatabase(const std::string& filename, DatabaseSelector dbs)
{
    std::vector<std::string> strvec = RT::split(filename, ':');

    std::string fn;

    switch (dbs)
    {
        case DBS_NEWER:
        {
            uint newer_idx = 0;
            for (uint i = 1; i < strvec.size(); ++i)
            {
                if (RT::FileIsNewer(strvec[i].c_str(), strvec[newer_idx].c_str())) newer_idx = i;
            }
            fn = strvec[newer_idx];
        }
        break;
        case DBS_FIRST:
            fn = strvec[0];
            break;
    }

    size_t dot_pos = fn.find_last_of(".");
    std::string ext = fn.substr(dot_pos);

    std::cerr << " Using database " << fn << std::endl;

    if (ext == ".db") db = new SqlDbReader(fn);

    if (ext == ".txt") db = new TextDbReader(fn);

    return db;
}
