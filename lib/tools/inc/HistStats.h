/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HISTSTATS_H
#define HISTSTATS_H

#ifndef __CINT__

#endif /* __CINT__ */

#include <vector>

#include "ToolsAbstract.h"

#include "globals.h"

enum HistStatsCommand
{
    HS_COUNTS,
    HS_INTEGRAL,
    HS_EOA
};

struct StatsOperation
{
    HistStatsCommand cmd;
    TString arg;
};

class HistStats : public SimpleToolsAbstract
{
public:
    HistStats();
    virtual ~HistStats();

    void AnaInitialize();
    void AnaExecute();
    void AnaFinalize();

    void PrintAnalysisInfo();

    int config_params(struct option*& opts);
    void config_execute(int code, char* optarg);

private:
    void PrepareArgsList();
    void PreparePlots(TCanvas* c, TH2* hexp, TH2* hgibuu, TH2* hurqmd);

public:
private:
    TFile* inputfile;

    std::vector<StatsOperation> hist_stats;

    std::vector<TString> infile;

    std::string hsp_names[HS_EOA];
};

#endif // HISTSTATS_H
