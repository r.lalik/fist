/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "SystematicErrors.h"

#ifndef __CINT__

#include "getopt.h"
#include <fstream>
#include <string>

#include "TCanvas.h"
#include "TChain.h"
#include "TDirectory.h"
#include "TError.h"
#include "TF1.h"
#include "TFile.h"
#include "TGaxis.h"
#include "TGraphAsymmErrors.h"
#include "TH2.h"
#include "TImage.h"
#include "TKey.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TMath.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TVector.h"
#include "TVirtualFitter.h"

#endif /* __CINT__ */

#include "RootTools.h"

#include "globals.h"

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

const Option_t h1opts[] = "h,E1";

const uint syst_lopps = 2;

SystematicErrors::SystematicErrors() : SimpleToolsAbstract()
{
    NicePalette();
    TGaxis::SetMaxDigits(3);
}

SystematicErrors::~SystematicErrors() {}

void SystematicErrors::AnaInitialize()
{
    if (cfgfile.IsNull())
    {
        std::cout << "No config file!" << std::endl;
        std::exit(EXIT_FAILURE);
    }

    std::ifstream fcfgfile(cfgfile);

    hasExp = hasLower_a = hasHigher_a = hasLower_b = hasHigher_b = hasAe = kFALSE;

    std::string line;
    while (std::getline(fcfgfile, line))
    {
        TString line_(line);
        line_.ReplaceAll("\t", " ");

        TObjArray* arr = line_.Tokenize(" ");

        char cmdchar = ((TObjString*)arr->At(0))->String()[0];
        switch (cmdchar)
        {
            case 'h':
                higherfile_a = ((TObjString*)arr->At(1))->String();
                hasHigher_a = kTRUE;
                break;
            case 'e':
                expfile = ((TObjString*)arr->At(1))->String();
                hasExp = kTRUE;
                break;
            case 'l':
                lowerfile_a = ((TObjString*)arr->At(1))->String();
                hasLower_a = kTRUE;
                break;
            case 'm':
                higherfile_b = ((TObjString*)arr->At(1))->String();
                hasHigher_b = kTRUE;
                break;
            case 'n':
                lowerfile_b = ((TObjString*)arr->At(1))->String();
                hasLower_b = kTRUE;
                break;
            case 'a':
                aefile = ((TObjString*)arr->At(1))->String();
                hasAe = kTRUE;
                break;
            case 'o':
                outputfile = ((TObjString*)arr->At(1))->String();
                break;
        }
    }

    if (!(hasExp and hasLower_a and hasHigher_a and hasLower_b and hasHigher_b))
    {
        std::cout << "Config file is not complete, check for exp, lower and higher data!"
                  << std::endl;
        std::exit(EXIT_FAILURE);
    }

    SetOutputFile(outputfile);

    SetExportDir("./");

    OpenExportFile();

    inputfile = TFile::Open(expfile, "READ");
    if (!inputfile->IsOpen())
    {
        std::cerr << "File " << expfile << " is not open!";
        std::exit(EXIT_FAILURE);
    }

    getConfigVector();
    getAnaNames();
    set = new SysErrSet[ananum];

    for (uint i = 0; i < ananum; ++i)
    {
        set[i].h_exp = (TH2F*)RootTools::GetObjectFromFile(
            inputfile,
            TString::Format("%s/h_%s_LambdaInvMassSig", ananames[i].c_str(), ananames[i].c_str())
                .Data(),
            "exp");
    }
    // 	inputfile->Close();

    inputfile = TFile::Open(lowerfile_a, "READ");
    if (!inputfile->IsOpen())
    {
        std::cerr << "File " << lowerfile_a << " is not open!";
        std::exit(EXIT_FAILURE);
    }

    for (uint i = 0; i < ananum; ++i)
    {
        set[i].h_lower[0] = (TH2F*)RootTools::GetObjectFromFile(
            inputfile,
            TString::Format("%s/h_%s_LambdaInvMassSig", ananames[i].c_str(), ananames[i].c_str())
                .Data(),
            "lower");
    }
    // 	inputfile->Close();

    // UrQMD
    inputfile = TFile::Open(higherfile_a, "READ");
    if (!inputfile->IsOpen())
    {
        std::cerr << "File " << higherfile_a << " is not open!";
        std::exit(EXIT_FAILURE);
    }

    for (uint i = 0; i < ananum; ++i)
    {
        set[i].h_higher[0] = (TH2F*)RootTools::GetObjectFromFile(
            inputfile,
            TString::Format("%s/h_%s_LambdaInvMassSig", ananames[i].c_str(), ananames[i].c_str())
                .Data(),
            "higher");
    }

    inputfile = TFile::Open(lowerfile_b, "READ");
    if (!inputfile->IsOpen())
    {
        std::cerr << "File " << lowerfile_b << " is not open!";
        std::exit(EXIT_FAILURE);
    }

    for (uint i = 0; i < ananum; ++i)
    {
        set[i].h_lower[1] = (TH2F*)RootTools::GetObjectFromFile(
            inputfile,
            TString::Format("%s/h_%s_LambdaInvMassSig", ananames[i].c_str(), ananames[i].c_str())
                .Data(),
            "lower");
    }
    // 	inputfile->Close();

    // UrQMD
    inputfile = TFile::Open(higherfile_b, "READ");
    if (!inputfile->IsOpen())
    {
        std::cerr << "File " << higherfile_b << " is not open!";
        std::exit(EXIT_FAILURE);
    }

    for (uint i = 0; i < ananum; ++i)
    {
        set[i].h_higher[1] = (TH2F*)RootTools::GetObjectFromFile(
            inputfile,
            TString::Format("%s/h_%s_LambdaInvMassSig", ananames[i].c_str(), ananames[i].c_str())
                .Data(),
            "higher");
    }

    if (hasAe)
    {
        inputfile = TFile::Open(aefile, "READ");
        if (!inputfile->IsOpen())
        {
            std::cerr << "File " << aefile << " is not open!";
            std::exit(EXIT_FAILURE);
        }

        for (uint i = 0; i < ananum; ++i)
        {
            set[i].hS_ae = (TH2F*)RootTools::GetObjectFromFile(
                inputfile,
                TString::Format("%s/h_%s_LambdaInvMassSig", ananames[i].c_str(),
                                ananames[i].c_str())
                    .Data(),
                "ae");
            set[i].hS_aesig = (TH2F*)RootTools::GetObjectFromFile(
                inputfile,
                TString::Format("%s/h_%s_LambdaInvMass", ananames[i].c_str(), ananames[i].c_str())
                    .Data(),
                "ae");
        }
    }

    // 	inputfile->Close();

    OpenExportFile();

    // 	h_PtYcm_Systematic_lower = (TH2F*)hLambdaPtYcm_exp->Clone("h_PtYcm_Systematic_lower");
    // 	hSystematicPcmCosThcm_lower =
    // (TH2F*)hLambdaPcmCosThcm_exp->Clone("hSystematicPcmCosThcm_lower"); 	hSystematicXcmPcm_lower
    // = (TH2F*)hLambdaXcmPcm_exp->Clone("hSystematicXcmPcm_lower");
    //
    // 	h_PtYcm_Systematic_higher = (TH2F*)hLambdaPtYcm_higher->Clone("h_PtYcm_Systematic_higher");
    // 	hSystematicPcmCosThcm_higher =
    // (TH2F*)hLambdaPcmCosThcm_higher->Clone("hSystematicPcmCosThcm_higher");
    // 	hSystematicXcmPcm_higher = (TH2F*)hLambdaXcmPcm_higher->Clone("hSystematicXcmPcm_higher");
    //
    // // 	h_PtYcm_Systematic_higher = (TH2F*)hLambdaPtYcm_exp->Clone("h_PtYcm_Systematic_higher");
    // // 	hSystematicPcmCosThcm_higher =
    // (TH2F*)hLambdaPcmCosThcm_exp->Clone("hSystematicPcmCosThcm_higher");
    // // 	hSystematicXcmPcm_higher = (TH2F*)hLambdaXcmPcm_exp->Clone("hSystematicXcmPcm_higher");
    //
    // 	h_PtYcm_Systematic_lower->Add(hLambdaPtYcm_lower, -1);
    // 	hSystematicPcmCosThcm_lower->Add(hLambdaPcmCosThcm_lower, -1);
    // 	hSystematicXcmPcm_lower->Add(hLambdaXcmPcm_lower, -1);
    //
    // 	h_PtYcm_Systematic_higher->Add(hLambdaPtYcm_exp, -1);
    // 	hSystematicPcmCosThcm_higher->Add(hLambdaPcmCosThcm_exp, -1);
    // 	hSystematicXcmPcm_higher->Add(hLambdaXcmPcm_exp, -1);
    //
    // // 	h_PtYcm_Systematic_higher->Add(hLambdaPtYcm_higher, -1);
    // // 	hSystematicPcmCosThcm_higher->Add(hLambdaPcmCosThcm_higher, -1);
    // // 	hSystematicXcmPcm_higher->Add(hLambdaXcmPcm_higher, -1);

    // 	h_PtYcm_Systematic_lower = (TH2F*)hLambdaPtYcm_exp->Clone("h_PtYcm_Systematic_lower");
    // 	hSystematicPcmCosThcm_lower =
    // (TH2F*)hLambdaPcmCosThcm_exp->Clone("hSystematicPcmCosThcm_lower"); 	hSystematicXcmPcm_lower
    // = (TH2F*)hLambdaXcmPcm_exp->Clone("hSystematicXcmPcm_lower");
    //
    // 	h_PtYcm_Systematic_higher = (TH2F*)hLambdaPtYcm_lower->Clone("h_PtYcm_Systematic_higher");
    // 	hSystematicPcmCosThcm_higher =
    // (TH2F*)hLambdaPcmCosThcm_lower->Clone("hSystematicPcmCosThcm_higher");
    // 	hSystematicXcmPcm_higher = (TH2F*)hLambdaXcmPcm_lower->Clone("hSystematicXcmPcm_higher");

    // 	h_PtYcm_Systematic_lower->Add(hLambdaPtYcm_higher, -1);
    // 	hSystematicPcmCosThcm_lower->Add(hLambdaPcmCosThcm_higher, -1);
    // 	hSystematicXcmPcm_lower->Add(hLambdaXcmPcm_higher, -1);
    //
    // 	h_PtYcm_Systematic_higher->Add(hLambdaPtYcm_exp, -1);
    // 	hSystematicPcmCosThcm_higher->Add(hLambdaPcmCosThcm_exp, -1);
    // 	hSystematicXcmPcm_higher->Add(hLambdaXcmPcm_exp, -1);

    for (uint i = 0; i < ananum; ++i)
    {
        for (uint j = 0; j < syst_lopps; ++j)
        {
            set[i].hS_lower[j] = (TH2F*)set[i].h_lower[j]->Clone(
                TString::Format("h_%s_Systematic_lower_%d", ananames[i].c_str(), j).Data());
            set[i].hS_higher[j] = (TH2F*)set[i].h_higher[j]->Clone(
                TString::Format("h_%s_Systematic_higher_%d", ananames[i].c_str(), j).Data());
        }
    }
}

void SystematicErrors::AnaExecute()
{
    PrintAnalysisInfo();

    for (uint i = 0; i < ananum; ++i)
    {
        set[i].arrSystematic = new TObjArray();
        set[i].arrSystematic->SetName(
            TString::Format("arr_%s_Systematic", ananames[i].c_str()).Data());
    }

    TObjArray* arrsl = new TObjArray();
    arrsl->SetName("arr_All_Systematic");

    for (uint i = 0; i < ananum; ++i)
    {
        printf("+=====================================================================+\n");

        double avg_percent_ae = 0;

        // number of bins
        int xbins = set[i].h_exp->GetNbinsX();
        int ybins = set[i].h_exp->GetNbinsY();

        // xaxis
        Double_t* axis_x = new Double_t[xbins];
        Double_t* axis_x_l = new Double_t[xbins];
        Double_t* axis_x_h = new Double_t[ybins];

        TAxis* xaxis = set[i].h_exp->GetXaxis();

        for (int k = 0; k < xbins; ++k)
        {
            Double_t center = xaxis->GetBinCenter(k + 1);
            Double_t lowedge = xaxis->GetBinLowEdge(k + 1);
            Double_t highedge = xaxis->GetBinUpEdge(k + 1);

            axis_x[k] = center;
            axis_x_l[k] = center - lowedge;
            axis_x_h[k] = highedge - center;
        }

        // yaxis
        Double_t* ax = new Double_t[ybins + 1];
        Double_t* aexl = new Double_t[ybins];
        Double_t* aexh = new Double_t[ybins];

        TAxis* yaxis = set[i].h_exp->GetYaxis();

        for (int k = 0; k < ybins; ++k)
        {
            Double_t center = yaxis->GetBinCenter(k + 1);
            Double_t lowedge = yaxis->GetBinLowEdge(k + 1);
            Double_t highedge = yaxis->GetBinUpEdge(k + 1);

            ax[k] = center;
            aexl[k] = center - lowedge;
            aexh[k] = highedge - center;
        }

        double sys1_l[2] = {0.0, 0.0};
        double sys1_h[2] = {0.0, 0.0};

        double sys2_l = 0.0;
        double sys2_h = 0.0;

        double sys3_l = 0.0;
        double sys3_h = 0.0;

        double total_ay = 0.0;

        set[i].grSystematic = new TGraphAsymmErrors*[xbins];

        int valid_bins_all = 0;

        int valid_bins_x = 0;

        // do not delete this arrays, TODO verify this... one day...
        Double_t* xarray_bin = new Double_t[xbins];
        Double_t* xarray_val = new Double_t[xbins];
        Double_t* xarray_err_l = new Double_t[xbins];
        Double_t* xarray_err_h = new Double_t[xbins];

        for (int j = 0; j < xbins; ++j)
        {
            // errors for slice
            double xbin_sys1_l[2] = {0.0, 0.0};
            double xbin_sys1_h[2] = {0.0, 0.0};

            double xbin_sys2_l = 0.0;
            double xbin_sys2_h = 0.0;

            double xbin_sys3_l = 0.0;
            double xbin_sys3_h = 0.0;

            double xbin_cs = 0.0;

            // do not delete this arrays, TODO verify this... one day...
            Double_t* yarray_bin = new Double_t[ybins];
            Double_t* yarray_val = new Double_t[ybins];
            Double_t* yarray_err_l = new Double_t[ybins];
            Double_t* yarray_err_h = new Double_t[ybins];

            printf("%s bin %d\n", ananames[i].c_str(), j);

            int valid_bins_y = 0;

            for (int k = 0; k < ybins; ++k)
            {
                double ybin_sys1_l[2] = {0.0};
                double ybin_sys1_h[2] = {0.0};

                double ybin_sys2_l = 0.0;
                double ybin_sys2_h = 0.0;

                double ybin_sys3_l = 0.0;
                double ybin_sys3_h = 0.0;

                Double_t cutvar_h[2], cutvar_l[2];

                Double_t cs_val = set[i].h_exp->GetBinContent(j + 1, k + 1);

                for (uint l = 0; l < syst_lopps; ++l)
                {
                    Double_t cs_val_l = set[i].hS_lower[l]->GetBinContent(j + 1, k + 1);
                    Double_t cs_val_h = set[i].hS_higher[l]->GetBinContent(j + 1, k + 1);

                    cutvar_h[l] = (cs_val_h - cs_val);
                    cutvar_l[l] = (cs_val - cs_val_l);

                    printf("(%d) slice %d given %.4g %.4g %.4g ->  h: %.4g\tl: %.4g", l + 1, k,
                           cs_val_l, cs_val, cs_val_h, cutvar_h[l], cutvar_l[l]);

                    // Cuts variation
                    if (cutvar_h[l] > 0. and cutvar_l[l] > 0.)
                    {
                        printf("  \t choosen #0 h: %f   l: %f\n", cutvar_h[l], cutvar_l[l]);
                    }
                    else if (cutvar_h[l] > 0. and cutvar_l[l] < 0.)
                    {
                        cutvar_h[l] = cutvar_h[l] > -cutvar_l[l] ? cutvar_h[l] : -cutvar_l[l];
                        cutvar_l[l] = 0.;
                        printf("  \t choosen #1 h: %f   l: %f\n", cutvar_h[l], cutvar_l[l]);
                    }
                    else if (cutvar_h[l] < 0. and cutvar_l[l] > 0.)
                    {
                        cutvar_l[l] = cutvar_l[l] > -cutvar_h[l] ? cutvar_l[l] : -cutvar_h[l];
                        cutvar_h[l] = 0.;
                        printf("  \t choosen #2 h: %f   l: %f\n", cutvar_h[l], cutvar_l[l]);
                    }
                    else if (cutvar_h[l] < 0. and cutvar_l[l] < 0.)
                    {
                        Double_t tmp = cutvar_l[l];
                        cutvar_l[l] = -cutvar_h[l];
                        cutvar_h[l] = -tmp;
                        printf("  \t choosen #3 h: %f   l: %f\n", cutvar_h[l], cutvar_l[l]);
                    }
                    else
                    {
                        printf("\n");
                        continue;
                    }

                    // systematic three sources
                    ybin_sys1_h[l] = cutvar_h[l] * cutvar_h[l]; // cuts variation
                    ybin_sys1_l[l] = cutvar_l[l] * cutvar_l[l];
                }

                // AE RMS
                double ae_offset = 0.0;
                if (hasAe)
                {
                    double aeref = set[i].hS_ae->GetBinContent(j + 1, k + 1);
                    double aesig = set[i].hS_aesig->GetBinContent(j + 1, k + 1);
                    if (aeref and aesig)
                    {
                        ae_offset = aesig / aeref; // 0.07 == 7% of normalization
                        printf("      ae_sig= %g/%g = %g %%\n", aesig, aeref, aesig / aeref * 100.);
                        avg_percent_ae += aesig / aeref; // FIXME wtf is thti
                    }
                    else if (aeref or aesig)
                    {
                        ae_offset = aesig / aeref; // 0.07 == 7% of normalization
                        printf(" !!!   ae_sig= %g/%g = %g %%\n", aesig, aeref,
                               aesig / aeref * 100.);
                        avg_percent_ae += aesig / aeref; // FIXME wtf is thti
                    }
                }

                double cs_val2 = cs_val * cs_val;

                // Normalization
                // 				double norm_syst = 0.07;

                ybin_sys2_h = ae_offset * ae_offset * cs_val2; // AE RMS
                ybin_sys2_l = ae_offset * ae_offset * cs_val2;

                ybin_sys3_h = 0; // norm_syst*norm_syst * cs_val2;		// Normalization
                ybin_sys3_l = 0; // norm_syst*norm_syst * cs_val2;

                for (uint l = 0; l < syst_lopps; ++l)
                {
                    xbin_sys1_l[l] += ybin_sys1_l[l];
                    xbin_sys1_h[l] += ybin_sys1_h[l];

                    sys1_l[l] += ybin_sys1_l[l];
                    sys1_h[l] += ybin_sys1_h[l];
                }
                xbin_sys2_l += ybin_sys2_l;
                xbin_sys2_h += ybin_sys2_h;
                xbin_sys3_l += ybin_sys3_l;
                xbin_sys3_h += ybin_sys3_h;

                sys2_l += ybin_sys2_l;
                sys2_h += ybin_sys2_h;
                sys3_l += ybin_sys3_l;
                sys3_h += ybin_sys3_h;

                // bin value
                double ybin_cs = cs_val;
                double ybin_value_l = 0.;
                double ybin_value_h = 0.;

                for (uint l = 0; l < syst_lopps; ++l)
                {
                    ybin_value_l += ybin_sys1_l[l];
                    ybin_value_h += ybin_sys1_h[l];
                }
                ybin_value_l += ybin_sys2_l + ybin_sys3_l;
                ybin_value_h += ybin_sys2_h + ybin_sys3_h;

                // diff errors
                yarray_bin[valid_bins_y] = ax[k];
                yarray_val[valid_bins_y] = ybin_cs;
                yarray_err_l[valid_bins_y] = sqrt(ybin_value_l);
                yarray_err_h[valid_bins_y] = sqrt(ybin_value_h);

                xbin_cs += ybin_cs;

                total_ay += cs_val2;

                ++valid_bins_y;
                ++valid_bins_all;
            }
            set[i].grSystematic[j] =
                new TGraphAsymmErrors(/*ybins*/ valid_bins_y, yarray_bin, yarray_val, aexl, aexh,
                                      yarray_err_l, yarray_err_h);
            set[i].grSystematic[j]->SetName(
                TString::Format("%s_asymmerr_bin%02d", set[i].h_exp->GetName(), j));
            set[i].grSystematic[j]->SetFillColor(2);
            set[i].grSystematic[j]->SetFillStyle(3001);
            set[i].grSystematic[j]->Draw("a2");
            set[i].grSystematic[j]->Draw("p");
            set[i].arrSystematic->AddLast(set[i].grSystematic[j]);

            // slice value
            double xbin_value_l = xbin_sys1_l[0] + xbin_sys1_l[1] + xbin_sys2_l + xbin_sys3_l;
            double xbin_value_h = xbin_sys1_h[0] + xbin_sys1_h[1] + xbin_sys2_h + xbin_sys3_h;

            double bin_width = set[i].h_exp->GetYaxis()->GetBinWidth(1);
            double bw2 = bin_width; // * bin_width;

            // slice errors
            xarray_bin[valid_bins_x] = set[i].h_exp->GetXaxis()->GetBinCenter(j + 1);
            xarray_val[valid_bins_x] = xbin_cs * bw2;
            xarray_err_l[valid_bins_x] = sqrt(xbin_value_l) * bw2;
            xarray_err_h[valid_bins_x] = sqrt(xbin_value_h) * bw2;

            ++valid_bins_x;

            delete[] yarray_bin;
            delete[] yarray_val;
            delete[] yarray_err_l;
            delete[] yarray_err_h;
        }

        set[i].grSystematicSlice = new TGraphAsymmErrors(
            valid_bins_x, xarray_bin, xarray_val, axis_x_l, axis_x_h, xarray_err_l, xarray_err_h);
        set[i].grSystematicSlice->SetName(
            TString::Format("%s_asymmerrslice_bin%02d", set[i].h_exp->GetName(), i));
        set[i].grSystematicSlice->SetFillColor(2);
        set[i].grSystematicSlice->SetFillStyle(3001);
        set[i].grSystematicSlice->Draw("a2");
        set[i].grSystematicSlice->Draw("p");
        arrsl->AddLast(set[i].grSystematicSlice);

        double s1h = sys1_h[0] + sys1_h[1];
        double s1l = sys1_l[0] + sys1_l[1];

        printf("Systematic errors (1): +%g -%g  |  +%g -%g\n", sys1_h[0], sys1_l[0],
               sqrt(sys1_h[0]), sqrt(sys1_l[0]));
        printf("Systematic errors (2): +%g -%g  |  +%g -%g\n", sys1_h[1], sys1_l[1],
               sqrt(sys1_h[1]), sqrt(sys1_l[1]));
        printf("Systematic errors (+): +%g -%g  |  +%g -%g\n", s1h, s1l, sqrt(s1h), sqrt(s1l));
        printf(" offset in percents: %g\n", avg_percent_ae / valid_bins_all * 100);

        printf("Summary     (h,l): sys1.1: %g %g, sys1.2: %g %g, sys1: %g %g, sys2: %g %g, sys3: "
               "%g %g\n",
               sys1_h[0], sys1_l[0], sys1_h[1], sys1_l[1], s1h, s1l, sys2_h, sys2_l, sys3_h,
               sys3_l);

        printf("Summary sqrt(h,l): sys1.1: %g %g, sys1.2: %g %g, sys1: %g %g, sys2: %g %g, sys3: "
               "%g %g\n",
               sqrt(sys1_h[0]), sqrt(sys1_l[0]), sqrt(sys1_h[1]), sqrt(sys1_l[1]), sqrt(s1h),
               sqrt(s1l), sqrt(sys2_h), sqrt(sys2_l), sqrt(sys3_h), sqrt(sys3_l));

        printf("Summary norm(h,l): sys1.1: %g %g, sys1.2: %g %g, sys1: %g %g, sys2: %g %g, sys3: "
               "%g %g\n",
               sqrt(sys1_h[0] / total_ay), sqrt(sys1_l[0] / total_ay), sqrt(sys1_h[1] / total_ay),
               sqrt(sys1_l[1] / total_ay), sqrt(s1h / total_ay), sqrt(s1l / total_ay),
               sqrt(sys2_h / total_ay), sqrt(sys2_l / total_ay), sqrt(sys3_h / total_ay),
               sqrt(sys3_l / total_ay));

        printf("Summary perc(h,l): sys1.1: %g %g, sys1.2: %g %g, sys1: %g %g, sys2: %g %g, sys3: "
               "%g %g\n",
               sqrt(sys1_h[0] / total_ay) * 100, sqrt(sys1_l[0] / total_ay) * 100,
               sqrt(sys1_h[1] / total_ay) * 100, sqrt(sys1_l[1] / total_ay) * 100,
               sqrt(s1h / total_ay) * 100, sqrt(s1l / total_ay) * 100,
               sqrt(sys2_h / total_ay) * 100, sqrt(sys2_l / total_ay) * 100,
               sqrt(sys3_h / total_ay) * 100, sqrt(sys3_l / total_ay) * 100);

        printf("Summary total(h,l): sys: %g %g\n", sqrt((s1h + sys2_h + sys3_h) / total_ay) * 100,
               sqrt((s1l + sys2_l + sys3_l) / total_ay) * 100);

        set[i].arrSystematic->Write(set[i].arrSystematic->GetName(), TObject::kSingleKey);

        delete[] xarray_bin;
        delete[] xarray_val;
        delete[] xarray_err_l;
        delete[] xarray_err_h;
    }
    arrsl->Write(arrsl->GetName(), TObject::kSingleKey);
}

void SystematicErrors::AnaFinalize()
{

    CloseExportFile();

    if (inputfile) inputfile->Close();
}

void SystematicErrors::PrintAnalysisInfo()
{
    std::cout << "\n=======================================================\n\n";
    std::cout << "\n=======================================================\n\n";
}

int SystematicErrors::config_params(struct option*& opts)
{
    static struct option lopts[] = {};
    opts = lopts;

    return sizeof(lopts) / sizeof(option);
}

void SystematicErrors::config_execute(int code, char* optarg)
{
    switch (code)
    {
        default:
            cfgfile = optarg;
            return;
            break;
    }

    // 	if (code) return;
}
