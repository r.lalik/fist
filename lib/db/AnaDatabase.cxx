#include <exception>
#include <string>

#include "AnaDatabase.h"

#include <TObjArray.h>
#include <TObjString.h>
#include <TString.h>

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

auto DatabaseRecord::sumErrorChainSquared(const std::vector<RT::ErrorsPair>& errors)
    -> RT::ErrorsPair
{
    RT::ErrorsPair ep;
    ep.high = 0;
    ep.low = 0;

    size_t size = errors.size();
    for (size_t i = 0; i < size; ++i)
    {
        ep.high += errors[i].high * errors[i].high;
        ep.low += errors[i].low * errors[i].low;
    }

    return ep;
}

auto DatabaseRecord::sumErrorChain(const std::vector<RT::ErrorsPair>& errors) -> RT::ErrorsPair
{
    auto ep = sumErrorChainSquared(errors);

    ep.high = sqrt(ep.high);
    ep.low = sqrt(ep.low);

    return ep;
}

DatabaseRecord AnaDatabase::getRecord(const std::string& key) const
{
    DbMap::const_iterator it = dbmap.find(key);
    if (it == dbmap.end())
    {
        std::cerr << "No " << key << " for " << __func__ << std::endl;
        abort();
    }
    return it->second;
}

std::string AnaDatabase::applyPlaceholders(const std::string& str)
{
    TString _str = str;

    for (uint i = 0; i < placeholders.size(); ++i)
    {
        _str.ReplaceAll(placeholders[i].first, placeholders[i].second);
    }

    std::string __str = _str.Data();
    return __str;
}

std::string AnaDatabase::getLatex(const std::string& key) const
{
    DbMap::const_iterator it = dbmap.find(key);
    if (it == dbmap.end())
    {
        std::cerr << "No " << key << " for " << __func__ << std::endl;
        abort();
    }
    return it->second.latex;
}

double AnaDatabase::getScale(const std::string& key) const
{
    DbMap::const_iterator it = dbmap.find(key);
    if (it == dbmap.end())
    {
        std::cerr << "No " << key << " for " << __func__ << std::endl;
        abort();
    }
    return it->second.scale;
}

double AnaDatabase::getSimEvents(const std::string& key) const
{
    DbMap::const_iterator it = dbmap.find(key);
    if (it == dbmap.end())
    {
        std::cerr << "No " << key << " for " << __func__ << std::endl;
        abort();
    }
    return it->second.simevents;
}

double AnaDatabase::getCS(const std::string& key) const
{
    DbMap::const_iterator it = dbmap.find(key);
    if (it == dbmap.end())
    {
        std::cerr << "No " << key << " for " << __func__ << std::endl;
        abort();
    }
    return it->second.cs;
}

double AnaDatabase::getAng(const std::string& key, int legnum) const
{
    DbMap::const_iterator it = dbmap.find(key);
    if (it == dbmap.end())
    {
        std::cerr << "No " << key << " for " << __func__ << std::endl;
        abort();
    }
    return it->second.ang[legnum];
}

std::string AnaDatabase::getStyle(const std::string& key) const
{
    DbMap::const_iterator it = dbmap.find(key);
    if (it == dbmap.end())
    {
        std::cerr << "No " << key << " for " << __func__ << std::endl;
        abort();
    }
    return it->second.style;
}

std::string AnaDatabase::getLocation(const std::string& key) const
{
    DbMap::const_iterator it = dbmap.find(key);
    if (it == dbmap.end())
    {
        std::cerr << "No " << key << " for " << __func__ << std::endl;
        abort();
    }
    return it->second.location;
}

auto AnaDatabase::getErrors(const std::string& key) -> std::vector<RT::ErrorsPair>
{
    DbMap::const_iterator it = dbmap.find(key);
    if (it == dbmap.end())
    {
        std::cerr << "No " << key << " for " << __func__ << std::endl;
        abort();
    }
    return it->second.getCsErrors();
}

auto AnaDatabase::getAngErrors(const std::string& key, int leg) -> std::vector<RT::ErrorsPair>
{
    DbMap::const_iterator it = dbmap.find(key);
    if (it == dbmap.end())
    {
        std::cerr << "No " << key << " for " << __func__ << std::endl;
        abort();
    }
    return it->second.getAngErrors(leg);
}

DatabaseRecord::DatabaseRecord()
{
    id = 0;
    label.clear();
    location.clear();
    style.clear();
    latex.clear();
    scale = 0;
    cs = 0;
    cs_errors_str.clear();
    size_t n = ang.size();
    for (uint i = 0; i < n; ++i)
    {
        ang[i] = 0;
        ang_errors_str[i].clear();
    }
}

bool AnaDatabase::hasKey(const std::string& key) const
{
    DbMap::const_iterator it = dbmap.find(key);
    return !(it == dbmap.end());
}
