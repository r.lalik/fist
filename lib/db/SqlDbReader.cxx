#include <exception>
#include <string>

#include "SqlDbReader.h"

#include <sqlite3.h>

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

const char* table_channels_new = "DROP TABLE IF EXISTS channels; "
                                 "CREATE TABLE channels ("
                                 " id	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,"
                                 " label	TEXT UNIQUE,"
                                 " location	TEXT,"
                                 " xsec	NUMERIC,"
                                 " xsec_errs	TEXT,"
                                 " a0	NUMERIC,"
                                 " a0_errs	TEXT,"
                                 " a2	NUMERIC,"
                                 " a2_errs	TEXT,"
                                 " a4	NUMERIC,"
                                 " a4_errs	TEXT,"
                                 " latex	TEXT,"
                                 " scale	NUMERIC DEFAULT 1.0,"
                                 " simevents NUMERIC"
                                 ");";

const char* table_insert_query = "INSERT OR REPLACE INTO channels ("
                                 " id, label, location, xsec, xsec_errs,"
                                 " a0, a0_errs, a2, a2_errs, a4, a4_errs,"
                                 " latex, scale, simevents"
                                 " ) VALUES ("
                                 " ?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14"
                                 " );";

const char* table_select_query = "SELECT * FROM 'channels';";

enum channels_fields
{
    CF_ID,
    CF_LABEL,
    CF_LOCATION,
    CF_XSEC,
    CF_XSECERR,
    CF_A0,
    CF_A0ERR,
    CF_A2,
    CF_A2ERR,
    CF_A4,
    CF_A4ERR,
    CF_LATEX,
    CF_SCALE,
    CF_SIMEVENTS
};

DatabaseRecord GetChannelData(int /*argc*/, char** argv, char** /*azColName*/)
{
    DatabaseRecord target;

    target.cs = atof(argv[CF_XSEC]);
    target.cs_errors_str = argv[CF_XSECERR];

    target.ang[0] = atof(argv[CF_A0]);
    target.ang_errors_str[0] = argv[CF_A0ERR];

    target.ang[1] = atof(argv[CF_A2]);
    target.ang_errors_str[1] = argv[CF_A2ERR];

    target.ang[2] = atof(argv[CF_A4]);
    target.ang_errors_str[2] = argv[CF_A4ERR];

    target.scale = atof(argv[CF_SCALE]);
    target.simevents = atof(argv[CF_SIMEVENTS]);

    target.id = atoi(argv[CF_ID]);
    target.label = argv[CF_LABEL];
    target.location = argv[CF_LOCATION];
    target.latex = argv[CF_LATEX];

    return target;
}

static int ChannelCallback(void* c, int argc, char** argv, char** azColName)
{
    DatabaseRecord dbrec = GetChannelData(argc, argv, azColName);
    SqlDbReader* cust = reinterpret_cast<SqlDbReader*>(c);
    cust->pushRecord(dbrec);

    return 0;
}

void SqlDbReader::prepare()
{
    sqlite3* db;
    int rc = sqlite3_open(dbfname.c_str(), &db);

    if (rc)
    {
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        return;
    }

    char* zErrMsg;

    rc = sqlite3_exec(db, table_select_query, ChannelCallback, this, &zErrMsg);

    if (rc != SQLITE_OK)
    {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }

    sqlite3_close(db);
}

void SqlDbReader::pushRecord(const DatabaseRecord& dbrec) { dbmap[dbrec.label] = dbrec; }

// static void profile(void */*context*/, const char *sql, sqlite3_uint64 ns)
// {
// 	fprintf(stderr, "Query: %s\n", sql);
// 	fprintf(stderr, "Execution Time: %llu ms\n", ns / 1000000);
// }

void SqlDbReader::dump(const char* filename)
{
    char fn[1024];
    if (!filename) { strcpy(fn, "fit.db"); }
    else
    {
        sprintf(fn, "%s", filename);
    }
    sqlite3* db;
    sqlite3_stmt* stmt;
    int rc = sqlite3_open(fn, &db);

    DatabaseRecord tmp_rec;

    if (rc)
    {
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        return;
    }

    char* zErrMsg;
    // 	sqlite3_profile(db, &profile, NULL);

    rc = sqlite3_exec(db, table_channels_new, 0, this, &zErrMsg);

    if (rc != SQLITE_OK)
    {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }

    rc = sqlite3_prepare_v2(db, table_insert_query, -1, &stmt, 0);

    DbMap::iterator it = dbmap.begin();
    for (; it != dbmap.end(); ++it)
    {
        tmp_rec = it->second;

        sqlite3_bind_int(stmt, 1, tmp_rec.id);
        sqlite3_bind_text(stmt, 2, tmp_rec.label.c_str(), -1, SQLITE_STATIC);
        sqlite3_bind_text(stmt, 3, tmp_rec.location.c_str(), -1, SQLITE_STATIC);
        sqlite3_bind_double(stmt, 4, tmp_rec.cs);
        sqlite3_bind_text(stmt, 5, tmp_rec.cs_errors_str.c_str(), -1, SQLITE_STATIC);
        sqlite3_bind_double(stmt, 6, tmp_rec.ang[0]);
        sqlite3_bind_text(stmt, 7, tmp_rec.ang_errors_str[0].c_str(), -1, SQLITE_STATIC);
        sqlite3_bind_double(stmt, 8, tmp_rec.ang[1]);
        sqlite3_bind_text(stmt, 9, tmp_rec.ang_errors_str[1].c_str(), -1, SQLITE_STATIC);
        sqlite3_bind_double(stmt, 10, tmp_rec.ang[2]);
        sqlite3_bind_text(stmt, 11, tmp_rec.ang_errors_str[2].c_str(), -1, SQLITE_STATIC);
        sqlite3_bind_text(stmt, 12, tmp_rec.latex.c_str(), -1, SQLITE_STATIC);
        sqlite3_bind_double(stmt, 13, tmp_rec.scale);
        sqlite3_bind_double(stmt, 14, tmp_rec.simevents);

        int local_rc = sqlite3_step(stmt);
        if (local_rc != SQLITE_DONE)
        {
            printf("\nCould not step (execute) stmt: %s\n %s\n", sqlite3_errmsg(db),
                   table_insert_query);
            abort();
        }

        sqlite3_reset(stmt);
    }

    sqlite3_finalize(stmt);
    sqlite3_close(db);
}