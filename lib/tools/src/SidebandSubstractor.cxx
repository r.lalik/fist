/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "SidebandSubstractor.h"

#ifndef __CINT__

#include "getopt.h"
#include <fstream>
#include <string>

#include "TCanvas.h"
#include "TChain.h"
#include "TDirectory.h"
#include "TError.h"
#include "TF1.h"
#include "TFile.h"
#include "TGaxis.h"
#include "TGraphAsymmErrors.h"
#include "TH2.h"
#include "TImage.h"
#include "TKey.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TMath.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TVector.h"
#include "TVirtualFitter.h"

#endif /* __CINT__ */

#include "RootTools.h"

#include "DifferentialFactory.h"

#include "globals.h"

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

const Option_t h1opts[] = "h,E1";

SidebandSubstractor::SidebandSubstractor() : SimpleToolsAbstract()
{
    NicePalette();
    TGaxis::SetMaxDigits(3);
}

SidebandSubstractor::~SidebandSubstractor() {}

void SidebandSubstractor::AnaInitialize()
{
    if (cfgfile.IsNull())
    {
        std::cout << "No config file!" << std::endl;
        std::exit(EXIT_FAILURE);
    }

    std::ifstream fcfgfile(cfgfile);

    hasExp = hasLower = hasHigher = kFALSE;

    std::string line;
    while (std::getline(fcfgfile, line))
    {
        TString line_(line);
        line_.ReplaceAll("\t", " ");

        TObjArray* arr = line_.Tokenize(" ");

        char cmdchar = ((TObjString*)arr->At(0))->String()[0];
        switch (cmdchar)
        {
            case 'h':
                ifile[2] = ((TObjString*)arr->At(1))->String();
                hasHigher = kTRUE;
                break;
            case 'e':
                ifile[0] = ((TObjString*)arr->At(1))->String();
                hasExp = kTRUE;
                break;
            case 'l':
                ifile[1] = ((TObjString*)arr->At(1))->String();
                hasLower = kTRUE;
                break;
            case 'o':
                outputfile = ((TObjString*)arr->At(1))->String();
                break;
        }
    }

    if (!(hasExp and hasLower and hasHigher))
    {
        std::cout << "Config file is not complete, check for exp, lower and higher data!"
                  << std::endl;
        std::exit(EXIT_FAILURE);
    }

    SetOutputFile(outputfile);

    SetExportDir("./");

    // 	OpenExportFile();

    for (int j = 0; j < 3; ++j)
    {
        inputfile = TFile::Open(ifile[j], "READ");
        if (!inputfile->IsOpen())
        {
            std::cerr << "File " << ifile[j] << " is not open!";
            std::exit(EXIT_FAILURE);
        }

        // 	inputfile->ls();
        getConfigVector();
        getAnaNames();

        fac[j] = new DifferentialFactory*[ananum];

        for (uint i = 0; i < ananum; ++i)
        {
            // 			PR(j);PR(i);

            std::string tmpname = ananames[i];
            tmpname.append("Ctx");
            DifferentialContext* dactx = (DifferentialContext*)inputfile->Get(tmpname.c_str());

            if (j == 0)
            {
                DifferentialContext* dactx_res = new DifferentialContext;
                *dactx_res = *dactx;
                dactx_res->hist_name += "_res";
                fac[3][i] = new DifferentialFactory(dactx_res);
                fac[3][i]->init();
            }

            fac[j][i] = new DifferentialFactory(dactx);
            fac[j][i]->setSource(inputfile);
            fac[j][i]->init();

            fac[j][i]->RegObject("@Ctx");

            // temporray name change
            char aaa[100];
            sprintf(aaa, "_%d", j);
            dactx->hist_name += aaa;
            // 			fac[j][i]->rename(dactx->hist_name.Data());
            fac[j][i]->reinit();
        }
    }
}

void SidebandSubstractor::AnaExecute()
{
    PrintAnalysisInfo();

    for (uint i = 0; i < ananum; ++i)
    {
        PR(i);
        PR(fac[3][i]->hSignalCounter->Integral());
        *fac[3][i] += *fac[0][i];
        PR(fac[3][i]->hSignalCounter->Integral());
        *fac[3][i] -= *fac[1][i];
        PR(fac[3][i]->hSignalCounter->Integral());
        *fac[3][i] -= *fac[2][i];
        PR(fac[3][i]->hSignalCounter->Integral());
    }
}

void SidebandSubstractor::AnaFinalize()
{

    OpenExportFile();
    GetExportFile()->cd();
    GetExportFile()->ls();

    for (uint i = 0; i < ananum; ++i)
    {
        PR(i);
        // 		fac[0][i]->prepareDiffCanvas();PR(1);
        // 		fac[0][i]->ctx.Print();PR(2);
        // 		fac[0][i]->ctx.Write();PR(3);

        // 			((Dim2AnalysisFactory*)(entries[i]->anas[j]))->prepareCanvas();
        // // 			((Dim2AnalysisFactory*)(entries[i]->anas[j]))->prepareDiffCanvas();
        // 			entries[i]->anas[j]->rename(ananames[j]);
        // 			entries[i]->anas[j]->write(f, ver);

        // 		fac[0][i]->write(GetExportFile());
        fac[3][i]->prepareCanvas();
        // 		fac[3][i]->prepareDiffCanvas();
        fac[3][i]->ctx.hist_name = ananames[i].c_str();
        fac[3][i]->reinit();
        fac[3][i]->write(GetExportFile());
        // 		GetExportFile()->ls();
    }
    setConfigVector();

    CloseExportFile();

    // 	if (secvertfile)
    // 		secvertfile->Close();

    if (inputfile) inputfile->Close();
}

void SidebandSubstractor::PrintAnalysisInfo()
{
    std::cout << "\n=======================================================\n\n";
    std::cout << "\n=======================================================\n\n";
}

int SidebandSubstractor::config_params(struct option*& opts)
{
    static struct option lopts[] = {};
    opts = lopts;

    return sizeof(lopts) / sizeof(option);
}

void SidebandSubstractor::config_execute(int code, char* optarg)
{
    switch (code)
    {
        default:
            cfgfile = optarg;
            return;
            break;
    }

    // 	if (code) return;
}
