/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPECIALISTS_H
#define SPECIALISTS_H

#ifndef __CINT__

#include "map"

class TH1F;
class TCanvas;
class TChain;
class TF1;
class TFile;
class TH1;
class TH1F;
class TH1I;
class TH2;
class TH2F;
class TStyle;

class SmartFactory;

#endif /* __CINT__ */

#include "AnaTools.h"
#include "SimpleToolsAbstract.h"
#include "globals.h"

#include <FitterFactory.h>

typedef std::vector<std::string> str_vec;
typedef std::vector<int> int_vec;
typedef std::vector<TFile*> tfile_vec;
typedef std::vector<bool> bool_vec;

struct CDFInputData
{
    std::vector<char> groupID;
    std::vector<std::string> groupName;
    std::vector<int> groupSplit;
    std::vector<std::string> groupAttr;
    std::vector<str_vec> entryID;
    std::vector<tfile_vec> entryFile;
    std::vector<str_vec> entryAttr;
    std::vector<bool_vec> entryCorr;

    std::string reffile;
    std::string corfile;
    std::string refAttr;
    std::string sumAttr;
};

class Specialists : public SimpleToolsAbstract
{
private:
public:
    Specialists();
    virtual ~Specialists();

    void AnaInitialize();
    void AnaExecute();
    void AnaFinalize();

    int config_params(struct option*& opts);
    void config_execute(int code, char* optarg);

private:
    void PrepareArgsList();
    void FetchFitInfo(TF1* fun, float& mean, float& width, float& sig, float& bkg,
                      TPad* pad = nullptr);

    void CreateCanvases(TFile* source);

public:
private:
    TString configfile_t;
    TString optfile;

    int flag_mirror;
    int flag_cs;
    int flag_alt_odd;
    int flag_alt_even;
    int flag_fit_and_fill;

    float par_max_scale;
    float par_min_scale;

    struct SpecSet
    {
        float cfg_miny;
        float cfg_maxy;
        SmartFactory* fa;
    } * set;

    CDFInputData* cdfdata;

    TString syserrfile_t;
    TObjArray* arrSystematic;
};

#endif // SPECIALISTS_H
