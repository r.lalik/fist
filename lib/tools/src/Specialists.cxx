/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "anaconfig.h"

#include <algorithm>
#include <fstream>
#include <getopt.h>
#include <string>
#include <sys/stat.h>

#include "TCanvas.h"
#include "TDirectory.h"
#include "TError.h"
#include "TF1.h"
#include "TFile.h"
#include "TGaxis.h"
#include "TGraph.h"
#include "TGraphAsymmErrors.h"
#include "TGraphErrors.h"
#include "TH2.h"
#include "TImage.h"
#include "TKey.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TLegendEntry.h"
#include "TMath.h"
#include "TObjString.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TSystem.h"
#include "TVector.h"
#include "TVirtualFitter.h"

#include "Math/IFunction.h"

#include "RootTools.h"

#include "DistributionContext.h"
#include "DistributionFactory.h"

#include "Specialists.h"

#include <FitterFactory.h>
#include <SmartFactory.h>

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

// Processing data bar width
const Int_t bar_dotqty = 10000;
const Int_t bar_width = 20;

const Option_t h1opts[] = "h,E1";

RootTools::PaintFormat pf;

int marker_pair[35] = {0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15, 16, 17,
                       18, 19, 24, 25, 26, 32, 20, 21, 22, 33, 34, 30, 29, 31, 23, 27, 28};

const size_t colors_num = 10;
int colors[colors_num] = {40, 41, 46, 38, 30, 42, 28, 44, 20, 39};
const size_t markers_num = 7;
int markers[markers_num] = {20, 21, 22, 23, 29, 33, 34};

size_t local_ananum = 2;
TObjArray* g_arrSystematic = nullptr;
int g_a = 0;

void init_pf()
{
    pf.pf.marginLeft = 0.15;
    pf.gf.y.to = 1.15;
    pf.gf.y.center_label = true;
    pf.gf.y.optimize = true;
}

int g_flag_alt_odd = 0;
int g_flag_alt_even = 0;
int g_flag_fit_and_fill = 0;

TString format_hist_zaxis(const DistributionContext& ctx)
{
    TString htitle = TString::Format("%s%s", ctx.z.label.Data(), ctx.z.format_unit().Data());

    return htitle;
}

void draw_systematics(TGraphAsymmErrors* sysgraph, TVirtualPad* pad, TH1D* cshist)
{
    if (sysgraph)
    {
        sysgraph->Print();
        sysgraph->SetFillColor(17); // 7
        sysgraph->SetFillStyle(1001);

        double* xxx = sysgraph->GetX();
        double* yyy = sysgraph->GetY();

        int points = sysgraph->GetN();
        for (int i = 0; i < points; ++i)
        {
            PR(xxx[i]);
            int pos = cshist->FindBin(xxx[i]);
            PR(pos);
            yyy[i] = cshist->GetBinContent(pos);
        }

        pad->cd();
        sysgraph->Draw("5");
        pad->cd();
        sysgraph->Draw("5");
    }
}

float DrawCSDifferential(TVirtualPad* c, TH2D* hist_data, DistributionContext* ctx, int uid)
{
    size_t binslen = hist_data->GetXaxis()->GetNbins();

    pf.gf.x.Ndiv = binslen * 100 + 2;

    RootTools::NicePad(c, pf.pf);
    RootTools::NiceHistogram(hist_data, pf.gf);

    c->cd();
    hist_data->Draw("colz,text");

    TString hlabel;
    hlabel = TString::Format("%s%s", ctx->x.label.Data(), ctx->x.format_unit().Data());
    hist_data->GetXaxis()->SetTitle(hlabel.Data());
    hlabel = TString::Format("%s%s", ctx->y.label.Data(), ctx->y.format_unit().Data());
    hist_data->GetYaxis()->SetTitle(hlabel.Data());

    const DistributionContext ct = (*ctx);
    TString htitlez = format_hist_zaxis(ct);
    // 	hlabel = TString::Format("%s%s", ctx->y.label.Data(), ctx->y.format_unit().c_str());
    hist_data->GetZaxis()->SetTitle(htitlez);

    return uid;
}

float DrawCSProjections(TVirtualPad* c, TH2D* hist_data, DistributionContext* ctx, int uid,
                        TLegend* leg = 0)
{
    double min = -0.01;
    double max = 0.;

    // 	RootTools::FindRangeExtremum(min, max, hist_data);
    RootTools::FindBoundaries(hist_data, min, max);

    size_t binslen = hist_data->GetXaxis()->GetNbins();
    size_t binsnum = hist_data->GetYaxis()->GetNbins();

    double* x = new double[binslen];  // x-vlaue
    double* y = new double[binslen];  // y value
    double* ex = new double[binslen]; // error-x
    double* ey = new double[binslen]; // error-y

    if (uid < 1) uid = rand() % 1000 + 1;

    TString str = TString::Format("%s_%d", hist_data->GetName(), uid);

    TH1D* hist_draft = hist_data->ProjectionX(
        TString::Format("%s_proj_%d", hist_data->GetName(), uid).Data(), 0, binsnum);
    hist_draft->Sumw2(kFALSE);
    hist_draft->Reset();

    pf.gf.x.Ndiv = binslen * 100 + 2;
    RootTools::NicePad(c, pf.pf);
    RootTools::NiceHistogram(hist_draft, pf.gf);

    for (size_t i = 0; i < binsnum; ++i)
    {
        int grs = 0;

        for (uint k = 0; k < binslen; ++k)
        {
            double bc = hist_data->GetBinContent(1 + k, 1 + i);

            if (bc)
            {
                double bcen = hist_data->GetXaxis()->GetBinCenter(1 + k);
                double bcerr = hist_data->GetBinError(1 + k, 1 + i);

                x[grs] = bcen;
                y[grs] = bc;
                ex[grs] = hist_data->GetXaxis()->GetBinWidth(1 + k);
                ey[grs] = bcerr;

                ++grs;
            }
        }

        TGraphErrors* gr1 = new TGraphErrors(grs, x, y, 0, ey);
        // 		TGraphErrors * gr2 = new TGraphErrors(grs, m, y, 0, ey);

        char legentry[100];
        sprintf(legentry, "%g < %s < %g", hist_data->GetYaxis()->GetBinLowEdge(1 + i),
                ctx->y.label.Data(), hist_data->GetYaxis()->GetBinUpEdge(1 + i));

        if (leg and
            ((g_flag_alt_odd == 0 and g_flag_alt_even == 0) or
             ((g_flag_alt_odd == 1) and (i % 2 == 1)) or ((g_flag_alt_even == 1) and (i % 2 == 0))))
        {
            leg->AddEntry(gr1, legentry, "lp");
        }

        gr1->SetLineColor(colors[i % colors_num]);
        gr1->SetLineWidth(1);
        gr1->SetLineStyle(1);
        gr1->SetMarkerColor(colors[i % colors_num]);
        gr1->SetMarkerStyle(markers[i % markers_num]);
        gr1->SetTitle("");

        if (i == 0)
        {
            char label[100];
            sprintf(label, "%s", ctx->x.label.Data());
            hist_draft->GetXaxis()->SetTitle(label);
            TString hist_title = TString::Format(
                "d^{2}#sigma/d%sd%s [#mub/%g%s/%g%s]", ctx->x.label.Data(), ctx->y.label.Data(),
                hist_data->GetYaxis()->GetBinWidth(1), ctx->y.unit.Data(),
                hist_data->GetXaxis()->GetBinWidth(1), ctx->x.unit.Data());
            hist_draft->GetYaxis()->SetTitle(hist_title.Data());

            hist_draft->Clone()->Draw("B");
        }

        if (grs)
        {
            if ((g_flag_alt_odd == 0 and g_flag_alt_even == 0) or
                ((g_flag_alt_odd == 1) and (i % 2 == 1)) or
                ((g_flag_alt_even == 1) and (i % 2 == 0)))
            {
                gr1->Draw("L P E2 same");
            }
            // 			gr2->Draw("L P E2 same");
        }
    }

    TList* l = c->GetListOfPrimitives();
    if (l->GetSize() and l->At(0)->InheritsFrom("TH1D"))
    {
        TH1D* h0 = (TH1D*)l->At(0);
        double dmm = (max - min) * 0.05;
        h0->GetYaxis()->SetRangeUser(min - dmm, max + dmm);
    }

    if (leg)
    {
        char header[200];
        sprintf(header, "#sigma(%s;%s):", ctx->x.label.Data(), ctx->y.label.Data());
        leg->SetHeader(header);
    }

    return uid;
}

TH1D* DrawCSIntegrals(TVirtualPad* c, TH2D* hist_data, DistributionContext* ctx, int& uid,
                      TLegend* leg = 0, bool do_fit = false)
{
    size_t binslen = hist_data->GetXaxis()->GetNbins();
    size_t binsnum = hist_data->GetYaxis()->GetNbins();

    double bin_areax = hist_data->GetXaxis()->GetBinWidth(1);
    double bin_areay = hist_data->GetYaxis()->GetBinWidth(1);
    double bin_area = bin_areax * bin_areay;

    double* x = new double[binslen];  // x-vlaue
    double* y = new double[binslen];  // y value
    double* ex = new double[binslen]; // error-x
    double* ey = new double[binslen]; // error-y

    if (uid < 1) uid = rand() % 1000 + 1;

    TString str = TString::Format("%s_%d", hist_data->GetName(), uid);

    TH1D* hist_draft = hist_data->ProjectionX(
        TString::Format("%s_projx_%d", hist_data->GetName(), uid).Data(), 0, binsnum);

    pf.gf.x.Ndiv = binslen * 100 + 4;
    pf.pf.marginLeft = 0.14;
    pf.pf.marginRight = 0.03;
    pf.gf.y.to -= 0.10;
    RootTools::NicePad(c, pf.pf);
    RootTools::NiceHistogram(hist_draft, pf.gf);

    int grs = 0;

    double cs_total = 0.0;
    double cs_err_total = 0.0;

    for (size_t i = 0; i < binslen; ++i)
    {
        double bcs = 0.0;
        double bcs_err = 0.0;

        for (uint k = 0; k < binsnum; ++k)
        {
            double bc = hist_data->GetBinContent(1 + i, 1 + k);
            double berr = hist_data->GetBinError(1 + i, 1 + k);
            bcs += bc;
            bcs_err += berr * berr;
        }

        cs_total += bcs;
        cs_err_total += bcs_err;

        bcs *= bin_area;
        bcs_err = sqrt(bcs_err) * bin_area;

        x[grs] = hist_data->GetXaxis()->GetBinCenter(1 + i);
        y[grs] = bcs;
        ex[grs] = hist_data->GetXaxis()->GetBinWidth(1 + i);
        ey[grs] = bcs_err;

        ++grs;
    }

    cs_total *= bin_area;

    cs_err_total = sqrt(cs_err_total);
    cs_err_total *= bin_area;

    hist_draft->Scale(bin_areay);

    TGraphErrors* gr1 = new TGraphErrors(grs, x, y, 0, ey);
    // 	TGraphErrors * gr2 = new TGraphErrors(grs, m, y, 0, ey);

    char legentry[100];
    sprintf(legentry, "%g < %s < %g", hist_data->GetYaxis()->GetBinLowEdge(1), ctx->y.label.Data(),
            hist_data->GetYaxis()->GetBinUpEdge(1 + binslen));

    if (leg) { leg->AddEntry(hist_draft, legentry, "lp"); }

    int i = 2;
    gr1->SetLineColor(colors[i % colors_num]);
    gr1->SetLineWidth(1);
    gr1->SetLineStyle(1);
    gr1->SetMarkerColor(colors[i % colors_num]);
    gr1->SetMarkerStyle(markers[i % markers_num]);
    gr1->SetTitle("");

    char label[100];
    sprintf(label, "%s", ctx->x.label.Data());
    hist_draft->GetXaxis()->SetTitle(label);
    TString hist_title = TString::Format("d#sigma/d%s [#mub/%g%s]", ctx->x.label.Data(),
                                         hist_data->GetXaxis()->GetBinWidth(1), ctx->x.unit.Data());
    hist_draft->GetYaxis()->SetTitle(hist_title);

    hist_draft->SetMarkerSize(21);
    hist_draft->SetMarkerSize(3);

    double stats[10];
    hist_draft->GetStats(stats);
    // 	TH1D * hist_draft2 = hist_draft->ProjectionX(TString::Format("%s_projy_%d",
    // hist_data->GetName(), uid).Data() , 0, 2); 	PR(hist_draft2->GetBinContent(1) * bin_area);
    // 	PR(hist_draft2->GetBinError(1) * bin_area);
    for (int i = 0; i < 10; ++i)
    {
        std::cout << " stats[" << i << "] = " << stats[i] << std::endl;
        std::cout << "*stats[" << i << "] = " << stats[i] * bin_area << std::endl;
    }

    gSystem->Load("libMathMore");

    TLatex* latex = new TLatex;
    latex->SetNDC();
    latex->SetTextAlign(23);

    TH1D* clone_draw = (TH1D*)hist_draft->Clone();

    TF1* legendrepoly = nullptr;

    if (do_fit)
    {
        TString fbody;
        if (g_a == 1)
        {
            fbody = "[0] * (ROOT::Math::legendre([3],x) + [1]*ROOT::Math::legendre([4],x) + "
                    "[2]*ROOT::Math::legendre([5],x) )";

            legendrepoly = new TF1("legendrepoly", fbody, -1, 1);
            legendrepoly->FixParameter(3, 0);
            legendrepoly->FixParameter(4, 2);
            legendrepoly->FixParameter(5, 4);

            legendrepoly->SetParameter(0, 100.0);
            legendrepoly->SetParameter(1, 1.7);
            legendrepoly->SetParameter(2, 0.1);

            // 		PR(hist_data->GetXaxis()->GetBinLowEdge(1));
            // 		PR(hist_data->GetXaxis()->GetBinUpEdge(binslen));

            legendrepoly->SetLineStyle(2);
            legendrepoly->SetLineColor(38);

            // 			legendrepoly->SetBit(TF1::kNotDraw);
            hist_draft->Fit(legendrepoly, "", "", hist_data->GetXaxis()->GetBinLowEdge(1),
                            hist_data->GetXaxis()->GetBinUpEdge(binslen));
            // 			legendrepoly->Delete();
        }
        else if (g_a == 0)
        {
            fbody = "[0] * TMath::Exp(-0.5 * ((x-[1])/[2])**2)";

            legendrepoly = new TF1("legendrepoly", fbody, -1, 1);
            legendrepoly->SetParameter(0, 200.0);
            legendrepoly->SetParameter(1, 0);
            legendrepoly->SetParameter(2, 1);
            // 			legendrepoly->SetParameter(3, 0.1);

            // 		PR(hist_data->GetXaxis()->GetBinLowEdge(1));
            // 		PR(hist_data->GetXaxis()->GetBinUpEdge(binslen));

            legendrepoly->SetLineStyle(2);
            legendrepoly->SetLineColor(38);

            // 			legendrepoly->SetBit(TF1::kNotDraw);
            // 			hist_draft->Fit(legendrepoly, "", "",
            // hist_data->GetXaxis()->GetBinLowEdge(1),
            // hist_data->GetXaxis()->GetBinUpEdge(binslen)); 			legendrepoly->Delete();
        }
        hist_draft->Draw("E0");
        if (legendrepoly) legendrepoly->Draw("same");
        // 		else
        // 			hist_draft->Draw("E0");
    }
    else
    {
        // 		TH1D * dummy_draw = (TH1D*)hist_draft->Clone();
        // 		dummy_draw->Draw("");
    }
    // 	hist_draft->Draw("E0");

    if (g_arrSystematic)
    {
        TGraphAsymmErrors* aser = (TGraphAsymmErrors*)g_arrSystematic->At(g_a);
        draw_systematics(aser, gPad, clone_draw);
    }
    // 	clone_draw->Draw("E0");
    // 	hist_draft->Draw("E0");
    // 	hist_draft->SetMarkerStyle(0);
    hist_draft->Draw("E0,same");

    TH1D* hd_clone = (TH1D*)hist_draft->Clone("hd_clone");
    hd_clone->SetMarkerStyle(0);
    hd_clone->SetLineWidth(2);
    hd_clone->Draw("same,E0");

    // 	latex->DrawLatex(0.55, 0.25, TString::Format("#sigma = %.1f #pm %.1f #mub", cs_total,
    // cs_err_total).Data()); 	latex->DrawLatex(0.25, 0.6, TString::Format("#sigma = %g #pm %g
    // #mub", hist_draft->Integral(), cs_err_total).Data());

    if (legendrepoly and g_a == 1)
    {
        // 		legendrepoly->Draw("same");

        latex->DrawLatex(
            0.55, 0.80,
            TString::Format(
                "#sigma(x) = #sigma_{0}/2 [ a_{0}P_{0}(x)+a_{2}P_{2}(x)+a_{4}P_{4}(x) ]")
                .Data());
        // 		latex->SetTextAlign(13);
        latex->DrawLatex(0.55, 0.68,
                         TString::Format("#sigma_{0} = %.1f #pm %0.1f",
                                         2.0 * legendrepoly->GetParameter(0),
                                         2.0 * legendrepoly->GetParError(0))
                             .Data());
        latex->DrawLatex(0.55, 0.63, TString::Format("a_{0} = %.3f #pm %0.3f", 1.0, 0.0));
        latex->DrawLatex(0.55, 0.58,
                         TString::Format("a_{2} = %.3f #pm %0.3f", legendrepoly->GetParameter(1),
                                         legendrepoly->GetParError(1))
                             .Data());
        latex->DrawLatex(0.55, 0.53,
                         TString::Format("a_{4} = %.3f #pm %0.3f", legendrepoly->GetParameter(2),
                                         legendrepoly->GetParError(2))
                             .Data());
    }
    if (legendrepoly and g_a == 0)
    {
        // 		legendrepoly->Draw("same");

        // 		if (hist_draft->GetListOfFunctions()->GetSize() > 0)
        // 			hist_draft->GetListOfFunctions()->At(0)->Delete();
    }

    hist_draft->GetYaxis()->SetRangeUser(0, 250);

    if (grs)
    {
        // 		gr1->Draw("L P E2 same");
        // 		gr2->Draw("L P E2 same");
    }

    if (leg)
    {
        char header[200];
        sprintf(header, "#sigma(%s;%s):", ctx->x.label.Data(), ctx->y.label.Data());
        leg->SetHeader(header);
    }

    printf("Hist integral = %g\n", hist_data->Integral() * hist_data->GetXaxis()->GetBinWidth(1) *
                                       hist_data->GetYaxis()->GetBinWidth(1));

    legendrepoly->Delete();
    return hist_draft;
}

float DrawMirrorProjections(TVirtualPad* c, TH1D* hist_draft, TH2F* hist_data,
                            DistributionContext* ctx, int uid, TLegend* leg = 0)
{
    double min = -0.01;
    double max = 0.;

    RootTools::FindBoundaries(hist_data, min, max);

    size_t binslen = hist_draft->GetXaxis()->GetNbins();
    size_t binsnum = hist_data->GetYaxis()->GetNbins();

    double* x = new double[binslen];
    double* m = new double[binslen];
    double* y = new double[binslen];
    double* ex = new double[binslen];
    double* ey = new double[binslen];

    if (uid < 1) uid = rand() % 1000 + 1;

    TString str = TString::Format("%s_%d", hist_data->GetName(), uid);

    for (size_t i = 0; i < binsnum; ++i)
    {
        pf.gf.x.Ndiv = binslen * 100 + 2;

        RootTools::NicePad(c, pf.pf);
        RootTools::NiceHistogram(hist_draft, pf.gf);

        int grs = 0;
        for (uint k = 0; k < binslen; ++k)
        {
            double bc = hist_data->GetBinContent(1 + k, 1 + i);
            if (bc)
            {
                double bcen = hist_data->GetXaxis()->GetBinCenter(1 + k);
                x[grs] = bcen;
                m[grs] = -bcen;
                y[grs] = bc;
                ex[grs] = hist_data->GetXaxis()->GetBinWidth(1 + k);
                ey[grs] = hist_data->GetBinError(1 + k, 1 + i);
                ++grs;
            }
        }

        TGraphErrors* gr1 = new TGraphErrors(grs, x, y, 0, ey);
        TGraphErrors* gr2 = new TGraphErrors(grs, m, y, 0, ey);

        char legentry[100];
        sprintf(legentry, "%g < %s < %g", hist_data->GetYaxis()->GetBinLowEdge(1 + i),
                ctx->y.label.Data(), hist_data->GetYaxis()->GetBinUpEdge(1 + i));

        if (leg and
            ((g_flag_alt_odd == 0 and g_flag_alt_even == 0) or
             ((g_flag_alt_odd == 1) and (i % 2 == 1)) or ((g_flag_alt_even == 1) and (i % 2 == 0))))
        {
            leg->AddEntry(gr1, legentry, "lp");
        }

        gr1->SetLineColor(colors[i % colors_num]);
        gr1->SetLineWidth(1);
        gr1->SetLineStyle(1);
        gr1->SetMarkerColor(colors[i % colors_num]);
        gr1->SetMarkerStyle(markers[i % markers_num]);
        gr1->SetTitle("");

        gr2->SetLineColor(colors[i % colors_num]);
        gr2->SetLineWidth(1);
        gr2->SetLineStyle(2);
        gr2->SetMarkerColor(colors[i % colors_num]);
        gr2->SetMarkerStyle(marker_pair[markers[i % markers_num]]);
        gr2->SetTitle("");

        if (i == 0)
        {
            char label[100];
            sprintf(label, "%s", ctx->x.label.Data());
            hist_draft->GetXaxis()->SetTitle(label);
            TString hist_title = TString::Format(
                "d^{2}#sigma/d%sd%s [#mub/%g%s/%g%s]", ctx->x.label.Data(), ctx->y.label.Data(),
                hist_data->GetYaxis()->GetBinWidth(1), ctx->y.unit.Data(),
                hist_data->GetXaxis()->GetBinWidth(1), ctx->x.unit.Data());
            hist_draft->GetYaxis()->SetTitle(hist_title);
            hist_draft->Clone()->Draw("B");
        }

        if (grs)
        {
            if ((g_flag_alt_odd == 0 and g_flag_alt_even == 0) or
                ((g_flag_alt_odd == 1) and (i % 2 == 1)) or
                ((g_flag_alt_even == 1) and (i % 2 == 0)))
            {
                gr1->Draw("L P E2 same");
                gr2->Draw("L P E2 same");
            }
            // 			}
        }
    }

    TList* l = c->GetListOfPrimitives();
    if (l->GetSize() and l->At(0)->InheritsFrom("TH1D"))
    {
        TH1D* h0 = (TH1D*)l->At(0);
        double dmm = (max - min) * 0.05;
        h0->GetYaxis()->SetRangeUser(min - dmm, max + dmm);
    }

    if (leg)
    {
        char header[200];
        sprintf(header, "#sigma(%s;%s):", ctx->x.label.Data(), ctx->y.label.Data());
        leg->SetHeader(header);
    }

    return uid;
}

template <class T>
float DrawRatioProjections(TVirtualPad* c, TH1D* hist_draft, T* hist_data, DistributionContext* ctx,
                           int uid, TLegend* leg = 0)
{
    double min = 0.0; // hist_data->GetMinimum(0.0);
    double max = 0.0; // hist_data->GetMaximum();

    RootTools::FindBoundaries(hist_data, min, max);

    size_t binslen = hist_draft->GetXaxis()->GetNbins();
    size_t binsnum = hist_data->GetYaxis()->GetNbins();

    double* x = new double[binslen];
    double* y = new double[binslen];
    double* ex = new double[binslen];
    double* ey = new double[binslen];

    if (uid < 1) uid = rand() % 1000 + 1;

    TString str = TString::Format("%s_%d", hist_data->GetName(), uid);

    for (size_t i = 0; i < binsnum; ++i)
    {
        pf.gf.x.Ndiv = binslen * 100 + 2;

        RootTools::NicePad(c, pf.pf);
        RootTools::NiceHistogram(hist_draft, pf.gf);

        int grs = 0;
        for (uint k = 0; k < binslen; ++k)
        {
            double bc = hist_data->GetBinContent(1 + k, 1 + i);
            if (bc)
            {
                double bcen = hist_data->GetXaxis()->GetBinCenter(1 + k);
                x[grs] = bcen;
                y[grs] = bc;
                ex[grs] = hist_data->GetXaxis()->GetBinWidth(1 + k);
                ey[grs] = 0; // hist_data->GetBinError(1+k, 1+i);
                ++grs;
            }
        }

        TGraphErrors* gr1 = new TGraphErrors(grs, x, y, 0, ey);

        char legentry[100];
        sprintf(legentry, "%g < %s < %g", hist_data->GetYaxis()->GetBinLowEdge(1 + i),
                ctx->y.label.Data(), hist_data->GetYaxis()->GetBinUpEdge(1 + i));

        if (leg and
            ((g_flag_alt_odd == 0 and g_flag_alt_even == 0) or
             ((g_flag_alt_odd == 1) and (i % 2 == 1)) or ((g_flag_alt_even == 1) and (i % 2 == 0))))
        {
            leg->AddEntry(gr1, legentry, "lp");
        }

        gr1->SetLineColor(colors[i % colors_num]);
        gr1->SetLineWidth(2);
        gr1->SetLineStyle(1);
        gr1->SetMarkerColor(colors[i % colors_num]);
        gr1->SetMarkerStyle(markers[i % markers_num]);
        gr1->SetTitle("");

        if (i == 0)
        {
            char label[100];
            sprintf(label, "%s", ctx->x.label.Data());
            hist_draft->GetXaxis()->SetTitle(label);
            hist_draft->GetYaxis()->SetTitle("Ratio L/R");
            hist_data->SetMarkerStyle(0);
            hist_draft->Clone()->Draw("B");
        }

        if (grs)
        {
            RootTools::FindBoundaries(gr1, min, max, (i == 0));
            gr1->Draw("L P same");
        }
    }

    TList* l = c->GetListOfPrimitives();
    if (l->GetSize() and l->At(0)->InheritsFrom("TH1D"))
    {
        TH1D* h0 = (TH1D*)l->At(0);
        double dmm = (max - min) * 0.05;
        h0->GetYaxis()->SetRangeUser(min - dmm, max + dmm);
    }

    if (leg)
    {
        char header[200];
        sprintf(header, "#sigma(%s;%s):", ctx->x.label.Data(), ctx->y.label.Data());
        leg->SetHeader(header);
    }

    return uid;
}

TH2D* FitAndFillCustomFunctions(TH2* hist, TH2D* target = nullptr, int mode = -1,
                                SmartFactory* sf = nullptr)
{
    char buff[200];

    if (!target)
    {
        target = (TH2D*)hist->Clone("fit_and_fill");
        target->Clear();
    }

    TF1* f_fit_and_fill = 0;

    float fit_min = 0, fit_max = 0;
    if (mode == 0)
    {
        fit_min = 0;
        fit_max = 600;
        f_fit_and_fill = new TF1("f_fit_and_fill",
                                 "[0] * x *sqrt(x*x + [1]*[1]) * exp( - sqrt(x*x + [1]*[1])/[2])",
                                 fit_min, fit_max);
        f_fit_and_fill->SetParameters(44, 1000, 60);
    }

    if (mode == 1)
    {
        fit_min = 0;
        fit_max = 1000;
        f_fit_and_fill = new TF1("f_fit_and_fill",
                                 "[0] * (sqrt(x*x + [1]*[1]) - [3]*x )/[4] * exp( - (sqrt(x*x + "
                                 "[1]*[1]) - [3]*x )/[4] /[2])",
                                 fit_min, fit_max);
        f_fit_and_fill->SetParameters(4.8e15, 1000, 84, 0.5, 0.25);
    }

    if (!f_fit_and_fill) return 0;

    int binsx = hist->GetXaxis()->GetNbins();
    int binsy = hist->GetYaxis()->GetNbins();

    for (int i = 0; i < binsx; ++i)
    {
        bool end_filling = false;

        sprintf(buff, "%s_proj_%d_%d", hist->GetName(), mode, i);
        TH1D* h4fit = (TH1D*)hist->ProjectionY(buff, i + 1, i + 1);
        if (sf)
        {
            sprintf(buff, "%s_fit_%d_%d", hist->GetName(), mode, i);
            TCanvas* can = sf->RegCanvas(buff, buff, 800, 600);
            can->cd();
        }
        else
        {
            gPad = 0;
        }
        h4fit->Fit(f_fit_and_fill, "Q", "", fit_min, fit_max);

        for (int j = 0; j < binsy; ++j)
        {
            Double_t bin_center = hist->GetYaxis()->GetBinCenter(j + 1);
            target->SetBinContent(i + 1, j + 1, f_fit_and_fill->Eval(bin_center));
            if (g_flag_fit_and_fill)
            {
                if (hist->GetBinContent(i + 1, j + 1) == 0.0)
                {
                    if (!end_filling)
                        hist->SetBinContent(i + 1, j + 1, f_fit_and_fill->Eval(bin_center));
                }
                else
                {
                    end_filling = true;
                }
            }
        }

        if (gPad)
        {
            sprintf(buff, "%s_projfit_%d_%d", hist->GetName(), mode, i);
            TH1D* h4fit_f = (TH1D*)target->ProjectionY(buff, i + 1, i + 1);
            h4fit_f->Draw("same");
        }
    }

    return target;
}

Specialists::Specialists()
    : SimpleToolsAbstract(), flag_mirror(0), flag_cs(0), flag_alt_odd(0), flag_alt_even(0),
      flag_fit_and_fill(0)
{
    NicePalette();
    TGaxis::SetMaxDigits(3);
    TVirtualFitter::SetPrecision(1e-6);

    // 	for (size_t a = 0; a < local_ananum; ++a)
    // 	{
    arrSystematic = nullptr;
    // 	}

    g_arrSystematic = arrSystematic;

    RootTools::AxisFormat x(10, 0.07, 0.005, 0.07, 0.95, kFALSE, kFALSE);
    RootTools::AxisFormat y(503, 0.07, 0.005, 0.07, 0.80, kFALSE, kTRUE);
    RootTools::AxisFormat z(10, 0.06, 0.005, 0.06, 0.95, kFALSE, kFALSE);

    RootTools::PadFormat padf = {0.08, 0.08, 0.14, 0.1};
    RootTools::GraphFormat graphf = {x, y, z};
    pf.pf = padf;
    pf.gf = graphf;

    srand(time(0));
}

Specialists::~Specialists() {}

void Specialists::AnaInitialize()
{
    if (configfile_t.IsNull())
    {
        std::cout << "No config file!" << std::endl;
        std::exit(EXIT_FAILURE);
    }

    cdfdata = new CDFInputData();

    std::ifstream fcfgfile(configfile_t.Data());
    if (!fcfgfile.is_open())
    {
        std::cerr << "File " << configfile_t << " is not open!";
        std::exit(EXIT_FAILURE);
    }

    SetOutputFile("Specialists.root");

    std::vector<char>::iterator iter;
    size_t idx = 0;

    TString filetmp;
    std::string line;

    cdfdata->groupID.push_back('a');
    cdfdata->groupName.push_back("all");
    cdfdata->groupSplit.push_back(true);
    cdfdata->groupAttr.push_back("");
    cdfdata->entryID.push_back(str_vec());
    cdfdata->entryFile.push_back(tfile_vec());
    cdfdata->entryAttr.push_back(str_vec());
    cdfdata->entryCorr.push_back(bool_vec());

    while (std::getline(fcfgfile, line))
    {
        if (!line.size()) continue;

        TString line_(line);
        line_.ReplaceAll("\t", " ");

        TObjArray* arr = line_.Tokenize(" ");

        char cmdchar = ((TObjString*)arr->At(0))->String()[0];
        TString tmpstr;

        size_t sidx = 0;
        TFile* f = 0;
        switch (cmdchar)
        {
            case 'e':
                cdfdata->entryID[idx].push_back("exp");
                filetmp = ((TObjString*)arr->At(1))->String().Data();
                f = TFile::Open(filetmp);
                cdfdata->entryFile[idx].push_back(f);
                cdfdata->entryAttr[idx].push_back(((TObjString*)arr->At(2))->String().Data());
                cdfdata->entryCorr[idx].push_back(false);
                sidx = cdfdata->entryCorr[idx].size() - 1;
                std::cout << " " << cdfdata->entryID[idx][sidx] << " " << filetmp << " "
                          << std::endl;
                getAnaNames(f);
                break;

            case 'c':
                if (((TObjString*)arr->At(1))->String() == "maxy")
                {
                    set[((TObjString*)arr->At(2))->String().Atoi()].cfg_maxy =
                        ((TObjString*)arr->At(3))->String().Atof();
                }
                if (((TObjString*)arr->At(1))->String() == "miny")
                {
                    set[((TObjString*)arr->At(2))->String().Atoi()].cfg_miny =
                        ((TObjString*)arr->At(3))->String().Atof();
                }
                break;

            case '#':
                continue;
                break;
            case 'g':
                break; // FIXME why it wasn't here, why g and s together?
            case 's':
                syserrfile_t = ((TObjString*)arr->At(1))->String().Data();
                break;

            default:
                idx = 0;
                cdfdata->entryID[idx].push_back(((TObjString*)arr->At(1))->String().Data());
                filetmp = ((TObjString*)arr->At(2))->String().Data();
                f = TFile::Open(filetmp);
                cdfdata->entryFile[idx].push_back(f);
                cdfdata->entryAttr[idx].push_back(((TObjString*)arr->At(3))->String().Data());
                cdfdata->entryCorr[idx].push_back(false);

                sidx = cdfdata->entryCorr[idx].size() - 1;
                std::cout << " " << cdfdata->entryID[idx][sidx] << " " << filetmp << " "
                          << std::endl;
                break;
        }
    }

    // if systematic errors file exists
    if (!syserrfile_t.IsNull())
    {
        TFile* syserrfile = TFile::Open(syserrfile_t, "READ");
        if (!syserrfile->IsOpen())
        {
            std::cerr << "File " << syserrfile_t << " is not open!";
            std::exit(EXIT_FAILURE);
        }

        // 		for (size_t a = 0; a < /*ananum*/2; ++a)
        // 		{
        arrSystematic = (TObjArray*)RootTools::GetObjectFromFile(
            syserrfile, TString::Format("arr_%s_Systematic", /*ananames[a].c_str()*/ "All"));
        // 		}
    }

    g_arrSystematic = arrSystematic;

    if (cdfdata->entryFile.size() and cdfdata->entryFile[0].size())
    {
        CreateCanvases(cdfdata->entryFile[0][0]);
    }

    set = new SpecSet[ananum];
}

void Specialists::CreateCanvases(TFile* source)
{
    for (size_t a = 0; a < ananum; ++a)
    {
        set[a].fa = new SmartFactory(ananames[a].c_str());
        set[a].fa->setSource(source);
    }
}

void Specialists::AnaExecute()
{
    g_flag_alt_even = flag_alt_even;
    g_flag_alt_odd = flag_alt_odd;
    g_flag_fit_and_fill = flag_fit_and_fill;

    const char* mir_input = "mirror/%s/%s_input_l_e%02d";
    const char* mir_pattern = "mirror/%s/%s_mirror_e%02d";
    const char* mir_patternl = "mirror/%s/%s_mirror_l_e%02d";
    const char* mir_patternr = "mirror/%s/%s_mirror_r_e%02d";
    const char* rat_pattern = "ratio/%s/%s_ratio_r_e%02d";
    const char* cs_pattern_diff = "cs/%s/%s_cs_diff_e%02d";
    const char* cs_pattern_fit = "cs/%s/%s_cs_fit_e%02d";
    const char* cs_pattern_proj = "cs/%s/%s_cs_proj_e%02d";
    const char* cs_pattern_xint = "cs/%s/%s_cs_xint_e%02d";

    init_pf();

    for (size_t a = 0; a < local_ananum; ++a)
    {
        g_a = a;
        if (flag_mirror)
        {
            std::string tmpname =
                TString::Format("%s/h_%s_Signal", ananames[a].c_str(), ananames[a].c_str()).Data();
            uint entrynum = cdfdata->entryID[0].size();

            const size_t can_nums = 6;
            TCanvas* canvases[can_nums] = {0};
            TLegend* legends[can_nums] = {0};
            TPad* pads_data[can_nums] = {0};
            TPad* pads_legends[can_nums] = {0};
            int legends_extra_offset[can_nums] = {0, 2, 0, 0, 0, 0};

            const char* can_names[can_nums] = {"@@@d/c_@@@a_LR_Input", "@@@d/c_@@@a_LR_Mirror",
                                               "@@@d/c_@@@a_LR_Ratio", "@@@d/c_@@@a_CS_diff",
                                               "@@@d/c_@@@a_CS_proj",  "@@@d/c_@@@a_CS_intx"};
            const char* can_titles[can_nums] = {"@@@a Input",   "@@@a Mirror",  "@@@a LR_Ratio",
                                                "@@@a CS-diff", "@@@a CS-proj", "@@@a CS-intx"};
            const char* graph_prefix[can_nums] = {"gr_input", "gr_mirror", "gr_ratio",
                                                  "cs_diff",  "cs_proj",   "cs_xint"};
            bool legend_mask[can_nums] = {false, true, true, false, true, false};

            // prepare legend
            const double ytop = 0.95;
            const double dytop = 0.05;

            for (uint i = 0; i < can_nums; ++i)
            {
                canvases[i] = set[a].fa->RegCanvas(can_names[i], can_titles[i], 800, 600);
                canvases[i]->cd();

                std::string p1m = TString::Format("%s/%s_%s_graphs", ananames[a].c_str(),
                                                  graph_prefix[i], ananames[a].c_str())
                                      .Data();
                std::string p2m = TString::Format("%s/%s_%s_legend", ananames[a].c_str(),
                                                  graph_prefix[i], ananames[a].c_str())
                                      .Data();

                canvases[i]->SetMargin(0, 0, 0, 0);
                // 				canvases[i]->SetFillColor(1);
                if (legend_mask[i])
                {
                    pads_data[i] = new TPad(p1m.c_str(), "", 0.01, 0.01, 0.86, 0.99);
                    pads_legends[i] = new TPad(p2m.c_str(), "", 0.84, 0.01, 0.99, 0.99);
                }
                else
                {
                    pads_data[i] = new TPad(p1m.c_str(), "", 0.0, 0.0, 1., 1.);
                    // 					pads_data[i]->SetFillColor(2);
                    pads_legends[i] = nullptr;
                }

                pads_data[i]->DivideSquare(entrynum);

                pads_data[i]->Draw();
                if (pads_legends[i]) pads_legends[i]->Draw();
            }

            // other stuff
            DistributionContext* ctx = 0;

            int bin_zero = 0;
            int bins_s = 0;
            int bins_y = 0;
            int bins_l = 0;
            int bins_r = 0;
            double* bins_arrx = 0;
            double* bins_arry = 0;

            for (uint e = 0; e < entrynum; ++e)
            {
                std::string strid = cdfdata->entryID[0][e];

                TH2F* htemp =
                    (TH2F*)SmartFactory::getObject(cdfdata->entryFile[0][e], tmpname.c_str());
                if (e == 0)
                {
                    // get first histogram to obtain dimensions
                    ctx = (DistributionContext*)SmartFactory::getObject(
                        cdfdata->entryFile[0][e], set[a].fa->format("@@@aCtx"));

                    // get x-axis and count number of bins
                    TAxis* xaxis = htemp->GetXaxis();
                    PR(xaxis);
                    bin_zero = xaxis->FindBin(0.0);
                    int bins = xaxis->GetNbins();

                    // print bins boundaries
                    for (int i = 0; i < bins; ++i)
                        std::cout << xaxis->GetBinLowEdge(i + 1) << " : ";
                    std::cout << xaxis->GetBinLowEdge(bins + 1) << std::endl;

                    // calculate left-side and right-side bins numbers
                    bins_l = bin_zero - 1;
                    bins_r = bins - bin_zero + 1;
                    std::cout << bins_l << "<->" << bins_r << std::endl;

                    // if bins zero-bin is outside the range, we must trick it
                    if (bins_l > bins) bins_l = bins;
                    if (bins_r > bins) bins_r = bins;

                    // mirror left side to right
                    if (bins_l > bins_r)
                    {
                        bins_s = 2 * bins_l;
                        bins_arrx = new double[bins_s + 1];
                        for (int i = 0; i < bins_l; ++i)
                        {
                            double edge = xaxis->GetBinLowEdge(i + 1);
                            bins_arrx[i] = edge;
                            bins_arrx[bins_s - i] = -edge;
                        }
                        bins_arrx[bins_l] = xaxis->GetBinLowEdge(bins_l + 1);
                    }
                    // mirror right side to left
                    else
                    {
                        bins_s = 2 * bins_r;
                        bins_arrx = new double[bins_s + 1];
                        for (int i = 0; i < bins_r; ++i)
                        {
                            double edge = xaxis->GetBinLowEdge(bin_zero + i);
                            bins_arrx[bins_r - i - 1] = -edge;
                            bins_arrx[bins_r + i] = edge;
                        }
                        bins_arrx[bins_s] = xaxis->GetBinLowEdge(bin_zero + bins_s + 1);
                    }

                    // print new bin division
                    set_color(std::cout, TC_GreenB);
                    std::cout << "Bins in x: ";
                    for (int i = 0; i < bins_s; ++i)
                        std::cout << bins_arrx[i] << " : ";
                    std::cout << bins_arrx[bins_s] << std::endl;

                    // Get bins in Y
                    TAxis* yaxis = htemp->GetYaxis();
                    bins_y = yaxis->GetNbins();
                    bins_arry = new double[bins_y + 1];

                    for (int i = 0; i < bins_y; ++i)
                        bins_arry[i] = yaxis->GetBinLowEdge(i + 1);
                    bins_arry[bins_y] = yaxis->GetBinLowEdge(bins_y + 1);

                    // print bins in y
                    set_color(std::cout, TC_YellowB);
                    std::cout << colstd << "Bins in y: ";
                    for (int i = 0; i < bins_y; ++i)
                        std::cout << bins_arry[i] << " : ";
                    std::cout << bins_arry[bins_y] << resstd << std::endl;

                    for (uint i = 0; i < can_nums; ++i)
                    {
                        legends[i] =
                            new TLegend(0.01, ytop - (bins_y + 1 + legends_extra_offset[i]) * dytop,
                                        0.99, ytop);
                        legends[i]->SetBorderSize(0);
                        legends[i]->SetTextSize(0.09);
                    }
                }
                const char* orig_name = htemp->GetName();

                char hname[1024];

                sprintf(hname, mir_pattern, ananames[a].c_str(), orig_name, e);
                TH1D* hmir = set[a].fa->RegTH1<TH1D>(hname, hname, bins_s, bins_arrx);

                sprintf(hname, mir_patternl, ananames[a].c_str(), orig_name, e);
                TH2D* hleft =
                    set[a].fa->RegTH2<TH2D>(hname, hname, bins_s, bins_arrx, bins_y, bins_arry);
                hleft->Sumw2();

                sprintf(hname, mir_patternr, ananames[a].c_str(), orig_name, e);
                TH2D* hright =
                    set[a].fa->RegTH2<TH2D>(hname, hname, bins_s, bins_arrx, bins_y, bins_arry);
                hright->Sumw2();

                sprintf(hname, rat_pattern, ananames[a].c_str(), orig_name, e);
                TH2D* hratio =
                    set[a].fa->RegTH2<TH2D>(hname, hname, bins_s, bins_arrx, bins_y, bins_arry);
                hratio->Sumw2();

                sprintf(hname, cs_pattern_diff, ananames[a].c_str(), orig_name, e);
                TH2D* hcs_diff =
                    set[a].fa->RegTH2<TH2D>(hname, hname, bins_s, bins_arrx, bins_y, bins_arry);
                hcs_diff->Sumw2();

                sprintf(hname, cs_pattern_fit, ananames[a].c_str(), orig_name, e);
                TH2D* hcs_fit =
                    set[a].fa->RegTH2<TH2D>(hname, hname, bins_s, bins_arrx, bins_y, bins_arry);
                hcs_fit->Sumw2();

                sprintf(hname, cs_pattern_proj, ananames[a].c_str(), orig_name, e);
                TH1D* hcs_proj = set[a].fa->RegTH1<TH1D>(hname, hname, bins_s, bins_arrx);
                // 				hcs_proj->Sumw2();

                sprintf(hname, cs_pattern_xint, ananames[a].c_str(), orig_name, e);
                TH1D* hcs_xint = set[a].fa->RegTH1<TH1D>(hname, hname, bins_s, bins_arrx);
                hcs_xint->Sumw2();

                // make left/right histograms
                for (int k = 0; k < bins_s; ++k)
                    for (int l = 0; l < bins_y; ++l)
                    {
                        double bc = htemp->GetBinContent(k + 1, l + 1);
                        if (bc == 0.0) continue;

                        double be = htemp->GetBinError(k + 1, l + 1);

                        hleft->SetBinContent(k + 1, l + 1, bc);
                        hleft->SetBinError(k + 1, l + 1, be);

                        hright->SetBinContent(bins_s - k, l + 1, bc);
                        hright->SetBinError(bins_s - k, l + 1, be);
                    }

                sprintf(hname, mir_input, ananames[a].c_str(), orig_name, e);
                TH2D* hinput = (TH2D*)hleft->Clone(hname);

                // make weighted sum of L+R histogram
                for (int k = 0; k < bins_s; ++k)
                    for (int l = 0; l < bins_y; ++l)
                    {
                        double bcl = hleft->GetBinContent(k + 1, l + 1);
                        double berrl = hleft->GetBinError(k + 1, l + 1);

                        double bcr = hright->GetBinContent(k + 1, l + 1);
                        double berrr = hright->GetBinError(k + 1, l + 1);

                        double bc_w = 0.0;
                        double berr_w = 0.0;

                        if (berrl != 0.0 and berrr != 0.0)
                        {
                            bc_w = (bcl / (berrl * berrl) + bcr / (berrr * berrr)) /
                                   (1.0 / (berrl * berrl) + 1.0 / (berrr * berrr));
                            berr_w = 1.0 / sqrt(1.0 / (berrl * berrl) + 1.0 / (berrr * berrr));
                        }
                        else if (berrl == 0.0)
                        {
                            bc_w = bcr;
                            berr_w = berrr;
                        }
                        else if (berrr == 0.0)
                        {
                            bc_w = bcl;
                            berr_w = berrl;
                        }

                        hcs_diff->SetBinContent(k + 1, l + 1, bc_w);
                        hcs_diff->SetBinError(k + 1, l + 1, berr_w);
                    }

                // make ratio, store in left
                hleft->Divide(hright);

                for (int k = 0; k < bins_s; ++k)
                    for (int l = 0; l < bins_y; ++l)
                    {
                        double bc = hleft->GetBinContent(k + 1, l + 1);
                        if (bc == 0.0) continue;

                        double be = hleft->GetBinError(k + 1, l + 1);

                        hratio->SetBinContent(k + 1, l + 1, bc);
                        hratio->SetBinError(k + 1, l + 1, be);
                    }

                RootTools::NiceHistogram(hmir, pf.gf);
                hmir->SetTitle(htemp->GetTitle());
                RootTools::NiceHistogram(hmir, cdfdata->entryAttr[0][e]);

                RootTools::NiceHistogram(hcs_diff, pf.gf);
                hcs_diff->SetTitle(htemp->GetTitle());
                RootTools::NiceHistogram(hcs_diff, cdfdata->entryAttr[0][0]);

                RootTools::NiceHistogram(hinput, pf.gf);
                hinput->SetTitle("");
                RootTools::NiceHistogram(hinput, cdfdata->entryAttr[0][0]);

                RootTools::NiceHistogram(hcs_proj, pf.gf);
                hcs_proj->SetTitle(htemp->GetTitle());
                RootTools::NiceHistogram(hcs_proj, cdfdata->entryAttr[0][e]);

                TVirtualPad* active_pad_m = pads_data[1]->cd(1 + e);
                if (e == 0)
                {
                    DrawMirrorProjections(active_pad_m, hmir, htemp, ctx, rand() % 1000 + 1,
                                          legends[0]);
                }
                else
                {
                    DrawMirrorProjections(active_pad_m, hmir, htemp, ctx, rand() % 1000 + 1);
                }

                TVirtualPad* active_pad_r = pads_data[2]->cd(1 + e);
                if (e == 0)
                {
                    DrawRatioProjections(active_pad_r, hmir, hratio, ctx, rand() % 1000 + 1,
                                         legends[1]);
                }
                else
                {
                    DrawRatioProjections(active_pad_r, hmir, hratio, ctx, rand() % 1000 + 1);
                }

                // 				TVirtualPad * active_pad_cs_diff = pads_data[2]->cd(1+e);
                TVirtualPad* active_pad_cs_diff = canvases[3]->cd();
                TVirtualPad* active_pad_cs_input = canvases[0]->cd();
                if (e == 0)
                {
                    DrawCSDifferential(active_pad_cs_diff, hcs_diff, ctx, rand() % 1000 + 1);
                    DrawCSDifferential(active_pad_cs_input, hinput, ctx, rand() % 1000 + 1);
                }
                else
                {
                    DrawCSDifferential(active_pad_cs_diff, hcs_diff, ctx, rand() % 1000 + 1);
                    DrawCSDifferential(active_pad_cs_input, hinput, ctx, rand() % 1000 + 1);
                }

                gPad = 0;
                FitAndFillCustomFunctions(hcs_diff, hcs_fit, a, set[a].fa);

                TVirtualPad* active_pad_cs_proj = pads_data[4]->cd(1 + e);
                if (e == 0)
                {
                    DrawCSProjections(active_pad_cs_proj, hcs_diff, ctx, rand() % 1000 + 1,
                                      legends[3]);
                }
                else
                {
                    DrawCSProjections(active_pad_cs_proj, hcs_diff, ctx, rand() % 1000 + 1);
                }

                int new_uid = rand() % 1000 + 1;

                TVirtualPad* active_pad_cs_xint = pads_data[5]->cd(1 + e);
                if (e == 0)
                {
                    DrawCSIntegrals(active_pad_cs_xint, hcs_diff, ctx, new_uid, legends[4],
                                    (a < 2));
                }
                else
                {
                    DrawCSIntegrals(active_pad_cs_xint, hcs_diff, ctx, new_uid, 0, (a < 2));
                }

                RootTools::NicePad(active_pad_cs_diff, def_pf_diff);
                RootTools::NiceHistogram(hcs_diff, def_gf_diff);
                hcs_diff->SetMarkerColor(0);
                hcs_diff->SetMarkerSize(1.4);

                RootTools::NicePad(active_pad_cs_input, def_pf_diff);
                RootTools::NiceHistogram(hinput, def_gf_diff);
                hinput->SetMarkerColor(0);
                hinput->SetMarkerSize(1.4);
            }

            TLine* line = new TLine;
            line->SetLineWidth(1);
            legends[0]->AddEntry(line->Clone(), "regular dists.", "lp");
            line->SetLineStyle(2);
            TLegendEntry* le = legends[0]->AddEntry(line, "mirror dists.", "lp");
            le->SetMarkerStyle(25);

            for (uint i = 0; i < can_nums - 1; ++i)
            {
                if (pads_legends[i] and legends[i])
                {
                    pads_legends[i]->cd();
                    legends[i]->Draw();
                }
            }
        }
    }
}

void Specialists::AnaFinalize()
{
    OpenExportFile();

    setConfigVector()->Write();

    for (size_t a = 0; a < ananum; ++a)
    {
        // 		cLambda_final[a]->cd();
        // 		TPad * pad_comp = new TPad(TString::Format("p%u_comp", a), "_comp", 0.01, 0.0,
        // 0.83, 1.0); 		pad_comp->Draw(); 		pad_comp->cd();
        // cLambda_comp[a]->DrawClonePad();
        //
        // 		cLambda_final[a]->cd();
        // 		TPad * pad_leg = new TPad(TString::Format("l%u_leg", a), "_leg", 0.83, 0.01, 0.99,
        // 0.99); 		pad_leg->Draw(); 		pad_leg->cd(); 		RootTools::NicePad(pad_leg, 0., 0., 0.,
        // 0.1); 		canleg[a]->DrawClonePad();
        //
        // 		TLatex * latex = new TLatex();
        // 		latex->SetNDC();
        // 		latex->SetTextColor(/*36*/1);
        // 		latex->SetTextSize(0.15);
        // 		latex->SetTextAlign(23);
        // 		latex->DrawLatex(0.5, 0.98, Form("d#sigma/d%s", ctx[a]->x.label.Data()));

        set[a].fa->write(GetExportFile());
    }

    CloseExportFile();
}

int Specialists::config_params(struct option*& opts)
{
    static struct option lopts[] = {
        /* These options set a flag. */
        {"mirror", no_argument, &flag_mirror, 1},
        {"alt-odd", no_argument, &flag_alt_odd, 1},
        {"alt-even", no_argument, &flag_alt_even, 1},
        {"fit-fill", no_argument, &flag_fit_and_fill, 1},
    };
    opts = lopts;

    return sizeof(lopts) / sizeof(option);
}

void Specialists::config_execute(int code, char* optarg)
{
    switch (code)
    {
        default:
            TString ftmp = optarg;
            configfile_t = ftmp;
            return;
            break;
    }
}
