/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Lambdactau.h"

#ifndef __CINT__

#include "getopt.h"
#include <fstream>
#include <string>

#include "TCanvas.h"
#include "TChain.h"
#include "TDirectory.h"
#include "TError.h"
#include "TF1.h"
#include "TFile.h"
#include "TGaxis.h"
#include "TH2.h"
#include "TImage.h"
#include "TKey.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TMath.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TVector.h"
#include "TVirtualFitter.h"

#endif /* __CINT__ */

#include "RootTools.h"

#include "Dim2AnalysisFactory.h"
#include "MultiDimAnalysisContext.h"

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

// Processing data bar width
const Int_t bar_dotqty = 10000;
const Int_t bar_width = 20;

const Option_t h1opts[] = "h,E1";

// const Int_t Lambdactau::can_width = 800;
// const Int_t Lambdactau::can_height = 600;
// const Int_t Lambdactau::invmass_bins = 65;
// const Int_t Lambdactau::invmass_min = 1070;
// const Int_t Lambdactau::invmass_max = 1200;
// const Int_t Lambdactau::impt_bins = 18;
// const Int_t Lambdactau::impt_min = 100;
// const Int_t Lambdactau::impt_max = 1000;
// const Int_t Lambdactau::imy_bins = 11;
// const Float_t Lambdactau::imy_min = -1.0;
// const Float_t Lambdactau::imy_max = 0.21;

Lambdactau::Lambdactau()
    : SimpleToolsAbstract(), nParNum(2 + 2 + 3 + 3 + 6) // S/B, range, GausFit, PolFit
{
    NicePalette();
    TGaxis::SetMaxDigits(3);
    TVirtualFitter::SetPrecision(1e-6);

    // 	fileLambdaFitted = "LambdaFitted.root";
}

Lambdactau::~Lambdactau() {}

void Lambdactau::AnaInitialize()
{
    if (flag_verbose)
        flags_fit_a = "B";
    else
        flags_fit_a = "B,Q";

    flags_fit_b = "";

    if (fileLambdaFitted.IsNull())
    {
        std::cout << "No input file!" << std::endl;
        std::exit(EXIT_FAILURE);
    }

    // this fixes problem with emulate TPaletteAxis
    // TCanvas * dummy = new TCanvas("dummy", "dummY", 10, 10);

    TDirectory* pwd = gDirectory;

    /*	if (flag_pluto == 1)
            fileSecVertex = "SecVertexReco-sim.root";
        else if (flag_gibuu == 1)
            fileSecVertex = "SecVertexReco-gibuu.root";
        else if (flag_exp == 1)
            fileSecVertex = "SecVertexReco-exp.root";
        else*/
    // 		fileSecVertex = "SecVertexReco.root";

    /*	if (flag_pluto == 1)
            SetOutputFile("FittedPtYLambda-sim.root");
        else if (flag_gibuu == 1)
            SetOutputFile("FittedPtYLambda-gibuu.root");
        else if (flag_exp == 1)
            SetOutputFile("FittedPtYLambda-exp.root");
        else*/
    SetOutputFile("Lambdactau.root");

    lambdafittedfile = TFile::Open(fileLambdaFitted, "READ");
    if (!lambdafittedfile->IsOpen())
    {
        std::cerr << "File " << fileLambdaFitted << " is not open!";
        std::exit(EXIT_FAILURE);
    }

    getConfigVector();

    MultiDimAnalysisContext* dactx_xcmpcm = nullptr;
    dactx_xcmpcm = (MultiDimAnalysisContext*)gDirectory->Get("XcmPcm");

    hctauSig = new TH1F("h_ctauSig", "c#tau_{#Lambda};p_{cm} [Mev/c];c#tau [m]",
                        dactx_xcmpcm->x_cbins, dactx_xcmpcm->x_cmin, dactx_xcmpcm->x_cmax);
    hctauSig->Sumw2();

    params = new TH1F("params", "params", nParNum, 0, nParNum);

    pwd->cd();

    TKey* key = nullptr;
    TIter nextkey(lambdafittedfile->GetListOfKeys());
    while ((key = (TKey*)nextkey()))
    {
        const char* classname = key->GetClassName();
        TClass* cl = gROOT->GetClass(classname);
        if (!cl) continue;

        TObject* obj = key->ReadObj();
        if (obj->InheritsFrom("TCanvas"))
        {
            // 			PR("Found Canavs");
            // 			PR(obj->GetName());
            // 			( (TCanvas *) obj )->Draw();
            // 			( (TCanvas *) obj )->Update();

            // 			TCanvas * can = (TCanvas*)obj;
            // pwd->cd();
            // 			TCanvas * can1 = (TCanvas*)can->Clone(can->GetName());
            // 			can->Close();

            // 			TCanvas * can1 = (TCanvas*)secvertfile->Get(can->GetName());
            // 			can1->Clone(can1->GetName());

            // 			can->Write();
            // 			can1->Update();
            // 			can1->Draw();
            // PR(can1->GetWindowWidth());
        }
        else if (obj->InheritsFrom("TH1"))
        {
            obj->Clone(obj->GetName());
            obj->Delete();
        }
        else if (obj->InheritsFrom("TObjArray"))
        {
            TObjArray* objarr = (TObjArray*)obj;
            Int_t n = objarr->GetEntries();
            for (Int_t i = 0; i < n; ++i)
            {
                objarr->At(i)->Clone(objarr->At(i)->GetName());
                objarr->At(i)->Delete();
            }
        }
    }

    // 	TDirectory * src = lambdafittedfile;

    // 	dactx_pcmxcm->useCached = true;
    facXcmPcm = new Dim2AnalysisFactory(dactx_xcmpcm);
    // 	facXcmPcm->Init(Dim2AnalysisFactory::FIT);
    facXcmPcm->Get(Dim2AnalysisFactory::RECO);
    // 	, src, GetExportFile()
}

void Lambdactau::AnaExecute()
{
    PrintAnalysisInfo();

    if (flag_acc) return;
}

void Lambdactau::AnaFinalize()
{
    OpenExportFile();
    GetExportFile()->cd();

    int slicesnum = facXcmPcm->ctx.x_cbins;

    TLatex* latex = new TLatex();
    latex->SetNDC();
    latex->SetTextColor(/*36*/ 4);
    latex->SetTextSize(0.07);

    const Float_t lamass = 1115;

    for (int i = 0; i < slicesnum; ++i)
    {
        facXcmPcm->cSliceXYDiff->cd(1 + i);

        TF1* fctaufit = new TF1(TString::Format("fctaufit_slice_%d", i), "[0]*exp(-x/[1])",
                                facXcmPcm->ctx.y_cmin, facXcmPcm->ctx.y_cmax);

        fctaufit->SetParameters(1000.0, 75.);

        TH1F* h = facXcmPcm->hSliceXYDiff[i];

        PR(h->GetMaximum());
        PR(h->GetBinCenter(h->GetMaximumBin()));

        Float_t fit_min = h->GetBinCenter(h->GetMaximumBin()); // 100;//facXcmPcm->ctx.y_cmin;
        Float_t fit_max = facXcmPcm->ctx.y_cmax;

        h->Fit(fctaufit, "Q", "", fit_min, fit_max);

        Float_t xdecay = fctaufit->GetParameter(1);
        Float_t xdecayerr = fctaufit->GetParError(1);

        Float_t pavg = facXcmPcm->ctx.x_cmin + facXcmPcm->ctx.x_cdelta * (i + 0.5);
        PR(pavg);

        // 		Float_t ctau = xdecay;// * TMath::Sqrt(pavg*pavg + lamass*lamass)/pavg *
        // TMath::Log(2); 		Float_t ctauerr = xdecayerr;// * TMath::Sqrt(pavg*pavg +
        // lamass*lamass)/pavg * TMath::Log(2);

        Float_t gamma =
            pavg / lamass; // 1;//TMath::Sqrt(1 + (pavg*pavg / (lamass*lamass)));PR(gamma);
        // 		Float_t ctau = xdecay * lamass / pavg / 3e10 / gamma;// * TMath::Sqrt(pavg*pavg +
        // lamass*lamass)/pavg * TMath::Log(2); 		Float_t ctauerr = xdecayerr * lamass / pavg / 3e10
        // / gamma;// * TMath::Sqrt(pavg*pavg + lamass*lamass)/pavg * TMath::Log(2);

        Float_t ctau =
            xdecay / gamma; // * TMath::Sqrt(pavg*pavg + lamass*lamass)/pavg * TMath::Log(2);
        Float_t ctauerr =
            xdecayerr / gamma; // * TMath::Sqrt(pavg*pavg + lamass*lamass)/pavg * TMath::Log(2);

        hctauSig->SetBinContent(1 + i, ctau);
        hctauSig->SetBinError(1 + i, ctauerr);

        facXcmPcm->hSliceXYDiff[i]->Draw(h1opts);
        PR(fctaufit->GetNDF());
        latex->DrawLatex(0.3, 0.80, TString::Format("c#tau = %.2f#pm%.2f [mm]", ctau, ctauerr));
        latex->DrawLatex(
            0.3, 0.75,
            TString::Format("#chi^{2}/ndf = %.2f", fctaufit->GetChisquare() / fctaufit->GetNDF()));
    }

    facXcmPcm->cSliceXYDiff->cd(1 + slicesnum);

    Dim2AnalysisFactory::niceHisto(gPad, hctauSig, 0.1, 0.01, 0.13, 0.1, 505, 406, 0.06, 0.07, 0.9,
                                   0.07, 0.07, 0.55);

    gPad->SetGridx(1);
    gPad->SetGridy(1);

    hctauSig->SetMarkerStyle(7);
    hctauSig->SetMarkerColor(/*kBlack*/ 4);
    hctauSig->SetLineColor(/*kBlack*/ 4);

    hctauSig->Draw();

    facXcmPcm->write(GetExportFile());

    hctauSig->Write();

    setConfigVector();

    CloseExportFile();

    if (lambdafittedfile) lambdafittedfile->Close();
}

void Lambdactau::PrintAnalysisInfo()
{
    std::cout << "\n=======================================================\n\n";
    std::cout << "\n=======================================================\n\n";
}

int Lambdactau::config_params(struct option*& opts)
{
    static struct option lopts[] = {
        /* These options set a flag. */
        // 			{"fit-params",	required_argument,	0, 1001},
        // 			{"fit-matrix",	required_argument,	0, 1002},
    };
    opts = lopts;

    return sizeof(lopts) / sizeof(option);
}

void Lambdactau::config_execute(int code, char* optarg)
{
    switch (code)
    {
        default:
            fileLambdaFitted = optarg;
            break;
    }
    // 	if (code) return;
}