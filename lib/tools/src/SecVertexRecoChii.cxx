/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __CINT__

#include "getopt.h"
#include <fstream>
#include <string>

#include "TCanvas.h"
#include "TChain.h"
#include "TDirectory.h"
#include "TError.h"
#include "TF1.h"
#include "TFile.h"
#include "TGaxis.h"
#include "TH2.h"
#include "TImage.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TMath.h"
#include "TStyle.h"
#include "TVector.h"

#endif /* __CINT__ */

#include "RootTools.h"
#include "SecVertexRecoChii.h"
#include "pp35Tools.h"

#include "Dim2AnalysisFactory.h"

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

// Processing data bar width
const Int_t bar_dotqty = 10000;
const Int_t bar_width = 20;

const Option_t h1opts[] = "h,E1";

// const Float_t E_kin_beam		= 3500.0;
// const Float_t E_kin_target		= 0.0;
// const Float_t E_total_beam		= E_kin_beam + HPidPhysicsConstants::mass(14);
// const Float_t E_total_target	= E_kin_target + HPidPhysicsConstants::mass(14);
// const Float_t pz_beam			=
// sqrt(E_total_beam*E_total_beam-HPidPhysicsConstants::mass(14)*HPidPhysicsConstants::mass(14));
//
// const TLorentzVector Vec_pp35_beam		= TLorentzVector(0.0, 0.0, pz_beam, E_total_beam);
// const TLorentzVector Vec_pp35_target	= TLorentzVector(0.0, 0.0, 0.0, E_total_target);
// const TLorentzVector Vec_pp35_sum		= Vec_pp35_beam + Vec_pp35_target;
// const Float_t cmrap						= Vec_pp35_sum.Rapidity();

SecVertexRecoChii::SecVertexRecoChii()
    : SimpleToolsAbstract(), chain(nullptr), b_fM(.0), b_fY(.0), b_fY_cms(.0), b_fPt(.0),
      b_fCosTheta_cms(.0), b_fMinTrackDist(.0), b_fVertDistX(.0), b_fVertDistA(.0),
      b_fVertDistB(.0), b_fPrimVertexX(.0), b_fPrimVertexY(.0), b_fPrimVertexZ(.0),
      b_fGeantInfoNum(0), /*b_fCGeantInfoNum(0),*/ b_fWeight(1.0), events(0),
      par_fNoVertexCuts(kFALSE), par_fMtd(10), par_fVertDistX(35), par_fVertDistP(5),
      par_fVertDistPi(5), par_fLambdaMCutMin(1102.0), par_fLambdaMCutMax(1127.0), par_scaledown(0),
      hasInputFiles(false), data_scaledown(1.)
{
    NicePalette();
    TGaxis::SetMaxDigits(3);
}

SecVertexRecoChii::~SecVertexRecoChii() {}

void SecVertexRecoChii::InitTree()
{
    if (chain) return;

    if (flag_acc)
        chain = new TChain("pp35_Lambda_Acc");
    else
        chain = new TChain("LambdaNT");

    chain->SetBranchAddress("InvMass", &b_fM);
    chain->SetBranchAddress("YLambda", &b_fY);
    // 	chain->SetBranchAddress("fY_cms",			&b_fY_cms);
    chain->SetBranchAddress("PtLambda", &b_fPt);
    chain->SetBranchAddress("PLambda", &b_fP);
    chain->SetBranchAddress("PcmLambda", &b_fP_cms);
    chain->SetBranchAddress("CosThetacmLambda", &b_fCosTheta_cms);

    chain->SetBranchAddress("VerDistX", &b_fVertDistX);

    if (flag_exp)
    {
        chain->SetBranchAddress("PrimVertexX", &b_fPrimVertexX);
        chain->SetBranchAddress("PrimVertexY", &b_fPrimVertexY);
        chain->SetBranchAddress("PrimVertexZ", &b_fPrimVertexZ);
    }

    if (!par_fNoVertexCuts)
    {
        chain->SetBranchAddress("MinTrackDist", &b_fMinTrackDist);
        chain->SetBranchAddress("VerDistA", &b_fVertDistA);
        chain->SetBranchAddress("VerDistB", &b_fVertDistB);
    }

    if (flag_acc or flag_pluto)
    {
        chain->SetBranchAddress("fCGeantInfoNum", &b_fGeantInfoNum);
        chain->SetBranchAddress("fPrimary", &b_fPrimary);
        chain->SetBranchAddress("fParentID", &b_fParentID);
        chain->SetBranchAddress("fGParentID", &b_fGParentID);
    }

    if (flag_fss) { chain->SetBranchAddress("fGeantInfoNum", &b_fGeantInfoNum); }

    if (flag_gibuu) { chain->SetBranchAddress("weight", &b_fWeight); }
}

void SecVertexRecoChii::AnaInitialize()
{
    if (!hasInputFiles)
    {
        std::cerr << "No input files given!\n";
        std::exit(EXIT_FAILURE);
    }

    MultiDimAnalysisContext ctx;
    // common
    ctx.var_z = &b_fM;
    ctx.var_weight = &b_fWeight;

    ctx.cutMin = par_fLambdaMCutMin;
    ctx.cutMax = par_fLambdaMCutMax;

    ctx.z_bins = invmass_bins;
    ctx.z_min = invmass_min;
    ctx.z_max = invmass_max;
    ctx.z_label = "asdad";

    // Pt-Y
    ctx.histPrefix = "PtYcm";
    ctx.x_bins = imycm_bins;
    ctx.x_min = imycm_min;
    ctx.x_max = imycm_max;
    ctx.y_bins = impt_bins;
    ctx.y_min = impt_min;
    ctx.y_max = impt_max;

    ctx.var_x = &b_fY_cms;
    ctx.var_y = &b_fPt;

    ctx.x_label = "y_{cm}";
    ctx.y_label = "p_{t} [MeV/c]";
    ctx.title_label = "d^{2}N/dp_{t}dy_{cm}";

    ctx.x_cbins = lay_bins;
    ctx.x_cmin = lay_min;
    ctx.x_cmax = lay_max;
    ctx.y_cbins = lapt_bins;
    ctx.y_cmin = lapt_min;
    ctx.y_cmax = lapt_max;
    ctx.x_cidx = "y_{cm}";
    ctx.y_cidx = "p_{t}";

    ctx.validate();
    facPtYcm = new Dim2AnalysisFactory(ctx);
    facPtYcm->Init(Dim2AnalysisFactory::RECO);

    // Pcm-CosThetacm
    ctx.histPrefix = "PcmCosThcm";
    ctx.x_bins = imcthcm_bins;
    ctx.x_min = imcthcm_min;
    ctx.x_max = imcthcm_max;
    ctx.y_bins = impcm_bins;
    ctx.y_min = impcm_min;
    ctx.y_max = impcm_max;

    ctx.var_x = &b_fCosTheta_cms;
    ctx.var_y = &b_fP_cms;

    ctx.x_label = "cos#theta_{cm}";
    ctx.y_label = "p_{cm} [MeV/c]";
    ctx.title_label = "d^{2}N/dp_{cm}dcos#theta_{cm}";

    ctx.x_cbins = lacthcm_bins;
    ctx.x_cmin = lacthcm_min;
    ctx.x_cmax = lacthcm_max;
    ctx.y_cbins = lapcm_bins;
    ctx.y_cmin = lapcm_min;
    ctx.y_cmax = lapcm_max;
    ctx.x_cidx = "cos#theta_{cm}";
    ctx.y_cidx = "p_{cm}";

    ctx.validate();
    facPcmCosThcm = new Dim2AnalysisFactory(ctx);
    facPcmCosThcm->Init(Dim2AnalysisFactory::RECO);

    // Pcm-Xcm
    // 	if (!flag_acc)
    {
        ctx.histPrefix = "XcmPcm";
        ctx.x_bins = impcm_bins;
        ctx.x_min = impcm_min;
        ctx.x_max = impcm_max;
        ctx.y_bins = 500;
        ctx.y_min = 0;
        ctx.y_max = 500;

        ctx.var_x = &b_fP;
        ctx.var_y = &b_fVertDistX;

        ctx.x_label = "p [MeV/c]";
        ctx.y_label = "Track Dist. [mm]";
        ctx.title_label = "d^{2}N/dxdp";

        // 	ctx.x_cbins		= 7;//lapcm_bins;
        // 	ctx.x_cmin		= 400;//lapcm_min;
        // 	ctx.x_cmax		= 1800;//lapcm_max;
        // 	ctx.y_cbins		= 15;
        // 	ctx.y_cmin		= 50;
        // 	ctx.y_cmax		= 350;
        // 	ctx.x_cidx		= "p";
        // 	ctx.y_cidx		= "x";

        ctx.x_cbins = 7;   // lapcm_bins;
        ctx.x_cmin = 500;  // lapcm_min;
        ctx.x_cmax = 1900; // lapcm_max;
        ctx.y_cbins = 15;
        ctx.y_cmin = 45;
        ctx.y_cmax = 345;
        ctx.x_cidx = "p";
        ctx.y_cidx = "x";

        ctx.validate();
        facXcmPcm = new Dim2AnalysisFactory(ctx);
        facXcmPcm->Init(Dim2AnalysisFactory::RECO);
    }

    // Pcm-Xcm
    // 	if (!flag_acc)
    {
        ctx.histPrefix = "VertXVertY";
        ctx.x_bins = 20;
        ctx.x_min = -10;
        ctx.x_max = 10;
        ctx.y_bins = 20;
        ctx.y_min = -10;
        ctx.y_max = 10;

        ctx.var_x = &b_fPrimVertexX;
        ctx.var_y = &b_fPrimVertexY;

        ctx.x_label = "x [mm]";
        ctx.y_label = "y [mm]";
        ctx.title_label = "d^{2}N/dxdy";

        ctx.x_cbins = 20; // lapcm_bins;
        ctx.x_cmin = -10; // lapcm_min;
        ctx.x_cmax = 10;  // lapcm_max;
        ctx.y_cbins = 20;
        ctx.y_cmin = -10;
        ctx.y_cmax = 10;
        ctx.x_cidx = "x";
        ctx.y_cidx = "y";

        ctx.validate();
        facVertXVertY = new Dim2AnalysisFactory(ctx);
        facVertXVertY->Init(Dim2AnalysisFactory::RECO);
    }

    // 	if (flag_exp) {
    // 	ctx.histPrefix	= "XcmPcm";
    // 	ctx.x_bins		= impcm_bins;
    // 	ctx.x_min		= impcm_min;
    // 	ctx.x_max		= impcm_max;
    // 	ctx.y_bins		= 500;
    // 	ctx.y_min		= 0;
    // 	ctx.y_max		= 500;
    //
    // 	ctx.var_x		= &b_fP_cms;
    // 	ctx.var_y		= &b_fVertDistX;1
    //
    // 	ctx.x_label		= "p_{cm} [MeV/c]";
    // 	ctx.y_label		= "x_{cm} [cm]";
    // 	ctx.title_label	= "d^{2}N/dx_{cm}dp_{cm}";
    //
    // 	ctx.x_cbins		= lapcm_bins;
    // 	ctx.x_cmin		= lapcm_min;
    // 	ctx.x_cmax		= lapcm_max;
    // 	ctx.y_cbins		= 15;
    // 	ctx.y_cmin		= 50;
    // 	ctx.y_cmax		= 500;
    // 	ctx.x_cidx		= "p_{cm}";
    // 	ctx.y_cidx		= "x_{cm}";
    //
    // 	ctx.validate();
    // 	facXcmPcm = new Dim2AnalysisFactory(ctx);
    // 	facXcmPcm->Init(Dim2AnalysisFactory::RECO);
    // 	}

    /*	if (flag_pluto == 1)
            SetOutputFile("SecVertexRecoChii-sim.root");
        else if (flag_gibuu == 1)
            SetOutputFile("SecVertexRecoChii-gibuu.root");
        else if (flag_exp == 1)
            SetOutputFile("SecVertexRecoChii-exp.root");
        else*/
    SetOutputFile("SecVertexRecoChii.root");

    // Signal: Invariant Mass
    hSignalInvMass =
        new TH1F("h_SignalInvMass",
                 "Invariant Mass Spectrum;Invariant Mass of p and #pi^{-} [MeV/c^{2}];Counts",
                 invmass_bins, invmass_min, invmass_max);
    hSignalInvMass->SetStats(0);
    hSignalInvMass->Sumw2();

    cSignalInvMass = MakeCanvas("c_SignalInvMass", "Invariant Mass", can_width, can_height);

    // Signal: Invariant Mass
    hSignalVertexXY =
        new TH2I("h_SignalVertexXY", "Primary Vertex XY;x;y", 100, -10, 10, 100, -10, 10);
    hSignalVertexXY->SetStats(0);
    hSignalVertexXY->Sumw2();

    cSignalVertexXY = MakeCanvas("c_SignalVertexXY", "Primary Vertex XY", can_width, can_height);

    hSignalVertexXZ =
        new TH2I("h_SignalVertexXZ", "Primary Vertex XZ;z;x", 140, -65, 5, 100, -10, 10);
    hSignalVertexXZ->SetStats(0);
    hSignalVertexXZ->Sumw2();

    cSignalVertexXZ = MakeCanvas("c_SignalVertexXZ", "Primary Vertex XZ", can_width, can_height);

    hSignalVertexYZ =
        new TH2I("h_SignalVertexYZ", "Primary Vertex YZ;z;y", 140, -65, 5, 100, -10, 10);
    hSignalVertexYZ->SetStats(0);
    hSignalVertexYZ->Sumw2();

    cSignalVertexYZ = MakeCanvas("c_SignalVertexYZ", "Primary Vertex YZ", can_width, can_height);
}

void SecVertexRecoChii::AnaExecute()
{
    Bool_t newBar = kTRUE;

    // 	chain->Print();
    entries = chain->GetEntries();
    if (events and (events < entries)) entries = events;

    PrintAnalysisInfo();

    TString fbody = "ROOT::Math::legendre([3],x)+([1]*ROOT::Math::legendre([4],x)+[2]*ROOT::Math::"
                    "legendre([5],x))/[0]";
    TF1* legandrepoly = new TF1("legandrepoly", fbody, -1, 1);
    legandrepoly->SetParameter(3, 0);
    legandrepoly->SetParameter(4, 2);
    legandrepoly->SetParameter(5, 4);

    enum PlutoChannelCodes
    {
        PC040 = 181114,
        PC044 = 701114,
        PC055 = 18140816,
        PC058 = 18110812,
        PC060 = 201114,
        PC061 = 18111408,
        PC066 = 20140816,
        PC068 = 183516, // Lambda, D++, K0
        PC072 = 711113,
        PC077 = 711416,
        PC080 = 2011140864,
        PC081 = 1813080832,
        PC082 = 1811140864,
        PC085 = 1814080768,
        PC088 = 731114,
        PC089 = 1811130880,
        PC090 = 1811140864,
        PC100 = 1814080768,
        PC102 = -1,
        PC106 = -1,
        PC400 = 203516, // Sigma0, D++, K0s
        PC401 = 183611, // Lambda, D+, K+
        NSTAR1650 = 14741650,
        NSTAR1720 = 14741720,
        NSTAR1900 = 14741900,
        NSTAR2190 = 14742190
    };

    // parameters for channel 040: pK+Lambda
    const Float_t ch040_par0 = 953098.1;
    const Float_t ch040_par1 = 757042.1;
    const Float_t ch040_par2 = 126775.5;
    // parameters for channel 068: Delta++LambdaK0
    const Float_t ch068_par0 = 100018.011;
    const Float_t ch068_par1 = 49050.177;
    const Float_t ch068_par2 = 0.0;
    // parameters for channel 060: pK+Sigma0
    const Float_t ch072_par0 = 12.97;
    const Float_t ch072_par1 = 18.51;
    const Float_t ch072_par2 = 5.29;
    // parameters for channel 400: Delta++Sigma0K0
    const Float_t ch400_par0 = 39998.854;
    const Float_t ch400_par1 = 1042.471;
    const Float_t ch400_par2 = 0.0;

    // parameters for NStar1650
    const Float_t ns1650_par0 = 1.29226e+04;
    const Float_t ns1650_par1 = 1.81102e+04;
    const Float_t ns1650_par2 = -3.99356e+03;

    // parameters for NStar1720
    const Float_t ns1710_par0 = 5.76060e+03;
    const Float_t ns1710_par1 = 1.45912e+02;
    const Float_t ns1710_par2 = 8.45554e+02;

    const Float_t ns1720_par0 = 1.92350e+03;
    const Float_t ns1720_par1 = 1.58873e+03;
    const Float_t ns1720_par2 = 1.22591e+03;

    const Float_t ns1900_par0 = 5.76060e+03;
    const Float_t ns1900_par1 = 1.45912e+02;
    const Float_t ns1900_par2 = 8.45554e+02;

    Float_t weightsum = 0;

    ULong64_t ev = 0;
    for (; ev < entries; ++ev)
    {

        if (newBar)
        {
            std::cout << "==> Processing data " << std::flush;
            newBar = kFALSE;
        }

        if (ev != 0 and (ev + 1) % bar_dotqty == 0) std::cout << "." << std::flush;

        if ((ev != 0 and (ev + 1) % (bar_dotqty * bar_width) == 0) or (ev == (entries - 1)))
        {
            Float_t event_percent = 100.0 * ev / (entries - 1);
            std::cout << " " << ev + 1 << " (" << event_percent << "%) "
                      << "\n"
                      << std::flush;
            newBar = kTRUE;
        }

        ReadEvent(ev);
        b_fY_cms = b_fY - 1.11788;

        // PR(b_fP_cms);
        // 		TLorentzVector Vec_beam_target = Vec_pp35_beam+Vec_pp35_target;
        // 		Double_t CMS_Beta		= Vec_beam_target.Beta();
        // 		Double_t CMS_Beta_x		=
        // CMS_Beta*sin(Vec_beam_target.Theta())*cos(Vec_beam_target.Phi());	 // x component of
        // BetaVector 		Double_t CMS_Beta_y		=
        // CMS_Beta*sin(Vec_beam_target.Theta())*sin(Vec_beam_target.Phi());	 // y component of
        // BetaVector
        // 		Double_t CMS_Beta_z		= CMS_Beta*cos(Vec_beam_target.Theta());				  // z component
        // of BetaVector
        //
        // 		TLorentzVector trackAB_CMS();
        // 		trackAB_CMS.Boost(-CMS_Beta_x, -CMS_Beta_y, -CMS_Beta_z);
        // 		fY_cms				= trackAB_CMS.Rapidity();
        // 		fP_cms				= trackAB_CMS.P();
        // 		fCosTheta_cms		= cos(trackAB_CMS.Theta());

        // 		if (b_fP_cms <= 1000.)
        // 			continue;

        // secondary vertex cut
        if (!par_fNoVertexCuts and
            !(b_fMinTrackDist < par_fMtd and b_fVertDistX > par_fVertDistX and
              b_fVertDistA > par_fVertDistP and b_fVertDistB > par_fVertDistPi))
        {
            continue;
        }

        if (flag_acc > 1) { b_fGeantInfoNum = flag_acc; }
        else if (flag_pluto > 1)
        {
            b_fGeantInfoNum = flag_pluto;
        }
        else if (flag_fss > 1)
        {
            b_fGeantInfoNum = flag_fss;
        }

        // PR(b_fPrimary); PR(b_fParentID); PR(b_fGParentID);
        if (flag_pluto or flag_acc)
        {
            switch (b_fGeantInfoNum)
            {
                case PC060:
                case PC066:
                case PC080:
                case PC400:
                    if ((b_fPrimary == 1) or !(b_fParentID == 20 and b_fGParentID == 0)) continue;
                    break;
                case PC044:
                    // 					PR(b_fPrimary); PR(b_fParentID);PR(b_fGParentID);PR(b_fM);
                    // 					PR(b_fParentID); PR(b_fGParentID);
                    if ((b_fPrimary == 1) or !(b_fParentID == 20 and b_fGParentID == 0)) continue;
                    break;
                case PC072:
                case PC077:
                case PC088:
                    if (b_fPrimary == 0) continue;
                    break;
                default:
                    if (b_fPrimary == 0) continue;
                    break;
            }
        }

        if (flag_pluto or flag_acc or flag_fss)
        {
            // if (flag_verbose)
            //  PR(b_fGeantInfoNum);
            switch (b_fGeantInfoNum)
            {
                case PC040: // pluto channel 040: pK+Lambda
                case NSTAR1900:
                case NSTAR2190:
                    legandrepoly->SetParameter(0, ch040_par0);
                    legandrepoly->SetParameter(1, ch040_par1);
                    legandrepoly->SetParameter(2, ch040_par2);
                    b_fWeight = legandrepoly->Eval(b_fCosTheta_cms);
                    break;
                case NSTAR1650:
                    legandrepoly->SetParameter(0, ns1650_par0);
                    legandrepoly->SetParameter(1, ns1650_par1);
                    legandrepoly->SetParameter(2, ns1650_par2);
                    b_fWeight = legandrepoly->Eval(b_fCosTheta_cms);
                    break;
                case NSTAR1720:
                    legandrepoly->SetParameter(0, ns1720_par0);
                    legandrepoly->SetParameter(1, ns1720_par1);
                    legandrepoly->SetParameter(2, ns1720_par2);
                    b_fWeight = legandrepoly->Eval(b_fCosTheta_cms);
                    break;
                case PC072: // pluto channel 072: nK+Sigma(1385)+
                case PC077: // pluto channel 072: pK0sSigma(1385)+
                case PC088: // pluto channel 072: pK+Sigma(1385)0
                    legandrepoly->SetParameter(0, ch072_par0);
                    legandrepoly->SetParameter(1, ch072_par1);
                    legandrepoly->SetParameter(2, ch072_par2);
                    b_fWeight = legandrepoly->Eval(b_fCosTheta_cms);
                    break;
                    // 				case PC061:		// If 401 is, why 061 not?
                case PC068:
                case PC401:
                    legandrepoly->SetParameter(0, ch068_par0);
                    legandrepoly->SetParameter(1, ch068_par1);
                    legandrepoly->SetParameter(2, ch068_par2);
                    b_fWeight = legandrepoly->Eval(b_fCosTheta_cms);
                    break;
                case PC400:
                    legandrepoly->SetParameter(0, ch400_par0);
                    legandrepoly->SetParameter(1, ch400_par1);
                    legandrepoly->SetParameter(2, ch400_par2);
                    b_fWeight = legandrepoly->Eval(b_fCosTheta_cms);
                    break;
            }
        }
        // PR(b_fPrimVertexX);PR(flag_exp);
        facPtYcm->Execute(Dim2AnalysisFactory::RECO);
        facPcmCosThcm->Execute(Dim2AnalysisFactory::RECO);
        // 		if (!flag_acc)
        facXcmPcm->Execute(Dim2AnalysisFactory::RECO);
        facVertXVertY->Execute(Dim2AnalysisFactory::RECO);

        hSignalInvMass->Fill(b_fM, b_fWeight);
        hSignalVertexXY->Fill(b_fPrimVertexX, b_fPrimVertexY);
        hSignalVertexXZ->Fill(b_fPrimVertexZ, b_fPrimVertexX);
        hSignalVertexYZ->Fill(b_fPrimVertexZ, b_fPrimVertexY);
    }
    this->events_proceed = ev;

    PR(weightsum);
}

void SecVertexRecoChii::AnaFinalize()
{
    TDirectory::Cd("/");

    // Invariant Mass
    cSignalInvMass->cd();
    RootTools::NicePad(gPad, 0.1, 0.1, 0.1, 0.1);
    hSignalInvMass->Scale(1. / data_scaledown);
    RootTools::NiceHistogram(hSignalInvMass, 1005, 1002, 0.05, 0.005, 0.05, 0.9, 0.05, 0.005, 0.05,
                             0.55);
    hSignalInvMass->Draw(h1opts);

    // Pt-Ycm
    facPtYcm->scale(1. / data_scaledown);

    facPtYcm->niceHists(0.1, 0.13, 0.1, 0.07, 210, 502, 0.05, 0.05, 0.9, 0.05, 0.05, 0.55);
    facPtYcm->niceDiffs(0.1, 0.05, 0.13, 0.1, 505, 1002, 0.08, 0.08, 0.9, 0.07, 0.07, 0.55);
    facPtYcm->niceSlices(0.1, 0.01, 0.13, 0.1, 505, 1002, 0.06, 0.07, 0.9, 0.07, 0.07, 0.55);
    facPtYcm->Finalize(Dim2AnalysisFactory::RECO, flag_details);

    facPtYcm->cSignalXY->cd(0);
    gPad->SetGridx(1);
    gPad->SetGridy(1);
    RootTools::DrawAngleLine(15, 0.615, 0.9, 270);
    RootTools::DrawAngleLine(45, 0.32, 0.55, 300);
    RootTools::DrawAngleLine(85, 0.15, 0.25, 345);
    RootTools::DrawMomentumLine(500, 0.75, 0.3, 325);
    RootTools::DrawMomentumLine(1200, 0.75, 0.5, 336);

    facPtYcm->cSignalCutXY->cd(0);
    gPad->SetGridx(1);
    gPad->SetGridy(1);
    RootTools::DrawAngleLine(15, 0.615, 0.9, 270);
    RootTools::DrawAngleLine(45, 0.32, 0.55, 300);
    RootTools::DrawAngleLine(85, 0.15, 0.25, 345);
    RootTools::DrawMomentumLine(500, 0.75, 0.3, 325);
    RootTools::DrawMomentumLine(1000, 0.75, 0.5, 336);
    RootTools::DrawMomentumLine(1100, 0.75, 0.5, 336);
    RootTools::DrawMomentumLine(1200, 0.75, 0.5, 336);
    RootTools::DrawMomentumLine(1300, 0.75, 0.5, 336);
    RootTools::DrawMomentumLine(1400, 0.75, 0.5, 336);

    // Pcm - CosTheta_cm
    facPcmCosThcm->scale(1. / data_scaledown);

    facPcmCosThcm->niceHists(0.1, 0.13, 0.1, 0.07, 210, 502, 0.05, 0.05, 0.9, 0.05, 0.05, 0.55);
    facPcmCosThcm->niceDiffs(0.1, 0.05, 0.13, 0.1, 505, 1002, 0.08, 0.08, 0.9, 0.07, 0.07, 0.55);
    facPcmCosThcm->niceSlices(0.1, 0.01, 0.13, 0.1, 505, 1002, 0.06, 0.07, 0.9, 0.07, 0.07, 0.55);
    facPcmCosThcm->Finalize(Dim2AnalysisFactory::RECO, flag_details);

    facPcmCosThcm->cSignalXY->cd(0);
    gPad->SetGridx(1);
    gPad->SetGridy(1);

    facPcmCosThcm->cSignalCutXY->cd(0);
    gPad->SetGridx(1);
    gPad->SetGridy(1);

    // Pcm - Xcm
    // 	if (!flag_acc)
    {
        facXcmPcm->scale(1. / data_scaledown);

        facXcmPcm->niceHists(0.1, 0.13, 0.1, 0.07, 210, 502, 0.05, 0.05, 0.9, 0.05, 0.05, 0.55);
        facXcmPcm->niceDiffs(0.1, 0.05, 0.13, 0.1, 505, 1002, 0.08, 0.08, 0.9, 0.07, 0.07, 0.55);
        facXcmPcm->niceSlices(0.1, 0.01, 0.13, 0.1, 505, 1002, 0.06, 0.07, 0.9, 0.07, 0.07, 0.55);
        facXcmPcm->Finalize(Dim2AnalysisFactory::RECO, flag_details);

        facXcmPcm->cSignalXY->cd(0);
        gPad->SetGridx(1);
        gPad->SetGridy(1);

        facXcmPcm->cSignalCutXY->cd(0);
        gPad->SetGridx(1);
        gPad->SetGridy(1);
    }

    {
        facVertXVertY->scale(1. / data_scaledown);

        facVertXVertY->niceHists(0.1, 0.13, 0.1, 0.07, 210, 502, 0.05, 0.05, 0.9, 0.05, 0.05, 0.55);
        facVertXVertY->niceDiffs(0.1, 0.05, 0.13, 0.1, 505, 1002, 0.08, 0.08, 0.9, 0.07, 0.07,
                                 0.55);
        facVertXVertY->niceSlices(0.1, 0.01, 0.13, 0.1, 505, 1002, 0.06, 0.07, 0.9, 0.07, 0.07,
                                  0.55);
        facVertXVertY->Finalize(Dim2AnalysisFactory::RECO, flag_details);

        facVertXVertY->cSignalXY->cd(0);
        gPad->SetGridx(1);
        gPad->SetGridy(1);

        facVertXVertY->cSignalCutXY->cd(0);
        gPad->SetGridx(1);
        gPad->SetGridy(1);
    }

    cSignalVertexXY->cd();
    hSignalVertexXY->Draw("colz");

    cSignalVertexXZ->cd();
    hSignalVertexXZ->Draw("colz");

    cSignalVertexYZ->cd();
    hSignalVertexYZ->Draw("colz");

    OpenExportFile();

    facPtYcm->ctx.Write();
    facPcmCosThcm->ctx.Write();
    facXcmPcm->ctx.Write();

    SaveAndClose(cSignalInvMass);
    hSignalInvMass->Write();

    facPtYcm->write(GetExportFile());
    facPcmCosThcm->write(GetExportFile());
    facXcmPcm->write(GetExportFile());
    // 	facVertXVertY->write(GetExportFile());
    // 	facVertXVertZ->write(GetExportFile());

    hSignalVertexXY->Write();
    hSignalVertexXZ->Write();
    hSignalVertexYZ->Write();

    SaveAndClose(cSignalVertexXY);
    SaveAndClose(cSignalVertexXZ);
    SaveAndClose(cSignalVertexYZ);

    setConfigVector();

    CloseExportFile();
}

void SecVertexRecoChii::Usage()
{
    std::cout << "\nUsage: program args files\n    where args are:\n";

    // 	Int_t s = optsize;
    //
    // 	for (int i = 0; i < s-1; ++i) {
    // 		if (opt_snames[i] != 0)
    // 			printf("\t-%c, --%s	\t\t%s\t\t%s\n", opt_snames[i], opt_lnames[i].Data(),
    // opt_args[i].Data(), opt_desc[i].Data()); 		else 			printf("\t    --%s
    // \t\t%s\t\t%s\n", opt_lnames[i].Data(), opt_args[i].Data(), opt_desc[i].Data());
    //
    // 	}
    // 	std::cout << "\n";
}

void SecVertexRecoChii::PrintAnalysisInfo()
{
    std::cout << colstd << "=======================================================" << resstd
              << std::endl;
    std::cout << colstd << "* Processing " << entries << "/" << chain->GetEntries() << " ( "
              << 100.0 * entries / chain->GetEntries() << " % ) events" << resstd << std::endl;
    std::cout << colstd << "* Secondary Vertex Cuts:" << resstd << std::endl;
    std::cout << colstd << "  MinTrackDist: " << par_fMtd << resstd << std::endl;
    std::cout << colstd << "  VertDistX:    " << par_fVertDistX << resstd << std::endl;
    std::cout << colstd << "  VertDistP:    " << par_fVertDistP << resstd << std::endl;
    std::cout << colstd << "  VertDistPi:   " << par_fVertDistPi << resstd << std::endl;
    std::cout << colstd << "=======================================================" << resstd
              << std::endl;
}

int SecVertexRecoChii::config_params(struct option*& opts)
{
    static struct option lopts[] = {
        /* These options set a flag. */
        // 			{"verbose", no_argument,       &verbose_flag, 1},
        // 			{"brief",   no_argument,       &verbose_flag, 0},
        /* These options don't set a flag.
            We distinguish them by their indices. */
        // 			{"add",     no_argument,       0, 'a'},
        // 			{"append",  no_argument,       0, 'b'},
        {"NoVertexCuts", no_argument, 0, 1000},     {"Mtd", required_argument, 0, 1001},
        {"VertDistX", required_argument, 0, 1002},  {"VertDistP", required_argument, 0, 1003},
        {"VertDistPi", required_argument, 0, 1004}, {"LambdaCutL", required_argument, 0, 2001},
        {"LambdaCutR", required_argument, 0, 2002}, {"ScaleDown", required_argument, 0, 2003},
        {"events", required_argument, 0, 2004}};
    opts = lopts;

    return sizeof(lopts) / sizeof(option);
}

void SecVertexRecoChii::config_execute(int code, char* optarg)
{
    switch (code)
    {
        case 1000:
            par_fNoVertexCuts = kTRUE;
            break;
        case 1001:
            par_fMtd = atoi(optarg);
            break;
        case 1002:
            par_fVertDistX = atoi(optarg);
            break;
        case 1003:
            par_fVertDistP = atoi(optarg);
            break;
        case 1004:
            par_fVertDistPi = atoi(optarg);
            break;
        case 2001:
            par_fLambdaMCutMin = atoi(optarg);
            break;
        case 2002:
            par_fLambdaMCutMax = atoi(optarg);
            break;
        case 2003:
            par_scaledown = atoi(optarg);
            break;
        case 2004:
            events = atoi(optarg);
            break;
        default:
            break;
    }

    if (code) return;

    InitTree();

    TH1F* hCounter = nullptr;

    TString fname(optarg);
    if (fname.EndsWith(".root"))
    {
        this->AddToChain(fname.Data());
        if (flag_verbose) std::cout << "Add file: " << fname.Data() << std::endl;
        hasInputFiles = true;

        if (par_scaledown > 0) { data_scaledown = par_scaledown; }
        else if (!hCounter)
        {
            if (flag_gibuu)
            {
                TFile* fCounter = TFile::Open(fname.Data(), "READ");

                if (fCounter)
                {
                    hCounter = (TH1F*)fCounter->Get("hCounter");
                    if (hCounter) data_scaledown = hCounter->GetEntries();
                    fCounter->Close();
                }
                else
                {
                    // 				data_scaledown = 1e9;
                }
            }
            else if (flag_urqmd)
            {
                TFile* fCounter = TFile::Open(fname.Data(), "READ");

                if (fCounter)
                {
                    hCounter = (TH1F*)fCounter->Get("hCounter");
                    if (hCounter)
                        data_scaledown = hCounter->GetBinContent(hCounter->GetMaximumBin());
                    fCounter->Close();
                }
                else
                {
                    data_scaledown = 1e9;
                }
            }
        }
    }
    else if (fname.EndsWith(".tlist"))
    {
        std::ifstream tlf(fname.Data());
        std::string fff;
        while (!tlf.eof())
        {
            tlf >> fff;
            this->AddToChain(fff.c_str());
            if (flag_verbose) std::cout << "Add file: " << fff.c_str() << std::endl;
            hasInputFiles = true;
        }
    }

    if (flag_gibuu) par_fNoVertexCuts = kTRUE;
}
