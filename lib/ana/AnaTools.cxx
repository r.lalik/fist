/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __CINT__

#include "getopt.h"
#include <fstream>
#include <string>

#include "TCanvas.h"
#include "TChain.h"
#include "TDirectory.h"
#include "TError.h"
#include "TF1.h"
#include "TGaxis.h"
#include "TH2.h"
#include "TImage.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TMath.h"
#include "TString.h"
#include "TStyle.h"
#include "TVector.h"

#endif /* __CINT__ */

#include "RootTools.h"

#include "AnaTools.h"

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

// Processing data bar width
// const Int_t bar_dotqty = 10000;
// const Int_t bar_width = 20;

void pp35Tools::FitInvMass(TH1* hist, TCanvas* can, TH1* params)
{
    const Int_t fit_invmassmin = 1095;
    const Int_t fit_invmassmax = 1150;

    TF1* tfLambdaSig = new TF1("tfLambdaSig", "gaus(0)+gaus(3)", fit_invmassmin, fit_invmassmax);
    tfLambdaSig->SetLineColor(kBlack);
    tfLambdaSig->SetLineWidth(1);
    tfLambdaSig->SetFillColor(kGray);
    tfLambdaSig->SetFillStyle(3000);

    TF1* tfLambdaBg = new TF1("tfLambdaBg", "landau(0)+pol1(3)", fit_invmassmin, fit_invmassmax);
    tfLambdaBg->SetLineColor(kRed);
    tfLambdaBg->SetLineWidth(1);

    // init params
    tfLambdaSig->SetParameter(0, hist->GetBinContent(hist->GetMaximumBin()));
    tfLambdaSig->SetParameter(1, 1115);
    tfLambdaSig->SetParameter(2, 10);
    tfLambdaSig->SetParLimits(1, 1113, 1117);
    tfLambdaSig->SetParLimits(2, 0.5, 6.0);

    tfLambdaSig->SetParameter(3, hist->GetBinContent(hist->GetMaximumBin()) / 10);
    tfLambdaSig->SetParameter(4, 1115);
    tfLambdaSig->SetParameter(5, 50);
    tfLambdaSig->SetParLimits(4, 1110, 1120);
    // 	tfLambdaSig->SetParLimits(2, 0.5, 6.0);

    // 	tfLambdaBg->SetParameter(0, 100);
    // 	tfLambdaBg->SetParameter(1, -0.05);
    // 	tfLambdaBg->SetParameter(2, 0);
    tfLambdaBg->SetParameter(0, 10.0);
    tfLambdaBg->SetParameter(1, 1000.0);
    tfLambdaBg->SetParameter(2, 100.0);

    // prepare fit fucntion
    TF1* tfInvMass = new TF1("tfInvMass", "tfLambdaBg+tfLambdaSig", fit_invmassmin, fit_invmassmax);
    tfInvMass->SetLineColor(kRed);
    tfInvMass->SetLineWidth(1);
    tfInvMass->SetParLimits(6, 1113, 1117);
    tfInvMass->SetParLimits(7, 0.5, 4.0);
    tfInvMass->SetParLimits(9, 1113, 1117);
    tfInvMass->SetParLimits(10, 0.5, 20.0);
    // init params and fit
    hist->Fit(tfInvMass, "N,B", "", fit_invmassmin, fit_invmassmax);

    // copy parameters from fit to base fucntions
    Int_t bgparnum = tfLambdaBg->GetNpar();
    for (Int_t n = 0; n < tfLambdaBg->GetNpar(); ++n)
        tfLambdaBg->SetParameter(n, tfInvMass->GetParameter(n));
    for (Int_t n = 0; n < tfLambdaSig->GetNpar(); ++n)
        tfLambdaSig->SetParameter(n, tfInvMass->GetParameter(bgparnum + n));

    Float_t fLambdaFitA = tfLambdaSig->GetParameter(0);
    Float_t fLambdaFitM = tfLambdaSig->GetParameter(1);
    Float_t fLambdaFitS = tfLambdaSig->GetParameter(2);

    can->cd();
    tfInvMass->Draw("same");
    tfLambdaSig->Draw("same");
    tfLambdaBg->Draw("same");

    // 	tfLambdaBg->SetParameter(1, tfInvMass->GetParameter(4));

    TLatex* latex = new TLatex();
    latex->SetNDC();
    latex->SetTextSize(0.03);
    latex->DrawLatex(0.70, 0.60, "Gauss:");
    latex->DrawLatex(0.70, 0.55, "A =");
    latex->DrawLatex(0.70, 0.50, "#mu =");
    latex->DrawLatex(0.70, 0.45, "#sigma =");
    latex->DrawLatex(0.78, 0.55, TString::Format("%.2f", fLambdaFitA));
    latex->DrawLatex(0.78, 0.50, TString::Format("%.2f", fLambdaFitM));
    latex->DrawLatex(0.78, 0.45, TString::Format("%.2f", fLambdaFitS));

    // calculate S/B
    Float_t fSBmin = fLambdaFitM - 3.0 * fLambdaFitS;
    Float_t fSBmax = fLambdaFitM + 3.0 * fLambdaFitS;

    Float_t fInvMassS = tfLambdaSig->Integral(fSBmin, fSBmax);
    Float_t fInvMassB = tfLambdaBg->Integral(fSBmin, fSBmax);

    latex->DrawLatex(0.70, 0.30, "S/B:");
    latex->DrawLatex(0.70, 0.25, "S =");
    latex->DrawLatex(0.70, 0.20, "B =");
    latex->DrawLatex(0.70, 0.15, "S/B =");
    latex->DrawLatex(0.78, 0.25, TString::Format("%.2f", fInvMassS));
    latex->DrawLatex(0.78, 0.20, TString::Format("%.2f", fInvMassB));
    latex->DrawLatex(0.78, 0.15, TString::Format("%.2f", fInvMassS / fInvMassB));

    if (params)
    {
        params->SetBinContent(1, fInvMassS);
        params->GetXaxis()->SetBinLabel(1, "S");
        params->SetBinContent(2, fInvMassB);
        params->GetXaxis()->SetBinLabel(2, "B");
        params->SetBinContent(3, fSBmin);
        params->GetXaxis()->SetBinLabel(3, "SBmin");
        params->SetBinContent(4, fSBmax);
        params->GetXaxis()->SetBinLabel(4, "SBmax");
        params->SetBinContent(5, fLambdaFitA);
        params->GetXaxis()->SetBinLabel(5, "gA");
        params->SetBinContent(6, fLambdaFitM);
        params->GetXaxis()->SetBinLabel(6, "GM");
        params->SetBinContent(7, fLambdaFitS);
        params->GetXaxis()->SetBinLabel(7, "gS");
        for (Int_t n = 0; n < tfLambdaBg->GetNpar(); ++n)
        {
            params->SetBinContent(8 + n, tfLambdaBg->GetParameter(0));
            params->GetXaxis()->SetBinLabel(8, TString::Format("pC%d", n));
        }
    }

    // show S/B lines on canvas
    TLine* tlSBline = new TLine();
    tlSBline->SetLineStyle(7);
    tlSBline->DrawLine(fSBmin, 0., fSBmin, fLambdaFitA);
    tlSBline->DrawLine(fSBmax, 0., fSBmax, fLambdaFitA);
}

Double_t pp35Tools::LegendrePolynomials5_FitFunc(Double_t* x_val, Double_t* par)
{
    Double_t x, y, par0, par1, par2, par3, par4, par5;
    par0 = par[0];
    par1 = par[1];
    par2 = par[2];
    par3 = par[3];
    par4 = par[4];
    par5 = par[5];

    x = x_val[0];
    y = par0 * 1.0 + par1 * x + par2 * 0.5 * ((3 * x * x) - 1) +
        par3 * 0.5 * ((5 * pow(x, 3)) - 3 * x) +
        par4 * (1 / 8) * (35 * pow(x, 4) - 30 * pow(x, 2) + 3) +
        par5 * (1 / 8) * (63 * pow(x, 5) - 70 * pow(x, 3) - 15 * x);
    return y;
}