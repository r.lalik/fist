/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "AECalculator.h"

#include <cstdlib>

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

int main(int argc, char* argv[])
{
    // 	TROOT AnalysisDST_Cal1("TreeAnalysis","compiled anaysisDST macros");
    // 	TApplication app("treeana", NULL, NULL, NULL, 0);

    AECalculator ana;
    ana.Configure(argc, argv);

    ana.Proceed();
    std::exit(EXIT_SUCCESS);
}