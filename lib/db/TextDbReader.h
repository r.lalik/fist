#ifndef TEXTDBREADER_H
#define TEXTDBREADER_H

#include <TObjArray.h>
#include <TObjString.h>
#include <TString.h>

#include <AnaDatabase.h>

class TextDbReader : public AnaDatabase
{
public:
    TextDbReader() {}
    TextDbReader(const char* dbfilename) : AnaDatabase(dbfilename) {}
    TextDbReader(const std::string& dbfilename) : AnaDatabase(dbfilename) {}
    virtual ~TextDbReader() {}

    virtual void prepare();
};

#endif /* TEXTDBREADER_H */