/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ToolsAbstract.h"

#include <DistributionContext.h>
#include <DistributionFactory.h>

#include <DifferentialContext.h>
#include <DifferentialFactory.h>

#include <TCanvas.h>
#include <TColor.h>
#include <TF1.h>
#include <TFile.h>
#include <TGaxis.h>
#include <TH1.h>
#include <TH2.h>
#include <TLatex.h>
#include <TObjString.h>
#include <TPad.h>
#include <TStyle.h>

#include <getopt.h>

#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <functional>
#include <iostream>

#include <sys/stat.h>

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

#undef NDEBUG
#include <assert.h>

#define test_array(arr, size) assert((sizeof(arr) / sizeof(arr[0])) == (size));

int mkpath(std::string s, mode_t mode)
{
    size_t pre = 0, pos;
    std::string dir;
    int mdret = 0;

    if (s[s.size() - 1] != '/')
    {
        // force trailing / so we can handle everything in loop
        s += '/';
    }

    while ((pos = s.find_first_of('/', pre)) != std::string::npos)
    {
        dir = s.substr(0, pos++);
        pre = pos;
        if (dir.size() == 0) continue; // if leading / first time is 0 length
        if ((mdret = mkdir(dir.c_str(), mode)) && errno != EEXIST) { return mdret; }
    }
    return mdret;
}

ToolsAbstract::ToolsAbstract()
    : SimpleToolsAbstract(), flag_exp(0), flag_null(0), flag_sim(0), flag_acc(0), flag_fss(0),
      flag_pluto(0), flag_gibuu(0), flag_urqmd(0), flag_pwa(0)
{
    // exp data
    add_cmdl_option({"exp", no_argument, &flag_exp, 1}, "Exp data");
    add_cmdl_option({"null", no_argument, &flag_null, 1}, "Empty target data");
    // sim data type ; sim, acceptance, final simulation
    add_cmdl_option({"sim", no_argument, &flag_sim, 1}, "Simulations");
    add_cmdl_option({"acc", no_argument, &flag_acc, 1}, "Acceptance data");
    add_cmdl_option({"fss", no_argument, &flag_fss, 1}, "Final stage simulation data");
    // sim data - pluto, urqmd or gibuu
    add_cmdl_option({"pluto", optional_argument, 0, 501}, "Pluto data");
    add_cmdl_option({"gibuu", optional_argument, 0, 502}, "GIBUU data");
    add_cmdl_option({"urqmd", optional_argument, 0, 503}, "UrQMD data");
    add_cmdl_option({"pwa", optional_argument, 0, 504}, "PWA data");

    add_cmdl_option_handler(std::bind(&ToolsAbstract::config_handler, this, std::placeholders::_1,
                                      std::placeholders::_2));
}

ToolsAbstract::~ToolsAbstract() {}

TCanvas* ToolsAbstract::MakeCanvas(const TString& name, const TString& title, int width, int height)
{
    TCanvas* can = new TCanvas(name, title, width, height);
    can->cd();

    if (!label.IsNull())
    {
        TLatex* l = new TLatex;
        l->SetTextSize(0.025);
        l->SetNDC();
        l->DrawLatex(0.02, 0.01, label);
    }

    return can;
}

void ToolsAbstract::proceed()
{
    initialize();

    print_basic_info();

    execute();

    finalize();
    std::cout << std::endl << colstd << "Analysis finished" << resstd << std::endl;
    std::cout << "=======================================================\n";
    // 	resstd();
}

bool ToolsAbstract::config_handler(int code, const char* optarg)
{
    switch (code)
    {
        case 501:
            if (optarg)
                flag_pluto = std::atoi(optarg);
            else
                flag_pluto = 1;
            break;
        case 502:
            if (optarg)
                flag_gibuu = std::atoi(optarg);
            else
                flag_gibuu = 1;
            break;
        case 503:
            if (optarg)
                flag_urqmd = std::atoi(optarg);
            else
                flag_urqmd = 1;
            break;
        case 504:
            if (optarg)
                flag_pwa = std::atoi(optarg);
            else
                flag_pwa = 1;
            break;
        case '?':
        default:
            return false;
    }

    return true;

    std::cout << "+ Selected modes\n";
    std::cout << "  Exp: " << bool(flag_exp) << "  ||";
    std::cout << "  Sim: " << bool(flag_sim);
    std::cout << "  Acc: " << bool(flag_acc);
    std::cout << "  Fss: " << bool(flag_fss) << "  ||";

    std::cout << "  Pluto: " << bool(flag_pluto) << " " << (flag_pluto ? flag_pluto : 0) << "  ||";
    std::cout << "  UrQMD: " << bool(flag_urqmd) << " " << (flag_urqmd ? flag_urqmd : 0) << "  ||";
    std::cout << "  GiBUU: " << bool(flag_gibuu) << " " << (flag_gibuu ? flag_gibuu : 0) << "  ||";
    std::cout << "  PWA: " << bool(flag_pwa) << " " << (flag_pwa ? flag_pwa : 0) << std::endl;

    if (!(flag_pluto or flag_gibuu or flag_urqmd or flag_pwa))
    {
        std::cerr << "* No pluto, gibuu, urqmd or pwa mode enabled, forcing Exp mode." << std::endl;
        flag_exp = 1;
    }

    if (flag_exp)
    {
        flag_pluto = flag_gibuu = flag_urqmd = flag_pwa = 0;
        flag_sim = flag_acc = flag_fss = 0;
    }
    else
    {
        if ((1 * (bool)flag_pluto + 1 * (bool)flag_gibuu + 1 * (bool)flag_urqmd +
             1 * (bool)flag_pwa) > 1)
        {
            std::cerr << "* Select only on of pluto, gibuu, urqmd or pwa." << std::endl;
            std::exit(EXIT_FAILURE);
        }

        if ((1 * (bool)flag_sim + 1 * (bool)flag_acc + 1 * (bool)flag_fss) > 1)
        {
            std::cerr << "* Select only on of sim, acc, fss." << std::endl;
            std::exit(EXIT_FAILURE);
        }
    }

    std::cout << "+ Accepted modes\n";
    std::cout << "  Exp: " << bool(flag_exp) << "  ||";
    std::cout << "  Sim: " << bool(flag_sim);
    std::cout << "  Acc: " << bool(flag_acc);
    std::cout << "  Fss: " << bool(flag_fss) << "  ||";

    std::cout << "  Pluto: " << bool(flag_pluto) << " " << (flag_pluto ? flag_pluto : 0) << "  ||";
    std::cout << "  UrQMD: " << bool(flag_urqmd) << " " << (flag_urqmd ? flag_urqmd : 0) << "  ||";
    std::cout << "  GiBUU: " << bool(flag_gibuu) << " " << (flag_gibuu ? flag_gibuu : 0) << "  ||";
    std::cout << "  PWA: " << bool(flag_pwa) << " " << (flag_pwa ? flag_pwa : 0) << std::endl;

    //     while (optind < argc)
    //     {
    //         config_execute(0, argv[optind++]);
    //     }

    return 0;
}

TH1F* ToolsAbstract::set_config_vector(TFile* f, TString cfgvector)
{
    cfgvec = nullptr;
    if (f)
        f->GetObject(cfgvector, cfgvec);
    else
        gDirectory->GetObject(cfgvector, cfgvec);

    const Int_t cfgsize = 40;
    if (!cfgvec) { cfgvec = new TH1F(cfgvector.Data(), "Config Vector", cfgsize, 0, cfgsize); }

    // 	cfgvec->Write();

    return cfgvec;
}

void ToolsAbstract::get_config_vector(TFile* f, TString cfgvector)
{
    cfgvec = nullptr;
    if (f)
        f->GetObject(cfgvector, cfgvec);
    else
        gDirectory->GetObject(cfgvector, cfgvec);

    if (!cfgvec)
    {
        std::cerr << "No vector " << cfgvector << std::endl;
        std::exit(EXIT_FAILURE);
    }

    can_width = cfgvec->GetBinContent(31);
    can_height = cfgvec->GetBinContent(32);
}
