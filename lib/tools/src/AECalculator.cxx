/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "AECalculator.h"

#ifndef __CINT__

#include <algorithm>
#include <exception>
#include <fstream>
#include <string>
#include <vector>
// #include <sstrings>

#include "getopt.h"

#include "TCanvas.h"
#include "TChain.h"
#include "TDirectory.h"
#include "TError.h"
#include "TF1.h"
#include "TFile.h"
#include "TGaxis.h"
#include "TH2.h"
#include "TImage.h"
#include "TKey.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TMap.h"
#include "TMath.h"
#include "TMinuit.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TVector.h"
#include "TVirtualFitter.h"
#include <TMatrixD.h>
#include <TRandom.h>

#include "TGraphAsymmErrors.h"
#include <TSystem.h>

#endif /* __CINT__ */

#include "RootTools.h"

#include "LambdaFitter.h"
#include "globals.h"
#include <DatabaseManager.h>
#include <DifferentialContext.h>
#include <DifferentialFactory.h>

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

const Option_t h1opts[] = "h,E";

const Float_t lamass = 1115.683;

const size_t houtnum = 2;
const std::string houtnames[houtnum] = {"@@@a/h_@@@a_Correction", "@@@a/h_@@@a_AECorrection"};

TMinuit* gmin = 0;
size_t global_npar = 0;
size_t global_nsimult = 1;

TH2** hist_ref;

const size_t ang_cnt = 3;

const bool integral_only = false;

// void fit_callback(DifferentialFactory * fac, DistributionFactory * sigfac, int fit_res, TH1 * h,
// int x_pos, int y_pos, int z_pos)
// {
// 	// declare functions for fit and signal
// 	TF1 * tfLambdaSum = nullptr;
// 	TF1 * tfLambdaSig = nullptr;
//
// 	if (fit_res)
// 	{
// 		tfLambdaSum = (TF1*)h->GetListOfFunctions()->At(0);
// 		tfLambdaSig = (TF1*)h->GetListOfFunctions()->At(1);
// 	}
// 	else
// 		return;
//
// 	FitResultData frd;
//
// 	if (!integral_only)
// 	{
// 		// get signal and mean value from fit
// 		// parameters are determined here, sadly no place for flexibility
// 		double signal = tfLambdaSig->GetParameter(0);
// 		double mean = tfLambdaSig->GetParameter(1);
// 		// relative fractions
// 		double r = tfLambdaSig->GetParameter(4);
// 		double q = 1.0 - r;
//
// 		// sigmas
// 		double s1 = TMath::Abs(tfLambdaSig->GetParameter(2));
// 		double s2 = TMath::Abs(tfLambdaSig->GetParameter(5));
//
// 		// averaged width
// 		double width = (r*s1*s1 + q*s2*s2)/(r*s1 + q*s2);
//
// 		// errors
// 		double signal_err = tfLambdaSig->GetParError(0);
//
// 		// find bin width at the mean value
// 		int fSBmean = h->FindBin(mean);
// 		double bin_width = h->GetBinWidth(fSBmean);
//
// 		// fil return structure and say 'bye bye'
// 		frd.fit_ok = true;
// 		frd.mean = mean;
// 		frd.sigma = width;
// 		frd.signal = signal / bin_width;
// 		frd.signal_err = signal_err / bin_width;
// 		frd.chi2 = tfLambdaSum->GetChisquare();
// 		frd.ndf = tfLambdaSum->GetNDF();
//
// 		std::cout << "    Signal: " << frd.signal << " +/- " << frd.signal_err << std::endl;
//
// // 		fac->hSliceXYDiff[x_pos]->SetBinContent(1+y_pos, frd.signal); FIXME
// // 		fac->hSliceXYDiff[x_pos]->SetBinError(1+y_pos, frd.signal_err);
// // 		fac->hDiscreteXYSig->SetBinContent(1+x_pos, 1+y_pos, frd.signal);
// // 		fac->hDiscreteXYSig->SetBinError(1+x_pos, 1+y_pos, frd.signal_err);
//
// 		if (frd.mean != 0)
// 		{
// // 			fac->hSliceXYFitQA[x_pos]->SetBinContent(1+y_pos, frd.mean); FIXME
// // 			fac->hSliceXYFitQA[x_pos]->SetBinError(1+y_pos, frd.sigma);
// // 			fac->hSliceXYChi2NDF[x_pos]->SetBinContent(1+y_pos, frd.chi2/frd.ndf);
// 		}
// 		else
// 		{
// // 			TVirtualPad * pad = fac->cDiscreteXYDiff[x_pos]->cd(y_pos); FIXME
// // 			pad->SetFillColor(42);
// 		}
// 	}
// 	else
// 	{
// 		frd.signal = fac->diffs->get(x_pos, y_pos)->Integral();
//
// 		if (frd.signal < 0)  // FIXME old value 500
// 		{
// 			frd.signal = 0;
// 		}
//
// 		frd.signal_err = RootTools::calcTotalError(
// 			fac->diffs->get(x_pos, y_pos), 1, fac->diffs->get(x_pos, y_pos)->GetNbinsX() );
// // 		fac->hSliceXYDiff[x_pos]->SetBinContent(1+y_pos, frd.signal); FIXME
// // 		fac->hSliceXYDiff[x_pos]->SetBinError(1+y_pos, frd.signal_err);
// // 		fac->hDiscreteXYSig->SetBinContent(1+x_pos, 1+y_pos, frd.signal); FIXME Sig here
// // 		fac->hDiscreteXYSig->SetBinError(1+x_pos, 1+y_pos, frd.signal_err);
// 	}
//
// }

double calcSimulationError() { return 0.0; };

auto createDumpFileNames(std::string dump_parameter) -> std::vector<std::string>
{
    auto strvec = RootTools::split(dump_parameter, ',');
    if (!strvec.size()) abort();

    std::string dump_file = strvec[0];

    char buff[200];
    if (strvec.size() > 1)
        sprintf(buff, "_%s.", strvec[1].c_str());
    else
        sprintf(buff, "_%s.", "fitres");

    size_t dot_pos = dump_file.find_last_of('.');
    if (dot_pos != dump_file.npos)
        dump_file.replace(dot_pos, 1, buff);
    else
        dump_file += buff;

    std::vector<std::string> dump_strings;
    dump_strings.push_back(strvec[0]);
    dump_strings.push_back(dump_file);

    return dump_strings;
}

void ChannelModifier::reset()
{
    has_cs_scalings = false;
    cs_scalings = 1.0;
    fit_cs_scalings = 1.0;
    cs_errors_u = 0.01;
    cs_errors_l = 0.01;
    cs_limits_u = 1.01;
    cs_limits_l = 0.99;

    has_ang_scalings = false;
    ang_scalings[0] = 1.0;
    ang_scalings[1] = 0.0;
    ang_scalings[2] = 0.0;
    ang_errors_u[0] = 0.01;
    ang_errors_l[0] = 0.01;
    ang_errors_u[1] = 0.01;
    ang_errors_l[1] = 0.01;
    ang_errors_u[2] = 0.01;
    ang_errors_l[2] = 0.01;
    fit_ang_scalings[0] = 1.0;
    fit_ang_scalings[1] = 0.0;
    fit_ang_scalings[2] = 0.0;
}

struct SystData
{
    double syst_mean[100][100][100];
    double syst_rms[100][100][100];
} * syst_data;

AECalculator::AECalculator()
    : SimpleToolsAbstract(), flag_nosum(1), flag_noscale(0), flag_nocsrand(0), flag_noangrand(0),
      par_max_scale(1.2), par_min_scale(2.0), dumpdb(nullptr), par_iter_number(1)
{
    set = new AESet[ananum];
    hist_ref = new TH2*[ananum];
    syst_data = new SystData[ananum];

    for (size_t a = 0; a < ananum; ++a)
    {
        set[a].cfg_maxy = -1;
        set[a].cfg_miny = 1;

        std::string name = ananames[a] + "Proj";
        set[a].projs = new SmartFactory(name.c_str());
    }

    NicePalette();
    TGaxis::SetMaxDigits(3);

    RootTools::AxisFormat x(15, 0.08, 0.005, 0.08, 0.6, kFALSE, kTRUE);
    RootTools::AxisFormat y(504, 0.08, 0.005, 0.08, 0.55, kFALSE, kTRUE);
    RootTools::AxisFormat z(15, 0.08, 0.005, 0.08, 0.6, kFALSE, kTRUE);

    RootTools::PadFormat padf = {0.08, 0.05, 0.1, 0.15};
    RootTools::GraphFormat graphf = {x, y, z};
    pf.pf = padf;
    pf.gf = graphf;

    srand(time(0));
}

AECalculator::~AECalculator() {}

void AECalculator::AnaInitialize()
{
    if (configfile_t.IsNull())
    {
        std::cout << "No config file!" << std::endl;
        std::exit(EXIT_FAILURE);
    }

    cdfdata = new InputData();

    std::ifstream fcfgfile(configfile_t.Data());
    if (!fcfgfile.is_open())
    {
        std::cerr << "File " << configfile_t << " is not open!";
        std::exit(EXIT_FAILURE);
    }

    SetOutputFile("AEresults.root");

    TString filetmp;
    std::string line;
    while (std::getline(fcfgfile, line))
    {
        if (!line.size()) continue;

        TString line_(line);
        line_.ReplaceAll("\t", " ");

        TObjArray* arr = line_.Tokenize(" ");

        char cmdchar = ((TObjString*)arr->At(0))->String()[0];
        TString tmpstr;
        char testchar = 0;
        bool usecorr = true;
        float scale = 1.0;

        EntryData ed_sim, ed_fss;

        switch (cmdchar)
        {
            case '#':
                continue;
                break;

            case 'c':
                if (((TObjString*)arr->At(1))->String() == "maxy")
                {
                    set[((TObjString*)arr->At(2))->String().Atoi()].cfg_maxy =
                        ((TObjString*)arr->At(3))->String().Atof();
                }
                if (((TObjString*)arr->At(1))->String() == "miny")
                {
                    set[((TObjString*)arr->At(2))->String().Atoi()].cfg_miny =
                        ((TObjString*)arr->At(3))->String().Atof();
                }

                break;

            case 'f':
                par_fit_params = ((TObjString*)arr->At(1))->String().Data();
                break;

            case 'n':
                par_iter_number = ((TObjString*)arr->At(1))->String().Atoi();
                break;

            default:
                testchar = ((TObjString*)arr->At(0))->String()(0);
                // if it is a letter
                if (testchar >= 'A' and testchar <= 'Z')
                {
                    usecorr = false;
                    testchar = testchar - 'A' + 'a';
                }
                ++global_npar;

                ed_sim.id = ((TObjString*)arr->At(1))->String().Data();
                TString fname = ((TObjString*)arr->At(2))->String();
                ed_sim.filename = fname.Data();
                ed_sim.attr = ((TObjString*)arr->At(3))->String().Data();
                ed_sim.use_corr = usecorr;

                // scaling factor for histogram
                if (arr->GetEntries() >= 5)
                {
                    TString str_scale = ((TObjString*)arr->At(4))->String();
                    scale = str_scale.Atof();
                }
                else
                {
                    scale = 1.0;
                }
                ed_sim.scale = scale;

                ed_fss = ed_sim;
                ed_fss.filename = fname.ReplaceAll("/sim/", "/fss/");

                cdfdata->entries_sim.push_back(ed_sim);
                cdfdata->entries_fss.push_back(ed_fss);

                std::cout << " " << ed_sim.id << " " << filetmp << " " << ed_sim.scale << " "
                          << ed_sim.filename << std::endl;
                break;
        }
    }

    chanmod = new ChannelModifier[global_npar];

    for (size_t j = 0; j < global_npar; ++j)
    {
        chanmod[j].reset();
    }

    int entrynum = cdfdata->entries_sim.size();

    const char* fn;

    for (int e = 0; e < entrynum; ++e)
    {
        // get input (par) histogram from the sim channel
        for (size_t a = 0; a < ananum; ++a)
        {
            std::string tmpname = TString::Format("%s/h_%s_LambdaInvMassSig", ananames[a].c_str(),
                                                  ananames[a].c_str())
                                      .Data();
            chanmod[e].label = cdfdata->entries_sim[e].id;

            // fetch cross sections, do it only once
            if (a == 0)
            {
                // this 1d histogram holds in bins:
                // [1] - cs, [2] - upper limit (from errors), [3] - lower limit

                fn = cdfdata->entries_sim[e].filename.c_str();
                TFile* f = TFile::Open(fn, "READ");

                TH1* htemp_cs = (TH1D*)SmartFactory::getObject(f, "h_cs");
                // If exists set scaling parameters, we scale down histogram by its cross-sections
                // which later is varied in the fit. Later on histogram must be scaled by fit result
                if (htemp_cs)
                {
                    chanmod[e].has_cs_scalings = true;
                    chanmod[e].cs_scalings =
                        htemp_cs->GetBinContent(1) * cdfdata->entries_sim[e].scale;
                    chanmod[e].cs_errors_u =
                        htemp_cs->GetBinContent(2) * cdfdata->entries_sim[e].scale;
                    chanmod[e].cs_errors_l =
                        htemp_cs->GetBinContent(3) * cdfdata->entries_sim[e].scale;
                    chanmod[e].cs_limits_u = chanmod[e].cs_scalings + chanmod[e].cs_errors_u;
                    chanmod[e].cs_limits_l = chanmod[e].cs_scalings - chanmod[e].cs_errors_l;
                    printf(" [cs] Scaling for %s is %f +%f -%f\n", fn, chanmod[e].cs_scalings,
                           chanmod[e].cs_limits_u, chanmod[e].cs_limits_l);
                }
                else
                {
                    printf("No CS found, default scaling for %s is %f\n", fn,
                           chanmod[e].cs_scalings);
                }

                TH2* htemp_ang = (TH2D*)SmartFactory::getObject(f, "h_ang");
                // If exists set scaling parameters, we scale down histogram by its cross-sections
                // which later is varied in the fit. Later on histogram must be scaled by fit result
                if (htemp_ang)
                {
                    chanmod[e].has_ang_scalings = true;
                    chanmod[e].ang_scalings[0] = htemp_ang->GetBinContent(1, 2);
                    chanmod[e].ang_scalings[1] = htemp_ang->GetBinContent(2, 2);
                    chanmod[e].ang_scalings[2] = htemp_ang->GetBinContent(3, 2);

                    chanmod[e].ang_errors_u[0] = htemp_ang->GetBinContent(1, 3);
                    chanmod[e].ang_errors_l[0] = htemp_ang->GetBinContent(1, 1);
                    chanmod[e].ang_errors_u[1] = htemp_ang->GetBinContent(2, 3);
                    chanmod[e].ang_errors_l[1] = htemp_ang->GetBinContent(2, 1);
                    chanmod[e].ang_errors_u[2] = htemp_ang->GetBinContent(3, 3);
                    chanmod[e].ang_errors_l[2] = htemp_ang->GetBinContent(3, 1);

                    chanmod[e].ang_limits_u[0] =
                        chanmod[e].ang_scalings[0] + chanmod[e].ang_errors_u[0];
                    chanmod[e].ang_limits_l[0] =
                        chanmod[e].ang_scalings[0] - chanmod[e].ang_errors_l[0];
                    chanmod[e].ang_limits_u[1] =
                        chanmod[e].ang_scalings[1] + chanmod[e].ang_errors_u[1];
                    chanmod[e].ang_limits_l[1] =
                        chanmod[e].ang_scalings[1] - chanmod[e].ang_errors_l[1];
                    chanmod[e].ang_limits_u[2] =
                        chanmod[e].ang_scalings[2] + chanmod[e].ang_errors_u[2];
                    chanmod[e].ang_limits_l[2] =
                        chanmod[e].ang_scalings[2] - chanmod[e].ang_errors_l[2];
                }
                else
                {
                    printf("No CS found, default scaling for %s is %f, %f, %f\n", fn,
                           chanmod[e].ang_scalings[0], chanmod[e].ang_scalings[1],
                           chanmod[e].ang_scalings[2]);
                }
                f->Close();
            }
        }
    }

    TSystem sys;
    par_fit_export = std::string(export_dir.Data()) + sys.BaseName(par_fit_params.c_str());

    // import simulation factories
    printf("Importing reference analyses (%lu): ", global_npar);
    for (size_t e = 0; e < global_npar; ++e)
    {
        // sim
        for (uint i = 0; i < ananum; ++i)
        {
            cdfdata->entries_sim[e].ctx = nullptr;
            cdfdata->entries_fss[e].ctx = nullptr;
        }

        cdfdata->entries_sim[e].file =
            ImportFromFile(cdfdata->entries_sim[e].filename.c_str(), cdfdata->entries_sim[e].ctx,
                           cdfdata->entries_sim[e].fac, "_DiscCtx", true);
        if (!cdfdata->entries_sim[e].file)
        {
            std::cerr << "File " << cdfdata->entries_sim[e].filename << " not open." << std::endl;
            abort();
        }
        // fss
        cdfdata->entries_fss[e].file =
            ImportFromFile(cdfdata->entries_fss[e].filename.c_str(), cdfdata->entries_fss[e].ctx,
                           cdfdata->entries_fss[e].fac, "_DiscCtx", (e != 0));
        if (!cdfdata->entries_fss[e].file)
        {
            std::cerr << "File " << cdfdata->entries_fss[e].filename << " not open." << std::endl;
            abort();
        }

        for (uint i = 0; i < ananum; ++i)
        {
            //       cdfdata->entries_sim[e].fac[i]->init();
            //       cdfdata->entries_sim[e].fac[i]->rename(TString::Format("%s_sim_e%ld_a%d_DiscCtx",
            //       cdfdata->entries_sim[e].ctx[i]->name.Data(), e, i).Data());
            //       cdfdata->entries_fss[e].fac[i]->rename(TString::Format("%s_fss_e%ld_a%d_DiscCtx",
            //       cdfdata->entries_fss[e].ctx[i]->name.Data(), e, i).Data());
        }

        if (e % 10 == 0)
            printf("|");
        else
            printf("=");
        fflush(stdout);
    }
    printf("\n");
}

void AECalculator::AnaExecute()
{
    if (global_npar == 0) return;

    // what is this? TODO
    ae_data* trials_aem = new ae_data;

    legpol = new TF1("legpol", "angdist", -1, 1);

    // 	PR(par_fit_params.c_str());
    // 	PR(par_fit_export.c_str());
    // trial preparation
    ff.setVerbose(false);
    ff.initFactoryFromFile(par_fit_params.c_str(), par_fit_export.c_str());
    HistFitParams::printStats();

    // create backup contexes
    DifferentialContext ctx[ananum];
    for (size_t a = 0; a < ananum; ++a)
    {
        ctx[a] = cdfdata->entries_fss[0].fac[a]->ctx;
        ctx[a].hist_name = cdfdata->entries_fss[0].fac[a]->ctx.hist_name;
        ctx[a].update();
    }

    TString tmpname;
    // create factories for AE matrix
    for (size_t a = 0; a < ananum; ++a)
    {
        // reference aem
        DifferentialContext ctx_aem_div(ctx[a]);
        tmpname = ctx[a].name;
        tmpname.ReplaceAll("Disc", "SignalDiv");
        ctx_aem_div.name = tmpname;
        ctx_aem_div.hist_name.Clear();
        ctx_aem_div.update();
        // 		ctx_aem_div.x.bins_arr = disc_x[a];
        // 		ctx_aem_div.y.bins_arr = disc_y[a];
        //         ctx_aem_div.z.bins_arr = disc_z[a];

        ref_trial_aem.fac_div[a] = new DifferentialFactory(ctx_aem_div);
        ref_trial_aem.fac_div[a]->init();

        DistributionContext ctx_aem(ctx[a]);
        tmpname = ctx[a].name;
        tmpname.ReplaceAll("Disc", "Signal");
        ctx_aem.name = tmpname;
        ctx_aem.hist_name.Clear();
        ctx_aem.update();
        //         ctx_aem.x.bins_arr = disc_x[a];
        //         ctx_aem.y.bins_arr = disc_y[a];
        //         ctx_aem.z.bins_arr = disc_z[a];

        ref_trial_aem.fac_ae[a] = new DistributionFactory(ctx_aem);
        ref_trial_aem.fac_ae[a]->init();
    }

    // create trial factories
    for (size_t a = 0; a < ananum; ++a)
    {
        DifferentialContext ctx_trial_div(ctx[a]);
        ctx_trial_div.hist_name += "_trialctx_div";
        ctx_trial_div.update();
        //         ctx_trial_div.x.bins_arr = disc_x[a];
        //         ctx_trial_div.y.bins_arr = disc_y[a];
        //         ctx_trial_div.z.bins_arr = disc_z[a];

        trials_aem->fac_div[a] = new DifferentialFactory(ctx_trial_div);
        trials_aem->fac_div[a]->init();

        DistributionContext ctx_trial(ctx[a]);
        ctx_trial.hist_name += "_trialaem";
        ctx_trial.update();
        //         ctx_trial.x.bins_arr = disc_x[a];
        //         ctx_trial.y.bins_arr = disc_y[a];
        //         ctx_trial.z.bins_arr = disc_z[a];

        trials_aem->fac_ae[a] = new DistributionFactory(ctx_trial);
        trials_aem->fac_ae[a]->init();
    }

    // projections, TODO think if we still need it
    // 	for (size_t a = 0; a < ananum; ++a)
    // 	{
    // 		size_t bins_x = ctx[a].x.bins;
    // 		size_t bins_y = ctx[a].y.bins;
    //
    // // 		hprojs[a] = new TH1D*[bins_x * bins_y]; FIXME
    //
    // // 		std::string tmpname1 = TString::Format("@@@d/@@@a/%s/c_%s_projs",
    // ananames[a].c_str(), ananames[a].c_str()).Data();
    // // 		std::string tmpname2 = TString::Format("c_@@@a_%s_projs",
    // ananames[a].c_str()).Data();
    // // 		cprojs[a] = projs[a]->RegCanvas(tmpname1.c_str(), tmpname2.c_str(), 1200, 1000);
    // FIXME
    // // 		cprojs[a]->Divide(bins_x, bins_y);
    // 	}

    // running trial
    printf("Running initial trial\n");
    ae_fit_function(ref_trial_aem, ff, -1, true);

    // 	char buff[200];
    // 	char buff2[200];
    for (size_t a = 0; a < ananum; ++a)
    {
        for (uint b = 0; b < ref_trial_aem.fac_div[a]->diffs->nhists; ++b)
        {
            uint bx, by, bz;
            ref_trial_aem.fac_div[a]->diffs->reverseBin(b, bx, by, bz);
            double mean =
                ref_trial_aem.fac_ae[a]->hSignalCounter->GetBinContent(bx + 1, by + 1, bz + 1);

            // 				sprintf(buff, "@@@d/@@@a/%s_proj_%02lu_%02lu", ananames[a].c_str(), x,
            // y); 				sprintf(buff2, "%s_proj_%02lu_%02lu", ananames[a].c_str(), x, y);
            // hprojs[a][bin] = projs[a]->RegTH1<TH1D>(buff, buff2, 100, mean * 0.7, mean * 1.3);
            // FIXME

            syst_data[a].syst_rms[bx][by][bz] = 0.0;
            syst_data[a].syst_mean[bx][by][bz] = mean;
        }
    }

    std::string trial_ae_name;

    if (!par_iter_number) return;

    printf("Run %d trials\n", par_iter_number);
    for (int i = 0; i < par_iter_number; ++i)
    {
        printf(" TRIAL %03d\n", i);

        ae_fit_function(*trials_aem, ff, i);

        printf("trial %d: %g\n", i, trials_aem->fac_ae[1]->hSignalCounter->GetBinContent(2, 6));

        for (size_t a = 0; a < ananum; ++a)
        {
            for (uint b = 0; b < ref_trial_aem.fac_div[a]->diffs->nhists; ++b)
            {
                uint bx, by, bz;
                ref_trial_aem.fac_div[a]->diffs->reverseBin(b, bx, by, bz);
                double mean =
                    ref_trial_aem.fac_ae[a]->hSignalCounter->GetBinContent(bx + 1, by + 1, bz + 1);

                // 			hprojs[a][bin]->Fill(mean); FIXME

                syst_data[a].syst_rms[bx][by][bz] += (mean - syst_data[a].syst_mean[bx][by][bz]) *
                                                     (mean - syst_data[a].syst_mean[bx][by][bz]);
            }
        }

        HistFitParams::printStats();
    }

    for (size_t a = 0; a < ananum; ++a)
    {
        for (uint b = 0; b < ref_trial_aem.fac_div[a]->diffs->nhists; ++b)
        {
            uint bx, by, bz;
            ref_trial_aem.fac_div[a]->diffs->reverseBin(b, bx, by, bz);
            syst_data[a].syst_rms[bx][by][bz] =
                sqrt(syst_data[a].syst_rms[bx][by][bz] /
                     par_iter_number); // FIXME what it does, what if par_iter_num == 0?
        }
    }

    for (size_t a = 0; a < ananum; ++a)
    {
        for (uint b = 0; b < ref_trial_aem.fac_div[a]->diffs->nhists; ++b)
        {
            uint bx, by, bz;
            ref_trial_aem.fac_div[a]->diffs->reverseBin(b, bx, by, bz);

            //       int bin = y * bins_x + x;

            double rms = 0; // FIXME hprojs[a][bin]->GetRMS();

            ref_trial_aem.fac_div[a]->hSignalCounter->SetBinContent(
                bx + 1, by + 1, bz + 1, syst_data[a].syst_rms[bx][by][bz]);
            ref_trial_aem.fac_div[a]->hSignalCounter->SetBinError(bx + 1, by + 1, bz + 1, rms);
        }
    }
}

// Fit fss and sim and calculate final AE matrix
void AECalculator::ae_fit_function(ae_data& trial_ae, FitterFactory& ff, int iter_cnt, bool backup)
{
    // 	ae_data trial_sim;
    // 	ae_data trial_fss;
    DifferentialFactory* trial_sim[ananum];
    DifferentialFactory* trial_fss[ananum];
    //   DistributionFactory * trial_fss_fit[ananum];

    char buff[200];
    for (size_t a = 0; a < ananum; ++a)
    {
        // 		if (a == 2) continue;		// FIXME Skip PcmXcm

        DifferentialContext ctx_sim = cdfdata->entries_sim[0].fac[a]->ctx;
        sprintf(buff, "sim_%06d_%s", (iter_cnt == -1 ? 999999 : iter_cnt), ctx_sim.name.Data());
        ctx_sim.hist_name = buff;
        ctx_sim.update();
        //         ctx_sim.x.bins_arr = disc_x[a];
        //         ctx_sim.y.bins_arr = disc_y[a];
        //         ctx_sim.z.bins_arr = disc_z[a];

        trial_sim[a] = new DifferentialFactory(ctx_sim);
        trial_sim[a]->init();

        DifferentialContext ctx_fss = cdfdata->entries_fss[0].fac[a]->ctx;
        //         ctx_fss.x.bins_arr = disc_x[a];
        //         ctx_fss.y.bins_arr = disc_y[a];
        //         ctx_fss.z.bins_arr = disc_z[a];

        trial_fss[a] = new DifferentialFactory(ctx_fss);
        trial_fss[a]->init();
    }

    HistFitParams* stdfit = HistFitParams::parseEntryFromFile(
        " * s2gaus pol3(6)+aexpo 0 1086 1179 0.001 : 0 1 1115 : 1100 1122 1.5 : 1 3 0.1 F 0 1 0.3 "
        ": 0 1 3.5 : 3 5 0.1 0.1 0.1 0.1 0.1 0.1 0.1");

    // trial execution
    // 0 - p_t - y_cm
    // 1 - p_cm - cosTheta_cm
    // 2 - x_cm - p_cm

    sprintf(buff, "h_fss_%06d_%s", (iter_cnt == -1 ? 999999 : iter_cnt), "");
    ff.setReplacement(buff, "h_");

    // iterate over analysis
    for (size_t a = 0; a < ananum; ++a)
    {
        // 		if (a == 2) continue;		// FIXME Skip PcmXcm

        // iterate over input channel
        for (size_t e = 0; e < global_npar; ++e)
        {
            DifferentialFactory* fac_sim = cdfdata->entries_sim[e].fac[a];
            DifferentialFactory* fac_fss = cdfdata->entries_fss[e].fac[a];
            // 			DifferentialFactory * fac_ae = trial_ae.fac_div[a];

            // 			double cs_scale = 1.0; // FIXME why this and the if {} below? Not used
            //
            // 			// If no CS data given, we keep original value and fit around 1.0 scaling
            // factor 			if (chanmod[e].has_cs_scalings)
            // 			{
            // 				cs_scale = 1.0;// /chanmod[e].cs_scalings;
            // 			}
            //
            // 			for (uint i = 0; i < fac_fss->ctx.x.bins; ++i)
            // 			{
            // 				double bin_l = fac_ae->hSignalCounter->GetXaxis()->GetBinLowEdge(i+1);
            // 				double bin_r = fac_ae->hSignalCounter->GetXaxis()->GetBinUpEdge(i+1);
            // 				double bin_w = bin_r - bin_l;
            //
            // 				// if analysis == 1 and has ang scaling
            // 				// prepare value to scale each y bin
            // 				double ang_dist = 1.0;
            // #define IGNORE_ANG_VAR
            // #ifndef IGNORE_ANG_VAR
            // 				if (a == 1 and chanmod[e].ang_scalings[0] != 0.0)
            // 				{
            // 					double r2 = 0.0;
            // 					double r4 = 0.0;
            //
            // 					// if iter counter is active, use randomization
            // 					if (iter_cnt >= 0 and !flag_noangrand)
            // 					{
            // 						double err22 =
            // chanmod[e].ang_errors_u[1]*chanmod[e].ang_errors_u[1]
            // + 						chanmod[e].ang_errors_l[1]*chanmod[e].ang_errors_l[1];
            // r2 = rootrand.Gaus(chanmod[e].ang_scalings[1], sqrt(err22));
            //
            // 						double err42 =
            // chanmod[e].ang_errors_u[2]*chanmod[e].ang_errors_u[2]
            // + 						chanmod[e].ang_errors_l[2]*chanmod[e].ang_errors_l[2];
            // r4 = rootrand.Gaus(chanmod[e].ang_scalings[2], sqrt(err42));
            // 					}
            // 					// otherwise use defaults
            // 					else
            // 					{
            // 						r2 = chanmod[e].ang_scalings[1];
            // 						r4 = chanmod[e].ang_scalings[2];
            // 					}
            //
            // 					legpol->SetParameter(0, 1.0);
            // 					legpol->SetParameter(1, r2);
            // 					legpol->SetParameter(2, r4);
            // 					double int_varied = legpol->Integral(bin_l, bin_r);
            //
            // 					legpol->SetParameter(0, 1.0);
            // 					legpol->SetParameter(1, chanmod[e].ang_scalings[1]);
            // 					legpol->SetParameter(2, chanmod[e].ang_scalings[2]);
            // 					double int_reference = legpol->Integral(bin_l, bin_r);
            //
            // 					ang_dist = int_varied / int_reference;
            // 				}
            // #endif /* IGNORE_ANG_VAR */
            // 				for (uint j = 0; j < fac_fss->ctx.y.bins; ++j)
            // 				{
            // 					double r = 1.0;
            //
            // 					if (iter_cnt >= 0 and !flag_nocsrand)
            // 					{
            // 						double err2 = chanmod[e].cs_errors_u*chanmod[e].cs_errors_u +
            // 							chanmod[e].cs_errors_l*chanmod[e].cs_errors_l;
            // 						r = rootrand.Gaus(chanmod[e].cs_scalings, sqrt(err2)) /
            // chanmod[e].cs_scalings;
            // 					}
            // 					else
            // 					{
            // 						r = 1.0;//chanmod[e].cs_scalings;
            // 					}
            //
            // 					trial_sim[a]->diffs->get(i, j)->Add(fac_sim->diffs->get(i, j), r *
            // ang_dist); 					trial_fss[a]->diffs->get(i, j)->Add(fac_fss->diffs->get(i, j),
            // r
            // * ang_dist);
            // 				}
            // 			}
            for (uint b = 0; b < ref_trial_aem.fac_div[a]->diffs->nhists; ++b)
            {
                (*trial_sim[a]->diffs)[b]->Add((*fac_sim->diffs)[b], 1.0);
                (*trial_fss[a]->diffs)[b]->Add((*fac_fss->diffs)[b], 1.0);
            }
        }
        // 		trial_fss.fac[a]->setFitCallback(fit_callback);

        for (uint b = 0; b < ref_trial_aem.fac_div[a]->diffs->nhists; ++b)
        {
            uint bx, by, bz;
            ref_trial_aem.fac_div[a]->diffs->reverseBin(b, bx, by, bz);

            HistFitParams hfp = *stdfit;

            TH1* h = (*trial_fss[a]->diffs)[b];
            FitterFactory::FIND_FLAGS fflags = ff.findParams(h->GetName(), hfp, true);

            bool hasfunc = (fflags == FitterFactory::USE_FOUND);

            if (((!hasfunc) or (hasfunc and !hfp.fit_disabled)))
            {
                if (h->GetEntries() / h->GetRMS() > 3)
                {
                    if (fflags == FitterFactory::USE_FOUND)
                        printf("+ Fitting %s (e=%g) IM with custom function\n", h->GetName(),
                               h->GetEntries());
                    else
                        printf("+ Fitting %s (e=%g) IM with standard function\n", h->GetName(),
                               h->GetEntries());

                    HistFitParams _hfp = hfp;

                    if ((*trial_sim[a]->diffs)[b]->Integral() > 0)
                        h->Scale(1.0 / (*trial_sim[a]->diffs)[b]->Integral());

                    bool res = trial_fss[a]->fitDiffHist(h, _hfp, 3);
                    if (res) { _hfp.update(); }

                    fit_callback(trial_fss[a], 0, res, h, bx, by, bz);

                    // 						FIXME
                    // 						std::cout << "    Signal: " << res.signal << " +/- " <<
                    // res.signal_err
                    // 							<< " (chi2/ndf) = (" << res.chi2 << "/" << res.ndf << ") =
                    // "
                    // << res.chi2/res.ndf << std::endl;
                    //
                    // 						trial_fss.fac[a]->hSliceXYDiff[i]->SetBinContent(1+j,
                    // res.signal);
                    // trial_fss.fac[a]->hSliceXYDiff[i]->SetBinError(1+j, res.signal_err);
                    // trial_fss.fac[a]->hDiscreteXYSig->SetBinContent(1+i, 1+j, res.signal);
                    // trial_fss.fac[a]->hDiscreteXYSig->SetBinError(1+i, 1+j, res.signal_err);
                    //
                    // 						if (res.mean != 0)
                    // 						{
                    // 							trial_fss.fac[a]->hSliceXYFitQA[i]->SetBinContent(1+j,
                    // res.mean);
                    // trial_fss.fac[a]->hSliceXYFitQA[i]->SetBinError(1+j, res.sigma);
                    // trial_fss.fac[a]->hSliceXYChi2NDF[i]->SetBinContent(1+j, res.chi2/res.ndf);
                    // 						}
                }

                trial_sim[a]->hSignalCounter->SetBinContent(1 + bx, 1 + by, 1 + bz,
                                                            (*trial_sim[a]->diffs)[b]->Integral());
                trial_sim[a]->hSignalCounter->SetBinError(
                    1 + bx, 1 + by, 1 + bz,
                    RootTools::calcTotalError((*trial_sim[a]->diffs)[b], 1,
                                              (*trial_sim[a]->diffs)[b]->GetNbinsX() + 1));
            }
        }

        *trial_ae.fac_ae[a] = *trial_fss[a];
        // 		*trial_ae.fac_ae[a] /= *trial_sim[a];

        copyHistogram(trial_ae.fac_ae[a]->hSignalCounter, trial_ae.fac_ae[a]->hSignalCounter);
        copyHistogram(trial_ae.fac_ae[a]->hSignalCounter, trial_ae.fac_div[a]->hSignalCounter);

        for (uint b = 0; b < ref_trial_aem.fac_div[a]->diffs->nhists; ++b)
        {
            uint bx, by, bz;
            ref_trial_aem.fac_div[a]->diffs->reverseBin(b, bx, by, bz);

            //       for (int k = 0; k < (*trial_fss.fac_ae[a]->diffs)[b]->GetXaxis()->GetNbins();
            //       ++k)
            //       {
            copyHistogram((*trial_fss[a]->diffs)[b], (*trial_ae.fac_div[a]->diffs)[b]);
            //       (*trial_ae.fac_div[a]->diffs)[b]->SetBinContent(b,
            //       (*trial_fss.fac[a]->diffs)[b]->GetBinContent(k));
            //       (*trial_ae.fac_div[a]->diffs)[b]->SetBinError(b,
            //       (*trial_fss.fac[a]->diffs)[b]->GetBinError(k));

            //       }

            //       if ((*trial_sim[a]->diffs)[b]->Integral() > 0)
            //         (*trial_ae.fac_div[a]->diffs)[b]->Scale(1.0/(*trial_sim[a]->diffs)[b]->Integral());
            //       else
            //         (*trial_ae.fac_div[a]->diffs)[b]->Reset();
        }

        if (backup)
        {
            trial_ae.fac_fss[a] = trial_fss[a];
            trial_ae.fac_sim[a] = trial_sim[a];
        }
        else
        {
            delete trial_sim[a];
            delete trial_fss[a];
        }
    }

    // 	printf("Closing analyses (%lu): ", global_npar);
    // 	for (size_t e = 0; e < global_npar; ++e)
    // 	{
    // 		for (size_t a = 0; a < ananum; ++a)
    // 		{
    // 			delete cdfdata->entries_fss[e].fac[a];
    // 			delete cdfdata->entries_sim[e].fac[a];
    // 		}
    //
    // 		cdfdata->entries_fss[e].file->Close();
    // 		delete cdfdata->entries_fss[e].file;
    //
    // 		cdfdata->entries_sim[e].file->Close();
    // 		delete cdfdata->entries_sim[e].file;
    //
    // 		if (e % 10 == 0)
    // 			printf("|");
    // 		else
    // 			printf("=");
    // 		fflush(stdout);
    // 	}

    // 	ff.Delete();
    printf("<\n");
}

void AECalculator::AnaFinalize()
{
    // 	for (size_t a = 0; a < ananum; ++a)
    // 	{
    // 		ref_trial_aem.fac_ae[a]->ctx.SetName((ananames[a] + "_SigCtx").c_str());
    // 		ref_trial_aem.fac_ae[a]->rename(ananames[a].c_str());
    // 		ref_trial_aem.fac_ae[a]->chdir(ananames[a].c_str());
    // 	}

    OpenExportFile();

    setConfigVector()->Write();

    for (size_t a = 0; a < ananum; ++a)
    {
        ref_trial_aem.fac_ae[a]->ctx.z.label = "Acceptance";
        ref_trial_aem.fac_ae[a]->ctx.z.unit = "";
        ref_trial_aem.fac_ae[a]->prepareCanvas();
        ref_trial_aem.fac_ae[a]->niceHists(def_pf_diff, def_gf_diff);
        ref_trial_aem.fac_ae[a]->write(GetExportFile());

        ref_trial_aem.fac_div[a]->prepareDiffCanvas();
        ref_trial_aem.fac_div[a]->prepareCanvas();
        ref_trial_aem.fac_div[a]->niceHists(def_pf_diff, def_gf_diff);
        ref_trial_aem.fac_div[a]->finalize();
        ref_trial_aem.fac_div[a]->write(GetExportFile());

        // 		ref_trial_aem.fac_fss[a]->ctx.Write();
        // 		ref_trial_aem.fac_fss[a]->niceHists(0.1, 0.11, 0.1, 0.13, 210, 505, 0.05, 0.05, 0.9,
        // 0.05, 0.05, 1.25); 		ref_trial_aem.fac_fss[a]->prepareDiffCanvas();
        // 		ref_trial_aem.fac_fss[a]->prepareCanvas(true);
        // 		ref_trial_aem.fac_fss[a]->write(GetExportFile());
        //
        // 		ref_trial_aem.fac_sim[a]->ctx.Write();
        // 		ref_trial_aem.fac_sim[a]->niceHists(0.1, 0.11, 0.1, 0.13, 210, 505, 0.05, 0.05, 0.9,
        // 0.05, 0.05, 1.25); 		ref_trial_aem.fac_sim[a]->prepareDiffCanvas();
        // 		ref_trial_aem.fac_sim[a]->prepareCanvas(true);
        // 		ref_trial_aem.fac_sim[a]->write(GetExportFile());
    }

    for (size_t a = 0; a < ananum; ++a)
    {
        set[a].projs->write(GetExportFile());
        ref_trial_aem.fac_ae[a]->ctx.Write();
    }

    CloseExportFile();

    // 	if (!flag_noaux)
    {
        ff.exportFactoryToFile();
    }

    if (!flag_nosum)
    {
        std::cout << "Sum file requesting, creating " << par_sumfile;
        TFile* sum_file = TFile::Open(par_sumfile.c_str(), "RECREATE");

        if (!sum_file) { std::cerr << " [ failed ]" << std::endl; }
        else
        {
            sum_file->cd();
            setConfigVector(sum_file)->Write();

            for (size_t a = 0; a < ananum; ++a)
            {
                // 				fac_pars[0][a]->rename(ctx[a]->histPrefix);
                // 				fac_pars[0][a]->prepareDiffCanvas();
                // 				fac_pars[0][a]->prepareCanvas(true);
                // 				fac_pars[0][a]->write(sum_file);
                // 				fac_pars[0][a]->ctx.Write();
            }
            sum_file->Close();

            std::cout << " [ done ]" << std::endl;
        }
    }

    if (!flag_nocsrand and !flag_noangrand)
    {
        std::ofstream ofs_fit_cfg(fit_cfg);
        for (uint i = 0; i < fit_ana_selection.size(); ++i)
            ofs_fit_cfg << fit_ana_selection[i] << " ";
        ofs_fit_cfg << std::endl;

        for (uint i = 0; i < global_npar; ++i)
            ofs_fit_cfg << chanmod[i].label << " " << chanmod[i].fit_cs_scalings << " "
                        << chanmod[i].fit_cs_errors << " " << chanmod[i].fit_ang_scalings[1] << " "
                        << chanmod[i].fit_ang_errors[1] << " " << chanmod[i].fit_ang_scalings[2]
                        << " " << chanmod[i].fit_ang_errors[2] << std::endl;
    }
    // 	abort();
}

int AECalculator::config_params(struct option*& opts)
{
    static struct option lopts[] = {
        {"nosum", no_argument, &flag_nosum, 1},
        {"noscale", no_argument, &flag_noscale, 1},
        {"noangrand", no_argument, &flag_noangrand, 1},
        {"nocsrand", no_argument, &flag_nocsrand, 1},
        {"max-scale", required_argument, 0, 1001},
        {"main-scale", required_argument, 0, 1002},
        {"fit-params", required_argument, 0, 1003},
    };
    opts = lopts;

    return sizeof(lopts) / sizeof(option);
}

void AECalculator::config_execute(int code, char* optarg)
{
    switch (code)
    {
        case 1001:
            par_max_scale = atof(optarg);
            break;

        case 1002:
            par_min_scale = atof(optarg);
            break;

        case 1003:
            par_fit_params = optarg;
            break;

        default:
            TString ftmp = optarg;
            configfile_t = ftmp;
            return;
            break;
    }
}

// FitCallback fclbk
// {
// }
