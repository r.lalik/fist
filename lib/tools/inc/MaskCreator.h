/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MASKCREATOR_H
#define MASKCREATOR_H

#ifndef __CINT__

#include "TChain.h"
#include "TString.h"
// #include "TVector.h"

#include "SecVertexReco.h"
#include "ToolsAbstract.h"

#endif /* __CINT__ */

class MaskCreator : public SecVertexReco
{
protected:
    // chain
    TChain* chain;

public:
    MaskCreator();
    virtual ~MaskCreator();

    inline void AddToChain(TChain* /*ch*/) {}
    inline void AddToChain(const TString& /*file*/) {}

    void AnaExecute();
};

#endif // MASKCREATOR_H
