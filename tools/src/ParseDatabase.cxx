#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>

#include <getopt.h>

#include <TObjArray.h>
#include <TObjString.h>
#include <TString.h>

#include "DatabaseManager.h"

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

void dump_group_header(const char* text, size_t columns_number = 6)
{
    printf("\\toprule\n\\multicolumn{%lu}{c}{%s} \\\\\n\\midrule\n", columns_number, text);
}

void dump_group(const char** channels, size_t length, AnaDatabase* db)
{
    std::string filler;
    char a2_filler[200];
    char a4_filler[200];

    for (uint i = 0; i < length; ++i)
    {
        DatabaseRecord dbrec = db->getRecord(channels[i]);
        if (dbrec.cs < 10)
            filler = "\\phantom{0}";
        else
            filler.clear();

        std::string latex_code = dbrec.latex.c_str();
        size_t hash_pos = 0;
        while (true)
        {
            hash_pos = latex_code.find_first_of('#');
            if (hash_pos == latex_code.npos) break;
            latex_code.replace(hash_pos, 1, "\\");
        }

        if (dbrec.ang[1] == 0.0 and atof(dbrec.ang_errors_str[1].c_str()) == 0.0)
            sprintf(a2_filler, " %s ", "---");
        else
            sprintf(a2_filler, "$%0.2f \\pm %0.2f$", dbrec.ang[1],
                    atof(dbrec.ang_errors_str[1].c_str()));

        if (dbrec.ang[2] == 0.0 and atof(dbrec.ang_errors_str[2].c_str()) == 0.0)
            sprintf(a4_filler, " %s ", "---");
        else
            sprintf(a4_filler, "$%0.2f \\pm %0.2f$", dbrec.ang[2],
                    atof(dbrec.ang_errors_str[2].c_str()));

        printf("  %s & $%s$ & $%s%.3f \\pm %.3f$ &\n"
               "     $1.0$ & %s & %s"
               " \\\\\n",
               dbrec.label.c_str(), latex_code.c_str(), filler.c_str(), dbrec.cs,
               atof(dbrec.cs_errors_str.c_str()), a2_filler, a4_filler);
    }
}

// #define WITH_LATEX 1

void dump_params(const char** channels, size_t length, AnaDatabase** dbs, size_t dbs_num)
{
    static bool do_it_once = false;

    if (!do_it_once)
    {
#ifdef WITH_LATEX
        std::cout << "label  latex  ";
#else
        std::cout << "label  ";
#endif

        for (uint j = 0; j < dbs_num; ++j)
        {
            printf(" \"%d%s\" %s^{%d} %s^{%d}   %s^{%d} %s^{%d} %s^{%d}   %s^{%d} %s^{%d} %s^{%d}  "
                   " %s^{%d} %s^{%d} %s^{%d} ",
                   j, "{/Symbol s}", "\\delta\\sigma^{u}", j, "\\delta\\sigma^{l}", j, "a_{0}", j,
                   "\\delta{}a_{0h}", j, "\\delta{}a_{0l}", j, "a_{1}", j, "\\delta{}a_{1h}", j,
                   "\\delta{}a_{1l}", j, "a_{2}", j, "\\delta{}a_{2h}", j, "\\delta{}a_{2l}", j);
        }
        std::cout << std::endl;

        do_it_once = true;
    }

    for (uint i = 0; i < length; ++i)
    {
#ifdef WITH_LATEX
        DatabaseRecord dbrec = dbs[0]->getRecord(channels[i]);
        std::string latex_code = dbrec.latex.c_str();
        std::cout << channels[i] << "  " << latex_code.c_str() << "  ";
#else
        std::cout << channels[i] << "  ";
#endif
        for (uint j = 0; j < dbs_num; ++j)
        {
            DatabaseRecord dbrec = dbs[j]->getRecord(channels[i]);

            std::string latex_code = dbrec.latex.c_str();
            size_t hash_pos = 0;
            while (true)
            {
                hash_pos = latex_code.find_first_of('#');
                if (hash_pos == latex_code.npos) break;
                latex_code.replace(hash_pos, 1, "\\");
            }

            ErrorsPair eps = DatabaseRecord::sumErrorChain(dbrec.getCsErrors());
            ErrorsPair epa0 = DatabaseRecord::sumErrorChain(dbrec.getAngErrors(0));
            ErrorsPair epa1 = DatabaseRecord::sumErrorChain(dbrec.getAngErrors(1));
            ErrorsPair epa2 = DatabaseRecord::sumErrorChain(dbrec.getAngErrors(2));
            // 			printf(" %s  %g %g   %g %g   %g %g   %g %g ", latex_code.c_str(),
            // 				   dbrec.cs, atof(dbrec.cs_errors_str.c_str()),
            // 				   dbrec.ang[0], atof(dbrec.ang_errors_str[0].c_str()),
            // 				   dbrec.ang[1], atof(dbrec.ang_errors_str[1].c_str()),
            // 				   dbrec.ang[2], atof(dbrec.ang_errors_str[2].c_str()));

            printf(" %g %g %g  %g %g %g   %g %g %g   %g %g %g ", dbrec.cs, eps.high, eps.low,
                   dbrec.ang[0], epa0.high, epa0.low, dbrec.ang[1], epa1.high, epa1.low,
                   dbrec.ang[2], epa2.high, epa2.low);
        }
        std::cout << std::endl;
    }
}

int main(int argc, char** argv)
{
    int flag_label = 0;
    int flag_scale = 0;
    int flag_evnum = 0;
    int flag_cs = 0;
    int flag_color = 0;
    int flag_location = 0;
    int flag_dumplatex = 0;
    TString flag_dumpparams;
    int flag_a0 = 0;
    int flag_a2 = 0;
    int flag_a4 = 0;
    int flag_ang = 0;
    int flag_rise = 0;
    int flag_lower = 0;

    std::string refname = "";
    std::string database = "";
    std::string placeholder = "";

    struct option long_options[] = {{"label", no_argument, &flag_label, 1},
                                    {"scale", no_argument, &flag_scale, 1},
                                    {"evnum", no_argument, &flag_evnum, 1},
                                    {"cs", no_argument, &flag_cs, 1},
                                    {"a0", no_argument, &flag_a0, 1},
                                    {"a2", no_argument, &flag_a2, 1},
                                    {"a4", no_argument, &flag_a4, 1},
                                    {"ang", no_argument, &flag_ang, 1},
                                    {"color", no_argument, &flag_color, 1},
                                    {"location", no_argument, &flag_location, 1},
                                    {"dump-latex", no_argument, &flag_dumplatex, 1},
                                    {"dump-params", required_argument, 0, 1001},
                                    {"database", required_argument, 0, 'd'},
                                    {"placeholder", required_argument, 0, 'p'},
                                    {"rise", no_argument, &flag_rise, 1},
                                    {"lower", no_argument, &flag_lower, 1},
                                    {0, 0, 0, 0}};

    int c = 0;
    while (1)
    {
        int option_index = 0;

        c = getopt_long(argc, argv, "hd:p:", long_options, &option_index);
        if (c == -1) break;

        switch (c)
        {
            case 0:
                if (long_options[option_index].flag != 0) break;
                std::printf("option %s", long_options[option_index].name);
                if (optarg) std::printf(" with arg %s", optarg);
                std::printf("\n");
                break;
            case 'd':
                database = optarg;
                break;
            case 'p':
                placeholder = optarg;
                break;

            case 1001:
                flag_dumpparams = optarg;
                break;

            case 'h':
                // 				Usage();
                exit(EXIT_SUCCESS);
                break;
            case '?':
                // 				abort();
                break;
            default:
                break;
        }
    }

    if (flag_dumplatex)
    {
        const size_t len_channels3b = 17;
        const char* channels3b[len_channels3b] = {
            "ch040", "pwa",   "ch060", "ch068", "ch400", "ch401", "ch402", "ch072", "ch077",
            "ch088", "ch044", "ch111", "ch420", "ch421", "ch422", "ch423", "ch424"};

        const size_t len_channels4b = 6;
        const char* channels4b[len_channels4b] = {"ch055", "ch058", "ch061",
                                                  "ch066", "ch410", "ch411"};

        AnaDatabase* db = DatabaseManager::getDatabase(database);
        db->prepare();

        dump_group_header("3-body channels");
        dump_group(channels3b, len_channels3b, db);
        dump_group_header("4-body channels");
        dump_group(channels4b, len_channels4b, db);

        // 		if (flag_location)	result		= db->getLocation(refname);
        // 		if (flag_cs)		{ tmp_num = db->getCS(refname); result =
        // 			TString::Format("%g", tmp_num); }
        // 		if (flag_color)		result		= db->getStyle(refname);
        // 		if (flag_label)		result		= db->getLatex(refname);
        // 		if (flag_scale)		{ tmp_num = db->getSimEvents(refname); result =
        // 			TString::Format("%g", tmp_num); }

        return 0;
    }

    if (flag_dumpparams.Length())
    {
        TObjArray* tokens = flag_dumpparams.Tokenize(':');
        size_t tokens_num = tokens->GetEntries();

        AnaDatabase** dbs = new AnaDatabase*[tokens_num];

        for (uint i = 0; i < tokens_num; ++i)
        {
            dbs[i] = DatabaseManager::getDatabase(((TObjString*)tokens->At(i))->String().Data());
            dbs[i]->prepare();
        }

        const size_t len_channels3b = 17;
        const char* channels3b[len_channels3b] = {
            "ch040", "pwa",   "ch060", "ch068", "ch400", "ch401", "ch402", "ch072", "ch077",
            "ch088", "ch044", "ch111", "ch420", "ch421", "ch422", "ch423", "ch424"};

        const size_t len_channels4b = 6;
        const char* channels4b[len_channels4b] = {"ch055", "ch058", "ch061",
                                                  "ch066", "ch410", "ch411"};

        dump_params(channels3b, len_channels3b, dbs, tokens_num);
        dump_params(channels4b, len_channels4b, dbs, tokens_num);

        return 0;
    }

    while (optind < argc)
    {
        refname = argv[optind++];
    }

    if (!refname.length())
    {
        std::cerr << "No input reference for database!" << std::endl;
        std::exit(EXIT_FAILURE);
    }

    if (!database.length())
    {
        std::cerr << "No input database!" << std::endl;
        std::exit(EXIT_FAILURE);
    }

    // 	std::ifstream dbfile(database.c_str());
    // 	if (!dbfile.is_open())
    // 	{
    // 		std::cerr << "File " << database << " can not be open!";
    // 		std::exit(EXIT_FAILURE);
    // 	}

    if (flag_label + flag_evnum + flag_scale + flag_cs + flag_color + flag_location > 1)
    {
        std::cerr << "Only one flag allowed." << std::endl;
        std::exit(EXIT_FAILURE);
    }

    std::string line;
    TString result;

    // 	while (std::getline(dbfile, line))
    // 	{
    // 		if (line.size() == 0)
    // 			continue;
    //
    // 		TString line_(line);
    // 		line_.ReplaceAll("\t", " ");
    // 		line_.Remove(TString::kBoth, ' ');
    //
    // 		if (line_.BeginsWith("#"))
    // 			continue;
    //
    // 		TObjArray * arr = line_.Tokenize(" ");
    //
    // 		TString cmdchar = ((TObjString *)arr->At(0))->String();
    // // 		printf("%s -> %s\n", cmdchar.Data(), refname.c_str());
    // 		if (cmdchar == refname)
    // 		{
    // 			if (flag_location)	result		= ((TObjString *)arr->At(1))->String();
    // 			if (flag_cs)		result		= ((TObjString *)arr->At(2))->String();
    // 			if (flag_color)		result		= ((TObjString *)arr->At(3))->String();
    // 			if (flag_label)		result		= ((TObjString *)arr->At(4))->String();
    // 			if (flag_scale)		result		= ((TObjString *)arr->At(5))->String();
    //
    // 			if (result.Length())
    // 				break;
    // 		}
    // 	}

    double tmp_num;
    AnaDatabase* db = DatabaseManager::getDatabase(database);
    db->prepare();

    double add_offset = 0;
    if (flag_rise or flag_lower)
    {
        ErrorsPair ep = DatabaseRecord::sumErrorChain(db->getErrors(refname));
        if (flag_rise) add_offset = ep.high;
        if (flag_lower) add_offset = -ep.low;
    }
    if (flag_location) result = db->getLocation(refname);
    if (flag_cs)
    {
        tmp_num = db->getCS(refname) + add_offset;
        result = TString::Format("%g", tmp_num);
    }
    if (flag_color) result = db->getStyle(refname);
    if (flag_label) result = db->getLatex(refname);
    if (flag_scale)
    {
        tmp_num = db->getScale(refname);
        result = TString::Format("%g", tmp_num);
    }
    if (flag_evnum)
    {
        tmp_num = db->getSimEvents(refname);
        result = TString::Format("%g", tmp_num);
    }
    if (flag_a0)
    {
        tmp_num = db->getAng(refname, 0);
        result = TString::Format("%g", tmp_num);
    }
    if (flag_a2)
    {
        tmp_num = db->getAng(refname, 1);
        result = TString::Format("%g", tmp_num);
    }
    if (flag_a4)
    {
        tmp_num = db->getAng(refname, 2);
        result = TString::Format("%g", tmp_num);
    }
    if (flag_ang)
    {
        double tmp_num0 = db->getAng(refname, 0);
        double tmp_num2 = db->getAng(refname, 1);
        double tmp_num4 = db->getAng(refname, 2);
        result = TString::Format("%g,%g,%g", tmp_num0, tmp_num2, tmp_num4);
    }

    if (placeholder.length()) { result.ReplaceAll("@", placeholder.c_str()); }

    std::cout << result << std::endl;
    return 0;
}
