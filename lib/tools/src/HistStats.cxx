/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "HistStats.h"

#ifndef __CINT__

#include "getopt.h"
#include <fstream>
#include <string>

#include "TFile.h"
#include "TGaxis.h"

#endif /* __CINT__ */

#include "RootTools.h"

#include <DifferentialFactory.h>

#include "globals.h"

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

const Option_t h1opts[] = "h,E1";

HistStats::HistStats() : SimpleToolsAbstract()
{
    NicePalette();
    TGaxis::SetMaxDigits(3);

    hsp_names[HS_COUNTS] = "counts";
}

HistStats::~HistStats() {}

void HistStats::AnaInitialize() {}

void HistStats::AnaExecute()
{
    if (!hist_stats.size())
    {
        HistStatsCommand hoc = HS_COUNTS;
        StatsOperation ho;
        ho.cmd = hoc;
        ho.arg = optarg;
        hist_stats.push_back(ho);
    }

    for (uint i = 0; i < infile.size(); ++i)
    {
        DifferentialFactory* ifac[ananum];
        // 		MultiDimAnalysisContext * ictx[ananum] = { 0 };

        TFile* f = ImportFromFile(infile[i], (DifferentialContext**)nullptr, ifac, "");

        if (!f) continue;

        // 		PrintAnalysisInfo();

        for (uint k = 0; k < hist_stats.size(); ++k)
        {
            HistStatsCommand cmd = hist_stats[k].cmd;
            TString arg = hist_stats[k].arg;

            printf("\n ---> Operation: %s\tArgument: %s\n", hsp_names[cmd].c_str(), arg.Data());
            bool is_number = arg.IsFloat();
            // 			Float_t num = is_number ? arg.Atof() : 0.0;

            // 			TFile * f_arg = 0;
            if (!is_number)
            {
                // 				f_arg = ImportAnalysesFromFile(arg, arg_fac, ictx);
                //
                // 				if (!f_arg or !f_arg->IsOpen())
                // 				{
                // 					std::cerr << "File " << arg << " can't be open!";
                // 					std::exit(EXIT_FAILURE);
                // 				}
            }

            for (uint j = 0; j < ananum; ++j)
            {
                switch (cmd)
                {
                    case HS_COUNTS:

                        // 						if (is_number)
                        // 							NoNumberAllowed();
                        // 						else
                        // 							*ofac[j] += *arg_fac[j];
                        // 						break;

                        ifac[j]->printCounts();
                        break;

                    case HS_INTEGRAL:

                        // 						if (is_number)
                        // 							NoNumberAllowed();
                        // 						else
                        // 							*ofac[j] += *arg_fac[j];
                        // 						break;

                        ifac[j]->printIntegrals();
                        break;
                    default:
                        break;
                }
            }
        }

        if (f) f->Close();
    }
}

void HistStats::AnaFinalize() {}

void HistStats::PrintAnalysisInfo() {}

int HistStats::config_params(struct option*& opts)
{
    static struct option lopts[] = {
        {"integral", no_argument, 0, 1001},
    };
    opts = lopts;

    return sizeof(lopts) / sizeof(option);
}

void HistStats::config_execute(int code, char* optarg)
{
    HistStatsCommand hoc = HS_COUNTS;

    switch (code)
    {
        case 1001:
        case 1002:
        case 1003:
        case 1004:
        case 1005:
        case 1006:
        case 1007:
        case 1008:
        {
            hoc = HistStatsCommand(code - 1000);

            StatsOperation ho;
            ho.cmd = hoc;
            ho.arg = optarg;
            hist_stats.push_back(ho);
        }
        break;
        default:
            infile.push_back(optarg);
            return;
            break;
    }

    // 	if (code) return;
}
