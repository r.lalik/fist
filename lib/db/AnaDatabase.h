#ifndef ANADATABASE_H
#define ANADATABASE_H

#include <array>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <map>
#include <vector>

#include <RootTools.h>

typedef std::pair<std::string, std::string> Placeholders;

struct DatabaseRecord
{
    DatabaseRecord();

    int id;

    // properties
    std::string label;
    std::string location;
    std::string style;
    std::string latex;

    // additional operations values
    double scale;
    double simevents;

    // x-sections
    double cs;
    // 	ErrorsChain cs_errors;
    std::string cs_errors_str;

    // angular distributions
    //     static const size_t ang_coeffs_num = 3;
    std::array<double, 3> ang;
    // 	ErrorsChain ang_errors[ang_coeffs_num];
    std::array<std::string, 3> ang_errors_str;

    // functions

    auto getCsErrors() const { return RT::errorsStrToArray(cs_errors_str); }
    auto getAngErrors(int leg) const { return RT::errorsStrToArray(ang_errors_str[leg]); }

    static auto sumErrorChainSquared(const std::vector<RT::ErrorsPair>& errors) -> RT::ErrorsPair;
    static auto sumErrorChain(const std::vector<RT::ErrorsPair>& errors) -> RT::ErrorsPair;
};

typedef std::map<std::string, DatabaseRecord> DbMap;

class AnaDatabase
{
public:
    AnaDatabase() {}
    AnaDatabase(const char* dbfilename) : dbfname(dbfilename) {}
    AnaDatabase(const std::string& dbfilename) : dbfname(dbfilename) {}
    virtual ~AnaDatabase() {}

    void addPlaceholder(const std::string& pattern, const std::string& value)
    {
        placeholders.push_back(Placeholders(pattern, value));
    }

    virtual DatabaseRecord getRecord(const std::string& key) const;
    virtual void pushRecord(const DatabaseRecord&) {}

    std::string getLatex(const std::string& key) const;
    inline std::string getLatex() const { return getLatex(key); }
    void setLatex(const std::string& key, const std::string& val) { dbmap[key].latex = val; }

    double getScale(const std::string& key) const;
    inline double getScale() const { return getScale(key); }
    void setScale(const std::string& key, double val) { dbmap[key].scale = val; }

    double getSimEvents(const std::string& key) const;
    inline double getSimEvents() const { return getSimEvents(key); }
    void setSimEvents(const std::string& key, double val) { dbmap[key].simevents = val; }

    double getCS(const std::string& key) const;
    inline double getCS() const { return getCS(key); }
    void setCS(const std::string& key, double val) { dbmap[key].cs = val; }

    double getAng(const std::string& key, int legnum) const;
    inline double getAng(int legnum) const { return getAng(key, legnum); }
    void setAng(const std::string& key, int legnum, double val) { dbmap[key].ang[legnum] = val; }

    std::string getStyle(const std::string& key) const;
    inline std::string getStyle() const { return getStyle(key); }
    void setStyle(const std::string& key, const std::string& val) { dbmap[key].style = val; }

    std::string getLocation(const std::string& key) const;
    inline std::string getLocation() const { return getLocation(key); }
    void setLocation(const std::string& key, const std::string& val) { dbmap[key].location = val; }

    virtual void prepare(){};
    inline void prepare(const std::string& dbfilename)
    {
        dbfname = dbfilename;
        prepare();
    }
    virtual void dump(const char* /*filename*/) {}

    bool hasKey(const std::string& key) const;

    auto getErrors(const std::string& key) -> std::vector<RT::ErrorsPair>;
    auto getAngErrors(const std::string& key, int leg) -> std::vector<RT::ErrorsPair>;

protected:
    std::string applyPlaceholders(const std::string& str);
    DbMap dbmap;
    std::string dbfname;
    std::string key;

    std::vector<Placeholders> placeholders;
};

#endif /* ANADATABASE_H */
