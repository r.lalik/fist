/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "CompareDistributions.h"

#ifndef __CINT__

#include <algorithm>
#include <exception>
#include <fstream>
#include <string>
#include <vector>
// #include <sstrings>

#include "getopt.h"

#include <TCanvas.h>
#include <TFile.h>
#include <TGaxis.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TMatrixD.h>
#include <TMinuit.h>
#include <TPaveText.h>

#include "TGraphAsymmErrors.h"

#endif /* __CINT__ */

#include "RootTools.h"

#include "globals.h"
#include <DatabaseManager.h>
#include <DistributionContext.h>
#include <DistributionFactory.h>
class weight;

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

const Option_t h1opts[] = "h,E";

const Float_t lamass = 1115.683;

const size_t houtnum = 2;
const std::string houtnames[houtnum] = {"@@@d/h_@@@a_Correction", "@@@d/h_@@@a_AECorrection"};

int error_fill_pattern = 1001;

TMinuit* gmin = 0;
size_t global_npar = 0;
size_t global_nsimult = 1;

TH1** hist_cs_pars[SimpleToolsAbstract::MAXANA];
TH1* hist_ref[SimpleToolsAbstract::MAXANA];

const size_t ang_cnt = 3;
TF1* legpol = nullptr;

struct ChannelData
{
    DistributionFactory** fac_pars;
    DistributionContext** ctx_pars;
    ChannelModifier chanmod;
} * chandata;

TGaxis* custom_axis(double x1, double y1, double x2, double y2, double w1, double w2, int ndiv,
                    const char* format = "-RBS")
{
    TGaxis* axis = new TGaxis(x1, y1, x2, y2, w1, w2, ndiv, format);
    axis->SetLabelFont(62);
    axis->SetTitleFont(62);
    axis->Draw();

    return axis;
}

Double_t ScaleX(Double_t x)
{
    Double_t v;
    v = x * 0.001; // "linear scaling" function example
    return v;
}

Double_t ScaleY(Double_t y)
{
    return y;
    Double_t v;
    v = 20 * y + 200; // "linear scaling" function example
    return v;
}

Double_t ScaleZ(Double_t z)
{
    return z;
    Double_t v;
    v = 30 * z + 300; // "linear scaling" function example
    return v;
}

void ScaleAxis(TAxis* a, Double_t (*Scale)(Double_t))
{
    if (!a) return; // just a precaution
    if (a->GetXbins()->GetSize())
    {
        // an axis with variable bins
        // note: bins must remain in increasing order, hence the "Scale"
        // function must be strictly (monotonically) increasing
        TArrayD X(*(a->GetXbins()));
        for (Int_t i = 0; i < X.GetSize(); i++)
            X[i] = Scale(X[i]);
        a->Set((X.GetSize() - 1), X.GetArray()); // new Xbins
    }
    else
    {
        // an axis with fix bins
        // note: we modify Xmin and Xmax only, hence the "Scale" function
        // must be linear (and Xmax must remain greater than Xmin)
        a->Set(a->GetNbins(),
               Scale(a->GetXmin()),  // new Xmin
               Scale(a->GetXmax())); // new Xmax
    }
}

void ScaleXaxis(TH1* h, Double_t (*Scale)(Double_t))
{
    if (!h) return; // just a precaution
    ScaleAxis(h->GetXaxis(), Scale);
    return;
}

void ScaleYaxis(TH1* h, Double_t (*Scale)(Double_t))
{
    if (!h) return; // just a precaution
    ScaleAxis(h->GetYaxis(), Scale);
    return;
}

void ScaleZaxis(TH1* h, Double_t (*Scale)(Double_t))
{
    if (!h) return; // just a precaution
    ScaleAxis(h->GetZaxis(), Scale);
    return;
}

double calcSimulationError() { return 0.0; };

auto createDumpFileNames(std::string dump_parameter, double sigmamult = 0.0)
    -> std::vector<std::string>
{
    auto strvec = RootTools::split(dump_parameter, ',');
    if (!strvec.size()) abort();

    std::string dump_file = strvec[0];

    char buff2[200];
    if (sigmamult) { sprintf(buff2, "_sm%.1f", sigmamult); }
    else
    {
        buff2[0] = 0;
    }

    char buff[250];
    if (strvec.size() > 1)
        sprintf(buff, "_%s%s.", strvec[1].c_str(), buff2);
    else
        sprintf(buff, "_%s%s.", "fitres", buff2);

    size_t dot_pos = dump_file.find_last_of('.');
    if (dot_pos != dump_file.npos)
        dump_file.replace(dot_pos, 1, buff);
    else
        dump_file += buff;

    std::vector<std::string> dump_strings;
    dump_strings.push_back(strvec[0]);
    dump_strings.push_back(dump_file);

    return dump_strings;
}

double sigmamult = 0.0;

double llpdf(double* x_, double* par_)
{
    double x = x_[0];
    double mean = par_[0];
    double sigma = par_[1];

    return TMath::Gaus(x, mean, sigma, true);
}

double pdf_ext(double x, double mean, double s_u, double s_l)
{
    // 	printf("pdf_ext: %g -> %g +%g -%g\n", x, mean, s_u, s_l);
    double pars[2] = {mean, 0.0};

    double diff_i = x - mean;
    if (diff_i < 0) { pars[1] = s_u; }
    else
    {
        pars[1] = s_l;
    }

    return llpdf(&x, pars);
}

// TMinuit needs an global function. This is a workaround
void glob_fit_fun(Int_t& /*npar*/, Double_t* /*gin*/, Double_t& f, Double_t* par, Int_t /*iflag*/)
{
    // 	printf("Minuit initial of %d parameters: ", npar);
    // 	for (int i = 0; i < npar; ++i)
    // 	{
    // 		printf(" %g ", par[i]);
    // 	}
    // 	printf("\n");
    //
    // 	printf("Global initial of %d parameters: ", global_npar);
    // 	for (int i = 0; i < global_npar; ++i)
    // 	{
    // 		printf(" %g ", par[i]);
    // 	}
    // 	printf("\n");

    // 	if (npar != (int)global_npar)
    // 		throw std::bad_alloc();

    // 	double chi2_total = 0.0;
    double lnL = 0.0;

    // 	for (int i = 0; i < 100; ++i)
    // 		printf("%g = %g\n", 0.1 * i, pdf_ext(0.1 * i, 5, 0.1, 0.1));
    // 	f=0;return;
    // 0 - Pt - y_cm
    // 1 - Pcm - CosTheta cm
    // 	size_t idx_s = 1;
    for (size_t idx_s = 0; idx_s < global_nsimult; ++idx_s)
    {
        size_t bins_x = hist_ref[idx_s]->GetXaxis()->GetNbins();
        size_t bins_y = hist_ref[idx_s]->GetYaxis()->GetNbins();

        // 		double chi2_sim_partial = 0.0;

        double angmap = 1.0;

        // skip first bin as bad for fitting (check spectra)
        for (size_t x = 1; x <= bins_x; ++x)
        {
            // 			double bin_l = hist_ref[idx_s]->GetXaxis()->GetBinLowEdge(x);
            // 			double bin_r = hist_ref[idx_s]->GetXaxis()->GetBinUpEdge(x);
            // 			double bin_w = bin_r - bin_l;

            for (size_t y = 1; y <= bins_y; ++y)
            {
                double numerator_ref = hist_ref[idx_s]->GetBinContent(x, y);
                double numerator_part = 0.0;
                // 				double numerator;
                double denominator = hist_ref[idx_s]->GetBinError(x, y);
                // 				double weight_sum = 0.0;
                // 				double weight_sum_sinv = 0.0;

                if (numerator_ref == 0.0 and denominator == 0.0) continue;

                for (size_t idx_p = 0; idx_p < global_npar; ++idx_p)
                {
                    // 					double weight_cs = 1.0;
                    // 					double weight_a2 = 1.0;
                    // 					double weight_a4 = 1.0;
                    // 					double weight_i = 0.0;

                    // 					weight_cs = pdf_ext(par[0*global_npar + idx_p],
                    // chanmod[idx_p].cs_scalings, chanmod[idx_p].cs_errors_u,
                    // chanmod[idx_p].cs_errors_l);

                    // 					if (par[0*global_npar + idx_p] == 0.0)
                    // 						{
                    // 						angmap = 1.0;
                    //
                    // // 						if (chanmod[idx_p].ang_scalings[1])
                    // // 							weight_a2 =
                    // pdf_ext(chanmod[idx_p].ang_scalings[1], chanmod[idx_p].ang_scalings[1],
                    // chanmod[idx_p].ang_errors_u[1], chanmod[idx_p].ang_errors_l[1]);
                    // // 						if (chanmod[idx_p].ang_scalings[2])
                    // // 							weight_a4 =
                    // pdf_ext(chanmod[idx_p].ang_scalings[2], chanmod[idx_p].ang_scalings[2],
                    // chanmod[idx_p].ang_errors_u[2], chanmod[idx_p].ang_errors_l[2]);
                    //
                    // 						}
                    // 					else
                    // 					{
                    // 						legpol->SetParameter(0, 1.0);
                    // 						legpol->SetParameter(1, par[1*global_npar + idx_p]);
                    // 						legpol->SetParameter(2, par[2*global_npar + idx_p]);
                    // 						angmap = legpol->Integral(bin_l, bin_r) / bin_w;
                    //
                    // // 						if (chanmod[idx_p].ang_scalings[1])
                    // // 							weight_a2 = pdf_ext(par[1*global_npar + idx_p],
                    // chanmod[idx_p].ang_scalings[1], chanmod[idx_p].ang_errors_u[1],
                    // chanmod[idx_p].ang_errors_l[1]);
                    // // 						if (chanmod[idx_p].ang_scalings[2])
                    // // 							weight_a4 = pdf_ext(par[2*global_npar + idx_p],
                    // chanmod[idx_p].ang_scalings[2], chanmod[idx_p].ang_errors_u[2],
                    // chanmod[idx_p].ang_errors_l[2]);
                    //
                    // // 						if (idx_p == 2)
                    // // 						{
                    // // 							printf(" @@@ cs=%g   ang1=%g   ang2=%g\n  [ %g - %g ]
                    // / %g = %g",
                    // // 								   par[idx_p], par[0*global_npar + idx_p],
                    // par[1*global_npar
                    // + idx_p],
                    // // 									bin_l, bin_r, bin_w, angmap);
                    // // 						}
                    // 					}

                    // weighted Chi2 here
                    // // // 					double part_mod = (chanmod[idx_p].cs_scalings +
                    // diff_i
                    // * weight_i*weight_i) * angmap;
                    // // // // 					if (weight_i)
                    // // // // // 						numerator -=
                    // (hist_cs_pars[idx_p][idx_s]->GetBinContent(x, y) * par[idx_p] * angmap) / (
                    // weight * weight );
                    // // // // 						numerator_part +=
                    // (hist_cs_pars[idx_p][idx_s]->GetBinContent(x, y) * par[idx_p] * angmap) *
                    // weight_i;
                    // // //
                    // // // // 						numerator_part += chanmod[idx_p].cs_scalings
                    // + diff_i
                    // * weight_i*weight_i;
                    // // // 						numerator_part +=
                    // hist_cs_pars[idx_p][idx_s]->GetBinContent(x, y)
                    // * part_mod;
                    // // // // 					else
                    // // // // 						numerator_part +=
                    // (hist_cs_pars[idx_p][idx_s]->GetBinContent(x, y) * par[idx_p] * angmap);
                    // // // 				}
                    // // //
                    // // // 				numerator_part /= weight_sum;
                    // // // // 				numerator = numerator_ref -
                    // numerator_part/weight_sum;
                    // // // 				numerator = numerator_ref - numerator_part;
                    // // // 				numerator *= numerator;
                    // // // 				denominator *= denominator;
                    // // //
                    // // // 				double chi2_per_bin = numerator/denominator;
                    // // // // 				double chi2_per_bin =
                    // numerator/(weight_sum*weight_sum);
                    // // //
                    // // // 				chi2_sim_partial += chi2_per_bin;
                    // PR(weight_cs);PR(weight_a2);PR(weight_a4);
                    // PR(hist_cs_pars[idx_p][idx_s]->GetBinContent(x, y));PR(par[0*global_npar +
                    // idx_p]);PR(angmap);

                    numerator_part +=
                        hist_cs_pars[idx_p][idx_s]->GetBinContent(x, y) * // original content
                        par[0 * global_npar + idx_p] /                    // par variation
                        chandata[idx_p].chanmod.cs_scalings *             // normalization factor
                        angmap; // angular scaling, 1.0 if no ang

                    // 					weight_i = weight_cs * weight_a2 * weight_a4;
                    // 					weight_sum += weight_i;
                    // 					weight_sum_sinv += 1/(weight_i*weight_i);
                }

                double pars[2] = {numerator_ref, denominator};
                // 				numerator_part /= weight_sum;
                // 				numerator = numerator_ref - numerator_part/weight_sum;
                // 				numerator = numerator_ref - numerator_part;
                // 				numerator *= numerator;
                // 				denominator *= denominator;

                // 				PR(numerator_part);PR(pars[0]);PR(pars[1]);
                double pdf = llpdf(&numerator_part, pars);
                // 				PR(pdf);
                if (pdf > 0.0) { lnL += log(pdf); }
                else
                {
                    fprintf(
                        stderr,
                        "WARNING -- pdf is negative for (%lu, %lu) : pdf=%g, x=%g, m=%g, s=%g\n", x,
                        y, pdf, numerator_part, numerator_ref, denominator);
                }
                // 				double chi2_per_bin = numerator/denominator;
                // 				double chi2_per_bin = numerator/(weight_sum*weight_sum);

                // 				chi2_sim_partial += chi2_per_bin;
            }
        }

        // 		chi2_total += chi2_sim_partial;
    }

    f = -2.0 * lnL;
    // 	f = chi2_total;
}

void FitCTau(TList* histos, TH1F* dsthisto, TCanvas* can = nullptr, Bool_t print_info = kFALSE)
{
    int slicesnum = histos->GetEntries();

    TLatex* latex = new TLatex();
    latex->SetNDC();
    latex->SetTextColor(/*36*/ 4);
    latex->SetTextSize(0.07);

    for (int i = 0; i < slicesnum; ++i)
    {
        can->cd(1 + i);

        TH1F* h = (TH1F*)histos->At(i);

        Float_t min = h->GetXaxis()->GetXmin();
        Float_t max = h->GetXaxis()->GetXmax();

        TF1* fctaufit = new TF1(TString::Format("fctaufit_slice_%d_%s", i, dsthisto->GetName()),
                                "[0]*exp(-x/[1])", min, max);

        fctaufit->SetParameters(/*1000.0*/ h->GetMaximum(), 75.);

        Float_t fit_min = min; // h->GetBinCenter(h->GetMaximumBin());//100;//facXcmPcm->ctx.y_cmin;
        Float_t fit_max = max;

        // 		Float_t fit_min = 45;
        // 		Float_t fit_max = 250;

        fctaufit->SetBit(TF1::kNotDraw);
        if (print_info)
        {
            h->Fit(fctaufit, "Q0", "", fit_min, fit_max);
            fctaufit->Draw("same");
        }
        else
        {
            h->Fit(fctaufit, "Q0", "", fit_min, fit_max);
        }

        Float_t xdecay = fctaufit->GetParameter(1);
        Float_t xdecayerr = fctaufit->GetParError(1);

        Float_t pavg = dsthisto->GetBinCenter(1 + i);

        Float_t gamma = pavg / lamass;

        Float_t ctau = xdecay / gamma;
        Float_t ctauerr = xdecayerr / gamma;

        dsthisto->SetBinContent(1 + i, ctau);
        dsthisto->SetBinError(1 + i, ctauerr);

        if (print_info)
        {
            latex->DrawLatex(0.25, 0.83, TString::Format("c#tau = %.2f#pm%.2f mm", ctau, ctauerr));
            latex->DrawLatex(0.25, 0.78,
                             TString::Format("#chi^{2}/ndf = %.2f",
                                             fctaufit->GetChisquare() / fctaufit->GetNDF()));
        }
    }
}

void ChannelModifier::reset()
{
    has_cs_scalings = false;
    cs_scalings = 1.0;
    fit_cs_scalings = 1.0;
    cs_errors_u = 0.01;
    cs_errors_l = 0.01;
    cs_limits_u = 1.01;
    cs_limits_l = 0.99;

    has_ang_scalings = false;
    ang_scalings[0] = 1.0;
    ang_scalings[1] = 0.0;
    ang_scalings[2] = 0.0;
    ang_errors_u[0] = 0.01;
    ang_errors_l[0] = 0.01;
    ang_errors_u[1] = 0.01;
    ang_errors_l[1] = 0.01;
    ang_errors_u[2] = 0.01;
    ang_errors_l[2] = 0.01;
    fit_ang_scalings[0] = 1.0;
    fit_ang_scalings[1] = 0.0;
    fit_ang_scalings[2] = 0.0;
}

CompareDistributions::CompareDistributions()
    : SimpleToolsAbstract(), flag_useae(0), flag_nosum(1), flag_dosumfile(0), flag_noscale(0),
      flag_inty(0), flag_negy(0), flag_fit(0), flag_nocsfit(0), flag_noangfit(0), flag_noang(0),
      flag_dump(0), flag_free(0), flag_free_cond(0), flag_sigmamult(0.0), par_max_scale(1.2),
      par_min_scale(2.0), dumpdb(nullptr), cov_mat(nullptr), binsnum(0)
{
    for (size_t a = 0; a < MAXANA; ++a)
    {
        set[a].arrSystematic = nullptr;
        set[a].cfg_maxy = -1;
        set[a].cfg_miny = 1;
    }

    NicePalette();
    TGaxis::SetMaxDigits(4);

    RootTools::AxisFormat x(10, 0.08, 0.005, 0.08, 0.95, kFALSE, kFALSE);
    RootTools::AxisFormat y(503, 0.08, 0.005, 0.08, 0.80, kFALSE, kTRUE);
    RootTools::AxisFormat z(10, 0.08, 0.005, 0.08, 0.95, kFALSE, kFALSE);

    RootTools::PadFormat padf = {0.08, 0.08, 0.12, 0.1};
    RootTools::GraphFormat graphf = {x, y, z};
    pf.pf = padf;
    pf.gf = graphf;

    pf_comp.marginTop = 0;
    pf_comp.marginRight = 0;
    pf_comp.marginBottom = 0;
    pf_comp.marginLeft = 0;

    srand(time(0));
}

CompareDistributions::~CompareDistributions() {}

void CompareDistributions::AnaInitialize()
{
    if (configfile_t.IsNull())
    {
        std::cout << "No config file!" << std::endl;
        std::exit(EXIT_FAILURE);
    }

    if (!par_corr.size()) { corrfile = nullptr; }
    else
    {
        corrfile = TFile::Open(par_corr.c_str(), "READ");
        if (!corrfile->IsOpen())
        {
            std::cerr << "File " << par_corr << " is not open!";
            std::exit(EXIT_FAILURE);
        }
    }

    cdfdata = new CDFInputData();

    std::ifstream fcfgfile(configfile_t.Data());
    if (!fcfgfile.is_open())
    {
        std::cerr << "File " << configfile_t << " is not open!";
        std::exit(EXIT_FAILURE);
    }

    SetOutputFile("ComparedSpectras.root");

    std::vector<CDGroupData>::iterator iter;
    size_t idx = 0;

    cdfdata->extras.clear();

    TString filetmp;
    std::string line;
    while (std::getline(fcfgfile, line))
    {
        if (!line.size()) continue;

        TString line_(line);
        line_.ReplaceAll("\t", " ");

        TObjArray* arr = line_.Tokenize(" ");

        char cmdchar = ((TObjString*)arr->At(0))->String()[0];
        TString tmpstr;
        char testchar = 0;
        bool usecorr = true;
        float scale = 1.0;

        CDGroupData gd;
        ed_vec edvec;
        CDEntryData ed;

        switch (cmdchar)
        {
            case '#':
                continue;
                break;

            case 'e':
                cdfdata->reffile = ((TObjString*)arr->At(1))->String();
                if (arr->GetEntries() >= 3)
                    cdfdata->refAttr = ((TObjString*)arr->At(2))->String();
                else
                    cdfdata->refAttr = "lc=1;mc=1;ms=0.5;mt=21";

                if (arr->GetEntries() >= 4)
                    cdfdata->sumAttr = ((TObjString*)arr->At(3))->String();
                else
                    cdfdata->sumAttr = "lc=2;lw=2;lt=2;ho=L,hist";
                break;

            case 'c':
                if (((TObjString*)arr->At(1))->String() == "maxy")
                {
                    set[((TObjString*)arr->At(2))->String().Atoi()].cfg_maxy =
                        ((TObjString*)arr->At(3))->String().Atof();
                }
                if (((TObjString*)arr->At(1))->String() == "miny")
                {
                    set[((TObjString*)arr->At(2))->String().Atoi()].cfg_miny =
                        ((TObjString*)arr->At(3))->String().Atof();
                }

                break;

            case '+':
            case '?':
                ed.id = ((TObjString*)arr->At(1))->String().Data();
                ed.filename = ((TObjString*)arr->At(2))->String().Data();
                ed.attr = ((TObjString*)arr->At(3))->String().Data();
                ed.use_corr = usecorr;

                // scaling factor for histogram
                if (arr->GetEntries() >= 5)
                {
                    TString str_scale = ((TObjString*)arr->At(4))->String();
                    scale = str_scale.Atof();
                }
                else
                {
                    scale = 1.0;
                }
                ed.scale = scale;

                if (cmdchar == '+') ed.flag_special = 0;
                if (cmdchar == '?') ed.flag_special = 1;

                cdfdata->extras.push_back(ed);

                std::cout << " " << ed.id << " " << filetmp << " " << ed.scale << " " << ed.filename
                          << std::endl;
                break;

            case 'g':
                gd.id = ((TObjString*)arr->At(1))->String()(0);
                tmpstr = ((TObjString*)arr->At(2))->String().Data();
                tmpstr.ReplaceAll('%', ' ');
                gd.name = tmpstr.Data();
                gd.do_split = ((TObjString*)arr->At(3))->String().Atoi();
                gd.attr = ((TObjString*)arr->At(4))->String().Data();
                cdfdata->groups.push_back(gd);
                cdfdata->entries.push_back(edvec);
                break;

            case 's':
                syserrfile_t = ((TObjString*)arr->At(1))->String().Data();
                break;

            default:
                testchar = ((TObjString*)arr->At(0))->String()(0);
                // if it is a letter
                if (testchar >= 'A' and testchar <= 'Z')
                {
                    usecorr = false;
                    testchar = testchar - 'A' + 'a';
                    ed.free_fit = false;
                }
                else
                {
                    ed.free_fit = true;
                }
                ++global_npar;

                CDGroupData gd_comp;
                gd_comp.id = testchar;
                // check if this letter exists in groups list
                iter = std::find(cdfdata->groups.begin(), cdfdata->groups.end(), gd_comp);
                // if group not found, ignore it
                if (iter == cdfdata->groups.end())
                {
                    std::cerr << " + Group " << ((TObjString*)arr->At(0))->String()(0)
                              << " not found! Ingoring entry." << std::endl;
                    continue;
                }

                // fill data
                idx = iter - cdfdata->groups.begin();

                ed.id = ((TObjString*)arr->At(1))->String().Data();
                ed.filename = ((TObjString*)arr->At(2))->String().Data();
                // 				TFile * f = TFile::Open(ed.filename.c_str());
                // 				ed.file = f;
                ed.attr = ((TObjString*)arr->At(3))->String().Data();
                ed.use_corr = usecorr;

                // scaling factor for histogram
                if (arr->GetEntries() >= 5)
                {
                    TString str_scale = ((TObjString*)arr->At(4))->String();
                    scale = str_scale.Atof();
                }
                else
                {
                    scale = 1.0;
                }
                ed.scale = scale;

                cdfdata->entries[idx].push_back(ed);

                std::cout << " " << ed.id << " " << filetmp << " " << ed.scale << " " << ed.filename
                          << std::endl;
                break;
        }
    }

    // if systematic errors file exists
    if (!syserrfile_t.IsNull())
    {
        TFile* syserrfile = TFile::Open(syserrfile_t, "READ");
        if (!syserrfile->IsOpen())
        {
            std::cerr << "File " << syserrfile_t << " is not open!";
            std::exit(EXIT_FAILURE);
        }

        for (size_t a = 0; a < ananum; ++a) // FIXME was local_ananum
        {
            set[a].arrSystematic = (TObjArray*)RootTools::GetObjectFromFile(
                syserrfile, TString::Format("arr_%s_Systematic", ananames[a].c_str()));
        }
    }

    fref = nullptr;

    if (cdfdata->reffile.length())
    {
        TFile* f = ImportFromFile(cdfdata->reffile, fref_ctx, fref_fac, "_SignalCtx");
        if (!f) f = ImportFromFile(cdfdata->reffile, fref_ctx, fref_fac, "_DiscCtx");
        if (!f) abort();

        fref = TFile::Open(cdfdata->reffile.c_str(), "READ");
        getConfigVector();
        getAnaNames();

        CreateCanvases(fref_fac);
    }
    else
    {
        flag_fit = 0;
    }

    if (flag_dump)
    {
        std::cout << "Opening dump file from " << par_dumpfile << std::endl;

        auto strvec = createDumpFileNames(par_dumpfile, flag_sigmamult);
        if (!strvec.size()) abort();

        if (RootTools::FileIsNewer(strvec[0].c_str()))
        {
            dumpdb = DatabaseManager::getDatabase(strvec[0].c_str());
            dumpdb->prepare();
        }
    }

    chandata = new ChannelData[global_npar];

    for (size_t j = 0; j < global_npar; ++j)
    {
        chandata[j].chanmod.reset();
        chandata[j].fac_pars = new DistributionFactory*[ananum];
        chandata[j].ctx_pars = new DistributionContext*[ananum];
        for (size_t a = 0; a < ananum; ++a)
        {
            chandata[j].ctx_pars[a] = nullptr;
        }
    }

    int groupsnum = cdfdata->groups.size();

    for (size_t a = 0; a < ananum; ++a) // FIXME was local_ananum
        hist_ref[a] = set[a].href;

    for (size_t i = 0; i < global_npar; ++i)
    {
        hist_cs_pars[i] = new TH1*[ananum];
    }

    double sm = 1.0;
    if (flag_sigmamult) sm = flag_sigmamult;

    double total_cs = 0;
    double total_cs_eh = 0;
    double total_cs_el = 0;

    npar_cnt = 0;
    for (int g = 0; g < groupsnum; ++g)
    {
        int entrynum = cdfdata->entries[g].size();

        for (int e = 0; e < entrynum; ++e)
        {
            const char* fn = cdfdata->entries[g][e].filename.c_str();
            TFile* f = ImportFromFile(fn, chandata[npar_cnt].ctx_pars, chandata[npar_cnt].fac_pars,
                                      "_SignalCtx", true);
            if (!f)
                f = ImportFromFile(fn, chandata[npar_cnt].ctx_pars, chandata[npar_cnt].fac_pars,
                                   "_DiscCtx", true);
            if (!f)
            {
                std::cerr << "No entries in group" << std::endl;
                abort();
            }
            // 			TFile * f = TFile::Open(fn, "READ");
            cdfdata->entries[g][e].file = f;

            // get input (par) histogram from the sim channel

            for (size_t a = 0; a < ananum; ++a) // FIXME was local_ananum
            {
                // we need only two first
                // 				if (a == 2 or a > 3)
                // 					continue;

                hist_cs_pars[npar_cnt][a] = chandata[npar_cnt].fac_pars[a]->hSignalCounter;
                if (!hist_cs_pars[npar_cnt][a])
                {
                    std::cerr << "No histogram in " << cdfdata->entries[g][e].filename << std::endl;
                    std::exit(1);
                }
                chandata[npar_cnt].chanmod.label = cdfdata->entries[g][e].id;
                chandata[npar_cnt].chanmod.free_fit =
                    (flag_free or (flag_free_cond and cdfdata->entries[g][e].free_fit));

                // fetch cross sections, do it only once
                if (a == 0)
                {
                    // this 1d histogram holds in bins:
                    // [1] - cs, [2] - upper limit (from errors), [3] - lower limit
                    TH1* htemp_cs = (TH1D*)SmartFactory::getObject(f, "h_cs");
                    // If exists set scaling parameters, we scale down histogram by its
                    // cross-sections which later is varied in the fit. Later on histogram must be
                    // scaled by fit result
                    if (htemp_cs)
                    {
                        chandata[npar_cnt].chanmod.has_cs_scalings = true;
                        chandata[npar_cnt].chanmod.cs_scalings =
                            htemp_cs->GetBinContent(1) * cdfdata->entries[g][e].scale;
                        chandata[npar_cnt].chanmod.cs_errors_u =
                            htemp_cs->GetBinContent(2) * cdfdata->entries[g][e].scale;
                        chandata[npar_cnt].chanmod.cs_errors_l =
                            htemp_cs->GetBinContent(3) * cdfdata->entries[g][e].scale;
                        chandata[npar_cnt].chanmod.cs_limits_u =
                            chandata[npar_cnt].chanmod.cs_scalings +
                            sm * chandata[npar_cnt].chanmod.cs_errors_u;
                        chandata[npar_cnt].chanmod.cs_limits_l =
                            chandata[npar_cnt].chanmod.cs_scalings -
                            sm * chandata[npar_cnt].chanmod.cs_errors_l;
                        printf(" [cs] Scaling for %s is %f +%f -%f\n", fn,
                               chandata[npar_cnt].chanmod.cs_scalings,
                               chandata[npar_cnt].chanmod.cs_limits_u,
                               chandata[npar_cnt].chanmod.cs_limits_l);

                        if (chandata[npar_cnt].chanmod.cs_limits_l < 0)
                            chandata[npar_cnt].chanmod.cs_limits_l = 0;

                        total_cs += chandata[npar_cnt].chanmod.cs_scalings;
                        total_cs_eh += chandata[npar_cnt].chanmod.cs_errors_u *
                                       chandata[npar_cnt].chanmod.cs_errors_u;
                        total_cs_el += chandata[npar_cnt].chanmod.cs_errors_l *
                                       chandata[npar_cnt].chanmod.cs_errors_l;

                        if (!(flag_fit and !flag_nocsfit))
                            chandata[npar_cnt].chanmod.fit_cs_scalings =
                                chandata[npar_cnt].chanmod.cs_scalings;
                    }
                    else
                    {
                        printf("No CS found, default scaling for %s is %f\n", fn,
                               chandata[npar_cnt].chanmod.cs_scalings);
                    }

                    TH2* htemp_ang = (TH2D*)SmartFactory::getObject(f, "h_ang");
                    // If exists set scaling parameters, we scale down histogram by its
                    // cross-sections which later is varied in the fit. Later on histogram must be
                    // scaled by fit result
                    if (htemp_ang /* and e != 1*/) // FIXME
                    {
                        chandata[npar_cnt].chanmod.has_ang_scalings = true;
                        chandata[npar_cnt].chanmod.ang_scalings[0] = htemp_ang->GetBinContent(1, 2);
                        chandata[npar_cnt].chanmod.ang_scalings[1] = htemp_ang->GetBinContent(2, 2);
                        chandata[npar_cnt].chanmod.ang_scalings[2] = htemp_ang->GetBinContent(3, 2);

                        chandata[npar_cnt].chanmod.ang_errors_u[0] = htemp_ang->GetBinContent(1, 3);
                        chandata[npar_cnt].chanmod.ang_errors_l[0] = htemp_ang->GetBinContent(1, 1);
                        chandata[npar_cnt].chanmod.ang_errors_u[1] = htemp_ang->GetBinContent(2, 3);
                        chandata[npar_cnt].chanmod.ang_errors_l[1] = htemp_ang->GetBinContent(2, 1);
                        chandata[npar_cnt].chanmod.ang_errors_u[2] = htemp_ang->GetBinContent(3, 3);
                        chandata[npar_cnt].chanmod.ang_errors_l[2] = htemp_ang->GetBinContent(3, 1);

                        chandata[npar_cnt].chanmod.ang_limits_u[0] =
                            chandata[npar_cnt].chanmod.ang_scalings[0] +
                            sm * chandata[npar_cnt].chanmod.ang_errors_u[0];
                        chandata[npar_cnt].chanmod.ang_limits_l[0] =
                            chandata[npar_cnt].chanmod.ang_scalings[0] -
                            sm * chandata[npar_cnt].chanmod.ang_errors_l[0];
                        chandata[npar_cnt].chanmod.ang_limits_u[1] =
                            chandata[npar_cnt].chanmod.ang_scalings[1] +
                            sm * chandata[npar_cnt].chanmod.ang_errors_u[1];
                        chandata[npar_cnt].chanmod.ang_limits_l[1] =
                            chandata[npar_cnt].chanmod.ang_scalings[1] -
                            sm * chandata[npar_cnt].chanmod.ang_errors_l[1];
                        chandata[npar_cnt].chanmod.ang_limits_u[2] =
                            chandata[npar_cnt].chanmod.ang_scalings[2] +
                            sm * chandata[npar_cnt].chanmod.ang_errors_u[2];
                        chandata[npar_cnt].chanmod.ang_limits_l[2] =
                            chandata[npar_cnt].chanmod.ang_scalings[2] -
                            sm * chandata[npar_cnt].chanmod.ang_errors_l[2];

                        // 						printf(" [ang] Scaling for %s is %f +%f -%f\n", fn,
                        // chandata[npar_cnt].chanmod.ang_scalings,
                        // chandata[npar_cnt].chanmod.ang_limits_u,
                        // chandata[npar_cnt].chanmod.ang_limits_l);

                        if (!flag_noang /* and flag_noangfit*/)
                        {
                            chandata[npar_cnt].chanmod.fit_ang_scalings[0] =
                                chandata[npar_cnt].chanmod.ang_scalings[0];
                            chandata[npar_cnt].chanmod.fit_ang_scalings[1] =
                                chandata[npar_cnt].chanmod.ang_scalings[1];
                            chandata[npar_cnt].chanmod.fit_ang_scalings[2] =
                                chandata[npar_cnt].chanmod.ang_scalings[2];
                        }
                    }
                    else
                    {
                        printf("No CS found, default scaling for %s is %f\n", fn,
                               chandata[npar_cnt].chanmod.ang_scalings[0]);
                    }
                }

                // 				If no CS data given, we keep original value and fit around 1.0
                // scaling factor 				if (chandata[npar_cnt].chanmod.has_cs_scalings)
                // 					hist_cs_pars[npar_cnt][a]->Scale(1./chandata[npar_cnt].chanmod.cs_scalings);
            }

            ++npar_cnt;
        }
    }

    total_cs_eh = sqrt(total_cs_eh);
    total_cs_el = sqrt(total_cs_el);
    printf("Total cs = %g +%g -%g\n", total_cs, total_cs_eh, total_cs_el);

    sigmamult = flag_sigmamult;
}

void CompareDistributions::CreateCanvases(DistributionFactory* fac[])
{
    for (size_t a = 0; a < ananum; ++a) // FIXME was local_ananum
    {
        if (DIM0 == fac[0]->ctx.dim)
            binsnum = 1;
        else
            binsnum = fac[a]->ctx.x.bins;

        fa[a] = new DistributionFactory(fac[a]->ctx);
        fa[a]->init();
        if (fref_fac and fref_fac[a]) set[a].href = (TH2DA*)fref_fac[a]->hSignalCounter;

        if (!set[a].href)
        {
            std::cerr << "No histogram found\n";
            continue;
        }

        set[a].hsum = (TH2DA*)set[a].href->Clone(TString::Format("hsum_%d", (int)a));
        set[a].hsum->Reset();

        set[a].canleg = fa[a]->RegCanvas("c_@@@a_legend", "c_legend", 200, 400);

        set[a].cLambda_comp = fa[a]->RegCanvas("c_@@@a_Comparison", "@@@a Comparison", 800, 600);
        RootTools::NicePad(set[a].cLambda_comp, pf_comp);
        set[a].cLambda_comp->DivideSquare(binsnum, 0, 0);

        set[a].cLambda_final =
            fa[a]->RegCanvas("c_@@@a_Comparison_final", "@@@a Comparison", 800, 600);
        set[a].cLambda_final->SetLeftMargin(0);

        set[a].cLambda_comp_log =
            fa[a]->RegCanvas("c_@@@a_Comparison_log", "@@@a Comparison", 800, 600);
        RootTools::NicePad(set[a].cLambda_comp_log, pf_comp);
        set[a].cLambda_comp_log->DivideSquare(binsnum, 0, 0);

        set[a].cLambda_final_log =
            fa[a]->RegCanvas("c_@@@a_Comparison_final_log", "@@@a Comparison", 800, 600);

        for (uint i = 0; i < binsnum; ++i)
        {
            set[a].cLambda_comp_log->cd(1 + i)->SetLogy();
        }

        if (flag_inty)
        {
            set[a].cIntY = fa[a]->RegCanvas("c_@@@a_IntY", "@@@a Integral", 800, 600);
        }

        // 		cLambda_yprojs[a] = fa[a]->RegCanvas("c_@@@a_ComparisonY", "@@@a Comparison Y", 800,
        // 600); 		RootTools::NicePad(cLambda_yprojs[a], pf);
    }
}

void CompareDistributions::AnaExecute()
{
    // cs, 3*ang - 1 -- we omit a0 since is always 1.0
    size_t total_npar = (1 + ang_cnt - 1) * global_npar;

    if (flag_fit)
    {
        legpol = new TF1("legpol", "angdist", -1, 1);

        TMinuit* gMinuit = new TMinuit(total_npar);
        gmin = gMinuit;

        gMinuit->SetFCN(glob_fit_fun);
        // 		gMinuit->SetObjectFit(this); TODO

        Double_t arglist[10];

        Int_t iflag = 0;
        arglist[0] = 1;
        gMinuit->mnexcm("SET ERR", arglist, 1, iflag);
        arglist[0] = 2;
        gMinuit->mnexcm("SET STRATEGY", arglist, 1, iflag);

        char buff[200];
        for (uint i = 0; i < global_npar; ++i)
        {
            sprintf(buff, "%s_cs", chandata[i].chanmod.label.c_str());
            // parn,name,start,stepsize,limit_low,limit_up,iflag

            double l_a = 0.0;
            double l_b = 0.0;

            if (!chandata[i].chanmod.free_fit)
            {
                l_a = chandata[i].chanmod.cs_limits_l;
                l_b = chandata[i].chanmod.cs_limits_u;
            }

            gMinuit->mnparm(i, buff, chandata[i].chanmod.cs_scalings,
                            chandata[i].chanmod.cs_scalings * 0.05, l_a, l_b, iflag);

            if (flag_nocsfit) { gMinuit->FixParameter(i); }
        }

        for (uint j = 1; j < ang_cnt; ++j)
        {
            for (uint i = 0; i < global_npar; ++i)
            {
                sprintf(buff, "%s_ang%d", chandata[i].chanmod.label.c_str(), j);

                bool set_fixed = false;
                double ang_limits_u = 0.0;
                double ang_limits_l = 0.0;
                double ang_scalings = chandata[i].chanmod.ang_scalings[j];
                double ang_scalings_step = (ang_scalings == 0.0) ? 0.01 : (0.05 * ang_scalings);

                if (chandata[i].chanmod.ang_errors_u[j] == 0 and
                    chandata[i].chanmod.ang_errors_l[j] == 0)
                {
                    set_fixed = true;
                    ang_limits_u =
                        chandata[i].chanmod.ang_limits_u[j] +
                        (chandata[i].chanmod.ang_limits_u[j] ? chandata[i].chanmod.ang_limits_u[j]
                                                             : 1);
                    ang_limits_l =
                        chandata[i].chanmod.ang_limits_l[j] -
                        (chandata[i].chanmod.ang_limits_l[j] ? chandata[i].chanmod.ang_limits_l[j]
                                                             : 1);
                }
                else
                {
                    ang_limits_u = chandata[i].chanmod.ang_limits_u[j];
                    ang_limits_l = chandata[i].chanmod.ang_limits_l[j];
                }

                // there is j-1 dut to skipping a0
                gMinuit->mnparm(global_npar * j + i, buff, ang_scalings, ang_scalings_step,
                                ang_limits_l, ang_limits_u, iflag);

                // fix parameters if no errors specified, e.g. flat phase-space
                if (set_fixed or flag_noangfit) { gMinuit->FixParameter(global_npar * j + i); }
            }
        }

        gMinuit->mnexcm("CALL FCN", arglist, 1, iflag);

        arglist[0] = 5000;
        arglist[1] = 1.;

        gMinuit->mnexcm("MIGRAD", arglist, 2, iflag);
        gMinuit->mnexcm("HESSE", arglist, 2, iflag);

        // 		arglist[0]=0;
        // 		gMinuit->mnexcm("SCAN", arglist, 1, iflag);

        for (uint i = 0; i < global_npar; ++i)
        {
            // parn,name,start,stepsize,limit_low,limit_up,iflag
            gMinuit->GetParameter(i, chandata[i].chanmod.fit_cs_scalings,
                                  chandata[i].chanmod.fit_cs_errors);
            printf("Fit %s obtained: %g +- %g ( %g +%g -%g ) ", chandata[i].chanmod.label.c_str(),
                   chandata[i].chanmod.fit_cs_scalings, chandata[i].chanmod.fit_cs_errors,
                   chandata[i].chanmod.cs_scalings, chandata[i].chanmod.cs_errors_u,
                   chandata[i].chanmod.cs_errors_l);

            for (uint j = 1; j < ang_cnt; ++j)
            {
                gMinuit->GetParameter(global_npar * j + i, chandata[i].chanmod.fit_ang_scalings[j],
                                      chandata[i].chanmod.fit_ang_errors[j]);
            }

            for (uint j = 0; j < ang_cnt; ++j)
                printf("[%d] = %g +- %g ( %g +%g -%g ) ", j * 2,
                       chandata[i].chanmod.fit_ang_scalings[j],
                       chandata[i].chanmod.fit_ang_errors[j], chandata[i].chanmod.ang_scalings[j],
                       chandata[i].chanmod.ang_errors_u[j], chandata[i].chanmod.ang_errors_l[j]);

            putchar('\n');
        }

        int npars = gMinuit->GetNumPars();
        cov_mat = new double[npars * npars];
        gMinuit->mnemat(cov_mat, npars);
        printf(" === cov mat obtained ===\n");
    }

    PreparePlots();
}

void CompareDistributions::AnaFinalize()
{
    OpenExportFile();

    setConfigVector()->Write();

    TLatex* latex = new TLatex();
    latex->SetNDC();
    latex->SetTextColor(/*36*/ 1);
    latex->SetTextSize(0.12);
    latex->SetTextAlign(23);

    TPad* pad_comp = nullptr;
    TPad* pad_leg = nullptr;

    RootTools::PadFormat pf;
    pf.marginTop = 0.06;
    pf.marginRight = 0.1;
    pf.marginBottom = 0.11;
    pf.marginLeft = 0.1;

    double b_x1 = pf.marginLeft;
    double b_x2 = 0.7;
    double b_y1 = pf.marginBottom;
    double b_y2 = 1.0 - pf.marginTop;

    float x_to = 1.03;
    float x_ts = 0.045;
    float y_to = 1.1;
    float y_ts = 0.045;

    float x_lo = 0.005;
    float x_ls = 0.045;
    float y_lo = 0.005;
    float y_ls = 0.045;

    for (size_t a = 0; a < ananum; ++a) // FIXME was local_ananum
    {
        if (a > 1) continue;

        const DistributionContext** ctx = (const DistributionContext**)chandata[0].ctx_pars;

        TGaxis* axis = 0;
        int row_axeses = ceil((binsnum) / 2.);
        int col_axeses = ceil((binsnum) / 2.);

        //*********************************************************************
        // regular plots
        set[a].cLambda_final->cd();
        pad_comp = new TPad(TString::Format("p%lu_comp", a), "_comp", b_x1, b_y1, b_x2, b_y2);
        pad_comp->Draw();
        pad_comp->cd();
        RootTools::NicePad(gPad, pf);
        gPad->SetRightMargin(0.2);
        set[a].cLambda_comp->SetRightMargin(0.2);
        set[a].cLambda_comp->DrawClonePad();

        // 		float syst_u = 0;
        // 		float syst_l = 0;
        //
        // 		if (a == 0)
        // 		{
        // 			syst_u = 4.4;
        // 			syst_l = 5.1;
        // 		}
        // 		else if (a == 1)
        // 		{
        // 			syst_u = 6.2;
        // 			syst_l = 7.6;
        // 		}

        TPaveText* pt = new TPaveText(0.44, 0.39, 0.76, 0.49);
        // 		pt->SetTextSize(0.05);
        pt->SetShadowColor(0);
        pt->SetBorderSize(0);
        pt->SetFillColor(0);
        pt->SetTextFont(22);
        // 		pt->AddText("Normalization error of 7.3% and");
        // 		pt->AddText("systematic error for the production");
        // 		pt->AddText(TString::Format("model ^{+%.1f%%}_{-%.1f%%} are not shown.", syst_u,
        // syst_l)); 		pt->AddText("Normalization error of 7.3%"); 		pt->AddText("is not
        // shown."); 		pt->Draw();

        set[a].cLambda_final->cd();
        pad_leg = new TPad(TString::Format("l%lu_leg", a), "_leg", b_x2 + 0.01, 0.01, 0.99, 0.95);
        pad_leg->Draw();
        pad_leg->cd();
        RootTools::NicePad(pad_leg, 0., 0., 0., 0.01);
        set[a].canleg->DrawClonePad(); // FIXME

        // 		latex->DrawLatex(0.5, 1.0, Form("d#sigma/d%s", ctx[a]->x.label.Data()));

        set[a].cLambda_final->cd();
        // y-axis
        for (int i = 0; i < col_axeses; ++i)
        {
            double axis_height = (b_y2 - b_y1) / col_axeses;
            axis = custom_axis(b_x1, b_y1 + i * axis_height, b_x1, b_y1 + (1.0 + i) * axis_height,
                               0.0, set[a].cfg_maxy, 510, "");
            if (DIM0 == ctx[a]->dim)
                axis->SetTitle(
                    Form("d#sigma/d%s [#mub/%s]", ctx[a]->x.label.Data(), ctx[a]->x.unit.Data()));
            else
                axis->SetTitle(
                    Form("d#sigma/d%s [#mub/%s]", ctx[a]->y.label.Data(), ctx[a]->y.unit.Data()));
            axis->SetTitleOffset(y_to);
            axis->SetTitleSize(y_ts);
            axis->SetLabelOffset(y_lo);
            axis->SetLabelSize(y_ls);
        }

        // x-axis
        for (int i = 0; i < row_axeses; ++i)
        {
            double axis_width = (b_x2 - b_x1) / row_axeses;
            if (DIM0 == ctx[a]->dim)
            {
                axis = custom_axis(b_x1 + i * axis_width, b_y1, b_x1 + (1.0 + i) * axis_width, b_y1,
                                   ctx[a]->x.min, ctx[a]->x.max, 510, "");
                axis->SetTitle(
                    TString::Format("%s [%s]", ctx[a]->x.label.Data(), ctx[a]->x.unit.Data())
                        .Data());
            }
            else
            {
                axis = custom_axis(b_x1 + i * axis_width, b_y1, b_x1 + (1.0 + i) * axis_width, b_y1,
                                   ctx[a]->x.min, ctx[a]->y.max, 3, "");
                axis->SetTitle(
                    TString::Format("%s [%s]", ctx[a]->y.label.Data(), ctx[a]->y.unit.Data())
                        .Data());
            }
            axis->SetTitleOffset(x_to);
            axis->SetTitleSize(x_ts);
            axis->SetLabelOffset(x_lo);
            axis->SetLabelSize(x_ls);
        }

        //*********************************************************************
        // log plots
        set[a].cLambda_final_log->cd();
        pad_comp = new TPad(TString::Format("p%lu_comp_log", a), "_comp", b_x1, b_y1, b_x2, b_y2);
        pad_comp->Draw();
        pad_comp->cd();
        RootTools::NicePad(gPad, pf);
        set[a].cLambda_comp_log->DrawClonePad();

        set[a].cLambda_final_log->cd();
        pad_leg =
            new TPad(TString::Format("l%lu_leg_log", a), "_leg", b_x2 + 0.01, 0.01, 0.99, 0.95);
        pad_leg->Draw();
        pad_leg->cd();
        RootTools::NicePad(pad_leg, 0., 0., 0., 0.01);
        set[a].canleg->DrawClonePad();

        // 		latex->DrawLatex(0.5, 1.0, Form("d#sigma/d%s", ctx[a]->x.label.Data()));

        set[a].cLambda_final_log->cd();
        // y-axis
        for (int i = 0; i < col_axeses; ++i)
        {
            double axis_height = (b_y2 - b_y1) / col_axeses;
            axis = custom_axis(b_x1, b_y1 + i * axis_height, b_x1, b_y1 + (1.0 + i) * axis_height,
                               set[a].ana_logy_lowlimit, set[a].cfg_maxy, 510,
                               "RBSG"); // -RBSG before, why - ?
            if (DIM0 == ctx[a]->dim)
                axis->SetTitle(
                    Form("d N/d%s [1/%s]", ctx[a]->x.label.Data(), ctx[a]->x.unit.Data()));
            else
                axis->SetTitle(
                    Form("d#sigma/d%s [#mub/%s]", ctx[a]->y.label.Data(), ctx[a]->y.unit.Data()));
            axis->SetTitleOffset(y_to);
            axis->SetTitleSize(y_ts);
            axis->SetLabelOffset(y_lo);
            axis->SetLabelSize(y_ls);
        }

        // x-axis
        for (int i = 0; i < row_axeses; ++i)
        {
            double axis_width = (b_x2 - b_x1) / row_axeses;
            if (DIM0 == ctx[a]->dim)
            {
                axis = custom_axis(b_x1 + i * axis_width, b_y1, b_x1 + (1.0 + i) * axis_width, b_y1,
                                   ctx[a]->x.min, ctx[a]->x.max, 510, "");
                axis->SetTitle(
                    TString::Format("%s [%s]", ctx[a]->x.label.Data(), ctx[a]->x.unit.Data())
                        .Data());
            }
            else
            {
                axis = custom_axis(b_x1 + i * axis_width, b_y1, b_x1 + (1.0 + i) * axis_width, b_y1,
                                   ctx[a]->x.min, ctx[a]->y.max, 3, "");
                axis->SetTitle(
                    TString::Format("%s [%s]", ctx[a]->y.label.Data(), ctx[a]->y.unit.Data())
                        .Data());
            }
            axis->SetTitleOffset(x_to);
            axis->SetTitleSize(x_ts);
            axis->SetLabelOffset(x_lo);
            axis->SetLabelSize(x_ls);
        }

        fa[a]->write(GetExportFile());
    }

    CloseExportFile();

    if (flag_dosumfile)
    {
        std::cout << "Sum file requesting, creating " << par_sumfile;
        TFile* sum_file = TFile::Open(par_sumfile.c_str(), "RECREATE");

        if (!sum_file) { std::cerr << " [ failed ]" << std::endl; }
        else
        {
            sum_file->cd();
            setConfigVector(sum_file)->Write();

            if (cov_mat)
            {
                TMatrixD matrix(global_npar * 3, global_npar * 3, cov_mat);
                matrix.Write("cov_matrix");
            }

            for (size_t a = 0; a < ananum; ++a) // FIXME was local_ananum
            {
                chandata[0].fac_pars[a]->ctx.hist_name = chandata[0].ctx_pars[a]->name;
                chandata[0].fac_pars[a]->reinit();
                // 				fac_pars[0][a]->prepareDiffCanvas();
                chandata[0].fac_pars[a]->prepareCanvas();
                chandata[0].fac_pars[a]->write(sum_file);
                chandata[0].fac_pars[a]->ctx.Write();
            }
            sum_file->Close();

            std::cout << " [ done ]" << std::endl;
        }
    }

    for (int i = 0; i < npar_cnt; ++i)
    {
        for (uint a = 0; a < ananum; ++a)
        {
            // 			delete fac_pars[i][a];
        }
    }

    if (flag_fit and (!flag_nocsfit or !flag_noangfit))
    {
        std::ofstream ofs_fit_cfg(fit_cfg);
        for (uint i = 0; i < fit_ana_selection.size(); ++i)
            ofs_fit_cfg << fit_ana_selection[i] << " ";
        ofs_fit_cfg << std::endl;

        for (uint i = 0; i < global_npar; ++i)
            ofs_fit_cfg << chandata[i].chanmod.label << " " << chandata[i].chanmod.fit_cs_scalings
                        << " " << chandata[i].chanmod.fit_cs_errors << " "
                        << chandata[i].chanmod.fit_ang_scalings[1] << " "
                        << chandata[i].chanmod.fit_ang_errors[1] << " "
                        << chandata[i].chanmod.fit_ang_scalings[2] << " "
                        << chandata[i].chanmod.fit_ang_errors[2] << std::endl;
    }

    if (flag_dump)
    {
        std::cout << "Creating dump file from " << par_dumpfile << std::endl;

        for (uint i = 0; i < global_npar; ++i)
        {
            // get record
            DatabaseRecord dbrec = dumpdb->getRecord(chandata[i].chanmod.label);

            // x-section
            dbrec.cs = chandata[i].chanmod.fit_cs_scalings;
            dbrec.cs_errors_str = std::to_string(chandata[i].chanmod.fit_cs_errors);

            // ang. dists.
            dbrec.ang[1] = chandata[i].chanmod.fit_ang_scalings[1];
            dbrec.ang_errors_str[1] = std::to_string(chandata[i].chanmod.fit_ang_errors[1]);
            dbrec.ang[2] = chandata[i].chanmod.fit_ang_scalings[2];
            dbrec.ang_errors_str[2] = std::to_string(chandata[i].chanmod.fit_ang_errors[2]);

            // put back record
            dumpdb->pushRecord(dbrec);
        }

        auto strvec = createDumpFileNames(par_dumpfile, flag_sigmamult);
        if (strvec.size() < 2) abort();

        dumpdb->dump(strvec[1].c_str());
    }
}

int CompareDistributions::config_params(struct option*& opts)
{
    static struct option lopts[] = {
        {"corr", required_argument, 0, 1001},
        {"useae", no_argument, &flag_useae, 1},
        {"nosum", no_argument, &flag_nosum, 1},
        {"noscale", no_argument, &flag_noscale, 1},
        {"noangfit", no_argument, &flag_noangfit, 1},
        {"noang", no_argument, &flag_noang, 1},
        {"nocsfit", no_argument, &flag_nocsfit, 1},
        {"inty", no_argument, &flag_inty, 1},
        {"negy", no_argument, &flag_negy, 1},
        {"max-scale", required_argument, 0, 1002},
        {"main-scale", required_argument, 0, 1003},
        {"fit", optional_argument, 0, 1004},
        {"sum", optional_argument, 0, 1005},
        {"dump", required_argument, 0, 1006},
        {"sm", required_argument, 0, 1007},
        {"free", no_argument, &flag_free, 1},
        {"cond-free", no_argument, &flag_free_cond, 1},
    };
    opts = lopts;

    return sizeof(lopts) / sizeof(option);
}

void CompareDistributions::config_execute(int code, char* optarg)
{
    switch (code)
    {
        case 1001:
            par_corr = optarg;
            break;

        case 1002:
            par_max_scale = atof(optarg);
            break;

        case 1003:
            par_min_scale = atof(optarg);
            break;

        case 1004:
            flag_fit = 1;
            if (optarg)
            {
                fit_cfg = optarg;
                flag_fit = 2;
            }
            break;

        case 1005:
            flag_nosum = 0;
            if (optarg)
            {
                par_sumfile = optarg;
                flag_dosumfile = 1;
            }
            break;

        case 1006:
            flag_dump = 1;
            if (optarg) { par_dumpfile = optarg; }
            break;

        case 1007:
            flag_sigmamult = atof(optarg);
            break;

        default:
            TString ftmp = optarg;
            configfile_t = ftmp;
            return;
            break;
    }
}

void CompareDistributions::PreparePlots()
{
    TLatex* latex = new TLatex();
    latex->SetNDC();
    latex->SetTextColor(/*36*/ 1);
    latex->SetTextFont(23);
    latex->SetTextSize(16);
    latex->SetTextAlign(23);

    // 	TH2F * hsum[ananum];
    TH2F* hgrsum[ananum];
    TLegend* leg[ananum];

    std::string real_same = "";

    // loop over analysises
    for (size_t a = 0; a < ananum; ++a) // FIXME was local_ananum
    {
        // 		if (a == 2 or a > 3)
        // 			continue;

        Double_t max_tot = 0., min_tot = 99999.;
        Double_t inty_max_tot = 0., inty_min_tot = 0.;

        leg[a] = new TLegend(0.01 /*77*/, 0.01, 1.0, 1.0);
        leg[a]->SetFillColor(kWhite);
        leg[a]->SetTextSize(0.07);

        TGraphAsymmErrors* gr = nullptr;

        std::string refdrawopt = "X0,E1";
        int hist_cnt = 0;

        if (fref)
        {
            RootTools::NiceHistogram(set[a].href, cdfdata->refAttr);

            // 			binsnum = set[a].href->GetNbinsX(); TODO should it be put back

            int uid =
                DrawProjection(set[a].cLambda_comp, set[a].href,
                               RootTools::MergeOptions("", set[a].href->GetOption(), refdrawopt),
                               TH2DA_PROJ, rand() % 1000 + 1);
            DrawProjection(set[a].cLambda_comp_log, set[a].href,
                           RootTools::MergeOptions("", set[a].href->GetOption(), refdrawopt),
                           TH2DA_PROJ, uid);
            RootTools::FindBoundaries(set[a].href, min_tot, max_tot);

            if (flag_inty)
            {
                Double_t m = DrawIntegralY(
                    set[a].cIntY, set[a].href,
                    RootTools::MergeOptions("", set[a].href->GetOption(), refdrawopt), uid);
                RootTools::FindRangeExtremum(inty_min_tot, inty_max_tot, m);
            }

            leg[a]->AddEntry(set[a].href, "p+p@3.5 GeV", "lpe");
            // 			leg[a]->AddEntry((TObject*)nullptr, "", "");

            // 			uid = DrawProjectionY(cLambda_yprojs[a], set[a].href,
            // RootTools::MergeOptions("", set[a].href->GetOption(), refdrawopt), TH2DA_PROJ, rand()
            // % 1000 + 1);

            real_same = "same";
        }

        if (!a and !fref and cdfdata->entries.size() and cdfdata->entries[0].size() and set[a].href)
        {
            // 			CreateCanvases(cdfdata->entries[0][0].file);
            CreateCanvases(chandata[0].fac_pars);
            // 			binsnum = set[a].href->GetNbinsX(); TODO should it be put back
        }

        std::string tmpname =
            TString::Format("%s/h_%s_Signal", ananames[a].c_str(), ananames[a].c_str()).Data();

        size_t extrasnum = cdfdata->extras.size();
        for (uint e = 0; e < extrasnum; ++e)
        {
            // 			if (cdfdata->extras[e].flag_special == 1 /*and !(flag_fit or !flag_noang)*/)
            // 				continue;

            std::string strid = cdfdata->extras[e].id;

            // 			cdfdata->extras[e].file = TFile::Open(cdfdata->extras[e].filename.c_str(),
            // "READ"); 			TH2 * htemp =
            // (TH2F*)SmartFactory::getObject(cdfdata->extras[e].file, tmpname.c_str());

            DistributionContext* ctx[ananum] = {nullptr};
            DistributionFactory* fac[ananum];

            TFile* f = ImportFromFile(cdfdata->extras[e].filename, ctx, fac, "_SignalCtx", true);
            if (!f) f = ImportFromFile(cdfdata->extras[e].filename, ctx, fac, "_DiscCtx", true);
            if (!f)
            {
                PR(cdfdata->extras[e].filename);
                abort();
            }
            TH1* htemp = fac[a]->hSignalCounter;

            RootTools::NiceHistogram(htemp, cdfdata->extras[e].attr);

            int uid = DrawProjection(
                set[a].cLambda_comp, (TH2*)htemp,
                RootTools::MergeOptions(real_same.c_str(), htemp->GetOption(), "hist,]["),
                TH2DA_AE_GRAPH, rand() % 1000 + 1);
            DrawProjection(
                set[a].cLambda_comp_log, (TH2*)htemp,
                RootTools::MergeOptions(real_same.c_str(), htemp->GetOption(), "hist,]["),
                TH2DA_AE_GRAPH, uid);
            RootTools::FindBoundaries(htemp, min_tot, max_tot, kFALSE);
            if (flag_inty)
            {
                Double_t m = DrawIntegralY(
                    set[a].cIntY, (TH2*)htemp,
                    RootTools::MergeOptions(real_same.c_str(), htemp->GetOption(), "hist,]["), uid);
                RootTools::FindRangeExtremum(inty_min_tot, inty_max_tot, m);
            }

            TString _id = cdfdata->extras[e].id;
            _id.ReplaceAll("__", " ");

            htemp->SetFillColor(6);
            htemp->SetFillStyle(error_fill_pattern);

            leg[a]->AddEntry(htemp, _id, "lpf");
        }
        if (!flag_nosum)
        {
            // 			TGraphErrors * grerr = new TGraphErrors(3);
            // 			grerr->SetFillColor(6);
            // 			grerr->SetFillStyle(error_fill_pattern);
            // 			grerr->Draw("same");
            // 			leg[a]->AddEntry(grerr, "Prod. modell fit", "lfp");

            leg[a]->AddEntry(set[a].hsum, "Prod. model fit", "lfp");
        }

        if (fref or extrasnum) { leg[a]->AddEntry((TObject*)nullptr, "", ""); }

        int groupsnum = cdfdata->groups.size();
        for (int g = 0; g < groupsnum; ++g)
        {
            int entrynum = cdfdata->entries[g].size();
            int split = cdfdata->groups[g].do_split;

            if (split == 0)
            {
                hgrsum[a] = (TH2F*)set[a].hsum->Clone(TString::Format(
                    "h_%s_%s_gr%d", ananames[a].c_str(), set[a].hsum->GetName(), g));
                hgrsum[a]->Reset();
            }

            for (int e = 0; e < entrynum; ++e)
            {
                std::string strid = cdfdata->entries[g][e].id;

                if (flag_dosumfile)
                {
                    if (!flag_noscale)
                    {
                        PR(chandata[hist_cnt].fac_pars[a]->hSignalCounter->Integral());
                        chandata[hist_cnt].fac_pars[a]->scale(
                            chandata[hist_cnt].chanmod.fit_cs_scalings /
                            chandata[hist_cnt].chanmod.cs_scalings);
                        PR(a);
                        PR(chandata[hist_cnt].chanmod.fit_cs_scalings /
                           chandata[hist_cnt].chanmod.cs_scalings);
                        PR(chandata[hist_cnt].fac_pars[a]->hSignalCounter->Integral());
                    }

                    // 					if (!flag_noang and a == 1 and
                    // chanmod[hist_cnt].has_ang_scalings)
                    // 					{
                    // 						fac_pars[hist_cnt][a]->applyAngDists(chanmod[hist_cnt].fit_ang_scalings[1],
                    // chanmod[hist_cnt].fit_ang_scalings[2]);
                    // 					}

                    if (hist_cnt > 0) *chandata[0].fac_pars[a] += *chandata[hist_cnt].fac_pars[a];
                }

                TH1* htemp = hist_cs_pars[hist_cnt][a];
                if (!flag_noscale)
                {
                    // 					float err_u = 0.0;
                    // 					float err_l = 0.0;
                    // 					if (a == 2)
                    // 				{
                    // 						err_u = htemp->GetBinErrorH()
                    // 				}
                    // 					htemp->Scale(chanmod[hist_cnt].fit_cs_scalings);
                    //           PR(chandata[hist_cnt].chanmod.fit_cs_scalings/chandata[hist_cnt].chanmod.cs_scalings);
                    // 					htemp->Scale(chandata[hist_cnt].chanmod.fit_cs_scalings/chandata[hist_cnt].chanmod.cs_scalings);
                }

                // 				if (!flag_noang and a == 1 and chanmod[hist_cnt].has_ang_scalings)
                // 				{
                // 					DistributionFactory::applyAngDists(htemp,
                // chanmod[hist_cnt].fit_ang_scalings[1], chanmod[hist_cnt].fit_ang_scalings[2]);
                // 				}

                ++hist_cnt;

                if (!flag_nosum) { set[a].hsum->Add(htemp); }

                if (split == 1 and a < 2)
                {
                    RootTools::NiceHistogram(htemp, cdfdata->entries[g][e].attr);

                    int uid = DrawProjection(
                        set[a].cLambda_comp, htemp,
                        RootTools::MergeOptions(real_same.c_str(), htemp->GetOption(), "hist,]["),
                        TH2D_PROJ, rand() % 1000 + 1);
                    DrawProjection(
                        set[a].cLambda_comp_log, htemp,
                        RootTools::MergeOptions(real_same.c_str(), htemp->GetOption(), "hist,]["),
                        TH2D_PROJ, uid);
                    RootTools::FindBoundaries(htemp, min_tot, max_tot, kFALSE);
                    if (flag_inty)
                    {
                        Double_t m =
                            DrawIntegralY(set[a].cIntY, htemp,
                                          RootTools::MergeOptions(real_same.c_str(),
                                                                  htemp->GetOption(), "hist,]["),
                                          uid);
                        RootTools::FindRangeExtremum(inty_min_tot, inty_max_tot, m);
                    }

                    TString t = htemp->GetTitle();
                    if (0 == t.Length()) t = cdfdata->entries[g][e].id;

                    leg[a]->AddEntry(htemp, t, "lp");
                }
                else if (split == 0 and a < 2)
                {
                    hgrsum[a]->Add(htemp);
                }
            }

            if (split == 0)
            {
                RootTools::NiceHistogram(hgrsum[a], cdfdata->groups[g].attr);

                int uid = DrawProjection(
                    set[a].cLambda_comp, hgrsum[a],
                    RootTools::MergeOptions(real_same.c_str(), hgrsum[a]->GetOption(), "hist"),
                    TH2D_PROJ, rand() % 1000 + 1);
                DrawProjection(
                    set[a].cLambda_comp_log, hgrsum[a],
                    RootTools::MergeOptions(real_same.c_str(), hgrsum[a]->GetOption(), "hist"),
                    TH2D_PROJ, uid);
                RootTools::FindBoundaries(hgrsum[a], min_tot, max_tot, kFALSE);
                if (flag_inty)
                {
                    Double_t m = DrawIntegralY(
                        set[a].cIntY, hgrsum[a],
                        RootTools::MergeOptions(real_same.c_str(), hgrsum[a]->GetOption(), "hist"),
                        uid);
                    RootTools::FindRangeExtremum(inty_min_tot, inty_max_tot, m);
                }

                leg[a]->AddEntry(hgrsum[a], cdfdata->groups[g].name.ReplaceAll("__", " "), "lpf");
            }
        }

        if (!flag_nosum)
        {
            RootTools::NiceHistogram(set[a].hsum, cdfdata->sumAttr);

            int uid = DrawProjection(
                set[a].cLambda_comp, set[a].hsum,
                RootTools::MergeOptions(real_same.c_str(), set[a].hsum->GetOption(), "hist"),
                TH2D_PROJ /*TH2DA_AE_GRAPH*/);
            DrawProjection(
                set[a].cLambda_comp_log, set[a].hsum,
                RootTools::MergeOptions(real_same.c_str(), set[a].hsum->GetOption(), "hist"),
                TH2D_PROJ /*TH2DA_AE_GRAPH*/);
            if (flag_inty)
            {
                Double_t m = DrawIntegralY(
                    set[a].cIntY, set[a].hsum,
                    RootTools::MergeOptions(real_same.c_str(), set[a].hsum->GetOption(), "hist"),
                    uid);
                RootTools::FindRangeExtremum(inty_min_tot, inty_max_tot, m);
            }
            // 			RootTools::FindRangeExtremum(min_tot, max_tot, set[a].hsum);
            RootTools::FindBoundaries(set[a].hsum, min_tot, max_tot, false, true);
        }

        if (!flag_nosum and (a < 2 or a == 3) and set[a].hsum)
        {
            TH2DA* proj2da = dynamic_cast<TH2DA*>(set[a].hsum);
            // FIXME why 0s?
            if (proj2da)
                printf("[%lu] Sum xsec = %g +%g -%g\n", a, proj2da->Integral(),
                       proj2da->GetBinErrorH(1, 1), proj2da->GetBinErrorL(1, 1));
        }

        if (fref)
        {
            if (set[a].arrSystematic)
            {
                for (size_t i = 0; i < binsnum; ++i)
                {
                    gr = (TGraphAsymmErrors*)set[a].arrSystematic->At(i);
                    gr->SetFillColor(18); // 7
                    gr->SetFillStyle(1001);

                    set[a].cLambda_comp->cd(1 + i);
                    gr->Draw("5");
                    set[a].cLambda_comp_log->cd(1 + i);
                    gr->Draw("5");
                }
            }

            int uid = DrawProjection(
                set[a].cLambda_comp, set[a].href,
                RootTools::MergeOptions(real_same.c_str(), set[a].href->GetOption(), refdrawopt),
                TH2DA_PROJ);
            DrawProjection(
                set[a].cLambda_comp_log, set[a].href,
                RootTools::MergeOptions(real_same.c_str(), set[a].href->GetOption(), refdrawopt),
                TH2DA_PROJ);
            if (flag_inty)
            {
                /*Double_t m = */ DrawIntegralY(set[a].cIntY, set[a].href,
                                                RootTools::MergeOptions(real_same.c_str(),
                                                                        set[a].href->GetOption(),
                                                                        refdrawopt),
                                                uid);
                // 				RootTools::FindRangeExtremum(inty_min_tot, inty_max_tot, m);
            }
            // 			RootTools::FindRangeExtremum(min_tot, max_tot, set[a].href);
        }

        TList* h = nullptr;
        for (size_t i = 0; i < binsnum; ++i)
        {
            set[a].cLambda_comp->cd(1 + i);
            h = gPad->GetListOfPrimitives();
            if (h->GetEntries())
            {
                if (flag_negy)
                    ((TH1*)(h->At(0)))
                        ->GetYaxis()
                        ->SetRangeUser(
                            (set[a].cfg_miny < 0 ? set[a].cfg_miny : min_tot * par_min_scale),
                            (set[a].cfg_maxy > 0 ? set[a].cfg_maxy : max_tot * par_max_scale));
                else
                    ((TH1*)(h->At(0)))
                        ->GetYaxis()
                        ->SetRangeUser(
                            0., (set[a].cfg_maxy > 0 ? set[a].cfg_maxy : max_tot * par_max_scale));
            }

            set[a].cLambda_comp_log->cd(1 + i);
            h = gPad->GetListOfPrimitives();
            if (h->GetEntries())
            {
                if (flag_negy)
                    ((TH1*)(h->At(0)))
                        ->GetYaxis()
                        ->SetRangeUser(
                            (set[a].cfg_miny < 0 ? set[a].cfg_miny : min_tot * par_min_scale),
                            (set[a].cfg_maxy > 0 ? set[a].cfg_maxy : max_tot * par_max_scale));
                else
                {
                    double log_val_low = log10(min_tot);
                    double log_limit_low = floor(log_val_low);

                    double log_val_high = log10(max_tot);
                    double log_limit_high = ceil(log_val_high);
                    // 					double limit_high = pow(10, log_limit_high);

                    if (log_limit_high - 0.5 < log_val_high)
                    {
                        log_limit_high += 1.0;
                        // 						limit_high = pow(10, log_limit_high);
                    }
                    // 					((TH1*)(h->At(0)))->GetYaxis()->SetRangeUser(pow(10,
                    // log_limit_low), 						( set[a].cfg_maxy > 0 ? set[a].cfg_maxy
                    // : limit_high));
                    // 					((TH1*)(h->At(0)))->GetYaxis()->SetRangeUser(pow(10,
                    // log_limit_low), limit_high);PR(limit_high);PR(a);
                    ((TH1*)(h->At(0)))
                        ->GetYaxis()
                        ->SetRangeUser(pow(10, log_limit_low), set[a].cfg_maxy);
                    set[a].ana_logy_lowlimit = pow(10, log_limit_low);
                }
            }
        }

        if (flag_inty)
        {
            set[a].cIntY->cd(0);
            TList* h = gPad->GetListOfPrimitives();
            if (h->GetEntries())
            {
                if (flag_negy)
                    ((TH1*)(h->At(0)))
                        ->GetYaxis()
                        ->SetRangeUser(inty_min_tot * par_min_scale, inty_max_tot * par_max_scale);
                else
                    ((TH1*)(h->At(0)))->GetYaxis()->SetRangeUser(0., inty_max_tot * par_max_scale);
            }
        }

        TString htitle;
        Float_t pos;

        if (binsnum > 1)
            for (uint j = 0; j < binsnum; ++j)
            {
                // normal
                set[a].cLambda_comp->cd(1 + j);
                htitle = Form("%.1f < %s < %.1f", set[a].href->GetXaxis()->GetBinLowEdge(1 + j),
                              chandata[0].ctx_pars[a]->x.label.Data(),
                              set[a].href->GetXaxis()->GetBinUpEdge(1 + j));
                pos = (1.0 - gPad->GetRightMargin() - gPad->GetLeftMargin()) / 2. +
                      gPad->GetLeftMargin();
                latex->DrawLatex(pos, 0.98, htitle);
                // 			latex->DrawLatex(pos, 0.98, Form("d#sigma/d%s",
                // ctx[a]->x.label.Data()));

                // log
                set[a].cLambda_comp_log->cd(1 + j);
                htitle = Form("%.1f < %s < %.1f", set[a].href->GetXaxis()->GetBinLowEdge(1 + j),
                              chandata[0].ctx_pars[a]->x.label.Data(),
                              set[a].href->GetXaxis()->GetBinUpEdge(1 + j));
                pos = (1.0 - gPad->GetRightMargin() - gPad->GetLeftMargin()) / 2. +
                      gPad->GetLeftMargin();
                latex->DrawLatex(pos, 0.98, htitle);
                // 			latex->DrawLatex(pos, 0.98, Form("d#sigma/d%s",
                // ctx[a]->x.label.Data()));
            }

        Float_t x1 = 0.02;
        Float_t x2 = 0.95;
        Float_t y2 = 1.0;
        // 		Float_t y1 = 0.02;//y2 - 0.07*leg[a]->GetNRows();
        Float_t y1 = y2 - 0.09 * leg[a]->GetNRows();
        // 		TLegend * tmpleg = new TLegend(x1, y1, x2, y2);
        // 		leg[a]->Copy(*tmpleg);
        // 		tmpleg[a]->Draw();
        leg[a]->SetX1(x1);
        leg[a]->SetX2(x2);
        leg[a]->SetY1(y1);
        leg[a]->SetY2(y2);

        set[a].canleg->cd();

        leg[a]->SetTextFont(52);
        leg[a]->SetTextSize(0.10);
        leg[a]->SetLineColor(0);
        leg[a]->DrawClone();
    }

    // 	if (flag_dosumfile)
    // 	{
    // 		for (size_t i = 0; i < global_npar, ++i)
    // 		{
    // 			for (size_t a = 0; i < ananum, ++a)
    // 				sum_fac
    // 		}
    // 	}
}

float CompareDistributions::DrawProjection(TCanvas* c, TH1* hist, const TString& opts,
                                           HPCFG painting, int uid)
{
    // 	size_t binsnum = hist->GetNbinsX(); TODO put it back?

    if (uid < 1) uid = rand() % 1000 + 1;

    TH1D* htemp = nullptr;
    for (size_t i = 0; i < binsnum; ++i)
    {
        c->cd(1 + i);
        //         TH1 * pro1 = dynamic_cast<TH1*>(hist);
        TH2* pro2 = dynamic_cast<TH2*>(hist);

        if (pro2)
            htemp = (TH1D*)pro2->ProjectionY(TString::Format("htmp_%d_%lu", uid, i), 1 + i, 1 + i);
        else
            htemp = (TH1D*)hist;

        if (painting == TH2DA_AE_GRAPH)
        {
#ifdef HAVE_HISTASYMMERRORS
            if (hist->InheritsFrom("TH2DA"))
            {
                TGraphAsymmErrors* errs = ((TH2DA*)hist)->ErrorsProjectionY(i, i);
                errs->SetLineColor(12);
                // 				errs->SetFillColor(16);
                errs->SetFillColor(6);
                errs->SetFillStyle(1001);

                // 				int extra_points = 3;
                // 				errs->Set(errs->GetN()+extra_points);
                // 				errs->SetPoint(errs->GetN()-1, 1e6, 0);
                // 				for (int i = errs->GetN()-2; i > 0; --i)
                // 				{
                // 					double x, y, exh, exl, eyh, eyl;
                // 					errs->GetPoint(i-1, x, y);
                // 					errs->SetPoint(i, x, y);
                //
                // 					exh = errs->GetErrorXhigh(i-1);
                // 					exl = errs->GetErrorXlow(i-1);
                // 					eyh = errs->GetErrorYhigh(i-1);
                // 					eyl = errs->GetErrorYlow(i-1);
                //
                // 					errs->SetPointError(i, exh, exl, eyh, eyl);
                // 				}
                // 				errs->SetPoint(1, 0, 0);
                // 				errs->SetPointError(1, 0, 0, 0, 0);
                //
                // 				double x, y;
                // 				errs->GetPoint(extra_points-1, x, y);
                // 				errs->SetPoint(0, -x*1e1, -y*1e1);
                // 				errs->SetPointError(0, 0, 0, 0, 0);

                // 				errs->Draw("same,4");

                int n = errs->GetN();
                TGraph* er_h = new TGraph(n);
                TGraph* er_l = new TGraph(n);
                TGraph* er_hl = new TGraph(n * 2);
                er_h->SetLineColor(6);
                er_l->SetLineColor(6);
                er_h->SetLineWidth(1);
                er_l->SetLineWidth(1);
                for (int i = 0; i < errs->GetN(); ++i)
                {
                    er_h->SetPoint(i, errs->GetX()[i], errs->GetY()[i] + errs->GetErrorYhigh(i));
                    er_l->SetPoint(i, errs->GetX()[i], errs->GetY()[i] - errs->GetErrorYlow(i));

                    er_hl->SetPoint(i, errs->GetX()[i], errs->GetY()[i] + errs->GetErrorYhigh(i));
                    er_hl->SetPoint(n + i, errs->GetX()[n - i - 1],
                                    errs->GetY()[n - i - 1] - errs->GetErrorYlow(n - i - 1));
                }
                // 				er_h->Clone()->Draw("l");
                // 				er_l->Draw("l");

                er_hl->SetFillStyle(error_fill_pattern);
                er_hl->SetFillColor(6);
                er_hl->Draw("f");

                // 				er_h->Clone()->Draw("c,same");
                // 				er_l->Clone()->Draw("c,same");
                // 				er_l->Draw("l,same");

                // 				PR(errs->GetY()[3]);
                // 				PR(errs->GetErrorYhigh(3));
                // 				PR(errs->GetErrorYlow(3));
            }
#endif
        }

        pf.gf.x.Ndiv = 505; // hist->GetNbinsY()*100 + 1;
        pf.gf.x.center_label = true;

        // 		pf.pf.marginBottom = 0.10;
        // 		pf.pf.marginTop = 0.07;
        pf.pf.marginTop = 0;
        pf.pf.marginRight = 0;
        pf.pf.marginBottom = 0;
        pf.pf.marginLeft = 0;

        // gPad->SetRightMargin(0.2);
        RootTools::NicePad(gPad, pf.pf);
        RootTools::NiceHistogram(htemp, pf.gf);

        htemp->SetLineColor(hist->GetLineColor());
        htemp->SetLineWidth(hist->GetLineWidth());
        htemp->SetLineStyle(hist->GetLineStyle());
        htemp->SetFillColor(hist->GetFillColor());
        htemp->SetFillStyle(hist->GetFillStyle());
        htemp->SetMarkerColor(hist->GetMarkerColor());
        // 		htemp->SetMarkerSize(hist->GetMarkerSize());
        htemp->SetMarkerStyle(hist->GetMarkerStyle());

        // 		htemp->SetTitle("");
        // 		htemp->GetXaxis()->SetTitleFont(73);
        // 		htemp->GetYaxis()->SetTitleFont(73);
        // 		htemp->GetXaxis()->SetLabelFont(63);
        // 		htemp->GetYaxis()->SetLabelFont(63);

        // 		htemp->GetXaxis()->SetTitleSize(16);
        // 		htemp->GetYaxis()->SetTitleSize(16);
        // 		htemp->GetXaxis()->SetLabelSize(16);
        // 		htemp->GetYaxis()->SetLabelSize(16);

        // 		htemp->GetXaxis()->SetTitleSize(0);
        // 		htemp->GetYaxis()->SetTitleSize(0);
        // 		htemp->GetXaxis()->SetLabelSize(0);
        // 		htemp->GetYaxis()->SetLabelSize(0);

        htemp->GetXaxis()->SetTitleOffset(1.4);
        htemp->GetYaxis()->SetTitleOffset(0.1);
        htemp->GetXaxis()->SetLabelOffset(0.01);
        htemp->GetYaxis()->SetLabelOffset(0.01);

        htemp->GetXaxis()->SetDecimals(1);
        htemp->GetYaxis()->SetDecimals(1);

        // 		TH1 * h = ((TH1 *)htemp->Clone());

        // 		TString str = h->GetXaxis()->GetTitle();
        // 		ScaleXaxis(h, ScaleX);
        // 		TString newstr = str.ReplaceAll("MeV", "GeV");
        // 		h->GetXaxis()->SetTitle(newstr.Data());

        // 		h->Draw(opts);
        htemp->Clone()->Draw(opts);
    }

    return uid;
}

float CompareDistributions::DrawProjectionY(TCanvas* c, TH1* hist, const TString& opts,
                                            HPCFG painting, int uid)
{
    // 	size_t binsnum = hist->GetNbinsX(); TODO put it back?

    if (uid < 1) uid = rand() % 1000 + 1;

    c->cd();

    TH1D* htemp = nullptr;
    for (size_t i = 0; i < binsnum; ++i)
    {
        //         TH1 * pro1 = dynamic_cast<TH1*>(hist);
        TH2* pro2 = dynamic_cast<TH2*>(hist);

        if (pro2)
            htemp = (TH1D*)pro2->ProjectionY(TString::Format("htmp_%d_%lu", uid, i), 1 + i, 1 + i);
        else
            htemp = (TH1D*)hist;

        if (painting == TH2DA_AE_GRAPH)
        {
#ifdef HAVE_HISTASYMMERRORS
            if (hist->InheritsFrom("TH2DA"))
            {
                TGraphAsymmErrors* errs = ((TH2DA*)hist)->ErrorsProjectionY(i, i);
                errs->SetLineColor(12);
                errs->SetFillColor(16);
                errs->SetFillStyle(1001);

                // 				errs->Set(errs->GetN()+1);
                errs->Draw("same,3");

                TGraph* er_h = new TGraph(errs->GetN());
                TGraph* er_l = new TGraph(errs->GetN());
                er_h->SetLineColor(14);
                er_l->SetLineColor(14);
                for (int i = 0; i < errs->GetN(); ++i)
                {
                    er_h->SetPoint(i, errs->GetX()[i], errs->GetY()[i] + errs->GetErrorYhigh(i));
                    er_l->SetPoint(i, errs->GetX()[i], errs->GetY()[i] - errs->GetErrorYlow(i));
                }
                // 				er_h->Clone()->Draw("l");
                // 				er_l->Draw("l");
                // 				PR(errs->GetY()[3]);
                // 				PR(errs->GetErrorYhigh(3));
                // 				PR(errs->GetErrorYlow(3));
            }
#endif
        }

        pf.gf.x.Ndiv = hist->GetNbinsY() * 100 + 1;
        pf.gf.x.center_label = true;

        // 		pf.pf.marginBottom = 0.10;
        // 		pf.pf.marginTop = 0.07;
        pf.pf.marginTop = 0;
        pf.pf.marginRight = 0;
        pf.pf.marginBottom = 0;
        pf.pf.marginLeft = 0;

        // 		RootTools::NicePad(gPad, pf.pf);
        RootTools::NiceHistogram(htemp, pf.gf);

        htemp->SetLineColor(hist->GetLineColor());
        htemp->SetLineWidth(hist->GetLineWidth());
        htemp->SetLineStyle(hist->GetLineStyle());
        htemp->SetFillColor(hist->GetFillColor());
        htemp->SetFillStyle(hist->GetFillStyle());
        htemp->SetMarkerColor(hist->GetMarkerColor());
        // 		htemp->SetMarkerSize(hist->GetMarkerSize());
        htemp->SetMarkerStyle(hist->GetMarkerStyle());

        htemp->SetTitle("");
        htemp->GetXaxis()->SetTitleFont(73);
        htemp->GetYaxis()->SetTitleFont(73);
        htemp->GetXaxis()->SetLabelFont(63);
        htemp->GetYaxis()->SetLabelFont(63);

        // 		htemp->GetXaxis()->SetTitleSize(16);
        // 		htemp->GetYaxis()->SetTitleSize(16);
        // 		htemp->GetXaxis()->SetLabelSize(16);
        // 		htemp->GetYaxis()->SetLabelSize(16);

        htemp->GetXaxis()->SetTitleSize(0);
        htemp->GetYaxis()->SetTitleSize(0);
        htemp->GetXaxis()->SetLabelSize(0);
        htemp->GetYaxis()->SetLabelSize(0);

        htemp->GetXaxis()->SetTitleOffset(1.4);
        htemp->GetYaxis()->SetTitleOffset(0.1);
        htemp->GetXaxis()->SetLabelOffset(0.01);
        htemp->GetYaxis()->SetLabelOffset(0.01);

        htemp->GetXaxis()->SetDecimals(1);
        htemp->GetYaxis()->SetDecimals(1);

        // 		TH1 * h = ((TH1 *)htemp->Clone());

        // 		TString str = h->GetXaxis()->GetTitle();
        // 		ScaleXaxis(h, ScaleX);
        // 		TString newstr = str.ReplaceAll("MeV", "GeV");
        // 		h->GetXaxis()->SetTitle(newstr.Data());

        // 		h->Draw(opts);PR(2);
        htemp->Clone()->Draw(opts);
    }

    return uid;
}

float CompareDistributions::DrawIntegralY(TCanvas* c, TH1* hist, const TString& opts, int uid)
{
    // 	size_t binsnum = hist->GetNbinsY(); TODO put it back?

    if (uid < 1) uid = rand() % 1000 + 1;

    TH1F* htemp = nullptr;
    c->cd(0);

    //     TH1 * pro1 = dynamic_cast<TH1*>(hist);
    TH2* pro2 = dynamic_cast<TH2*>(hist);

    if (pro2)
        htemp = (TH1F*)pro2->ProjectionX(TString::Format("inty_%d", uid), 1, 1 + binsnum);
    else
        htemp = (TH1F*)hist;

    RootTools::NicePad(gPad, pf.pf);
    RootTools::NiceHistogram(htemp, pf.gf);

    htemp->SetLineColor(hist->GetLineColor());
    htemp->SetLineWidth(hist->GetLineWidth());
    htemp->SetLineStyle(hist->GetLineStyle());
    htemp->SetFillColor(hist->GetFillColor());
    htemp->SetFillStyle(hist->GetFillStyle());
    htemp->SetMarkerColor(hist->GetMarkerColor());
    // 	htemp->SetMarkerSize(hist->GetMarkerSize());
    htemp->SetMarkerStyle(hist->GetMarkerStyle());

    htemp->SetTitle("");
    htemp->SetTitleFont(72, "X");
    htemp->SetTitleFont(72, "Y");
    htemp->SetLabelFont(62, "X");
    htemp->SetLabelFont(62, "Y");
    // 		htemp->DrawCopy(opts.c_str());
    // 		htemp->Delete();
    ((TH1*)htemp->Clone())->Draw(opts);
    // 		htemp->Delete();

    return htemp->GetMaximum();
}
