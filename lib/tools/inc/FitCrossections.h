/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PLUTOACCEPTANCEMATRIX_H
#define PLUTOACCEPTANCEMATRIX_H

#ifndef __CINT__

#include "TChain.h"
#include "TString.h"

class TCanvas;
class TChain;
class TF1;
class TFile;
class TH1;
class TH1F;
class TH1I;
class TH2;
class TH2F;
class TStyle;

class InputData;
class AcceptanceMatrixFactory;
// class PlutoChannelAccMatrixSet;

#endif /* __CINT__ */

#include "ToolsAbstract.h"

class FitCrossections : public SimpleToolsAbstract
{
private:
    // chain
    TChain* chain;

public:
    FitCrossections();
    // 	FitCrossections(Int_t nxnum = 4, Int_t channum = 128, const TString & exportdir = "Export");
    virtual ~FitCrossections();

    void Initialize();
    void Execute();
    void Finalize();

    void PrintAnalysisInfo();

    int config_params(struct option*& opts);
    void config_execute(int code, char* optarg);

    Double_t FittnessFunction(const Double_t* par);

private:
    void PrepareArgsList();

    void FitSpectras();

    size_t fitCoeffsNum;
    Float_t* fitCoeffs;
    TH2F*** hKinds;

public:
private:
    TString fileFitted;

    Bool_t hasExp, hasSim, hasRes;

    Int_t flag_nocorr;
    Int_t flag_nofit;

    TString cfgfile;

    InputData* data;

    AcceptanceMatrixFactory* PtYcmacc;
    AcceptanceMatrixFactory* PcmCosThcmacc;
    AcceptanceMatrixFactory* XcmPcmacc;

    // 	TObjArray * totalaccmatrixesPtY;
    // 	TObjArray * totalaccmatrixesPCosTh;
};

#endif // PLUTOACCEPTANCEMATRIX_H
