/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "TifiniReader.h"

#include "AnaTools.h"
#include "DatabaseManager.h"

#include <DifferentialFactory.h>
#include <RootTools.h>

#include "TCanvas.h"
#include "TChain.h"
#include "TDirectory.h"
#include "TEllipse.h"
#include "TError.h"
#include "TF1.h"
#include "TFile.h"
#include "TGaxis.h"
#include "TH2.h"
#include "TImage.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TLorentzVector.h"
#include "TMath.h"
#include "TStyle.h"
#include "TVector3.h"

#include <getopt.h>

#include <fstream>
#include <iomanip>
#include <string>

#undef NDEBUG
#include <assert.h>

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

// static const float D2R = TMath::DegToRad();
static const float R2D = TMath::RadToDeg();

// Processing data bar width
const Int_t bar_dotqty = 10000;
const Int_t bar_width = 20;

const Option_t h1opts[] = "h,E1";

Float_t p_dummy = 0.5;

TH1D* h1 = 0;

TifiniReader::TifiniReader()
    : ToolsAbstract(), chain(nullptr), custom(nullptr), events(0), flag_noweights(0),
      flag_ang_alt(0), flag_ang_backfold(0), flag_ar(0), flag_al(0), cuts_used(20),
      par_scaledown(0), hasInputFiles(false), data_scaledown(1.), total_weight(0.), raw_weight(1.0),
      raw_total_weight(0.0), db(nullptr), tree_init(false), filter_tree(nullptr),
      filter_file(nullptr)
{
    TGaxis::SetMaxDigits(3);

    // clearing up cuts array
    for (size_t i = 0; i < C_do_not_use_this; ++i)
        par_carr[i] = 0;

    // cuts definitions
    {
        //         par_carr[C_def]          = 0.;          par_name[C_def]          = "precuts"; //
        //         pre cuts
        par_carr[C_det_sel] = 3.;
        par_name[C_det_sel] = "det_sel"; // pre cuts
        // Lambda
        par_carr[C_la_mass_min] = 0.;
        par_name[C_la_mass_min] = "la_mass_min"; // mass 1102.;
        par_carr[C_la_mass_max] = 10000.;
        par_name[C_la_mass_max] = "la_mass_max"; // mass 1127.;

        par_carr[C_la_mm_min] = -10000.;
        par_name[C_la_mm_min] = "la_mm_min"; // missing mass
        par_carr[C_la_mm_max] = 10000. * 10000.;
        par_name[C_la_mm_max] = "la_mm_max"; // missing mass

        par_carr[C_la_z_min] = -9999.;
        par_name[C_la_z_min] = "la_z_min"; // Zmin
        par_carr[C_la_z_max] = 99999.;
        par_name[C_la_z_max] = "la_z_max"; // Zmax

        par_carr[C_la_r_min] = 0.;
        par_name[C_la_r_min] = "la_r_min"; // Rmin
        par_carr[C_la_r_max] = 9999.;
        par_name[C_la_r_max] = "la_r_max"; // Rmax

        par_carr[C_la_mtd_max] = 99999.;
        par_name[C_la_mtd_max] = "la_mtd_max"; // MTD max
        par_carr[C_la_pva_max] = 4.;
        par_name[C_la_pva_max] = "la_pva_max"; // Lambda PVA max
        par_carr[C_la_vdx_min] = 0.;
        par_name[C_la_vdx_min] = "la_vdx_min"; // VertDistX max

        par_carr[C_la_theta_min] = 0.;
        par_name[C_la_theta_min] = "la_theta_min"; // Theta min
        par_carr[C_la_theta_max] = 3.15;
        par_name[C_la_theta_max] = "la_theta_max"; // Theta max

        par_carr[C_la_real] = -1;
        par_name[C_la_real] =
            "la_real"; // real particle: <0 - ignore, 0 - only not real, >0 - only real
        par_carr[C_la_excl_min] = 1e10;
        par_name[C_la_excl_min] = "la_excl_min"; // mass 1102.;
        par_carr[C_la_excl_max] = 0.;
        par_name[C_la_excl_max] = "la_excl_max"; // mass 1127.;

        // S(1385)+
        par_carr[C_s1385p_mass_min] = 0.;
        par_name[C_s1385p_mass_min] = "s1385p_mass_min"; // mass 1102.;
        par_carr[C_s1385p_mass_max] = 10000.;
        par_name[C_s1385p_mass_max] = "s1385p_mass_max"; // mass 1127.;

        par_carr[C_s1385p_mm_min] = -10000.;
        par_name[C_s1385p_mm_min] = "s1385p_mm_min"; // missing mass
        par_carr[C_s1385p_mm_max] = 10000. * 10000.;
        par_name[C_s1385p_mm_max] = "s1385p_mm_max"; // missing mass

        par_carr[C_s1385p_z_min] = -9999.;
        par_name[C_s1385p_z_min] = "s1385p_z_min"; // Zmin
        par_carr[C_s1385p_z_max] = 99999.;
        par_name[C_s1385p_z_max] = "s1385p_z_max"; // Zmax

        par_carr[C_s1385p_r_min] = 0.;
        par_name[C_s1385p_r_min] = "s1385p_r_min"; // Rmin
        par_carr[C_s1385p_r_max] = 9999.;
        par_name[C_s1385p_r_max] = "s1385p_r_max"; // Rmax

        par_carr[C_s1385p_mtd_max] = 99999.;
        par_name[C_s1385p_mtd_max] = "s1385p_mtd_max"; // MTD max

        par_carr[C_s1385p_vdx_min] = 0.;
        par_name[C_s1385p_vdx_min] = "s1385p_vdx_min"; // VertDistX max

        par_carr[C_s1385p_theta_min] = 0.;
        par_name[C_s1385p_theta_min] = "s1385p_theta_min"; // Theta min
        par_carr[C_s1385p_theta_max] = 3.15;
        par_name[C_s1385p_theta_max] = "s1385p_theta_max"; // Theta max

        par_carr[C_s1385p_real] = -1;
        par_name[C_s1385p_real] =
            "s1385p_real"; // real particle: <0 - ignore, 0 - only not real, >0 - only real

        par_carr[C_gen] = 0.;
        par_name[C_gen] = "gen"; // 0, all, 1 prim, 2 seco Lambdas

        // need review
        //         par_carr[C_rphi_min]     = -1.;         par_name[C_rphi_min]        =
        //         "r_phi_min";      // Rphi_min par_carr[C_rphi_max]     = 370.;
        //         par_name[C_rphi_max]        = "r_phi_max";      // Rphi_max

        //         par_carr[C_p_min]        = 0.;          par_name[C_p_min]        = "p_min"; // p
        //         min par_carr[C_p_max]        = 10000.;      par_name[C_p_max]        = "p_max";
        //         // p max

        //         par_carr[C_pacms_min]    = 0.;          par_name[C_pacms_min]    = "pacms_min";
        //         // PA_CMS min par_carr[C_pacms_max]    = 10000.;      par_name[C_pacms_max]    =
        //         "pacms_max";  // PA_CMS max par_carr[C_pbcms_min]    = 0.; par_name[C_pbcms_min]
        //         = "pbcms_min";  // PB_CMS min par_carr[C_pbcms_max]    = 10000.;
        //         par_name[C_pbcms_max]    = "pbcms_max";  // PB_CMS max

        //         par_carr[C_dz_min]       = -10000.;     par_name[C_dz_min]       = "dz_min"; //
        //         Lambda Zmin par_carr[C_dz_max]       = 10000.;      par_name[C_dz_max]       =
        //         "dz_max";     // Lambda Zmax par_carr[C_dr_min]       = 0.; par_name[C_dr_min] =
        //         "dr_min";     // Lambda Rmin par_carr[C_dr_max]       = 10000.;
        //         par_name[C_dr_max]       = "dr_max";     // Lambda Rmax

        //         par_carr[C_pva_max]      = 10000.;      par_name[C_pva_max]      = "pva_max"; //
        //         Primary vertex angle variation

        //         par_carr[C_oa_min]       = 0.;          par_name[C_oa_min]       = "oa_min"; //
        //         Max Missing Mass par_carr[C_oa_max]       = 10.;         par_name[C_oa_max] =
        //         "oa_max";     // Max Missing Mass

        //         par_carr[C_pt_min]       = 0.;          par_name[C_pt_min]       = "pt_min"; // p
        //         min par_carr[C_pt_max]       = 10000.;      par_name[C_pt_max]       = "pt_max";
        //         // p max
    }

    // clearing up cuts used array
    for (size_t i = 0; i < C_do_not_use_this; ++i)
        par_used[i] = 0;

    custom = new SmartFactory("custom");
}

TifiniReader::~TifiniReader() {}

void TifiniReader::InitTree()
{
    if (tree_init) return;

    if (!chain)
    {
        if (flag_acc)
            chain = new TChain("T");
        else
            chain = new TChain("T");
        return;
    }

    tree_init = true;
}

void TifiniReader::AnaInitialize()
{
    if (!hasInputFiles)
    {
        std::cerr << "No input files given!\n";
        std::exit(EXIT_FAILURE);
    }

    TString jsonconfig = get_export_dir() + "anaconfig.json";

    set_output_file("SecVertexReco.root");

    //****************************************************************************
    // histogram and canvases
}

void TifiniReader::AnaExecute()
{
    Bool_t newBar = kTRUE;

    entries = chain->GetEntries();
    if (events and (events < entries)) entries = events;

    PrintAnalysisInfo();

    //****************************************************************************
    // preconfigure analysis for simulations
    //****************************************************************************

    if ((flag_pluto or flag_pwa) and par_dbfiles.length())
    {
        // read first event to get basic info about channel
        ReadEvent(0);
    }

    ULong64_t ev = 0;
    ULong64_t events_accepted = 0;
    long int prim_cnt = 0;
    long int seco_cnt = 0;
    long int prim_cnt_acc = 0;
    long int seco_cnt_acc = 0;

    // Some p corrections / check old TUM slides // TODO
    Float_t p_corrs[20] = {1.000, 1.000, 1.074, 1.808, 1.416, 1.226, 0.959, 0.823, 0.809, 0.785,
                           0.782, 0.824, 1.001, 1.121, 0.989, 0.871, 1.000, 1.000, 1.000, 1.000};
    TH1D* h_p_corr = new TH1D("h_p_corr", "h_p_corr", 20, 0, 3000);

    for (int i = 0; i < 20; ++i)
    {
        h_p_corr->SetBinContent(i + 1, p_corrs[i]);
    }

    for (ev = 0; ev < entries; ++ev)
    {
        if (newBar)
        {
            std::cout << "==> Processing data " << std::flush;
            newBar = kFALSE;
        }

        if (ev != 0 and (ev + 1) % bar_dotqty == 0) std::cout << "." << std::flush;

        if ((ev != 0 and (ev + 1) % (bar_dotqty * bar_width) == 0) or (ev == (entries - 1)))
        {
            Double_t event_percent = 100.0 * ev / (entries - 1);
            std::cout << " " << ev + 1 << " (" << event_percent << "%) "
                      << "\n"
                      << std::flush;
            newBar = kTRUE;
        }

        ReadEvent(ev);

        //         if (flag_fss and (b_fLambda_VertDistX < par_fVertDistX))
        //             continue;

        bool skip_event = false;

        if (skip_event) continue;

        // printf("id=%d  const=%d,  ew = %f\n", b_fGeantInfoNum, PC088, event_weight);
        if (flag_noweights) event_weight = 1.0;

        total_weight += event_weight;
        raw_total_weight += raw_weight;

        ++events_accepted;

        filter_tree->Fill();
    }
    this->events_proceed = ev;

    if (flag_pwa)
    {
        //         data_scaledown = total_weight/events_proceed;
        data_scaledown = 830.291 /*815.901*/ /*834.812*/; // FIXME this must be recalculated for
                                                          // proper fss normalisation
        PR(data_scaledown);
        PR((total_weight / events_proceed));
    }
    else
    {
        data_scaledown = 1.0; // total_weight/events_accepted;
    }

    std::cout << "Events statistics" << std::endl;
    std::cout << " Events number    " << entries << std::endl;
    std::cout << " Events proceeded " << events_proceed << std::endl;
    std::cout << " Events accepted  " << events_accepted << std::endl;
    std::cout << " Total weight     " << total_weight << std::endl;
    std::cout << " Scaling factor   " << total_weight / events_proceed << std::endl;

    std::cout << " Primary Lambdas total/accepted   " << prim_cnt << " / " << prim_cnt_acc
              << std::endl;
    std::cout << " Secondary Lambdas total/accepted " << seco_cnt << " / " << seco_cnt_acc
              << std::endl;

    std::cout << "Used cuts statistics" << std::endl;
    uint field_width = 20;
    uint row_len = 6;
    uint rows = (C_do_not_use_this - 1) / row_len;
    //     if ((C_do_not_use_this-1) % row_len)
    //         ++rows;

    uint limit = 0;
    for (uint r = 0; r < rows; ++r)
    {
        limit =
            C_do_not_use_this < (r + 1) * row_len ? (uint)C_do_not_use_this : (r + 1) * row_len + 1;

        if (r == 0)
        {
            std::cout << "-----";
            for (size_t i = r * row_len; i < (limit - 1) * field_width; ++i)
                std::cout << "-";
            std::cout << std::endl;
        }

        std::cout << "No. |";
        for (size_t i = r * row_len + 1; i < limit; ++i)
            std::cout << std::setw(field_width) << i;
        std::cout << std::endl;

        std::cout << "    |";
        for (size_t i = r * row_len + 1; i < limit; ++i)
            std::cout << std::setw(field_width) << par_name[i];
        std::cout << std::endl;

        std::cout << "Val |";
        for (size_t i = r * row_len + 1; i < limit; ++i)
            std::cout << std::setw(field_width) << par_carr[i];
        std::cout << std::endl;

        std::cout << "Cnt |";
        for (size_t i = r * row_len + 1; i < limit; ++i)
            std::cout << std::setw(field_width) << par_used[i];
        std::cout << std::endl;

        std::cout << "-----";
        for (size_t i = (r * row_len) * field_width; i < (limit - 1) * field_width; ++i)
            std::cout << "-";
        std::cout << std::endl;
    }
}

void TifiniReader::prettyMe(DifferentialFactory* fac)
{
    fac->niceHists(def_pf_diff, def_gf_diff);
    fac->niceDiffs(0.1, 0.05, 0.13, 0.1, 505, 1002, 0.08, 0.08, 0.9, 0.07, 0.07, 0.55);
    fac->niceSlices(0.1, 0.01, 0.13, 0.1, 505, 1002, 0.06, 0.07, 0.9, 0.07, 0.07, 0.55);
    fac->finalize();
}

void TifiniReader::prettyMe(DistributionFactory* fac)
{
    fac->niceHists(def_pf_diff, def_gf_diff);
    fac->finalize();
}

void TifiniReader::AnaFinalize()
{
    open_export_file();

    custom->exportStructure(get_export_file());
    set_config_vector()->Write();

    if (h1) h1->Write();
    close_export_file();

    filter_file->cd();
    filter_tree->Write();
    filter_file->Close();
}

void TifiniReader::Usage()
{
    std::cout << "\nUsage: program args files\nwhere args are:\n";

    //     Int_t s = optsize;
    //
    //     for (int i = 0; i < s-1; ++i) {
    //         if (opt_snames[i] != 0)
    //             printf("\t-%c, --%s    \t\t%s\t\t%s\n", opt_snames[i], opt_lnames[i].Data(),
    //             opt_args[i].Data(), opt_desc[i].Data());
    //         else
    //             printf("\t    --%s    \t\t%s\t\t%s\n", opt_lnames[i].Data(), opt_args[i].Data(),
    //             opt_desc[i].Data());
    //
    //     }
    //     std::cout << "\n";
}

void TifiniReader::PrintAnalysisInfo()
{
    std::cout << colstd << "=======================================================" << resstd
              << std::endl;
    std::cout << colstd << "* Processing " << entries << "/" << chain->GetEntries() << " ( "
              << 100.0 * entries / chain->GetEntries() << " % ) events" << resstd << std::endl;
    //     std::cout << colstd << "* Secondary Vertex Cuts:" << resstd << std::endl;
    std::cout << colstd << "=======================================================" << resstd
              << std::endl;
}

int TifiniReader::config_params(struct option*& opts)
{
    uint parcnt = 0;
    for (uint i = 0; i < C_do_not_use_this; ++i)
        if (!par_name[i].empty()) ++parcnt;

    const uint offset = 9;
    int cuts_used = parcnt;
    const uint total_len = offset + 2 * cuts_used;
    option* lopts = new option[total_len];

    lopts[0] = {"ScaleDown", required_argument, 0, 2000};
    lopts[1] = {"events", required_argument, 0, 2001};
    lopts[2] = {"NoWeights", no_argument, &flag_noweights, 1};
    lopts[3] = {"ang_alt", required_argument, 0, 2002};
    lopts[4] = {"db", required_argument, 0, 2003};
    lopts[5] = {"backfold", no_argument, &flag_ang_backfold, 1};
    lopts[6] = {"ar", no_argument, &flag_ar, 1};
    lopts[7] = {"al", no_argument, &flag_al, 1};
    lopts[8] = {"c0", required_argument, 0, 3000};

    uint icnt = 0;
    for (uint i = 1; i < C_do_not_use_this; ++i)
    {
        if (par_name[i].empty()) continue;

        char* cs = new char[100];
        sprintf(cs, "c%d", i);
        char* cl = new char[100];
        sprintf(cl, "c-%s", par_name[i].c_str());
        lopts[offset + icnt * 2] = {cs, required_argument, 0, 3000 + (int)i};
        lopts[offset + icnt * 2 + 1] = {cl, required_argument, 0, 3000 + (int)i};

        ++icnt;
    }

    opts = lopts;
    return total_len;
}

void TifiniReader::config_execute(int code, char* optarg)
{
    switch (code)
    {
        case 2000:
            par_scaledown = atoi(optarg);
            break;
        case 2001:
            events = atoi(optarg);
            break;
        case 2002:
            flag_ang_alt = atoi(optarg);
            break;
        case 2003:
            par_dbfiles = optarg;
            break;
        default:
            for (size_t i = 0; i < C_do_not_use_this; ++i)
            {
                int ref = 3000 + i;
                if (code == ref) { par_carr[i] = atof(optarg); }
            }
            break;
    }

    if (code) return;

    InitTree();

    TH1D* hCounter = nullptr;

    TString fname(optarg);
    if (fname.EndsWith(".root"))
    {
        this->AddToChain(fname.Data());
        if (flag_verbose) std::cout << "Add file: " << fname.Data() << std::endl;
        hasInputFiles = true;

        if (par_scaledown > 0) { data_scaledown = par_scaledown; }
        else if (!hCounter)
        {
            if (flag_gibuu)
            {
                TFile* fCounter = TFile::Open(fname.Data(), "READ");

                if (fCounter)
                {
                    hCounter = (TH1D*)fCounter->Get("hCounter");
                    if (hCounter) data_scaledown = hCounter->GetEntries();
                    fCounter->Close();
                }
                else
                {
                    //                 data_scaledown = 1e9;
                }
            }
            else if (flag_urqmd)
            {
                TFile* fCounter = TFile::Open(fname.Data(), "READ");

                if (fCounter)
                {
                    hCounter = (TH1D*)fCounter->Get("hCounter");
                    if (hCounter)
                        data_scaledown = hCounter->GetBinContent(hCounter->GetMaximumBin());
                    fCounter->Close();
                    PR(data_scaledown);
                }
                else
                {
                    data_scaledown = 1e9;
                }
            }
        }
    }
    else if (fname.EndsWith(".tlist"))
    {
        std::ifstream tlf(fname.Data());
        std::string fff;
        while (!tlf.eof())
        {
            tlf >> fff;
            this->AddToChain(fff.c_str());
            if (flag_verbose) std::cout << "Add file: " << fff.c_str() << std::endl;
            hasInputFiles = true;
        }
    }
    InitTree();
}

void TifiniReader::FitTargetWindowZ(TCanvas* can, TH1* hist, char* name, char* sig, char* bg,
                                    Double_t x_min, Double_t x_max, Double_t leg_x, Double_t leg_y)
{
    (void)can;

    TString sig_name("window_sig_");
    TString bg_name("window_bg_");
    TString sum_name("window_sum_");
    TString funexpr = TString::Format("%s + %s", sig, bg);

    //     can->cd();

    TF1* tfWindowSig = new TF1(sig_name + name, sig, x_min, x_max);
    TF1* tfWindowBg = new TF1(bg_name + name, bg, x_min, x_max);
    TF1* tfWindowSum = new TF1(sum_name + name, funexpr, x_min, x_max);

    tfWindowSig->SetLineColor(kGreen);
    tfWindowSig->SetLineWidth(1);
    //     tfWindowSig->SetFillColor(kGray);
    //     tfWindowSig->SetFillStyle(3000);

    tfWindowBg->SetLineColor(kBlue);
    tfWindowBg->SetLineWidth(1);
    tfWindowBg->SetLineStyle(7);

    tfWindowSum->SetLineColor(kRed);
    tfWindowSum->SetLineWidth(1);

    Double_t x_avg = (x_min + x_max) / 2.0;
    Int_t bc = hist->FindBin(x_avg);
    Double_t bavg = hist->Integral(bc - 2, bc + 2) / 5.;
    PR(bavg);

    tfWindowSum->SetParameters(bavg, x_avg, 3.5, 10, 0.1);

    tfWindowSig->SetBit(TF1::kNotDraw);
    tfWindowBg->SetBit(TF1::kNotDraw);
    tfWindowSum->SetBit(TF1::kNotDraw);

    hist->Fit(tfWindowSum, "R+", "same", x_min, x_max);
    tfWindowSig->SetParameters(tfWindowSum->GetParameters());
    tfWindowBg->SetParameters(tfWindowSum->GetParameters());

    hist->GetListOfFunctions()->Add(tfWindowSig);
    hist->GetListOfFunctions()->Add(tfWindowBg);
    hist->GetListOfFunctions()->Add(tfWindowSum);

    Double_t fWindowFitA = tfWindowSig->GetParameter(0);
    Double_t fWindowFitM = tfWindowSig->GetParameter(1);
    Double_t fWindowFitS = tfWindowSig->GetParameter(2);

    Double_t sigmean = fWindowFitS;
    Double_t meanmean = fWindowFitM;
    // calculate S/B
    Double_t fSBmin = meanmean - 3.0 * sigmean;
    Double_t fSBmax = meanmean + 3.0 * sigmean;
    //     Int_t fSBminBin = hist->FindBin(fSBmin);
    //     Int_t fSBmaxBin = hist->FindBin(fSBmax);

    TLatex* latex = new TLatex();
    latex->SetNDC();
    latex->SetTextSize(0.03);
    latex->DrawLatex(leg_x - 0.03, leg_y,
                     TString::Format("Lambda : %s", tfWindowSig->GetExpFormula().Data()));
    latex->DrawLatex(leg_x, leg_y - 0.04, TString::Format("A=%g", fWindowFitA));
    latex->DrawLatex(leg_x, leg_y - 0.07, TString::Format("#mu=%g", fWindowFitM));
    latex->DrawLatex(leg_x, leg_y - 0.10, TString::Format("#sigma=%g", fWindowFitS));
    //     latex->DrawLatex(0.75, 0.77, TString::Format("A=%g", fLambdaFitA2));
    //     latex->DrawLatex(0.75, 0.74, TString::Format("#mu=%g", fLambdaFitM2));
    //     latex->DrawLatex(0.75, 0.71, TString::Format("#sigma=%g", fLambdaFitS2));

    //     latex->DrawLatex(0.60, 0.67, TString::Format("/#mu=%g", meanmean));
    //     latex->DrawLatex(0.60, 0.64, TString::Format("/#sigma=%g", sigmean));

    Double_t _sig = tfWindowSig->Integral(fSBmin, fSBmax);
    Double_t _bg = tfWindowBg->Integral(fSBmin, fSBmax);

    latex->DrawLatex(leg_x - 0.03, leg_y - 0.15, "S/B:");
    latex->DrawLatex(leg_x, leg_y - 0.19, TString::Format("S=%g", _sig));
    latex->DrawLatex(leg_x, leg_y - 0.22, TString::Format("B=%g", _bg));
    latex->DrawLatex(leg_x, leg_y - 0.25, TString::Format("S/B=%g", _sig / _bg));

    tfWindowSig->Draw("same");
    tfWindowBg->Draw("same");
    tfWindowSum->Draw("same");
}
