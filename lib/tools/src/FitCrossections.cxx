/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "FitCrossections.h"

#ifndef __CINT__

#include "getopt.h"
#include <fstream>
#include <string>

#include "TCanvas.h"
#include "TChain.h"
#include "TDirectory.h"
#include "TError.h"
#include "TF1.h"
#include "TFile.h"
#include "TGaxis.h"
#include "TH2.h"
#include "TImage.h"
#include "TKey.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TMath.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TVector.h"
#include "TVirtualFitter.h"

#include "Math/BrentMinimizer1D.h"
#include "Math/Factory.h"
#include "Math/Functor.h"
#include "Math/GSLMinimizer.h"
#include "Math/IFunction.h"
#include "Math/Minimizer.h"
#include "TMinuitMinimizer.h"

#endif /* __CINT__ */

#include "RootTools.h"

#include "AcceptanceMatrixFactory.h"

#include "globals.h"

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

const Option_t h1opts[] = "h,E1";

FitCrossections::PlutoAcceptanceMatrix()
    : SimpleToolsAbstract(), fitCoeffsNum(0), flag_nocorr(0), flag_nofit(0)
{
    NicePalette();
    TGaxis::SetMaxDigits(3);
}

FitCrossections::~PlutoAcceptanceMatrix() {}

void FitCrossections::AnaInitialize()
{
    if (cfgfile.IsNull())
    {
        std::cout << "No config file!" << std::endl;
        std::exit(EXIT_FAILURE);
    }

    // 	TH1F * hist;
    ifstream fcfgfile(cfgfile);
    if (!fcfgfile.is_open())
    {
        std::cerr << "File " << cfgfile << " is not open!";
        std::exit(EXIT_FAILURE);
    }

    data = new InputData;
    hasExp = hasSim = hasRes = kFALSE;

    data->nocorrections = flag_nocorr;

    TString dir_sim, dir_acc, dir_fss, dir_;
    std::string line;
    while (std::getline(fcfgfile, line))
    {
        TString line_(line);
        line_.ReplaceAll("\t", " ");

        TObjArray* arr = line_.Tokenize(" ");

        PlutoEntry entry;
        char cmdchar = ((TObjString*)arr->At(0))->String()[0];
        switch (cmdchar)
        {
            case 'e':
                data->expentry.dir_sim = ((TObjString*)arr->At(1))->String();
                data->expentry.label = ((TObjString*)arr->At(2))->String();
                hasExp = kTRUE;
                break;
            case 'd':
                dir_sim = ((TObjString*)arr->At(1))->String();
                dir_acc = ((TObjString*)arr->At(2))->String();
                dir_fss = ((TObjString*)arr->At(3))->String();
                break;
            case 's':
                dir_ = ((TObjString*)arr->At(1))->String();
                entry.dir_sim = dir_ + dir_sim + "/";
                entry.dir_acc = dir_ + dir_acc + "/";
                entry.dir_fss = dir_ + dir_fss + "/";
                entry.id = ((TObjString*)arr->At(2))->String();
                entry.cs = ((TObjString*)arr->At(3))->String().Atof();
                entry.color = ((TObjString*)arr->At(4))->String().Atoi();
                entry.label = ((TObjString*)arr->At(5))->String();
                entry.scale = ((TObjString*)arr->At(6))->String().Atof();
                if (entry.cs < 0)
                {
                    if (flag_nofit) { entry.cs = -entry.cs; }
                    else
                    {
                        entry.fitpar = -entry.cs;
                        entry.cs = 1.0;
                        entry.fit = kTRUE;
                        ++fitCoeffsNum;
                    }
                }
                else
                {
                    entry.fit = kFALSE;
                }
                data->simentry.push_back(entry);
                data->simids->AddLast(new TObjString(entry.id));
                data->simlabels->AddLast(new TObjString(entry.label));
                hasSim = kTRUE;
                break;
            case 'r':
                data->resentry.dir_sim = ((TObjString*)arr->At(1))->String();
                data->resentry.label = ((TObjString*)arr->At(2))->String();
                hasRes = kTRUE;
                break;
        }
    }

    if (!hasExp and hasSim and hasRes)
    {
        std::cout << "Config file is not complete, check for exp, sim and res data!" << std::endl;
        std::exit(EXIT_FAILURE);
    }

    fileFitted = "/FittedLambda.root";

    SetOutputFile(data->resentry.label + ".root");

    data->fileexp = TFile::Open(data->expentry.dir_sim + fileFitted, "READ");
    if (!data->fileexp->IsOpen())
    {
        std::cerr << "Error opening file " << data->expentry.dir_acc + fileFitted << " . Exiting..."
                  << std::endl;
        exit(EXIT_FAILURE);
    }

    size_t simnum = data->simentry.size();
    data->filesim.reserve(simnum);
    data->fileacc.reserve(simnum);
    data->filefss.reserve(simnum);

    for (uint i = 0; i < simnum; ++i)
    {
        data->filesim[i] = TFile::Open(data->simentry[i].dir_sim + fileFitted, "READ");
        if (!data->filesim[i]->IsOpen())
        {
            std::cerr << "Error opening file " << data->simentry[i].dir_sim + fileFitted
                      << " . Exiting..." << std::endl;
            exit(EXIT_FAILURE);
        }

        data->fileacc[i] = TFile::Open(data->simentry[i].dir_acc + fileFitted, "READ");
        if (!data->fileacc[i]->IsOpen())
        {
            std::cerr << "Error opening file " << data->simentry[i].dir_acc + fileFitted
                      << " . Exiting..." << std::endl;
            exit(EXIT_FAILURE);
        }

        data->filefss[i] = TFile::Open(data->simentry[i].dir_fss + fileFitted, "READ");
        if (!data->filefss[i]->IsOpen())
        {
            std::cerr << "Error opening file " << data->simentry[i].dir_fss + fileFitted
                      << " . Exiting..." << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    OpenExportFile();

    PtYcmacc = new AcceptanceMatrixFactory("PtYcm", data);
    PcmCosThcmacc = new AcceptanceMatrixFactory("PcmCosThcm", data);
    XcmPcmacc = new AcceptanceMatrixFactory("XcmPcm", data);

    // 	getConfigVector();
}

void FitCrossections::AnaExecute()
{
    PrintAnalysisInfo();

    Float_t i1, i2, i3;

    i1 = PtYcmacc->hLambda_sig->Integral();
    i2 = PcmCosThcmacc->hLambda_sig->Integral();
    i3 = XcmPcmacc->hLambda_sig->Integral();
    std::cout << "EXP: Uncorrected integrals:\n";
    std::cout << "\t: " << i1 << "\t" << i2 << "\t" << i3 << std::endl;
    std::cout << "\t: " << i1 / i2 << "\t" << i2 / i3 << "\t" << i3 / i1 << std::endl;

    PtYcmacc->Execute();
    PcmCosThcmacc->Execute();
    XcmPcmacc->Execute();

    size_t num = PtYcmacc->fss_data->GetEntries();

    TH2F* h = nullptr;
    for (size_t i = 0; i < num; ++i)
    {
        // 			PR(i);
        Float_t i1, i2, i3;

        // 			std::cout << "== " << i << std::endl;

        h = (TH2F*)PtYcmacc->sim_data->At(i);
        i1 = h->Integral();
        h = (TH2F*)PcmCosThcmacc->sim_data->At(i);
        i2 = h->Integral();
        h = (TH2F*)XcmPcmacc->sim_data->At(i);
        i3 = h->Integral();

        std::cout << i << "\tSIM: "
                  << "\t: " << i1 << "\t" << i2 << "\t" << i3 << std::endl;
        std::cout << "\t\t: " << i1 / i2 << "\t" << i2 / i3 << "\t" << i3 / i1 << std::endl;

        h = (TH2F*)PtYcmacc->acc_data->At(i);
        i1 = h->Integral();
        h = (TH2F*)PcmCosThcmacc->acc_data->At(i);
        i2 = h->Integral();
        h = (TH2F*)XcmPcmacc->acc_data->At(i);
        i3 = h->Integral();

        std::cout << "\tACC: "
                  << "\t: " << i1 << "\t" << i2 << "\t" << i3 << std::endl;
        std::cout << "\t\t: " << i1 / i2 << "\t" << i2 / i3 << "\t" << i3 / i1 << std::endl;

        h = (TH2F*)PtYcmacc->fss_data->At(i);
        i1 = h->Integral();
        h = (TH2F*)PcmCosThcmacc->fss_data->At(i);
        i2 = h->Integral();
        h = (TH2F*)XcmPcmacc->fss_data->At(i);
        i3 = h->Integral();

        std::cout << "\tFSS: "
                  << "\t: " << i1 << "\t" << i2 << "\t" << i3 << std::endl;
        std::cout << "\t\t: " << i1 / i2 << "\t" << i2 / i3 << "\t" << i3 / i1 << std::endl;
    }

    i1 = PtYcmacc->hLambda_sum_fss_raw->Integral();
    i2 = PcmCosThcmacc->hLambda_sum_fss_raw->Integral();
    i3 = XcmPcmacc->hLambda_sum_fss_raw->Integral();
    std::cout << "SIM: Uncorrected integrals:\n";
    std::cout << "\t: " << i1 << "\t" << i2 << "\t" << i3 << std::endl;
    std::cout << "\t: " << i1 / i2 << "\t" << i2 / i3 << "\t" << i3 / i1 << std::endl;

    i1 = PtYcmacc->hLambda_sum_fss->Integral();
    i2 = PcmCosThcmacc->hLambda_sum_fss->Integral();
    i3 = XcmPcmacc->hLambda_sum_fss->Integral();
    std::cout << "SIM: Corrected integrals:\n";
    std::cout << "\t: " << i1 << "\t" << i2 << "\t" << i3 << std::endl;
    std::cout << "\t: " << i1 / i2 << "\t" << i2 / i3 << "\t" << i3 / i1 << std::endl;

    i1 = PtYcmacc->hLambda_sig->Integral();
    i2 = PcmCosThcmacc->hLambda_sig->Integral();
    i3 = XcmPcmacc->hLambda_sig->Integral();
    std::cout << "EXP: Corrected integrals:\n";
    std::cout << "\t: " << i1 << "\t" << i2 << "\t" << i3 << std::endl;
    std::cout << "\t: " << i1 / i2 << "\t" << i2 / i3 << "\t" << i3 / i1 << std::endl;

    FitSpectras();

    num = PtYcmacc->fss_data->GetEntries();
    PR(num);

    i1 = PtYcmacc->hLambda_sig->Integral();
    i2 = PcmCosThcmacc->hLambda_sig->Integral();
    i3 = XcmPcmacc->hLambda_sig->Integral();
    std::cout << "EXP: Uncorrected integrals:\n";
    std::cout << "\t: " << i1 << "\t" << i2 << "\t" << i3 << std::endl;
    std::cout << "\t: " << i1 / i2 << "\t" << i2 / i3 << "\t" << i3 / i1 << std::endl;

    PtYcmacc->Execute();
    PcmCosThcmacc->Execute();
    XcmPcmacc->Execute();

    num = PtYcmacc->fss_data->GetEntries();
    PR(num);

    h = nullptr;
    for (size_t i = 0; i < num; ++i)
    {
        // 			PR(i);
        Float_t i1, i2, i3;

        // 			std::cout << "== " << i << std::endl;

        h = (TH2F*)PtYcmacc->sim_data->At(i);
        i1 = h->Integral();
        h = (TH2F*)PcmCosThcmacc->sim_data->At(i);
        i2 = h->Integral();
        h = (TH2F*)XcmPcmacc->sim_data->At(i);
        i3 = h->Integral();

        std::cout << i << "\tSIM: "
                  << "\t: " << i1 << "\t" << i2 << "\t" << i3 << std::endl;
        std::cout << "\t\t: " << i1 / i2 << "\t" << i2 / i3 << "\t" << i3 / i1 << std::endl;

        h = (TH2F*)PtYcmacc->acc_data->At(i);
        i1 = h->Integral();
        h = (TH2F*)PcmCosThcmacc->acc_data->At(i);
        i2 = h->Integral();
        h = (TH2F*)XcmPcmacc->acc_data->At(i);
        i3 = h->Integral();

        std::cout << "\tACC: "
                  << "\t: " << i1 << "\t" << i2 << "\t" << i3 << std::endl;
        std::cout << "\t\t: " << i1 / i2 << "\t" << i2 / i3 << "\t" << i3 / i1 << std::endl;

        h = (TH2F*)PtYcmacc->fss_data->At(i);
        i1 = h->Integral();
        h = (TH2F*)PcmCosThcmacc->fss_data->At(i);
        i2 = h->Integral();
        h = (TH2F*)XcmPcmacc->fss_data->At(i);
        i3 = h->Integral();

        std::cout << "\tFSS: "
                  << "\t: " << i1 << "\t" << i2 << "\t" << i3 << std::endl;
        std::cout << "\t\t: " << i1 / i2 << "\t" << i2 / i3 << "\t" << i3 / i1 << std::endl;
    }

    i1 = PtYcmacc->hLambda_sum_fss_raw->Integral();
    i2 = PcmCosThcmacc->hLambda_sum_fss_raw->Integral();
    i3 = XcmPcmacc->hLambda_sum_fss_raw->Integral();
    std::cout << "SIM: Uncorrected integrals:\n";
    std::cout << "\t: " << i1 << "\t" << i2 << "\t" << i3 << std::endl;
    std::cout << "\t: " << i1 / i2 << "\t" << i2 / i3 << "\t" << i3 / i1 << std::endl;

    i1 = PtYcmacc->hLambda_sum_fss->Integral();
    i2 = PcmCosThcmacc->hLambda_sum_fss->Integral();
    i3 = XcmPcmacc->hLambda_sum_fss->Integral();
    std::cout << "SIM: Corrected integrals:\n";
    std::cout << "\t: " << i1 << "\t" << i2 << "\t" << i3 << std::endl;
    std::cout << "\t: " << i1 / i2 << "\t" << i2 / i3 << "\t" << i3 / i1 << std::endl;

    i1 = PtYcmacc->hLambda_sig->Integral();
    i2 = PcmCosThcmacc->hLambda_sig->Integral();
    i3 = XcmPcmacc->hLambda_sig->Integral();
    std::cout << "EXP: Corrected integrals:\n";
    std::cout << "\t: " << i1 << "\t" << i2 << "\t" << i3 << std::endl;
    std::cout << "\t: " << i1 / i2 << "\t" << i2 / i3 << "\t" << i3 / i1 << std::endl;
}

void FitCrossections::AnaFinalize()
{
    GetExportFile()->cd();

    PtYcmacc->Finalize(flag_details);
    PcmCosThcmacc->Finalize(flag_details);
    XcmPcmacc->Finalize(flag_details);

    data->Write();

    setConfigVector();

    CloseExportFile();
}

void FitCrossections::PrintAnalysisInfo()
{
    std::cout << "\n=======================================================\n\n";
    std::cout << "\n=======================================================\n\n";
}

int FitCrossections::config_params(struct option*& opts)
{
    static struct option lopts[] = {{"nocorr", no_argument, &flag_nocorr, 1},
                                    {"nofit", no_argument, &flag_nofit, 1}};
    opts = lopts;

    return sizeof(lopts) / sizeof(option);
}

void FitCrossections::config_execute(int code, char* optarg)
{
    switch (code)
    {
        case 0:
            cfgfile = optarg;
            return;
            break;
    }

    // 	if (code) return;
}

const size_t hkindsnum = 3;

void FitCrossections::FitSpectras()
{
    PR(fitCoeffsNum);
    if (!fitCoeffsNum) return;
    fitCoeffs = new Float_t[fitCoeffsNum];

    size_t entrynum = data->simentry.size();

    const AcceptanceMatrixFactory* mFactories[hkindsnum] = {PtYcmacc, PcmCosThcmacc, XcmPcmacc};

    hKinds = new TH2F**[hkindsnum];
    for (size_t j = 0; j < hkindsnum; ++j)
    {
        hKinds[j] = new TH2F*[fitCoeffsNum + 2]; // hists to fit + sum of rests + reference
        hKinds[j][fitCoeffsNum] = (TH2F*)mFactories[j]->hLambda_sum_fss; // sum of rests
        hKinds[j][fitCoeffsNum + 1] = (TH2F*)mFactories[j]->hLambda_sig; // reference
    }

    Double_t* Sigmafit = new Double_t[fitCoeffsNum];
    Double_t* Sigmafit_Errors = new Double_t[fitCoeffsNum];
    Double_t* Sigmafit_Step = new Double_t[fitCoeffsNum];

    for (size_t k = 0; k < fitCoeffsNum; ++k)
    {
        Sigmafit_Step[k] = 0.0001;
    }

    size_t tmpcnt = 0;
    for (size_t i = 0; i < entrynum; ++i)
    {
        if (data->simentry[i].fit)
        {
            for (size_t j = 0; j < hkindsnum; ++j)
            {
                hKinds[j][tmpcnt] = (TH2F*)mFactories[j]->fss_data->At(i);
            }
            Sigmafit[tmpcnt] = data->simentry[i].fitpar;
            ++tmpcnt;
        }
    }

    //*************** Minimzing *************************************
    ROOT::Math::Minimizer* minSigma = ROOT::Math::Factory::CreateMinimizer("Minuit2", "Migrad");

    minSigma->SetMaxFunctionCalls(100000000);
    minSigma->SetMaxIterations(100000000);
    // minSigma->SetMaxFunctionCalls(100000);
    // minSigma->SetMaxIterations(100000);
    minSigma->SetTolerance(0.5);

    ROOT::Math::Functor fSigma(this, &FitCrossections::FittnessFunction,
                               fitCoeffsNum); //+1 to include Version number
    minSigma->SetFunction(fSigma);

    // 	double step[2] = {0.01,0.01};
    // starting point

    //    double variable[2] = { -1.,1.2};
    //    if (randomSeed >= 0) {
    //       TRandom2 r(randomSeed);
    //       variable[0] = r.Uniform(-20,20);
    //       variable[1] = r.Uniform(-20,20);
    //    }
    //
    //     for(Int_t iter = 0; iter < N_iter; iter++)
    //     {
    //         //Set the free variables to be minimized and iteration steps!
    //         //Total free = the lower limit is 0;
    //         if(Version==0||Version==1||Version==2)
    //         {
    //             for(Int_t i=0;i<N_Ch;i++)
    //             {
    //                 Name = "p";
    //                 Name += i;
    //                 SetLLimit[i] = 0.0;
    //                 SetULimit[i] = 1.0;
    //
    //                 if(Version==0)
    //                 {
    //                     minSigma->SetLowerLimitedVariable(i,Name.Data(),MinimStartPar[i],0.001,SetLLimit[i]);
    //                 }
    //                 if(Version==1)       //minimize within the limits SetLLimit[i],SetULimit[i]
    //                 {
    //
    //                     if(i==10)
    //                     minSigma->SetLimitedVariable(i,Name.Data(),MinimStartPar[i],0.001,SetLLimit[i],SetULimit[i]);
    //                     else
    //                     minSigma->SetLowerLimitedVariable(i,Name.Data(),MinimStartPar[i],0.001,SetLLimit[i]);
    //                 }
    //             }
    //         }
    //
    //         if(iter==0)
    //         {
    //             TString par_ver = "p";
    //             par_ver         += (N_Ch);
    //             minSigma->SetFixedVariable((N_Ch),par_ver.Data(),Version);
    //         }
    //         else minSigma->SetVariableValue((N_Ch),Version);
    //

    for (size_t k = 0; k < fitCoeffsNum; ++k)
    {
        minSigma->SetVariable(k, std::string(TString::Format("v%02d", k).Data()), Sigmafit[k],
                              Sigmafit_Step[k]);
    }

    // 	PR(Sigmafit);
    PR("Before fit");
    for (size_t k = 0; k < fitCoeffsNum; ++k)
    {
        PR(Sigmafit[k]);
    }

    // 	minSigma->SetVariableValues(Sigmafit);
    minSigma->Minimize();

    PR("After fit");
    for (size_t k = 0; k < fitCoeffsNum; ++k)
    {
        PR(Sigmafit[k]);
    }

    tmpcnt = 0;
    for (size_t i = 0; i < entrynum; ++i)
    {
        if (data->simentry[i].fit)
        {
            data->simentry[i].fit = 0;
            data->simentry[i].cs = Sigmafit[tmpcnt];
            ++tmpcnt;
        }
    }

    // 	Double_t pars[] = { 13. , 13., 13., 13., 1., 1., 1., 1. };
    // 	Float_t ff = FittnessFunction((Double_t *)pars);
    // 	PR(ff);
}

Double_t FitCrossections::FittnessFunction(const Double_t* par)
{
    // PR(par);
    // 	Float_t retval = 0;
    Float_t chi2[hkindsnum] = {0.0};
    Float_t chi2total = 0.0;

    for (size_t j = 0; j < hkindsnum; ++j)
    {
        const size_t xnum = hKinds[j][fitCoeffsNum]->GetNbinsX();
        const size_t ynum = hKinds[j][fitCoeffsNum]->GetNbinsY();

        // 		PR(chi2[j]);

        chi2[j] = 0.0;

        for (size_t x = 0; x < xnum; ++x)
        {
            for (size_t y = 0; y < ynum; ++y)
            {
                // PR(x);PR(y);
                Float_t restval = hKinds[j][fitCoeffsNum]->GetBinContent(1 + x, 1 + y);
                Float_t refval = hKinds[j][fitCoeffsNum + 1]->GetBinContent(1 + x, 1 + y);
                Float_t refvals = hKinds[j][fitCoeffsNum + 1]->GetBinError(1 + x, 1 + y);

                Float_t tmpval = 0.0;
                for (size_t k = 0; k < fitCoeffsNum; ++k)
                {
                    // PR(hKinds[j][k]->Integral());
                    // PR(par[k]);
                    // PR(1);
                    // PR(hKinds[j][k]->GetBinContent(1+x, 1+y));
                    Float_t binval = par[k] * hKinds[j][k]->GetBinContent(1 + x, 1 + y);
                    // if (binval > 0)
                    // PR(binval);
                    tmpval += binval;
                }

                Float_t tmpchi2 = 0.0;
                if (refvals > 0.0)
                    tmpchi2 = pow((refval - restval - tmpval) / refvals, 2.0);
                else
                    tmpchi2 = pow((refval - restval - tmpval), 2.0);

                // 				PR(tmpchi2);
                chi2[j] += tmpchi2;
            }
        }

        chi2total += chi2[j];
    }
    // 	PR(chi2total);
    return chi2total /*retval*/;
}