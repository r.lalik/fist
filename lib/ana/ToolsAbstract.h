/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TOOLSABSTRACT_H
#define TOOLSABSTRACT_H

#include "SimpleToolsAbstract.h"

class TCanvas;
class TF1;
class TFile;
class TH1;
class TH1F;
class TH2;
class TPad;
class TStyle;
class TVirtualPad;

struct option;

class DistributionContext;
class DistributionFactory;

class DifferentialContext;
;
class DifferentialFactory;

#ifndef __CINT__

#include "TString.h"

#endif /* __CINT__ */

#include "RootTools.h"

#include <iomanip>

#define RegVarInVector(id, vec, var)                                                               \
    vec->SetBinContent(id, var);                                                                   \
    vec->GetXaxis()->SetBinLabel(id, #var);

#define GetVarGromVector(id, vec, var) var = vec->GetBinContent(id);

struct option;

class ToolsAbstract : public SimpleToolsAbstract
{
public:
    ToolsAbstract();
    virtual ~ToolsAbstract();
    void proceed();

    RT::AxisFormat def_ax_diff;
    RT::AxisFormat def_ay_diff;
    RT::AxisFormat def_az_diff;
    RT::GraphFormat def_gf_diff;

    RT::PadFormat def_pf_diff;

protected:
    TCanvas* MakeCanvas(const TString& name, const TString& title, int width = 800,
                        int height = 600);

    virtual void initialize() override{};
    virtual void execute() override = 0;
    virtual void finalize() override{};

    TH1F* set_config_vector(TFile* f = nullptr, TString cfgvector = "anaparams");
    void get_config_vector(TFile* f = nullptr, TString cfgvector = "anaparams");

private:
    bool config_handler(int code, const char* optarg);

private:
    TStyle* sOrig;
    TStyle* sCustom;

protected:
    int flag_verbose;
    int flag_exp;  // exp data
    int flag_null; // exp null data

    int flag_sim; // 4pi simulation
    int flag_acc; // gcleaner in ACC
    int flag_fss; // full scale simulation

    int flag_pluto; // pluto simulation
    int flag_gibuu; // GiBUU
    int flag_urqmd; // UrQMD
    int flag_pwa;   // UrQMD

    int flag_details;

    // canvas size
    UInt_t can_width, can_height;

    TH1F* cfgvec;

private:
    TFile* outf;
    struct option* long_options_usage;
    size_t s_lo;
};

#endif // TOOLSABSTRACT_H
