/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "HistOperator.h"

#ifndef __CINT__

#include "getopt.h"
#include <fstream>
#include <string>

#include "TCanvas.h"
#include "TChain.h"
#include "TDirectory.h"
#include "TError.h"
#include "TF1.h"
#include "TFile.h"
#include "TGaxis.h"
#include "TGraphAsymmErrors.h"
#include "TH2.h"
#include "TImage.h"
#include "TKey.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TMath.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TSystem.h"
#include "TVector.h"
#include "TVirtualFitter.h"

#endif /* __CINT__ */

#include "RootTools.h"

#ifdef HAVE_HISTASYMMERRORS
#include "TH2DA.h"
#endif

#include <DifferentialFactory.h>
#include <DistributionFactory.h>
// #include <SmartFactory.h>

#include "DatabaseManager.h"

#include "globals.h"

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

const Option_t h1opts[] = "h,E1";

#ifdef HAVE_HISTASYMMERRORS
double calcTotalError(TH2DA* h, bool norm_bins = false, bool verbose = false)
{
    double scale = 1.0;

    if (norm_bins) { scale = h->GetXaxis()->GetBinWidth(1) * h->GetYaxis()->GetBinWidth(1); }

    double err_h = h->GetTotalErrorH() * scale;
    double err_l = h->GetTotalErrorL() * scale;

    if (verbose)
    {
        double tot = h->Integral() * scale;

        printf("int = %g\nerr-h = %g\nerr-l = %g\n", tot, err_h, err_l);
        printf("err-h/tot = %g\nerr-l/tot = %g\n", err_h / tot, err_l / tot);
        printf("err-h/tot = %g %%\nerr-l/tot = %g %%\n", err_h / tot * 100., err_l / tot * 100.);
    }
    return sqrt(err_h * err_h + err_l * err_l);
    ;
}
#endif

#ifdef HAVE_HISTASYMMERRORS
double calcTotalError2(TH2DA* h, bool norm_bins = false, bool verbose = false)
{
    double scale = 1.0;

    if (norm_bins) { scale = h->GetXaxis()->GetBinWidth(1) * h->GetYaxis()->GetBinWidth(1); }

    int bin_plus = h->GetXaxis()->FindBin(0.005);

    for (int i = bin_plus; i < h->GetXaxis()->GetNbins(); ++i)
    {
        for (int j = 0; j < h->GetYaxis()->GetNbins(); ++j)
        {
            h->SetBinContent(i + 1, j + 1, 0.0);
            h->SetBinErrorH(i + 1, j + 1, 0.0);
            h->SetBinErrorL(i + 1, j + 1, 0.0);
            h->SetBinError(i + 1, j + 1, 0.0);
        }
    }

    double err_h = h->GetTotalErrorH() * scale * sqrt(2.0);
    double err_l = h->GetTotalErrorL() * scale * sqrt(2.0);

    if (verbose)
    {
        double tot = h->Integral() * scale * 2.0;

        printf("int = %g\nerr-h = %g\nerr-l = %g\n", tot, err_h, err_l);
        printf("err-h/tot = %g\nerr-l/tot = %g\n", err_h / tot, err_l / tot);
        printf("err-h/tot = %g %%\nerr-l/tot = %g %%\n", err_h / tot * 100., err_l / tot * 100.);
    }
    return sqrt(err_h * err_h + err_l * err_l);
    ;
}
#endif

double calcTotalError(TH1* h, bool verbose)
{
    size_t bins_x = h->GetXaxis()->GetNbins();
    size_t bins_y = h->GetYaxis()->GetNbins();

    double total_err = 0.0;
    for (size_t x = 1; x <= bins_x; ++x)
    {
        for (size_t y = 1; y <= bins_y; ++y)
        {
            double err = h->GetBinError(x, y);
            if (err != 0.0)
            {
                total_err += err * err;
                if (verbose)
                    printf("[%lu, %lu] Adding %g * %g = %g -> %g\n", x, y, err, err, err * err,
                           total_err);
            }
        }
    }

    printf("  sqrt(%g) = %g\n", total_err, sqrt(total_err));
    return sqrt(total_err);
}

void local_csnames(TObject* dst, const TObject*)
{
    if (dst and dst->InheritsFrom("TH2"))
    {
        TString name = ((TNamed*)dst)->GetTitle();
        name.ReplaceAll("N", "#sigma");
        ((TNamed*)dst)->SetTitle(name);
    }
}

void local_copyrelerrors(TObject* dst, const TObject* src)
{
    if (!dst)
    {
        std::cerr << "dst object doesn't exists" << std::endl;
        return;
    }

    if (!src)
    {
        std::cerr << "dst object doesn't exists" << std::endl;
        return;
    }

    if (dst->InheritsFrom("TH2") and src->InheritsFrom("TH2"))
    {
        std::cout << " Calculating relative errors for " << dst->GetName() << std::endl;
        RootTools::copyRelativeErrors((TH1*)dst, (TH1*)src);
    }
}

void local_calcbinomerrors(TObject* p, const TObject* N)
{
    if (!p)
    {
        std::cerr << "dst object doesn't exists" << std::endl;
        return;
    }

    if (!N)
    {
        std::cerr << "dst object doesn't exists" << std::endl;
        return;
    }

    if (p->InheritsFrom("TH2") and N->InheritsFrom("TH2"))
    {
        std::cout << " Calculating relative errors for " << p->GetName() << std::endl;
        RootTools::calcBinomialErrors((TH1*)p, (TH1*)N);
    }
}

void local_efficiency(TObject* p, const TObject* N)
{
    if (!p)
    {
        std::cerr << "dst object doesn't exists" << std::endl;
        return;
    }

    if (!N)
    {
        std::cerr << "dst object doesn't exists" << std::endl;
        return;
    }

    if (p->InheritsFrom("TH2") and N->InheritsFrom("TH2"))
    {
        std::cout << " Calculating relative errors for " << p->GetName() << std::endl;
        RootTools::calcBinomialErrors((TH1*)p, (TH1*)N);
    }
}

// void local_applysystematics(DistributionFactory * target, const DistributionFactory * aesyst)
// {
// 	int idx = 0;
//
// 	idx = target->findIndexByRawname("@@@d/h_@@@a_LambdaInvMassSig");
// 	if (idx < 0)
// 	{
// 		std::cerr << "No index for htr\n";
// 		return;
// 	}
// 	TH2 * htrg = (TH2*)target->findObject(idx);
//
// 	idx = aesyst->findIndexByRawname("@@@d/h_@@@a_LambdaInvMass");
// 	if (idx < 0)
// 	{
// 		std::cerr << "No index for haesyst\n";
// 		return;
// 	}
// 	TH2 * haesyst = (TH2*)aesyst->findObject(idx);
//
// 	idx = aesyst->findIndexByRawname("@@@d/h_@@@a_LambdaInvMassSig");
// 	if (idx < 0)
// 	{
// 		std::cerr << "No index for hae\n";
// 		return;
// 	}
// 	TH2 * hae = (TH2*)aesyst->findObject(idx);
//
// 	const size_t bins_x = htrg->GetNbinsX();
// 	const size_t bins_y = htrg->GetNbinsY();
//   const size_t bins_z = htrg->GetNbinsZ();
//
//   htrg->GetBin();// FIXME
//   for (size_t x = 0; x < bins_x; ++x)
//     for (size_t z = 0; z < bins_z; ++z)
//       for (size_t y = 0; y < bins_y; ++y)
//       {
// // 			int bin = y * bins_x + x;
//
//         double bc = htrg->GetBinContent(x+1, y+1, z+1);
//         double be = htrg->GetBinError(x+1, y+1);
//         if (bc == 0.0 and be == 0.0)
//           continue;
//
//         double rms = haesyst->GetBinContent(x+1, y+1);
//         double ae = hae->GetBinContent(x+1, y+1);
//
//         double ae_systerr = rms;
//
//         double exp_aecorr = bc/ae;
//         double exp_aecorr_err = exp_aecorr * ae_systerr;
//
//         htrg->SetBinContent(x+1, y+1, exp_aecorr);
//         htrg->SetBinError(x+1, y+1, exp_aecorr_err);
//       }
//     }
//   }
// }

HistOperator::HistOperator()
    : SimpleToolsAbstract(), no_export_operation(true), has_csscale(false), flag_rise(false),
      flag_lower(false)
{
    NicePalette();
    TGaxis::SetMaxDigits(3);
    RootTools::MyMath();

    SetOutputFile("");
    // 	SetExportDir("");

    hop_names[HO_NOP] = "no operation";
    hop_names[HO_ADD] = "addition";
    hop_names[HO_SUB] = "substraction";
    hop_names[HO_MULT] = "multiplying";
    hop_names[HO_DIV] = "division";
    hop_names[HO_NEG] = "negation";
    hop_names[HO_NORM] = "normalization";
    hop_names[HO_NORMX] = "extended normalization";
    hop_names[HO_NDIV] = "normalization with division";
    hop_names[HO_PROJ] = "projection";
    hop_names[HO_SMOOTH] = "smoothing";
    hop_names[HO_BINNORM] = "bin size normalizing";
    hop_names[HO_ERRORS] = "error applier";
    hop_names[HO_COPYRELERRS] = "copy relative errors";
    hop_names[HO_BINOMERRORS] = "binominal errors";
    hop_names[HO_APPLYSYST] = "apply systematic errors";
    hop_names[HO_EFFICIENCY] = "ae matrix";
    hop_names[HO_FIXANGDIST] = "fix ang dist";

    // 	hop_names[HO_EOA] = "end of array";
}

HistOperator::~HistOperator() {}

void HistOperator::AnaInitialize() {}

SmartFactory* make_bank(size_t counter, TH1D*& h_cs, TH1D*& h_cs_errs, TH2D*& h_ang,
                        TH1D*& h_ang_errs)
{
    char buff[20];
    sprintf(buff, "bank%lu", counter);
    SmartFactory* csdata = new SmartFactory(buff);
    // 	h_cs = csdata->RegTH1<TH1D>("@@@d/h_@@@a_cs", "Cross-Section", 3, 0, 3, false);
    // 	h_cs_errs = csdata->RegTH1<TH1D>("@@@d/h_@@@a_cs_errors", "X-Sec Errors", 8, 0, 8, false);
    // 	h_ang = csdata->RegTH2<TH2D>("@@@d/h_@@@a_ang", "Angular Coefficients", 3, 0, 3, 3, 0, 3,
    // false); 	h_ang_errs = csdata->RegTH1<TH1D>("@@@d/h_@@@a_ang_errors", "Angular Coefficients
    // Errors", 6, 0, 6, false);
    h_cs = csdata->RegTH1<TH1D>("h_cs", "Cross-Section", 3, 0, 3, false);
    h_cs_errs = csdata->RegTH1<TH1D>("h_cs_errors", "X-Sec Errors", 8, 0, 8, false);
    h_ang = csdata->RegTH2<TH2D>("h_ang", "Angular Coefficients", 3, 0, 3, 3, 0, 3, false);
    h_ang_errs =
        csdata->RegTH1<TH1D>("h_ang_errors", "Angular Coefficients Errors", 6, 0, 6, false);

    return csdata;
}

bool calcErrorsFromDatabase(TFile* target, const TString& db, const TString& id)
{
    AnaDatabase* dbp = DatabaseManager::getDatabase(db.Data());
    dbp->prepare();

    auto cerrs = dbp->getErrors(id.Data());

    target->cd();

    TH1D* h_cs = nullptr;
    TH1D* h_cs_errs = nullptr;
    TH2D* h_ang = nullptr;
    TH1D* h_ang_errs = nullptr;

    SmartFactory* csdata = make_bank(0, h_cs, h_cs_errs, h_ang, h_ang_errs);

    DatabaseRecord dbr = dbp->getRecord(id.Data());
    h_cs->SetBinContent(1, dbr.cs * dbr.scale);

    for (size_t i = 0; i < 3; ++i)
    {
        h_ang->SetBinContent(i + 1, 2, dbr.ang[i]);

        ErrorsPair ep = DatabaseRecord::sumErrorChain(dbr.getAngErrors(i));
        h_ang_errs->SetBinContent(1 + 2 * i + 0, ep.high);
        h_ang_errs->SetBinContent(1 + 2 * i + 1, ep.low);

        h_ang->SetBinContent(i + 1, 3, ep.high);
        h_ang->SetBinContent(i + 1, 1, ep.low);
    }

    Double_t high = 0.0;
    Double_t low = 0.0;
    Double_t high_o = 0.0;
    Double_t low_o = 0.0;

    size_t size = cerrs.size();

    for (size_t i = 0; i < size; ++i)
    {
        high += cerrs[i].high * cerrs[i].high;
        low += cerrs[i].low * cerrs[i].low;

        if (i == 0)
        {
            h_cs_errs->SetBinContent(3, cerrs[i].high * dbr.scale);
            h_cs_errs->SetBinContent(4, cerrs[i].low * dbr.scale);
        }
        if (i == 1)
        {
            h_cs_errs->SetBinContent(5, cerrs[i].high * dbr.scale);
            h_cs_errs->SetBinContent(6, cerrs[i].low * dbr.scale);
        }
        if (i > 1)
        {
            high_o += cerrs[i].high * cerrs[i].high;
            low_o += cerrs[i].low * cerrs[i].low;
        }
    }

    high = sqrt(high);
    low = sqrt(low);
    high_o = sqrt(high_o);
    low_o = sqrt(low_o);

    h_cs_errs->SetBinContent(1, high * dbr.scale);
    h_cs_errs->SetBinContent(2, low * dbr.scale);

    h_cs_errs->SetBinContent(7, high_o * dbr.scale);
    h_cs_errs->SetBinContent(8, low_o * dbr.scale);

    h_cs->SetBinContent(2, high * dbr.scale);
    h_cs->SetBinContent(3, low * dbr.scale);

    csdata->write(target);

    return true;
}

bool calcErrorsFromDatabase(DistributionFactory* fac, const TString& db, const TString& id)
{
    AnaDatabase* dbp = DatabaseManager::getDatabase(db.Data());
    dbp->prepare();

    DatabaseRecord dbr = dbp->getRecord(id.Data());
    auto cerrs = dbr.getCsErrors();

    ErrorsPair ep = DatabaseRecord::sumErrorChain(cerrs);
    double err_avg = sqrt(ep.high * ep.high + ep.low * ep.low) / 2.0;

    double frac_h = ep.high / dbr.cs;
    double frac_l = ep.low / dbr.cs;

    uint bins_x = fac->hSignalCounter->GetNbinsX();
    uint bins_y = fac->hSignalCounter->GetNbinsY();
    //     uint bins_z = fac->hSignalCounter->GetNbinsZ();

    uint bins = 1;
    if (bins_x != 0) bins *= bins_x;
    if (bins_y != 0) bins *= bins_y;
    //   if (bins_z != 0) bins *= bins_z;

    for (size_t i = 0; i < bins; ++i)
    {
#ifdef HAVE_HISTASYMMERRORS
        int b_x = i % bins_x;
        int b_y = i / bins_x;
        double val = fac->hSignalCounter->GetBinContent(b_x + 1, b_y + 1);
        TH2DA* h2da = dynamic_cast<TH2DA*>(fac->hSignalCounter);
        if (h2da)
        {
            ((TH2DA*)fac->hSignalCounter)->SetBinErrorH(b_x + 1, b_y + 1, val * frac_h);
            ((TH2DA*)fac->hSignalCounter)->SetBinErrorL(b_x + 1, b_y + 1, val * frac_l);
        }
        else
        {
            double val = fac->hSignalCounter->GetBinContent(i);
            // PR(ep.high);        PR(val);PR(err_avg * val);PR(err_avg);
            fac->hSignalCounter->SetBinError(i, err_avg * val / dbr.cs);
        }
#else
        double val = fac->hSignalCounter->GetBinContent(i);
        fac->hSignalCounter->SetBinError(i, err_avg * val / dbr.cs);
#endif
    }

    return true;
}

bool calcErrorsFromDatabase(TFile* target, const TString& db_with_id)
{
    TObjArray* arr = db_with_id.Tokenize(',');
    size_t db_str_len = arr->GetEntries();
    if (db_str_len != 2) exit(1);

    return calcErrorsFromDatabase(target, ((TObjString*)arr->At(0))->String(),
                                  ((TObjString*)arr->At(1))->String());
}

bool calcErrorsFromDatabase(DistributionFactory* fac, const TString& db_with_id)
{
    TObjArray* arr = db_with_id.Tokenize(',');
    size_t db_str_len = arr->GetEntries();
    if (db_str_len != 2) exit(1);

    return calcErrorsFromDatabase(fac, ((TObjString*)arr->At(0))->String(),
                                  ((TObjString*)arr->At(1))->String());
}

void HistOperator::AnaExecute()
{

    if (!no_export_operation) OpenExportFile();

    if (!no_export_operation) SetOutputFile(outputfile);

    PrintAnalysisInfo();

    ProcessContextsGroup<DistributionContext, DistributionFactory>("_TotalCtx");
    ProcessContextsGroup<DifferentialContext, DifferentialFactory>("_DiscCtx");
    ProcessContextsGroup<DistributionContext, DistributionFactory>("_SignalCtx");
    //   ProcessContextsGroup("Ctx");

    GetExportFile()->cd();

    printf("\n + Saving config vector\n");
    setConfigVector()->Write();

    printf(" + Closing export file\n");
    CloseExportFile();
}

template <typename T1, typename T2>
bool HistOperator::ProcessContextsGroup(const TString& ctx_suffix)
{
    TSystem sys;
    for (uint i = 0; i < infile.size(); ++i)
    {
        TString fdir;
        TString fbase;

        if (output_file != "")
        {
            fdir = sys.DirName(output_file);
            fbase = sys.BaseName(output_file);
        }
        else
        {
            fdir = sys.DirName(infile[i]);
            fbase = sys.BaseName(infile[i]);
        }

        TString ofsuffix =
            export_suffix.Length()
                ? (export_suffix.BeginsWith('_') ? export_suffix : "_" + export_suffix)
                : "";
        TString outputfile;

        Int_t last_dot = fbase.Last('.');
        if (last_dot > fbase.Length())
        {
            outputfile = fdir + "/" + fbase + export_suffix /* + "__"*/;
        }
        else
        {
            outputfile = fdir + "/" + fbase.Replace(last_dot, 0, ofsuffix) /* + "__"*/;
        }

        T2* ifac[ananum];
        T2* ofac[ananum];
        T2* arg_fac[ananum];
        T1* ictx[ananum] = {0};
        T1* octx[ananum] = {0};

        TFile* f = ImportFromFile(infile[i], ictx, ifac, ctx_suffix);
        SmartFactory* custom = new SmartFactory("custom");
        // 		if (!custom->importStructure(f))
        {
            delete custom;
            custom = 0;
        }

        if (!f) continue;

        TString oldnames[ananum];
        for (uint j = 0; j < ananum; ++j)
        {
            //       DifferentialContext * dctx = dynamic_cast<DifferentialContext *>(ictx[j]);
            //       PR(dctx);
            //       if (dctx)
            //       {
            //         octx[j] = (DistributionContext *) new DifferentialContext(*dctx);
            //         octx[j]->name += "_res";
            //
            //         DifferentialFactory * dfac = new DifferentialFactory(*(DifferentialContext
            //         *)octx[j]); PR(dfac->Class_Name()); ofac[j] = (DistributionFactory *) dfac;
            //         PR(ofac[j]->Class_Name());
            //         DifferentialFactory * dfac2 = dynamic_cast<DifferentialFactory *>(dfac);
            //         PR(dfac2->Class_Name());
            //
            //         ofac[j]->init();
            //         ofac[j] = (DifferentialFactory *)ifac[j];
            //       }
            //       else
            {
                octx[j] = new T1(ifac[j]->ctx);
                oldnames[j] = octx[j]->name;

                //         octx[j]->name += "_res";
                //         octx[j]->update();
                //         ofac[j] = new T2(octx[j]);
                //         ofac[j]->init();
                //         (*ofac[j]) = *(ifac[j]);
                ofac[j] = ifac[j];
                ofac[j]->ctx.name += "_res";
                ofac[j]->ctx.hist_name.Clear();
                ofac[j]->reinit();
            }
        }

        for (uint k = 0; k < hist_operations.size(); ++k)
        {
            HistOperationCommand cmd = hist_operations[k].cmd;

            printf(" ---> Operation: %s\tArgument: %s\n", hop_names[cmd].c_str(),
                   hist_operations[k].arg.Data());

            TString arg;
            double arg_err;

            auto strvec = RootTools::split(hist_operations[k].arg.Data(), ':');
            arg = strvec[0];
            if (strvec.size() > 1)
            {
                auto arg_err_chain = RootTools::errorsStrToArray(strvec[1]);
                double err_l;
                RootTools::calcTotalError(arg_err_chain, arg_err, err_l);
            }
            else
            {
                arg_err = 0;
            }

            bool is_string = arg.IsAscii();
            TString str = is_string ? arg : "";

            bool is_number = arg.IsFloat();
            Float_t num = is_number ? arg.Atof() : 0.0;

            SmartFactory* arg_custom = new SmartFactory("custom");

            TFile* f_arg = 0;
            if (!is_number and (cmd != HO_ERRORS) and (cmd != HO_ANGDIST))
            {
                f_arg = ImportFromFile<T1, T2>(arg, ictx, arg_fac, ctx_suffix, true);
                // 				if (!arg_custom->importStructure(f_arg))
                {
                    delete arg_custom;
                    arg_custom = 0;
                }

                if (!f_arg /*or !f_arg->IsOpen()*/)
                {
                    std::cerr << "File " << arg << " can't be open!";
                    std::exit(EXIT_FAILURE);
                }
            }

            for (uint j = 0; j < ananum; ++j)
            {
                DifferentialFactory* dfac = nullptr;
                dfac = dynamic_cast<DifferentialFactory*>(ofac[j]);

                DifferentialFactory* dafac = nullptr;

                if (!is_number and arg_fac[j] and cmd != HO_ERRORS)
                    dafac = dynamic_cast<DifferentialFactory*>(arg_fac[j]);

                switch (cmd)
                {
                    case HO_ADD:

                        if (is_number)
                            NoNumberAllowed();
                        else
                        {
                            *ofac[j] += *arg_fac[j];
                            if (dfac && dafac && dfac->diffs) (*dfac->diffs) += (*dafac->diffs);
                            if (j == 0 and custom and arg_custom) *custom += *arg_custom;
                        }
                        break;

                    case HO_SUB:

                        if (is_number)
                            NoNumberAllowed();
                        else
                        {
                            *ofac[j] -= *arg_fac[j];
                            if (dfac && dafac && dfac->diffs && dafac->diffs)
                                (*dfac->diffs) -= (*dafac->diffs);
                            if (j == 0 and custom and arg_custom) *custom -= *arg_custom;
                        }
                        break;

                    case HO_MULT:

                        if (is_number)
                        {
                            *ofac[j] *= num;
                            if (dfac && dfac->diffs) (*dfac->diffs) *= num;

                            if (j == 0 and custom) *custom *= num;

                            RootTools::calcErrorPropagationMult(ofac[j]->hSignalCounter, num,
                                                                arg_err);
                            // TODO the same for diffs
                        }
                        else
                        {
                            *ofac[j] *= *arg_fac[j];
                            if (dfac && dafac && dfac->diffs && dafac->diffs)
                                (*dfac->diffs) *= (*dafac->diffs);
                            if (j == 0 and custom and arg_custom) *custom *= *arg_custom;
                        }
                        break;

                    case HO_DIV:

                        if (is_number)
                        {
                            *ofac[j] /= num;
                            if (dfac && dfac->diffs) (*dfac->diffs) /= num;
                            if (j == 0 and custom) *custom /= num;

                            RootTools::calcErrorPropagationMult(ofac[j]->hSignalCounter, num,
                                                                arg_err);
                            // TODO the same for diffs
                        }
                        else
                        {
                            *ofac[j] /= *arg_fac[j];
                            if (dfac && dafac && dfac->diffs) (*dfac->diffs) /= (*dafac->diffs);
                            if (j == 0 and custom and arg_custom) *custom /= *arg_custom;
                        }
                        break;

                    case HO_NEG:
                        if (is_number)
                        {
                            *ofac[j] *= -1;
                            if (dfac && dfac->diffs) (*dfac->diffs) *= -1;
                            if (j == 0 and custom) *custom *= -1;
                        }
                        else
                            NoFactoryAllowed();
                        break;

                    case HO_NORM:
                        if (is_number)
                        {
                            ofac[j]->norm(num);
                            if (dfac && dfac->diffs) dfac->diffs->norm(num);
                            if (j == 0 and custom) custom->norm(num);
                        }
                        else
                        {
                            ofac[j]->norm(*arg_fac[j]);
                            if (dfac && dafac && dfac->diffs) dfac->diffs->norm(*(dafac->diffs));
                            if (j == 0 and custom and arg_custom) custom->norm(*arg_custom);
                        }
                        break;

                    case HO_NORMX:
                        if (is_number)
                        {
                            ofac[j]->norm(num);
                            if (dfac && dfac->diffs) dfac->diffs->norm(num);
                            if (j == 0 and custom) custom->norm(num);
                        }
                        else
                        {
                            ofac[j]->norm(*arg_fac[j], true);
                            if (dfac && dafac) dfac->diffs->norm(*(dafac->diffs), true);
                            if (j == 0 and custom and arg_custom) custom->norm(*arg_custom, true);
                        }
                        break;

                    case HO_NDIV:

                        if (is_number)
                            NoNumberAllowed();
                        else
                        {
                            ofac[j]->norm(*arg_fac[j]);
                            *ofac[j] /= *arg_fac[j];

                            if (dfac && dafac && dfac->diffs)
                            {
                                dfac->diffs->norm(*(dafac->diffs));
                                (*dfac->diffs) /= (*dafac->diffs);
                            }

                            if (j == 0 and custom and arg_custom)
                            {
                                custom->norm(*arg_custom);
                                *custom /= *arg_custom;
                            }
                        }
                        break;

                    case HO_PROJ:

                        if (is_number)
                            NoNumberAllowed();
                        else
                        {
                            *ofac[j] *= *arg_fac[j];
                            *ofac[j] /= *arg_fac[j];

                            if (dfac && dafac && dfac->diffs)
                            {
                                (*dfac->diffs) *= (*dafac->diffs);
                                (*dfac->diffs) /= (*dafac->diffs);
                            }

                            if (j == 0 and custom and arg_custom)
                            {
                                *custom += *arg_custom;
                                *custom /= *arg_custom;
                            }
                        }
                        break;

                    case HO_SMOOTH:
                        if (is_number)
                        {
                            // 							ofac[j]->smooth(num);
                            // 							if (j == 0 and custom)
                            // 								custom->smooth();
                        }
                        else
                            NoFactoryAllowed();
                        break;

                    case HO_BINNORM:
                        // 						if (is_number)
                        ofac[j]->binnorm();
                        //               if (dfac) dfac->diffs->binnorm(); TODO do we need?
                        // 							if (j == 0 and custom)
                        // 								custom->binnorm();
                        // 						else
                        // 							NoFactoryAllowed();
                        break;

                    case HO_ERRORS:

                        // 						if (j == 0)
                        // 						{
                        // 							calcErrorsFromDatabase(GetExportFile(), str);
                        // 						}
                        calcErrorsFromDatabase(ofac[j], str);
                        //             if (dfac) calcErrorsFromDatabase(dfac->diffs, str); // TODO
                        //             do we need?

                        break;

                    case HO_COPYRELERRS:

                        if (is_number)
                            NoNumberAllowed();
                        else
                        {
                            ofac[j]->callFunctionOnObjects(arg_fac[j], local_copyrelerrors);
                            if (dfac && dafac && dfac->diffs)
                                dfac->diffs->callFunctionOnObjects(dafac, local_copyrelerrors);
                        }
                        break;

                    case HO_BINOMERRORS:

                        if (is_number)
                            NoNumberAllowed();
                        else
                        {
                            ofac[j]->callFunctionOnObjects(arg_fac[j], local_calcbinomerrors);
                            if (dfac && dafac && dfac->diffs)
                                dfac->diffs->callFunctionOnObjects(dafac, local_calcbinomerrors);
                        }
                        break;

                    case HO_EFFICIENCY:

                        if (is_number)
                            NoNumberAllowed();
                        else
                        {
                            ofac[j]->callFunctionOnObjects(arg_fac[j], local_calcbinomerrors);
                            if (dfac && dafac && dfac->diffs)
                                dfac->diffs->callFunctionOnObjects(dafac, local_calcbinomerrors);
                        }
                        break;

                    case HO_APPLYSYST:

                        if (is_number)
                            NoNumberAllowed();
                        else
                        {
                            // 							local_applysystematics(ofac[j], arg_fac[j]);
                            // FIXME
                            std::cerr << "Operation not permitted for HO_APPLYSYST!" << std::endl;
                            abort();
                        }
                        break;

                    case HO_TOTALERRORS:
                    {
                        double cont, err;
                        RootTools::calcTotalHistogramValues(ofac[j]->hSignalCounter, cont, err);
                        printf("[%d] = %g +/- %g (%g)\n", j, cont, err, err / cont * 100);

                        // 							calcTotalError(ofac[j]->hSignalCounter, true,
                        // true);
                        // FIXME
                        std::cerr << "Operation not permitted for HO_TOTALERRORS!" << std::endl;
                        abort();
                    }
                    break;

                    case HO_TOTALERRORS2:
                    {
                        double cont, err;
                        RootTools::calcTotalHistogramValues(ofac[j]->hSignalCounter, cont, err);
                        printf("[%d] = %g +/- %g (%g)\n", j, cont, err, err / cont * 100);

                        // 							calcTotalError2(ofac[j]->hSignalCounter, true,
                        // true);
                        // FIXME
                        std::cerr << "Operation not permitted for HO_TOTALERRORS2!" << std::endl;
                        abort();
                    }
                    break;

                    case HO_FIXANGDIST:
                    {
                        if (j != 1) break;

                        TH2* htemp_ang = (TH2D*)SmartFactory::getObject(f, "h_ang");
                        double ang_scalings[3] = {1., 0., 0.};
                        if (htemp_ang)
                        {
                            ang_scalings[0] = htemp_ang->GetBinContent(1, 2);
                            ang_scalings[1] = htemp_ang->GetBinContent(2, 2);
                            ang_scalings[2] = htemp_ang->GetBinContent(3, 2);
                        }
                        ofac[j]->applyAngDists(ang_scalings[1], ang_scalings[2]);
                        //             if (dfac) dfac->diffs->applyAngDists(ang_scalings[1],
                        //             ang_scalings[2]); // TODO do we need?
                    }

                    default:
                        break;
                }
            }
            if (f_arg)
            {
                for (uint j = 0; j < ananum; ++j)
                {
                    if (arg_fac[j])
                    {
                        delete arg_fac[j];
                        arg_fac[j] = nullptr;
                    }
                }
                f_arg->Close();
            }
        }

        if (!no_export_operation)
        {
            GetExportFile()->cd();
            // 			GetExportFile()->ls();

            DifferentialContext* dctx = nullptr;
            DifferentialFactory* dfac = nullptr;
            printf(" + Writing analysis :");
            for (uint j = 0; j < ananum; ++j)
            {
                ofac[j]->ctx.name.ReplaceAll("_res", "");
                ofac[j]->ctx.hist_name.Clear();
                ofac[j]->ctx.update();
                ofac[j]->reinit();

                dctx = dynamic_cast<DifferentialContext*>(octx[j]);
                dfac = dynamic_cast<DifferentialFactory*>(ofac[j]);

                if (has_csscale)
                {
                    // 					ofac[j]->callFunctionOnObjects(0, local_csnames);
                    ofac[j]->ctx.diff_var_name = "#sigma";
                    if (dctx && dfac) dctx->format_diff_axis();
                }

                printf(" %d", j);

                if (title.Length())
                {
                    ofac[j]->ctx.title = title;
                    ofac[j]->setTitleForAll(title);
                }

                // 				if (j == 1)
                ofac[j]->niceHists(def_pf_diff, def_gf_diff);

                if (dctx && dfac) dfac->prepareDiffCanvas();
                ofac[j]->prepareCanvas();

                ofac[j]->ctx.hist_name = oldnames[j].Data();
                ofac[j]->reinit();
                // 			ofac[j]->Finalize();

                if (dctx && dfac)
                    dfac->write(GetExportFile());
                else
                    ofac[j]->write(GetExportFile());
            }

            for (uint j = 0; j < ananum; ++j)
            {
                dctx = dynamic_cast<DifferentialContext*>(octx[j]);
                dfac = dynamic_cast<DifferentialFactory*>(ofac[j]);
                if (dctx && dfac)
                    dfac->ctx.Write();
                else
                    ofac[j]->ctx.Write();
            }
            if (custom) custom->exportStructure(GetExportFile());
        }

        // 	if (secvertfile)
        // 		secvertfile->Close();

        printf(" + Closing input file\n");
        if (f) f->Close();

        printf(" ++++ All done for %s\n", infile[i].Data());
    }
    return true;
}

void HistOperator::AnaFinalize() {}

void HistOperator::PrintAnalysisInfo() {}

int HistOperator::config_params(struct option*& opts)
{
    static struct option lopts[] = {
        {"cs", no_argument, &has_csscale, 1},
        {"add", required_argument, 0, 1000 + HO_ADD},
        {"sub", required_argument, 0, 1000 + HO_SUB},
        {"mult", required_argument, 0, 1000 + HO_MULT},
        {"div", required_argument, 0, 1000 + HO_DIV},
        {"neg", required_argument, 0, 1000 + HO_NEG},
        {"norm", required_argument, 0, 1000 + HO_NORM},
        {"normx", required_argument, 0, 1000 + HO_NORMX},
        {"normdiv", required_argument, 0, 1000 + HO_NDIV},
        {"proj", required_argument, 0, 1000 + HO_PROJ},
        {"smooth", required_argument, 0, 1000 + HO_SMOOTH},
        {"binnorm", no_argument, 0, 1000 + HO_BINNORM},
        {"errors", required_argument, 0, 1000 + HO_ERRORS},
        {"angdist", required_argument, 0, 1000 + HO_ANGDIST},
        {"copy-rel-errors", required_argument, 0, 1000 + HO_COPYRELERRS},
        {"binom-errors", required_argument, 0, 1000 + HO_BINOMERRORS},
        {"ae", required_argument, 0, 1000 + HO_EFFICIENCY},
        {"apply-syst", required_argument, 0, 1000 + HO_APPLYSYST},
        {"total-errors", no_argument, 0, 1000 + HO_TOTALERRORS},
        {"total-errors2", no_argument, 0, 1000 + HO_TOTALERRORS2},
        {"fix-ang-dists", no_argument, 0, 1000 + HO_FIXANGDIST},
        {"rise", no_argument, &flag_rise, 1},
        {"lower", no_argument, &flag_lower, 1}};
    opts = lopts;

    return sizeof(lopts) / sizeof(option);
}

void HistOperator::config_execute(int code, char* optarg)
{
    HistOperationCommand hoc = HO_NOP;

    switch (code)
    {
        case 1001:
        case 1002:
        case 1003:
        case 1004:
        case 1005:
        case 1006:
        case 1007:
        case 1008:
        case 1009:
        case 1010:
        case 1012:
        case 1013:
        case 1014:
        case 1015:
        case 1016:
        case 1017:
            no_export_operation = false;
            {
                hoc = HistOperationCommand(code - 1000);

                HistOperation ho;
                ho.cmd = hoc;
                ho.arg = optarg;
                hist_operations.push_back(ho);
            }
            break;
        case 1011:
        case 1020:
            no_export_operation = false;
            // fall through
        case 1018:
        case 1019:
        {
            hoc = HistOperationCommand(code - 1000);

            HistOperation ho;
            ho.cmd = hoc;
            ho.arg = "0.";
            hist_operations.push_back(ho);
        }
        break;
        default:
            infile.push_back(optarg);
            return;
            break;
    }

    if (flag_rise && flag_lower)
    {
        std::cerr << "Flags rise and lower are ..." << std::endl;
        exit(0);
    }
    // 	if (code) return;
}

void HistOperator::NoNumberAllowed()
{
    std::cerr << "Sorry, No numerical argument allowed for this operation, aborting..."
              << std::endl;
    // 	abort();
    exit(EXIT_FAILURE);
}

void HistOperator::NoFactoryAllowed()
{
    std::cerr << "Sorry, No file argument allowed for this operation, aborting..." << std::endl;
    // 	abort();
    exit(EXIT_FAILURE);
}

template bool HistOperator::ProcessContextsGroup<DistributionContext, DistributionFactory>(
    const TString& ctx_suffix);
template bool HistOperator::ProcessContextsGroup<DifferentialContext, DifferentialFactory>(
    const TString& ctx_suffix);
