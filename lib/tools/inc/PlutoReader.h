/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PLUTOREADER_H
#define PLUTOREADER_H

#ifndef __CINT__

#include "TChain.h"
#include "TString.h"
// #include "TVector.h"

#include "SecVertexReco.h"

class TCanvas;
class TChain;
class TClonesArray;
class TF1;
class TFile;
class TH1;
class TH1F;
class TH2;
class TH2F;
class TH3;
class TH3F;
class TStyle;

#endif /* __CINT__ */

class PlutoReader : public SecVertexReco
{
public:
    PlutoReader();
    // 	PlutoReader(Int_t nxnum = 4, Int_t channum = 128, const TString & exportdir = "Export");
    virtual ~PlutoReader();

    void AnaInitialize();

protected:
    virtual void ReadEvent(ULong64_t ev);
    virtual void InitTree();

private:
    // branches
    TClonesArray* b_fPart;
    Int_t b_fPartNum;

    Double_t CMS_Beta;
    Double_t CMS_Beta_x; // x component of BetaVector
    Double_t CMS_Beta_y; // y component of BetaVector
    Double_t CMS_Beta_z;
};

#endif // PLUTOREADER_H
