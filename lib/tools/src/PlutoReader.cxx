/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __CINT__

#include "getopt.h"
#include <fstream>
#include <string>

#include "TCanvas.h"
#include "TChain.h"
#include "TClonesArray.h"
#include "TDirectory.h"
#include "TError.h"
#include "TF1.h"
#include "TFile.h"
#include "TGaxis.h"
#include "TH2.h"
#include "TImage.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TLorentzVector.h"
#include "TMath.h"
#include "TStyle.h"
#include "TVector.h"

#endif /* __CINT__ */

#include "PlutoReader.h"
#include "RootTools.h"
#include "pp35Tools.h"

#include "PParticle.h"

// #include "hphysicsconstants.h"
// #include "hpidphysicsconstants.h"

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

static const Float_t E_kin_beam = 3500.0;
static const Float_t E_kin_target = 0.0;
static const Float_t E_total_beam = E_kin_beam + 938;     // HPidPhysicsConstants::mass(14);
static const Float_t E_total_target = E_kin_target + 938; // HPidPhysicsConstants::mass(14);
static const Float_t pz_beam =
    sqrt(E_total_beam * E_total_beam -
         /*HPidPhysicsConstants::mass(14)*HPidPhysicsConstants::mass(14)*/ 938 * 938);

static const TLorentzVector Vec_pp35_beam = TLorentzVector(0.0, 0.0, pz_beam, E_total_beam);
static const TLorentzVector Vec_pp35_target = TLorentzVector(0.0, 0.0, 0.0, E_total_target);
static const TLorentzVector Vec_pp35_sum = Vec_pp35_beam + Vec_pp35_target;
static const Float_t cmrap = Vec_pp35_sum.Rapidity();

TLorentzVector Vec_beam_target = Vec_pp35_beam + Vec_pp35_target;

Float_t PiConst = TMath::Pi();

// Processing data bar width
const Int_t bar_dotqty = 10000;
const Int_t bar_width = 20;

const Option_t h1opts[] = "h,E1";

PlutoReader::PlutoReader() : SecVertexReco(), b_fPart(nullptr), b_fPartNum(0) {}

PlutoReader::~PlutoReader() {}

void PlutoReader::InitTree()
{
    if (chain) return;

    chain = new TChain("data");

    chain->SetBranchAddress(
        "Particles", &b_fPart); // Nimmt Ast mit Namen particles und weist evtA den Eintrag zu
    chain->SetBranchAddress("Npart",
                            &b_fPartNum); // Nimmt Ast mit Namen Npart und weist NpartAAddToChain(
}

void PlutoReader::AnaInitialize()
{
    SecVertexReco::AnaInitialize();

    CMS_Beta = Vec_beam_target.Beta();
    CMS_Beta_x = CMS_Beta * sin(Vec_beam_target.Theta()) *
                 cos(Vec_beam_target.Phi()); // x component of BetaVector
    CMS_Beta_y = CMS_Beta * sin(Vec_beam_target.Theta()) *
                 sin(Vec_beam_target.Phi()); // y component of BetaVector
    CMS_Beta_z = CMS_Beta * cos(Vec_beam_target.Theta());
}

void PlutoReader::ReadEvent(ULong64_t ev)
{
    SecVertexReco::ReadEvent(ev);

    //	if(b_fPartnum >= 60) cout << "arrays are too small!! "  << endl;

    // cout << "******* START of track loop ********" << endl;
    for (Int_t a = 0; a < b_fPartNum; a++)
    { // Start loop
        PParticle* particle = (PParticle*)(b_fPart->At(
            a)); // Weist dem 1. Eintrag von Array(particle) ein PParticle(klar) aus evtA zu
        // Im evtA sthen aber schon PParticles mit bestimmter Adresse
        Int_t pid = particle->ID();

        // VecPt       = VecPt*1000.0;
        // VecP        = particle[a]->P();
        Float_t VecTheta = particle->Theta();
        // VecPx       = particle[a]->Px();
        // VecPy       = particle[a]->Py();
        // VecPz       = particle[a]->Pz();
        // TVector3 Vertex   = particle[a]->getVertex();
        Float_t VecM = particle->M();
        VecTheta = (360 / (2 * PiConst)) * VecTheta;
        // cout << "Theta "<<VecTheta<<endl;
        // PR(pid);PR(VecM);
        if (pid == 18 or pid == 20)
        {
            b_fY = particle->Rapidity();
            b_fPt = particle->Pt() * 1000; // Pt Transversalimpuls
            b_fM = particle->M() * 1000;

            // 			TLorenzVector trackAB(particle->Px()*1000, particle->Py()*1000,
            // particle->Pz()*1000, particle->E()*1000);

            // z component of BetaVector

            TLorentzVector trackAB_CMS = *particle;
            trackAB_CMS.Boost(-CMS_Beta_x, -CMS_Beta_y, -CMS_Beta_z);
            b_fY_cms = trackAB_CMS.Rapidity();
            b_fP_cms = trackAB_CMS.P();
            b_fCosTheta_cms = cos(trackAB_CMS.Theta());
            // PR(b_fP_cms);
            // PR(b_fCosTheta_cms);
            // PR(b_fMinTrackDist);

            return;
        }
    }
}