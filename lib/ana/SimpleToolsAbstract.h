/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SIMPLETOOLSABSTRACT_H
#define SIMPLETOOLSABSTRACT_H

class TCanvas;
class TF1;
class TFile;
class TH1;
class TH1F;
class TH2;
class TObjArray;
class TPad;
class TStyle;
class TVirtualPad;

struct option;

class DistributionContext;
class DistributionFactory;

class DifferentialContext;
class DifferentialFactory;

#include <RootTools.h>

#include "TString.h"

#include <getopt.h>
#include <iomanip>

#define RegVarInVector(id, vec, var)                                                               \
    vec->SetBinContent(id, var);                                                                   \
    vec->GetXaxis()->SetBinLabel(id, #var);

#define GetVarGromVector(id, vec, var) var = vec->GetBinContent(id);

struct option;

class SimpleToolsAbstract
{
public:
    SimpleToolsAbstract();
    virtual ~SimpleToolsAbstract();

    void proceed();
    int configure(int argc, char** argv);

    RT::AxisFormat def_ax_diff;
    RT::AxisFormat def_ay_diff;
    RT::AxisFormat def_az_diff;
    RT::GraphFormat def_gf_diff;

    RT::PadFormat def_pf_diff;

protected:
    TCanvas* MakeCanvas(const TString& name, const TString& title, int width = 800,
                        int height = 600);

    virtual void initialize(){};
    virtual void execute() = 0;
    virtual void finalize(){};

    void set_label(const TString& t) { title = t; }
    void set_title(const TString& l) { label = l; }

    TString get_label() const { return label; };
    TString get_title() const { return title; };

    void set_export_dir(const TString& exportdir);
    void set_output_file(const TString& outputf);

    TString get_export_dir() { return export_directory; }
    TFile* export_file() { return outf; }

    void set_export_flags(Bool_t ePNG, Bool_t eEPS, Bool_t ePDF);

    void save_and_close(TCanvas* can, Bool_t export_images = kTRUE);

    void open_export_file(const char* mode = "RECREATE");
    void close_export_file();
    TFile* get_export_file() { return outf; };

    virtual void print_basic_info();

    TH1F* set_config_vector(TFile* f = nullptr, TString cfgvector = "anaparams");
    void get_config_vector(TFile* f = nullptr, TString cfgvector = "anaparams");

    void set_ana_num(size_t n);
    uint get_ana_names(TFile* f = nullptr);
    void set_ana_names(TFile* /*f*/ = nullptr) {}

    template <typename T1, typename T2>
    TFile* import_from_file(const TString& file, T1** ctx, T2** fac, const TString& suffix,
                            bool rename = false);

    typedef std::pair<DistributionFactory*, DistributionContext*> FacPair;
    std::vector<FacPair> import_all_from_file(const TString& file, TFile* f);

    // 	static void colstd(TermColors c = TC_YellowB);
    // 	static void resstd();

protected:
    void usage(char** argv);
    void add_cmdl_option(option opt, std::string description);
    void add_cmdl_option_handler(std::function<bool(int, const char*)> f);

public:
    const static size_t MAXANA = 20;

protected:
    Bool_t hasExportEnabled, hasOutputFile;
    Bool_t exportPNG, exportEPS, exportPDF;

    TString label;
    TString title;
    TString export_directory;
    TString output_file;
    TString export_prefix;
    TString export_suffix;

    int flag_verbose;
    int flag_details;

    // canvas size
    UInt_t can_width, can_height;

    TH1F* cfgvec;
    size_t ananum;
    TObjArray* p_anaarr;
    std::vector<std::string> ananames;

private:
    TStyle* sOrig;
    TStyle* sCustom;

    TFile* outf;
    struct option* long_options_usage;
    size_t s_lo;

    std::vector<option> cmdl_options;
    std::vector<std::string> cmdl_options_descriptions;
    std::vector<std::function<bool(int, const char*)>> cmdl_options_handlers;
};

#endif // SIMPLETOOLSABSTRACT_H
