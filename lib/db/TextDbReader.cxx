#include <exception>
#include <string>

#include "TextDbReader.h"

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

void TextDbReader::prepare()
{
    std::ifstream dbfile(dbfname.c_str());
    if (!dbfile.is_open())
    {
        std::cerr << "File " << dbfname << " can not be open!";
        std::exit(EXIT_FAILURE);
    }

    std::string line;
    TString result;

    int counter = 1;
    while (std::getline(dbfile, line))
    {
        if (line.size() == 0) continue;

        TString line_(line);
        line_.ReplaceAll("\t", " ");
        line_.Remove(TString::kBoth, ' ');

        if (line_.BeginsWith("#")) continue;

        TObjArray* arr = line_.Tokenize(" ");

        DatabaseRecord dbe;
        dbe.id = counter;
        dbe.label = ((TObjString*)arr->At(0))->String();
        dbe.location = ((TObjString*)arr->At(1))->String();
        dbe.cs = ((TObjString*)arr->At(2))->String().Atof();
        // 		dbe.color = ((TObjString *)arr->At(3))->String().Atoi();
        dbe.latex = ((TObjString*)arr->At(4))->String();
        dbe.scale = ((TObjString*)arr->At(5))->String().Atof();
        if (arr->GetEntries() >= 7) dbe.cs_errors_str = ((TObjString*)arr->At(6))->String();
        dbe.ang[0] = 1.0;
        dbe.ang[1] = 0.0;
        dbe.ang[2] = 0.0;

        dbmap[dbe.label] = dbe;
        ++counter;
    }
}