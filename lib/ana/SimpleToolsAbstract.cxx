/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "SimpleToolsAbstract.h"

// #include "globals.h"

#include <DistributionContext.h>
#include <DistributionFactory.h>

#include <DifferentialContext.h>
#include <DifferentialFactory.h>

#include <TCanvas.h>
#include <TColor.h>
#include <TF1.h>
#include <TFile.h>
#include <TGaxis.h>
#include <TH1.h>
#include <TH2.h>
#include <TLatex.h>
#include <TObjString.h>
#include <TPad.h>
#include <TStyle.h>

#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <iostream>

#include <sys/stat.h>

#undef NDEBUG
#include <assert.h>

#define PR(x)                                                                                      \
    std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

#define test_array(arr, size) assert((sizeof(arr) / sizeof(arr[0])) == (size));

static int mkpath(std::string s, mode_t mode)
{
    size_t pre = 0, pos;
    std::string dir;
    int mdret = 0;

    if (s[s.size() - 1] != '/')
    {
        // force trailing / so we can handle everything in loop
        s += '/';
    }

    while ((pos = s.find_first_of('/', pre)) != std::string::npos)
    {
        dir = s.substr(0, pos++);
        pre = pos;
        if (dir.size() == 0) continue; // if leading / first time is 0 length
        if ((mdret = mkdir(dir.c_str(), mode)) && errno != EEXIST) { return mdret; }
    }
    return mdret;
}

SimpleToolsAbstract::SimpleToolsAbstract()
    : flag_verbose(0), flag_details(1), can_width(800), can_height(600), cfgvec(nullptr), ananum(0),
      p_anaarr(nullptr)
{
    // verbose outputs
    add_cmdl_option({"verbose", no_argument, &flag_verbose, 1}, "Vebose output");
    add_cmdl_option({"brief", no_argument, &flag_verbose, 0}, "Quiet output");

    // show details in plots
    add_cmdl_option({"details", no_argument, &flag_details, 1}, "Add details to output");
    add_cmdl_option({"no-details", no_argument, &flag_details, 0}, "Do not show details");

    //
    add_cmdl_option({"dir", required_argument, 0, 'x'}, "Output directory");
    add_cmdl_option({"prefix", required_argument, 0, 'p'}, "Prefix");
    add_cmdl_option({"suffix", required_argument, 0, 's'}, "Suffix");
    add_cmdl_option({"label", required_argument, 0, 'l'}, "Label");
    add_cmdl_option({"title", required_argument, 0, 't'}, "Title");
    add_cmdl_option({"output", required_argument, 0, 'o'}, "Output file");
    add_cmdl_option({"help", no_argument, 0, 'h'}, "Show this help");

    TGaxis::SetMaxDigits(3);

    RT::MyMath();

    set_output_file("output.root");
    set_export_dir("./");
    set_export_flags(kTRUE, kTRUE, kFALSE);

    def_ax_diff = RT::AxisFormat{208, 0.06, 0.005, 0.07, 0.95, false, true, RT::AxisFormat::FALL};
    def_ay_diff = RT::AxisFormat{502, 0.06, 0.005, 0.07, 1.05, false, true, RT::AxisFormat::FALL};
    def_az_diff = RT::AxisFormat{10, 0.05, 0.005, 0.07, 1.0, false, true, RT::AxisFormat::FALL};

    def_gf_diff = {def_ax_diff, def_ay_diff, def_az_diff};

    def_pf_diff = {0.07, 0.2, 0.15, 0.15};
}

SimpleToolsAbstract::~SimpleToolsAbstract() {}

TCanvas* SimpleToolsAbstract::MakeCanvas(const TString& name, const TString& title, int width,
                                         int height)
{
    TCanvas* can = new TCanvas(name, title, width, height);
    can->cd();

    if (!label.IsNull())
    {
        TLatex* l = new TLatex;
        l->SetTextSize(0.025);
        l->SetNDC();
        l->DrawLatex(0.02, 0.01, label);
    }

    return can;
}

void SimpleToolsAbstract::proceed()
{
    initialize();

    print_basic_info();

    execute();

    finalize();
    std::cout << std::endl << colstd << "Analysis finished" << resstd << std::endl;
    std::cout << "=======================================================\n";
    // 	resstd();
}

void SimpleToolsAbstract::save_and_close(TCanvas* can, Bool_t export_images)
{
    RT::SaveAndClose(can, outf, export_images, export_directory);
}

void SimpleToolsAbstract::set_export_dir(const TString& exportdir)
{
    export_directory = exportdir;

    if (export_directory.Length() and !export_directory.EndsWith("/")) export_directory += "/";

    hasExportEnabled = kTRUE;
}

void SimpleToolsAbstract::set_output_file(const TString& outputf)
{
    output_file = outputf;
    hasOutputFile = kTRUE;
}

void SimpleToolsAbstract::set_export_flags(Bool_t ePNG, Bool_t eEPS, Bool_t ePDF)
{
    exportPNG = ePNG;
    exportEPS = eEPS;
    exportPDF = ePDF;
}

int SimpleToolsAbstract::configure(int argc, char** argv)
{
    cmdl_options.push_back({0, 0, 0, 0});

    option* long_options = cmdl_options.data();

    bool show_help = false;
    Int_t c = 0;
    while (1)
    {
        int option_index = 0;

        c = getopt_long(argc, argv, "hx:p:s:l:t:o:", long_options, &option_index);
        if (c == -1) break;

        switch (c)
        {
            case 0:
                if (long_options[option_index].flag != 0) break;
                std::printf("option %s", long_options[option_index].name);
                if (optarg) std::printf(" with arg %s", optarg);
                std::printf("\n");
                break;
            case 'o':
                set_output_file(optarg);
                if (!output_file.EndsWith(".root")) output_file += ".root";
                break;
            case 'x':
                set_export_dir(optarg);
                break;
            case 'p':
                export_prefix = optarg;
                break;
            case 's':
                export_suffix = optarg;
                break;
            case 'e':
                //                 events = atoll(optarg);
                break;
            case 'l':
                set_label(optarg);
                break;
            case 't':
                title = optarg;
                break;
            case 'h':
                show_help = true;
                break;
            case '?':
            default:
            {
                bool ret = false;
                for (auto f : cmdl_options_handlers)
                {
                    ret = f(c, optarg);
                    if (!ret) break;
                }
                if (!ret) { std::cerr << "Argument " << c << " not handled\n"; }
            }
            break;
        }
    }

    if (show_help)
    {
        usage(argv);
        exit(EXIT_SUCCESS);
    }
    // PR(flag_urqmd);

    // 	if ( ( ( 1*(bool)flag_pluto + 1*(bool)flag_gibuu + 1*(bool)flag_urqmd + 1*(bool)flag_pwa +
    // 1*(bool)flag_exp ) > 1 ) or 		 ( ( 1*(bool)flag_sim + 1*(bool)flag_acc + 1*(bool)flag_fss
    // ) > 1 ) )
    // 	{
    // 		std::cerr << "Select only on of exp, sim, acc, fss and pluto, gibuu, urqmd" <<
    // std::endl; 		std::exit(EXIT_FAILURE);
    // 	}
    //
    // 	if (!(flag_pluto or flag_gibuu or flag_urqmd or flag_pwa))
    // 		flag_exp = 1;

    // 	std::cout << "+ Accepted modes\n";

    //     while (optind < argc)
    //     {
    //         config_execute(0, argv[optind++]);
    //     }

    return 0;
}

void SimpleToolsAbstract::open_export_file(const char* mode)
{
    // we do not want to track all histograms, only selected one written with hist->Write()
    // The same for canvases

    mkpath(export_directory.Data(), 0755);

    TString ofile = export_directory + "/" + output_file;
    outf = new TFile(ofile.Data(), mode);
    if (!outf->IsOpen())
    {
        std::cerr << "ERROR: Opening file " << ofile.Data() << " failed. Data are not saved!";
        exit(EXIT_FAILURE);
    }

    if (outf->IsZombie())
    {
        std::cerr << "ERROR: File " << ofile.Data() << " is zombie. Data are not saved!";
        exit(EXIT_FAILURE);
    }
}

void SimpleToolsAbstract::close_export_file()
{
    if (outf)
    {
        std::cout << "Closing " << output_file.Data() << std::endl;
        outf->Write();
        outf->Close();
        outf->Delete();
        outf = 0;
    }
}

void SimpleToolsAbstract::print_basic_info()
{
    // 	colstd();
    std::cout << "\n=======================================================\n";
    std::cout << "* Basic Info:\n";
    std::cout << "  Export dir:    " << export_directory << std::endl;
    std::cout << "  Output file:   " << output_file << std::endl;
    std::cout << "  Export prefix: " << export_prefix << std::endl;
    std::cout << "  Label:         " << label << std::endl;
    std::cout << "=======================================================\n";
    // 	resstd();

    // 	PrintAnalysisInfo();
}

TH1F* SimpleToolsAbstract::set_config_vector(TFile* f, TString cfgvector)
{
    cfgvec = nullptr;
    if (f)
        f->GetObject(cfgvector, cfgvec);
    else
        gDirectory->GetObject(cfgvector, cfgvec);

    const Int_t cfgsize = 40;
    if (!cfgvec) { cfgvec = new TH1F(cfgvector.Data(), "Config Vector", cfgsize, 0, cfgsize); }

    cfgvec->SetBinContent(1, can_width);
    cfgvec->GetXaxis()->SetBinLabel(1, "can_width");
    cfgvec->SetBinContent(2, can_height);
    cfgvec->GetXaxis()->SetBinLabel(2, "can_height");

    // 	cfgvec->Write();

    return cfgvec;
}

void SimpleToolsAbstract::get_config_vector(TFile* f, TString cfgvector)
{
    cfgvec = nullptr;
    if (f)
        f->GetObject(cfgvector, cfgvec);
    else
        gDirectory->GetObject(cfgvector, cfgvec);

    if (!cfgvec)
    {
        std::cerr << "No vector " << cfgvector << std::endl;
        return;
        std::exit(EXIT_FAILURE);
    }

    can_width = cfgvec->GetBinContent(31);
    can_height = cfgvec->GetBinContent(32);
}

void SimpleToolsAbstract::set_ana_num(size_t n)
{
    if (ananum)
    {
        std::cerr << "::setAnaNum() : ananum already configured!" << std::endl;
        abort();
    }

    if (n > MAXANA)
    {
        std::cerr << "::setAnaNum() : number of " << n << " is more than allowed " << MAXANA << "!"
                  << std::endl;
        abort();
    }
    ananum = n;
}

uint SimpleToolsAbstract::get_ana_names(TFile* f)
{
    if (p_anaarr) delete p_anaarr;

    if (ananum)
    {
        std::cerr << "::getAnaNames() : ananum already configured!" << std::endl;
        abort();
    }

    ananames.clear();

    if (f)
        f->GetObject("_ana_names_", p_anaarr);
    else
        gDirectory->GetObject("_ana_names_", p_anaarr);

    if (!p_anaarr)
    {
        std::cerr << "No ananames vector found" << std::endl;
        std::exit(EXIT_FAILURE);
    }

    set_ana_num(p_anaarr->GetEntries());

    for (uint i = 0; i < ananum; ++i)
    {
        TObjString* str = dynamic_cast<TObjString*>(p_anaarr->At(i));
        if (str)
            ananames.push_back(str->GetString().Data());
        else
            std::cerr << "Invalid entry in _ana_names_ array" << std::endl;
    }

    return ananum;
}

template <typename T1, typename T2>
TFile* SimpleToolsAbstract::import_from_file(const TString& file, T1** ctx, T2** fac,
                                             const TString& suffix, bool rename)
{
    static int cnt = 0;
    TFile* inputfile = nullptr;
    inputfile = TFile::Open(file, "READ");

    if (!inputfile or !inputfile->IsOpen())
    {
        std::cerr << "File " << file << " can't be open!" << std::endl;
        return nullptr;
    }
    get_config_vector();
    get_ana_names();

    if (!ananum)
    {
        ctx = nullptr;
        fac = nullptr;
        return inputfile;
    }

    ctx = new T1*[ananum];
    fac = new T2*[ananum];

    ++cnt;

    std::string tmpname;
    bool reading_failed = false;
    uint i = 0;
    for (i = 0; i < ananum; ++i)
    {
        T1* dactx = nullptr;
        if (!ctx || !ctx[i])
        {
            tmpname = ananames[i];
            tmpname.append(suffix);
            dactx = (T1*)inputfile->Get(tmpname.c_str());
            if (!dactx)
            {
                reading_failed = true;
                break;
            }
            if (ctx && !ctx[i]) ctx[i] = dactx;
        }
        else
        {
            dactx = ctx[i];
        }

        fac[i] = new T2(dactx);
        fac[i]->setSource(inputfile);
        fac[i]->init();
        fac[i]->RegObject((TString("@") + suffix).Data());

        if (rename)
        {
            // temporary name change
            char aaa[100];
            sprintf(aaa, "_%d", cnt);
            fac[i]->ctx.hist_name += aaa;
            fac[i]->ctx.dir_name += aaa;
            fac[i]->ctx.update();
            fac[i]->reinit();
        }
    }

    if (reading_failed)
    {
        std::cerr << "Reading failed in " << file << " for " << tmpname << std::endl;
        for (uint j = 0; j < i; ++j)
        {
            if (fac[j]) delete fac[j];
            fac[j] = nullptr;
        }
        inputfile->Close();
        return nullptr;
    }
    return inputfile;
}

std::vector<SimpleToolsAbstract::FacPair>
SimpleToolsAbstract::import_all_from_file(const TString& file, TFile* f)
{
    std::vector<FacPair> vec;
    static int cnt = 0;
    f = TFile::Open(file, "READ");

    if (!f or !f->IsOpen())
    {
        std::cerr << "File " << file << " can't be open!";
        return vec;
    }
    get_config_vector();

    ++cnt;

    f->GetListOfKeys();
    f->Print();
    /*
        for (uint i = 0; i < ananum; ++i)
        {
            DistributionContext * dactx = nullptr;
            if (!ctx)
            {
                std::string tmpname = ananames[i];
                tmpname.append(suffix);
                dactx = (DistributionContext *)inputfile->Get(tmpname.c_str());
            }
            else
            {
                dactx = ctx[i];
            }

            fac[i] = new DistributionFactory(dactx);
            fac[i]->setSource(inputfile);
        fac[i]->init();
            fac[i]->RegObject((TString("@") + suffix).Data());

            if (!no_rename)
            {
                // temporary name change
                char aaa[100];
                sprintf(aaa, "_%d", cnt);
                TString hp = dactx->hist_name + aaa;
                fac[i]->rename(hp.Data());
            }
        }

        return inputfile;*/
    return vec;
}

void SimpleToolsAbstract::usage(char** argv)
{
    std::cout << "Usage: " << argv[0] << " [ options ]\n";
    std::cout << " options: \n";

    size_t n = cmdl_options_descriptions.size();
    for (uint i = 1; i < n; ++i)
    {
        auto& opt = cmdl_options[i];
        std::cout << "\t--" << std::setw(15) << std::left << opt.name << ": "
                  << cmdl_options_descriptions[i] << "\n";
    }
    std::cout << std::endl;
}

void SimpleToolsAbstract::add_cmdl_option(option opt, std::string description)
{
    cmdl_options.push_back(opt);
    cmdl_options_descriptions.push_back(std::move(description));
}

void SimpleToolsAbstract::add_cmdl_option_handler(std::function<bool(int, const char*)> f)
{
    cmdl_options_handlers.push_back(f);
}

template TFile* SimpleToolsAbstract::import_from_file(const TString& file,
                                                      DifferentialContext** ctx,
                                                      DifferentialFactory** fac,
                                                      const TString& suffix, bool rename);

template TFile* SimpleToolsAbstract::import_from_file(const TString& file,
                                                      DistributionContext** ctx,
                                                      DistributionFactory** fac,
                                                      const TString& suffix, bool rename);
