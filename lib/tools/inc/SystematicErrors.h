/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SYSTEMATICERRORS_H
#define SYSTEMATICERRORS_H

#ifndef __CINT__

#include "TChain.h"
#include "TString.h"
#include <SmartFactory.h>
// #include "TVector.h"

class TCanvas;
class TChain;
class TF1;
class TFile;
class TGraphAsymmErrors;
class TH1;
class TH1F;
class TH1I;
class TH2;
class TH2F;
class TObjArray;
class TStyle;

#endif /* __CINT__ */

#include "ToolsAbstract.h"

class SystematicErrors : public SimpleToolsAbstract
{
private:
    // chain
    TChain* chain;

public:
    SystematicErrors();
    // 	SystematicErrors(Int_t nxnum = 4, Int_t channum = 128, const TString & exportdir =
    // "Export");
    virtual ~SystematicErrors();

    void AnaInitialize();
    void AnaExecute();
    void AnaFinalize();

    void PrintAnalysisInfo();

    int config_params(struct option*& opts);
    void config_execute(int code, char* optarg);

private:
    void PrepareArgsList();

    void PreparePlots(TCanvas* c, TH2* hexp, TH2* hgibuu, TH2* hurqmd);

public:
private:
    TFile* inputfile;

    TObjArray* simlabels;

    struct SysErrSet
    {
        TH2F* h_exp;
        TH2F* h_lower[2];
        TH2F* h_higher[2];

        TH2F* hS_lower[2];
        TH2F* hS_higher[2];
        TH2F* hS_ae;

        TH2F* hS_aesig;

        // 	    SmartFactory * facAEsyst;
        TGraphAsymmErrors** grSystematic;
        TGraphAsymmErrors* grSystematicSlice;
        TObjArray* arrSystematic;
    } * set;

    Bool_t hasExp, hasLower_a, hasHigher_a, hasLower_b, hasHigher_b, hasAe;

    TString expfile;
    TString lowerfile_a;
    TString higherfile_a;
    TString lowerfile_b;
    TString higherfile_b;
    TString aefile;

    TString outputfile;
    TString cfgfile;
};

#endif // SYSTEMATICERRORS_H
