/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TIFINIREADER_H
#define TIFINIREADER_H

#include "ToolsAbstract.h"

#include "TChain.h"
#include "TString.h"

class DistributionFactory;
class DifferentialFactory;

class TCanvas;
class TChain;
class TF1;
class TFile;
class TH1;
class TH1F;
class TH1D;
class TH1D;
class TH2;
class TH2F;
class TH2D;
class TH3;
class TH3F;
class TStyle;

class SmartFactory;
class AnaDatabase;

struct ChannelCodes
{
    int geant_code;
    std::string custom_code;
};

class TifiniReader : public ToolsAbstract
{
protected:
    // chain
    TChain* chain;

public:
    TifiniReader();
    virtual ~TifiniReader();

    inline void AddToChain(TChain* ch) { chain->Add(ch); }
    inline void AddToChain(const TString& file) { chain->Add(file); }

    void AnaInitialize();
    void AnaExecute();
    void AnaFinalize();

    int config_params(struct option*& opts);
    void config_execute(int code, char* optarg);

    void Usage();
    void PrintAnalysisInfo();

    void FitTargetWindowZ(TCanvas* can, TH1* hist, char* name, char* sig, char* bg, Double_t x_min,
                          Double_t x_max, Double_t leg_x, Double_t leg_y);

    enum CutsNames
    {
        C_def = 0,
        C_det_sel,
        // Lambda related
        C_la_mass_min,
        C_la_mass_max,
        C_la_mm_min,
        C_la_mm_max,
        C_la_z_min,
        C_la_z_max,
        C_la_r_min,
        C_la_r_max,
        C_la_mtd_max,
        C_la_pva_max,
        C_la_vdx_min,
        C_la_theta_min,
        C_la_theta_max,
        C_la_real,
        C_la_excl_min,
        C_la_excl_max,
        // Xi- related
        C_s1385p_mass_min,
        C_s1385p_mass_max,
        C_s1385p_mm_min,
        C_s1385p_mm_max,
        C_s1385p_z_min,
        C_s1385p_z_max,
        C_s1385p_r_min,
        C_s1385p_r_max,
        C_s1385p_mtd_max,
        C_s1385p_vdx_min,
        C_s1385p_theta_min,
        C_s1385p_theta_max,
        C_s1385p_real,

        C_gen,
        //                     C_p_min, C_p_max,
        //                     C_pacms_min, C_pacms_max, C_pbcms_min, C_pbcms_max,
        //                     C_dz_min, C_dz_max, C_dr_min, C_dr_max, C_pva_max,
        //                     C_oa_min, C_oa_max,
        //                     C_pt_min, C_pt_max,
        //                     C_rphi_min, C_rphi_max,

        // other
        C_do_not_use_this
    };

protected:
    virtual void ReadEvent(ULong64_t ev) { chain->GetEntry(ev); };
    virtual void InitTree();

private:
    void prettyMe(DifferentialFactory* fac);
    void prettyMe(DistributionFactory* fac);

protected:
    SmartFactory* custom;

    // Events statistic
    ULong64_t events;
    ULong64_t entries; // keeps number of all analysed entries
    ULong64_t events_proceed;

    int flag_noweights;
    int flag_ang_alt;
    int flag_ang_backfold;

    int flag_ar;
    int flag_al;

    const size_t cuts_used;
    std::string par_name[C_do_not_use_this];
    float par_carr[C_do_not_use_this];
    long int par_used[C_do_not_use_this];

    int par_scaledown;

    // temporary for fot params
    Double_t peak, fwhm;
    bool hasInputFiles;
    Double_t data_scaledown;
    Double_t total_weight;

    // pwa channel must be threated different
    Float_t raw_weight;
    Float_t event_weight;
    Double_t raw_total_weight;

    // database
    AnaDatabase* db;
    std::string par_dbfiles;

    bool tree_init;
    TTree* filter_tree;
    TFile* filter_file;
};

#endif // TIFINIREADER_H
