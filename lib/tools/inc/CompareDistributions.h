/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COMPAREDISTRIBUTIONS_H
#define COMPAREDISTRIBUTIONS_H

#ifndef __CINT__

#include "TChain.h"
#include "TString.h"
// #include "TVector.h"

class TFile;
class DistributionContext;
class DistributionFactory;
class SmartFactory;
class TH2;
class TCanvas;
class TChain;
class TF1;
class TFile;
class TH1;
class TH1F;
class TH1I;
class TH2;
class TH2F;
class TH2DA;
class TStyle;

class TObjArray;
#endif /* __CINT__ */

#include "SimpleToolsAbstract.h"
#include "globals.h"
#include <RootTools.h>

class AnaDatabase;

#include <vector>

typedef std::vector<std::string> str_vec;
typedef std::vector<int> int_vec;
typedef std::vector<TFile*> tfile_vec;
typedef std::vector<bool> bool_vec;
typedef std::vector<float> float_vec;

struct CDEntryData
{
    std::string id;
    std::string filename;
    TFile* file;
    std::string attr;
    bool use_corr;
    bool free_fit;
    float scale;
    int flag_special;
};

struct CDGroupData
{
    char id;
    TString name;
    int do_split;
    std::string attr;

    bool operator==(const CDGroupData& gd) { return id == gd.id; }
};

struct ChannelModifier
{
    std::string label;

    // cs
    bool has_cs_scalings;
    double cs_scalings;
    double cs_errors_u;
    double cs_errors_l;
    double cs_limits_u;
    double cs_limits_l;
    double fit_cs_scalings;
    double fit_cs_errors;
    bool free_fit;

    // ang
    bool has_ang_scalings;
    double ang_scalings[3];
    double ang_errors_u[3];
    double ang_errors_l[3];
    double ang_limits_u[3];
    double ang_limits_l[3];
    double fit_ang_scalings[3];
    double fit_ang_errors[3];

    void reset();
};

typedef std::vector<CDEntryData> ed_vec;

struct CDFInputData
{
    std::vector<CDGroupData> groups;
    std::vector<ed_vec> entries;
    ed_vec extras;

    std::string reffile;
    std::string corfile;
    std::string refAttr;
    std::string sumAttr;
};

enum HPCFG
{
    TH2D_PROJ,
    TH2DA_PROJ,
    TH2DA_AE_GRAPH
};

class CompareDistributions : public SimpleToolsAbstract
{
private:
    // chain
    TChain* chain;

public:
    CompareDistributions();
    // 	CompareDistributions(Int_t nxnum = 4, Int_t channum = 128, const TString & exportdir =
    // "Export");
    virtual ~CompareDistributions();

    void AnaInitialize();
    void AnaExecute();
    void AnaFinalize();

    int config_params(struct option*& opts);
    void config_execute(int code, char* optarg);

private:
    void PrepareArgsList();

    void PreparePlots();
    float DrawProjection(TCanvas* c, TH1* hist, const TString& opts, HPCFG painting = TH2D_PROJ,
                         int uid = 0);
    float DrawIntegralY(TCanvas* c, TH1* hist, const TString& opts, int uid = 0);
    float DrawProjectionY(TCanvas* c, TH1* hist, const TString& opts, HPCFG painting = TH2D_PROJ,
                          int uid = 0);

    void CreateCanvases(DistributionFactory* fac[]);

public:
private:
    TFile* configfile;
    TFile* corrfile;
    TFile* fref;

    std::string par_corr;
    std::string par_sumfile;
    int flag_useae;
    int flag_nosum;
    int flag_dosumfile;
    int flag_noscale;
    int flag_inty;
    int flag_negy;
    int flag_fit;
    int flag_nocsfit;
    int flag_noangfit;
    int flag_noang;
    int flag_dump;
    int flag_free;
    int flag_free_cond;
    double flag_sigmamult;
    std::string fit_cfg;
    std::vector<int> fit_ana_selection;
    std::map<std::string, double> fit_map;
    std::string par_dumpfile;

    float par_max_scale;
    float par_min_scale;
    float par_scale;

    // 	bool * has_cs_scalings;
    // 	double * cs_scalings;
    // 	double * cs_errors_u;
    // 	double * cs_errors_l;
    // 	double * cs_limits_u;
    // 	double * cs_limits_l;
    // 	std::string * cs_names;
    // 	double * fit_scalings;
    // 	double * fit_errors;
    // 	double * fit_limits_l;

    TString configfile_t;
    TString syserrfile_t;

    DistributionFactory** fa;
    DistributionContext** fref_ctx;
    DistributionFactory** fref_fac;
    CDFInputData* cdfdata;

    struct CompDistSet
    {
        TObjArray* arrSystematic;
        TH1* href;
        TH1* hsum;
        TCanvas* canleg;

        TCanvas* cLambda_comp;
        TCanvas* cLambda_final;
        TCanvas* cLambda_comp_log;
        TCanvas* cLambda_final_log;
        TCanvas* cLambda_yprojs;
        // 	    TCanvas * cLambda_diffexp[ananum];
        // 	    TCanvas * cLambda_diffsum[ananum];

        // 	    TH1F * hIntY[ananum];
        TCanvas* cIntY;

        float cfg_miny, cfg_maxy;

        double ana_logy_lowlimit;

        DistributionFactory* sum_fac;
    } set[MAXANA];

    RootTools::PaintFormat pf;

    AnaDatabase* dumpdb;

    double* cov_mat;

    int npar_cnt;
    size_t binsnum;
    RootTools::PadFormat pf_comp;
};

#endif // COMPAREDISTRIBUTIONS_H
