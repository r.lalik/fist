#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <map>
#include <vector>

#include <AnaDatabase.h>

class DatabaseManager
{
public:
    enum DatabaseSelector
    {
        DBS_NEWER,
        DBS_FIRST
    };
    static AnaDatabase* getDatabase(const std::string& filename, DatabaseSelector dbs = DBS_NEWER);

private:
    DatabaseManager() {}
    DatabaseManager(const DatabaseManager&) {}

    static AnaDatabase* db;
};

#endif /* DATABASEMANAGER_H */