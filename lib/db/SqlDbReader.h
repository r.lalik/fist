#ifndef SQLDBREADER_H
#define SQLDBREADER_H

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <map>
#include <vector>

#include <TObjArray.h>
#include <TObjString.h>
#include <TString.h>

#include <AnaDatabase.h>

class SqlDbReader : public AnaDatabase
{
public:
    SqlDbReader() {}
    SqlDbReader(const char* dbfilename) : AnaDatabase(dbfilename) {}
    SqlDbReader(const std::string& dbfilename) : AnaDatabase(dbfilename) {}
    virtual ~SqlDbReader() {}

    virtual void prepare();
    virtual void dump(const char* filename = 0);

    void pushRecord(const DatabaseRecord& dbrec);
};

#endif /* SQLDBREADER_H */