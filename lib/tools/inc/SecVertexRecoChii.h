/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SECVERTEXRECOCHII_H
#define SECVERTEXRECOCHII_H

#ifndef __CINT__

#include "TChain.h"
#include "TString.h"
// #include "TVector.h"

#include "ToolsAbstract.h"

class Dim2AnalysisFactory;

class TCanvas;
class TChain;
class TF1;
class TFile;
class TH1;
class TH1F;
class TH2;
class TH2F;
class TH2I;
class TH3;
class TH3F;
class TStyle;

#endif /* __CINT__ */

class SecVertexRecoChii : public SimpleToolsAbstract
{
protected:
    // chain
    TChain* chain;

public:
    SecVertexRecoChii();
    // 	SecVertexRecoChii(Int_t nxnum = 4, Int_t channum = 128, const TString & exportdir =
    // "Export");
    virtual ~SecVertexRecoChii();

    inline void AddToChain(TChain* ch) { chain->Add(ch); }
    inline void AddToChain(const TString& file) { chain->Add(file); }

    void Initialize();
    void Execute();
    void Finalize();

    // 	enum HLimits { HTime, HChan, HADC, HLast };

    // 	void SetLimits(HLimits type, Int_t bins, Int_t lower, Int_t upper);

    // 	void PrepareTimeScalePlot(TH1 * h);

    int config_params(struct option*& opts);
    void config_execute(int code, char* optarg);

    void Usage();
    void PrintAnalysisInfo();

    // 	void Equalize(TH1 * h, Float_t sum);
protected:
    virtual void ReadEvent(ULong64_t ev) { chain->GetEntry(ev); };
    virtual void InitTree();

protected:
    // branches
    Float_t b_fM;
    Float_t b_fY;
    Float_t b_fY_cms;
    Float_t b_fPt;
    Float_t b_fP;
    Float_t b_fP_cms;
    Float_t b_fCosTheta_cms;
    Float_t b_fMinTrackDist;
    Float_t b_fVertDistX;
    Float_t b_fVertDistA;
    Float_t b_fVertDistB;

    Float_t b_fPrimVertexX;
    Float_t b_fPrimVertexY;
    Float_t b_fPrimVertexZ;

    Int_t b_fGeantInfoNum;
    // 	Int_t b_fCGeantInfoNum;
    Int_t b_fPrimary;
    Int_t b_fParentID;
    Int_t b_fGParentID;

    // for GiBUU
    Float_t b_fWeight;

    ULong64_t events;

    // 	Float_t deltaIMY, deltaIMPt;
    // 	Float_t deltaIMCTh, deltaIMP;

    Float_t lay_delta;
    Float_t lapt_delta;

    Float_t lacthcm_delta;
    Float_t lapcm_delta;

    // Lambda +++++++++++++++++++++++++++++++++++++++++++++
    //

    // signal
    TH1F* hSignalInvMass; // Invariant mass of p-pi
    TCanvas* cSignalInvMass;

    TH2I* hSignalVertexXY; // Invariant mass of p-pi
    TCanvas* cSignalVertexXY;
    TH2I* hSignalVertexXZ; // Invariant mass of p-pi
    TCanvas* cSignalVertexXZ;
    TH2I* hSignalVertexYZ; // Invariant mass of p-pi
    TCanvas* cSignalVertexYZ;

    Dim2AnalysisFactory* facPtYcm;
    Dim2AnalysisFactory* facPcmCosThcm;
    Dim2AnalysisFactory* facXcmPcm;

    Dim2AnalysisFactory* facVertXVertY;
    Dim2AnalysisFactory* facVertXVertZ;

    // Events statistic
    ULong64_t entries; // keeps number of all analysed entries
    ULong64_t events_proceed;

    // options parameters
    Bool_t par_fNoVertexCuts; // CLI
    int par_fMtd;             // CLI
    int par_fVertDistX;       // CLI
    int par_fVertDistP;       // CLI
    int par_fVertDistPi;      // CLI

    float par_fLambdaMCutMin;
    float par_fLambdaMCutMax;

    int par_scaledown;

    // temporary for fot params
    Double_t peak, fwhm;
    bool hasInputFiles;
    Double_t data_scaledown;
};

#endif // SECVERTEXRECOCHII_H
