/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef AECALCULATOR_H
#define AECALCULATOR_H

#ifndef __CINT__

#include "TChain.h"
#include "TString.h"
// #include "TVector.h"

class TFile;
class DifferentialContext;
class SmartFactory;
class TH2;
class TCanvas;
class TChain;
class TF1;
class TFile;
class TH1;
class TH1F;
class TH1I;
class TH2;
class TH2F;
class TStyle;

class TObjArray;
#endif /* __CINT__ */

#include "ToolsAbstract.h"
#include "globals.h"
#include <RootTools.h>
#include <TRandom.h>

class AnaDatabase;

#include <DifferentialFactory.h>
#include <FitterFactory.h>
#include <vector>

typedef std::vector<std::string> str_vec;
typedef std::vector<int> int_vec;
typedef std::vector<TFile*> tfile_vec;
typedef std::vector<bool> bool_vec;
typedef std::vector<float> float_vec;

struct EntryData
{
    EntryData()
    {
        fac = nullptr;
        file = 0;
        use_corr = false;
        scale = 1.0;
        flag_special = 0;
    };

    std::string id;
    std::string filename;
    TFile* file;
    std::string attr;
    bool use_corr;
    float scale;
    int flag_special;
    DifferentialFactory** fac;
    DifferentialContext** ctx;
};

struct ChannelModifier
{
    std::string label;

    // cs
    bool has_cs_scalings;
    double cs_scalings;
    double cs_errors_u;
    double cs_errors_l;
    double cs_limits_u;
    double cs_limits_l;
    double fit_cs_scalings;
    double fit_cs_errors;

    // ang
    bool has_ang_scalings;
    double ang_scalings[3];
    double ang_errors_u[3];
    double ang_errors_l[3];
    double ang_limits_u[3];
    double ang_limits_l[3];
    double fit_ang_scalings[3];
    double fit_ang_errors[3];

    void reset();
};

struct ae_data
{
    DifferentialFactory** fac_fss;
    DifferentialFactory** fac_sim;
    DifferentialFactory** fac_div;
    DistributionFactory** fac_ae;
};

typedef std::vector<EntryData> ed_vec;

struct InputData
{
    ed_vec entries_sim;
    ed_vec entries_fss;
};

// FitCallback fclbk;

class AECalculator : public SimpleToolsAbstract
{
private:
    // chain
    TChain* chain;

public:
    AECalculator();
    // 	AECalculator(Int_t nxnum = 4, Int_t channum = 128, const TString & exportdir = "Export");
    virtual ~AECalculator();

    void AnaInitialize();
    void AnaExecute();
    void AnaFinalize();

    int config_params(struct option*& opts);
    void config_execute(int code, char* optarg);

private:
    void PrepareArgsList();

    void ae_fit_function(ae_data& trial_ae, FitterFactory& ff, int iter_cnt = -1,
                         bool backup = false);

private:
    TFile* configfile;

    std::string par_sumfile;
    int flag_nosum;
    int flag_noscale;
    int flag_nocsrand;
    int flag_noangrand;
    std::string fit_cfg;
    std::vector<int> fit_ana_selection;
    std::map<std::string, double> fit_map;
    std::string par_dumpfile;

    float par_max_scale;
    float par_min_scale;
    float par_scale;

    ChannelModifier* chanmod;

    TString configfile_t;

    InputData* cdfdata;

    struct AESet
    {
        float cfg_miny;
        float cfg_maxy;

        SmartFactory* fa;
        TCanvas* canleg;

        DifferentialContext* ctx;
        SmartFactory* projs;
    } * set;

    // 	TH1D ** hprojs[ananum]; FIXME?
    // 	TCanvas * cprojs[ananum];

    RootTools::PaintFormat pf;

    AnaDatabase* dumpdb;

    TF1* legpol = nullptr;

    TRandom rootrand;

    std::string par_fit_params;
    std::string par_fit_export;
    int par_iter_number;

    // what this does?
    ae_data ref_trial_sim, ref_trial_fss, ref_trial_aem;

    FitterFactory ff;
};

#endif // AECALCULATOR_H
