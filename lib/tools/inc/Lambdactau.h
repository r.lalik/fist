/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  <copyright holder> <email>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LAMBDACTAU_H
#define LAMBDACTAU_H

#ifndef __CINT__

#include "TChain.h"
#include "TString.h"
// #include "TVector.h"

class TCanvas;
class TChain;
class TF1;
class TFile;
class TH1;
class TH1F;
class TH1I;
class TH2;
class TH2F;
class TStyle;

class Dim2AnalysisFactory;

#endif /* __CINT__ */

#include "ToolsAbstract.h"
#include "pp35Tools.h"

class Lambdactau : public SimpleToolsAbstract
{
private:
    // chain
    TChain* chain;

public:
    Lambdactau();
    virtual ~Lambdactau();

    void Initialize();
    void Execute();
    void Finalize();

    void PrintAnalysisInfo();

    int config_params(struct option*& opts);
    void config_execute(int code, char* optarg);

private:
    void PrepareArgsList();

    // params vector
    const Int_t nParNum;
    TH1F* params;

public:
private:
    TFile* lambdafittedfile;
    // Lambda +++++++++++++++++++++++++++++++++++++++++++++
    TH1F* hSignalInvMass; // Invariant mass of p-pi
    TCanvas* cSignalInvMass;

    TH1F* hctauSig;

    Dim2AnalysisFactory* facXcmPcm;

    TString fileLambdaFitted;

    TString flags_fit_a;
    TString flags_fit_b;
};

#endif // LAMBDACTAU_H
